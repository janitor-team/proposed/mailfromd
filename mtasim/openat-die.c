/* This file is part of Mailfromd.
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <sysexits.h>
#include <mailutils/error.h>
#include <mailutils/errno.h>
#include "libmf.h"


void
openat_save_fail(int errno)
{
	mu_error(_("unable to record current working directory: %s"),
		  mu_strerror(errno));
	exit(EX_OSERR);
}

void
openat_restore_fail(int errno)
{
	mu_error(_("failed to return to initial working directory: %s"),
		  mu_strerror(errno));
	exit(EX_OSERR);
}

