# This file is part of gacopyz.
# Copyright (C) 2006-2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 

/#.*/ { sub("#.*","") }

NF==0 { next }

/[ \t]+/ && NF==1 {
	   if (!cur) {
	       print NR ": no current element" >"/dev/stderr"
	       next
           }
	   if (!statedecl[$1] && !undeclared[$1])
	      undeclared[$1] = NR
           trans[cur,$1] = 1
	   next
}

/^[A-Z][a-z]*/ && NF==1 {
	   states[stn++] = $1
	   statedecl[$1] = NR
	   delete undeclared[$1]	   
	   cur = $1
	   next	   
}

{ print NR ": Unrecognized line">"/dev/stderr" } 

END {
	for (st in undeclared) {
		print undeclared[st] ": " st " undeclared" >"/dev/stderr"
		count++
	}
	if (count)
		exit(1)

	print "/* -*- buffer-read-only: t -*- vi: set ro:"
        print "   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT."
        print "*/"
		
	print "enum state {"
	print "\tst_none = -1,"		
	for (i = 0; i < stn; i++)
		print "\tst_" tolower(states[i]) ","
        print "\tst_last" 			
        print "};"
	print "char *state_name[] = {"
	for (i = 0; i < stn; i++)
		print "\t\"" tolower(states[i]) "\","
        print "};"
			
	print "char transtab[st_last][st_last] = {"
	for (i = 0; i < stn; i++) {
		printf "\t{ "
		for (j = 0; j < stn; j++)
			printf("%d, ", trans[states[i],states[j]])
		print "},"
	}	
        print "};"

	print "unsigned long state_nr_mask[] = {"
	for (i = 0; i < stn; i++) {
		if (system("grep -q SMFIP_NR_" states[i] " " header_file) == 0)
			print "\tSMFIP_NR_" states[i] ","
		else
                        print "\t0,"
	}
	print "};"
}
