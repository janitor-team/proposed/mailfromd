/* This file is part of gacopyz.
   Copyright (C) 2005-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <gacopyz.h>

typedef struct macro_assoc {
	char **argv;
	char *buffer;
} macro_assoc_t;

struct smfi_str {
	struct smfiDesc const *desc; /* parent description */
	int sd;                /* socket descriptor */
	milter_sockaddr_t addr;/* client address */
	socklen_t addrlen;     /* length of addr */
        int state;             /* state; FIXME: should be enum state */
	unsigned long version; /* negotiated protocol version */
        unsigned long pflags;  /* protocol flags */
	unsigned long mta_pflags; /* pflags supported by MTA */
	unsigned long aflags;  /* milter action flags (from xxfi_flags) */
        int nmacros;           /* Number of entries in macros */
        macro_assoc_t macros[gacopyz_stage_max]; /* Macro tables */
	char *req_macros[gacopyz_stage_max]; /* Required macros */
        char *reply;           /* reply code */
        void *privdata;        /* private data */
	void *closure;         /* call data */ 
};

struct gacopyz_conn {
	int sd;
	int foreground;        /* operate in foreground mode */
 	int stop;              /* */
	struct timeval master_timeout;
	struct smfiDesc desc;
	pid_t *pidtab;
	size_t pidcount;
	void (*cleanup) (gacopyz_conn_t, void*);
	void *cleanup_data;
};

#define GACOPYZ_TIMEOUT  7210	

#ifdef GACOPYZ_UINT32_T
typedef GACOPYZ_UINT32_T gacopyz_uint32_t; 
#else
# warning "assuming 32-bit unsigned long"
typedef unsigned long gacopyz_uint32_t;
#endif

#define GACOPYZ_OPTLEN  (sizeof(gacopyz_uint32_t) * 3) /* length of options */

extern void (*__gacopyz_log_printer)(int, char *, va_list);

#define GACOPYZ_DESC_LOG_MATCH(dsc, level) \
  ((dsc)->logmask & SMI_LOG_MASK(level))
#define GACOPYZ_CTX_LOG_MATCH(ctx, level) \
  GACOPYZ_DESC_LOG_MATCH((ctx)->desc, level)
#define GACOPYZ_CONN_LOG_MATCH(conn, level) \
  GACOPYZ_DESC_LOG_MATCH(&(conn)->desc, level)

#define GACOPYZ_IPV6PREFIX_STR "IPv6:"
#define GACOPYZ_IPV6PREFIX_LEN (sizeof(GACOPYZ_IPV6PREFIX_STR)-1)


#ifndef timersub
# define timersub(a, b, result)						\
	do {								\
		(result)->tv_sec = (a)->tv_sec - (b)->tv_sec;		\
		(result)->tv_usec = (a)->tv_usec - (b)->tv_usec;        \
		if ((result)->tv_usec < 0) {                            \
			--(result)->tv_sec;				\
			(result)->tv_usec += 1000000;			\
		}                                                       \
	} while (0)
#endif



struct gacopyz_iod {
	int sd;
	struct timeval timeout[GACOPYZ_TO_COUNT];
	int logmask;
};

int _gacopyz_read (gacopyz_iod_t iod, char *buf, size_t size);
int _gacopyz_write(gacopyz_iod_t iod, const char *buf, size_t size);
int gacopyz_send_command(gacopyz_iod_t iod,
			 int cmd, const void *data, size_t size);
int gacopyz_read_command(gacopyz_iod_t iod, int tm, unsigned char *cmd,
			 size_t *pcount, char **pbuf, size_t *psize);
int gacopyz_register_child(gacopyz_conn_t conn, pid_t pid);
void gacopyz_unregister_child(gacopyz_conn_t conn, pid_t pid);


/* NLS */
#undef _
#undef N_
#undef gettext
#undef dgettext
#undef ngettext
#undef textdomain
#undef bindtextdomain

#ifdef ENABLE_NLS
# include <gettext.h>
# define _(String) gettext(String)
# define N_(String) String
#else
# define _(String) (String)
# define N_(String) String
# define gettext(msgid) (msgid)
# define dgettext(domain, msgid) (msgid)
# define ngettext(sg,pl,cnt) (cnt == 1) ? (sg) : (pl)
# define textdomain(Domain)
# define bindtextdomain(Package, Directory)
#endif /* ENABLE_NLS */
