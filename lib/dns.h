/* This file is part of Mailfromd.
   Copyright (C) 2005-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __mfd_dns_h
#define __mfd_dns_h

#define NELEMS(a) (sizeof(a) / sizeof((a)[0]))

typedef enum {
	dns_success,
	dns_not_found,
	dns_failure,
	dns_temp_failure,
	dns_too_many
} dns_status;

typedef enum {
	dns_reply_ip,
	dns_reply_str
} dns_reply_type;

struct dns_reply {
	dns_reply_type type;
	int count;
	size_t maxcount;
	union {
		char **str;
		GACOPYZ_UINT32_T *ip;
		void *ptr;
	} data;
};

void dnsbase_real_init(char *configtext);
void dnsbase_file_init(char const *file);

void dns_reply_init(struct dns_reply *reply, dns_reply_type type, size_t count);
void dns_reply_free(struct dns_reply *r);

int dns_str_is_ipv4(const char *addr);

int dns_reverse_ipstr(const char *ipstr, char *revipstr);

dns_status dns_resolve_ipstr(const char *ipstr, const char *domain,
			     char **hbuf);

dns_status dns_resolve_hostname(const char *host, char **ipbuf);

dns_status soa_check(const char *name, int ip, struct dns_reply *repl);

dns_status a_lookup(const char *host, struct dns_reply *repl);

dns_status ptr_lookup(struct in_addr ip, struct dns_reply *repl);
dns_status txt_lookup(const char *name, struct dns_reply *repl);

dns_status ptr_validate(const char *ipstr, struct dns_reply *repl);

dns_status spf_lookup(const char *domain, char **record);
dns_status dkim_lookup(const char *domain, const char *sel, char ***retval);

dns_status mx_lookup(const char *host, int resolve, struct dns_reply *repl);

dns_status ns_lookup(const char *host, int resolve, struct dns_reply *reply);

#endif
