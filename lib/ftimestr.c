/* This file is part of Mailfromd.
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <libmf.h>
#include <time.h>

char *time_format_string = "%c"; /* String to format the time stamps */

size_t
format_time_str(FILE *fp, time_t timestamp)
{
	struct tm tm;
	char buf[1024];
	size_t len;
	
	localtime_r(&timestamp, &tm);
	len = strftime(buf, sizeof(buf), time_format_string, &tm);
	return fwrite(buf, 1, len, fp);
}
