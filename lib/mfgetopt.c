/* This file is part of Mailfromd.
   Copyright (C) 2016-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include "libmf.h"
#include "filenames.h"

void
mf_getopt(struct mu_cli_setup *cli, int *pargc, char ***pargv, char **capa,
	  int flags)
{
	struct mu_parseopt pohint;
	struct mu_cfg_parse_hints cfhint;

	cfhint.flags = 0;
	if (!(flags & MF_GETOPT_NO_CONFIG)) {
		cfhint.flags = MU_CFHINT_SITE_FILE;
		cfhint.site_file = DEFAULT_CONFIG_FILE;
	}
	
	pohint.po_flags = 0;
	
	pohint.po_package_name = PACKAGE_NAME;
	pohint.po_flags |= MU_PARSEOPT_PACKAGE_NAME;

	pohint.po_package_url = PACKAGE_URL;
	pohint.po_flags |= MU_PARSEOPT_PACKAGE_URL;

	pohint.po_bug_address = PACKAGE_BUGREPORT;
	pohint.po_flags |= MU_PARSEOPT_BUG_ADDRESS;

	pohint.po_version_hook = mailfromd_version;
	pohint.po_flags |= MU_PARSEOPT_VERSION_HOOK;

	pohint.po_negation = "no-";
	pohint.po_flags |= MU_PARSEOPT_NEGATION;

	if (flags & MF_GETOPT_IN_ORDER)
		pohint.po_flags |= MU_PARSEOPT_IN_ORDER;

	mu_cli_ext (*pargc, *pargv, cli, &pohint, &cfhint, capa, NULL,
		    pargc, pargv);
}
	
