/* This file is part of Mailfromd.
   Copyright (C) 2005-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <mailutils/alloc.h>

#include "libmf.h"

struct namerec {
	struct namerec *next;
	char **nameptr;
};

static struct namerec *head, *tail;

void
mf_namefixup_register(char **ptr, const char *initval)
{
	struct namerec *nrec = mu_alloc(sizeof(*nrec));
	nrec->next = NULL;
	nrec->nameptr = ptr;
	*ptr = initval ? mu_strdup(initval) : NULL;
		
	if (tail)
		tail->next = nrec;
	else
		head = nrec;
	tail = nrec;
}

void
mf_file_name_ptr_fixup(char **ptr, char *dir, size_t dirlen)
{
	if (*ptr && **ptr != '/') {
		char *name = *ptr;
		size_t olen = strlen(name);
		size_t flen = dirlen + 1 + olen + 1;
		char *p = mu_realloc(name, flen);
		memmove(p + dirlen + 1, p, olen + 1);
		memcpy(p, dir, dirlen);
		p[dirlen] = '/';
		*ptr = p;
	}
}

void
mf_namefixup_run(char *dir)
{
	size_t dirlen = strlen(dir);
	struct namerec *p;

	for (p = head; p; p = p->next)
		mf_file_name_ptr_fixup(p->nameptr, dir, dirlen);
}

void
mf_namefixup_free()
{
	struct namerec *p = head;

	while (p) {
		struct namerec *next = p->next;
		free(p);
		p = next;
	}
}


	
