/* This file is part of Mailfromd.
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <libmf.h>
#include <stdlib.h>
#include <mailutils/locker.h>
#include <mailutils/alloc.h>


char **
config_array_to_argv (mu_config_value_t *val)
{
  int i, j;
  int argc;
  char **argv;
  
  argc = val->v.arg.c;
  argv = mu_calloc (argc + 1, sizeof (argv[0]));
  for (i = j = 0; i < argc; i++)
    {
      if (mu_cfg_assert_value_type (&val->v.arg.v[i], MU_CFG_STRING) == 0)
	argv[j++] = mu_strdup (val->v.arg.v[i].v.string);
    }
  argv[j] = NULL;
  return argv;
}

char *
config_array_to_string (mu_config_value_t *val)
{
  size_t len = 0;
  int i;
  char *str, *p;

  for (i = 0; i < val->v.arg.c; i++)
    {
      if (mu_cfg_assert_value_type (&val->v.arg.v[i], MU_CFG_STRING))
	return NULL;
      len += strlen (val->v.arg.v[i].v.string) + 1;
    }

  str = mu_alloc (len);
  p = str;
  for (i = 0; i < val->v.arg.c; i++)
    {
      size_t n = strlen (val->v.arg.v[i].v.string);
      memcpy (p, val->v.arg.v[i].v.string, n);
      p += n;
      *p++ = ' ';
    }
  str[len-1] = 0;
  return str;
}


int
config_cb_timeout (struct timeval *pt, mu_config_value_t *val)
{
  int rc;
  const char *endp;
  time_t t;
  const char *str;
  char *alloc_str = NULL;
  
  switch (val->type)
    {
    case MU_CFG_STRING:
      str = val->v.string;
      break;

    case MU_CFG_ARRAY:
      str = alloc_str = config_array_to_string (val);
      if (!str)
	return 1;
      break;

    case MU_CFG_LIST:
      mu_error (_("unexpected list"));
      return 1;

    default:
      mu_error (_("INTERNAL ERROR at %s:%d: please report"),
		__FILE__, __LINE__);
      abort();
    }      

  rc = parse_time_interval (str, &t, &endp);
  if (rc)
    mu_error (_("unrecognized time format (near `%s')"), endp);
  else
    {
      pt->tv_usec = 0;
      pt->tv_sec = t;
    }
  free (alloc_str);
  return 0;
}

int
config_cb_ignore(void *data, mu_config_value_t *val)
{
	mu_diag_output (MU_DIAG_WARNING,
			_("this statement has no effect in %s"),
			PACKAGE_STRING);
	return 0;
}

int
config_cb_lock_retry_count(void *data, mu_config_value_t *val)
{
	int rc;
	char *errmsg;
	size_t v;
	
	if (mu_cfg_assert_value_type(val, MU_CFG_STRING))
		return 1;
	rc = mu_str_to_c(val->v.string, mu_c_size, &v, &errmsg);
	if (rc) {
		mu_error(_("%s: not a valid number"), val->v.string);
		free(errmsg);
		return 1;
	}
	mu_locker_set_default_retry_count(v);
	return 0;
}

int
config_cb_lock_retry_timeout(void *data, mu_config_value_t *val)
{
	int rc;
	char *errmsg;
	time_t v;
	
	rc = mu_str_to_c(val->v.string, mu_c_time, &v, &errmsg);
	if (rc) {
		mu_error(_("%s: not a valid interval"), val->v.string);
		free(errmsg);
		return 1;
	}
	mu_locker_set_default_retry_timeout(v);
	return 0;
}

int
stderr_closed_p()
{
	int fd = dup(0);
	if (fd < 0)
		return 1;
	close(fd);
	return fd <= 2;
}


int
mf_list_compare_string(const void *item, const void *value)
{
	return strcmp(item, value);
}




