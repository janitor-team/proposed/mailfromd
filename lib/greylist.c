/* This file is part of Mailfromd.
   Copyright (C) 2005-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <mailutils/mailutils.h>
#include "libmf.h"
#include "mfdb.h"
#include "filenames.h"

static void
greylist_print_item(struct mu_dbm_datum const *key,
		    struct mu_dbm_datum const *val)
{
	time_t timestamp = *(time_t*) val->mu_dptr;
	int size = key->mu_dsize - 1; /* Size includes the trailing nul */
	
	printf("%*.*s ", size, size, key->mu_dptr);
	format_time_str(stdout, timestamp);
	putchar('\n');
}

static int
greylist_expire_item(struct mu_dbm_datum const *content)
{
	time_t timestamp = *(time_t*) content->mu_dptr;
	return greylist_format->expire_interval &&
		time(NULL) - timestamp > greylist_format->expire_interval;
}


static struct db_format greylist_format_struct = {
	"greylist",
	DEFAULT_GREYLIST_DATABASE,
	1,
	0,
	DEFAULT_EXPIRE_INTERVAL,
	greylist_print_item,
	greylist_expire_item
};

struct db_format *greylist_format = &greylist_format_struct;
