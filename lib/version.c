/* This file is part of Mailfromd.
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <gettext.h>
#define _(String) gettext(String)
#include <mailutils/cli.h>
#include <mailutils/stream.h>
#include "libmf.h"

const char mailfromd_version_etc_copyright[] =
  /* Do *not* mark this string for translation.  %s is a copyright
     symbol suitable for this locale, and %d is the copyright
     year.  */
  "Copyright %s 2005-%d Sergey Poznyakoff";
int mailfromd_copyright_year = 2022;

void
mailfromd_version(struct mu_parseopt *po, mu_stream_t stream)
{
#ifdef GIT_DESCRIBE
	mu_stream_printf (stream, "%s (%s) %s [%s]\n",
			  po->po_prog_name,
			  PACKAGE, PACKAGE_VERSION, GIT_DESCRIBE);
#else
	mu_stream_printf (stream, "%s (%s) %s\n",
			  po->po_prog_name, PACKAGE, PACKAGE_VERSION);
#endif
	/* TRANSLATORS: Translate "(C)" to the copyright symbol
	   (C-in-a-circle), if this symbol is available in the user's
	   locale.  Otherwise, do not translate "(C)"; leave it as-is.  */
	mu_stream_printf (stream, mailfromd_version_etc_copyright, 
                          _("(C)"), mailfromd_copyright_year);

	mu_stream_printf (stream, "%s", _("\
\n\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
\n\
"));
	
	/* TRANSLATORS: %s denotes an author name.  */
	mu_stream_printf (stream, _("Written by %s.\n"), "Sergey Poznyakoff");
}
	

