/* This file is part of Mailfromd.
   Copyright (C) 2005-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <mailutils/dbm.h>

typedef void (*db_item_printer_t)(struct mu_dbm_datum const *,
				  struct mu_dbm_datum const *);
typedef int (*db_expire_t)(struct mu_dbm_datum const *content);

struct db_format {
	char *name;
	char *dbname;
	int enabled;
	mu_debug_handle_t debug_handle;
	time_t expire_interval;
	db_item_printer_t print_item;
	db_expire_t expire;
};

typedef int (*dbfmt_enumerator_t)(struct db_format *fmt, void *);

extern mu_debug_handle_t db_debug_handle;
extern struct db_format *cache_format;
extern int mf_database_mode;

extern struct db_format *cache_format;
extern struct db_format *rate_format;
extern struct db_format *tbf_rate_format;
extern struct db_format *greylist_format;

extern int predict_next_option; 
extern double predict_rate;  /* Prediction rate for --list --format=rates*/

mu_dbm_file_t mf_dbm_open(char *dbname, int access);
int mf_file_mode_to_safety_criteria(int mode);


void db_format_setup(void);
void db_format_enumerate(dbfmt_enumerator_t fp, void *data);
struct db_format *db_format_install(struct db_format *fmt);
struct db_format *db_format_lookup(const char *name);
int db_list_item(char *dbname, char *email, db_item_printer_t fun);
int db_list(char *dbname, db_item_printer_t fun);
int db_expire(char *dbname, db_expire_t fun);
int db_delete(char *dbname, char *id);
int db_compact(char *dbname, db_expire_t fun);

void database_cfg_init(void);
int cb_database_mode(void *data, mu_config_value_t *arg);

/* cache.c */
mf_status cache_get(const char *email);
void cache_insert(const char *email, mf_status rc);
mf_status cache_get2(const char *email, const char *client_addr);
void cache_insert2(const char *email, const char *client_addr, mf_status rc);

extern time_t negative_expire_interval;

/* rate.c */
mf_status get_rate(char *email, long *ret, unsigned long interval,
		   size_t mincount, size_t threshold);

/* tbf_rate.c */
mf_status check_tbf_rate(char *email, int *ret,
			 size_t cost, size_t interval, size_t burst_size);

extern int ignore_failed_reads_option;
extern char *time_format_string; /* String to format the time stamps */



