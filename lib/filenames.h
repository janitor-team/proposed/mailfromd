/* This file is part of Mailfromd.
   Copyright (C) 2005-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/* Default file names */
#define DEFAULT_CALLOUT_SOCKET "callout"
#define DEFAULT_DATABASE "mailfromd.db"
#define DEFAULT_RATE_DATABASE "rates.db"
#define DEFAULT_GREYLIST_DATABASE "greylist.db"
#define DEFAULT_DNS_DATABASE "dns.db"
#define DEFAULT_SCRIPT_FILE SYSCONFDIR "/mailfromd.mf"
#define DEFAULT_CONFIG_FILE SYSCONFDIR "/mailfromd.conf"
#define DEFAULT_FROM_ADDRESS "<>"
#ifdef DEFAULT_PREPROCESSOR
# define DEF_EXT_PP DEFAULT_PREPROCESSOR
#else
# define DEF_EXT_PP NULL
#endif

