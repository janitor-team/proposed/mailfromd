/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#ifdef __DBGMOD_C_ARRAY
"callout",
#else
# define MF_SOURCE_CALLOUT (callout_debug_handle + 0)
#endif
#ifdef __DBGMOD_C_ARRAY
"savsrv",
#else
# define MF_SOURCE_SAVSRV (callout_debug_handle + 1)
#endif
