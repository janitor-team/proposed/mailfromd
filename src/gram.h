/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_GRAM_H_INCLUDED
# define YY_YY_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 294 "gram.y" /* yacc.c:1909  */

#define YYLTYPE struct mu_locus_range

#line 48 "gram.h" /* yacc.c:1909  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    T_ACCEPT = 258,
    T_REJECT = 259,
    T_TEMPFAIL = 260,
    T_CONTINUE = 261,
    T_DISCARD = 262,
    T_ADD = 263,
    T_REPLACE = 264,
    T_DELETE = 265,
    T_PROG = 266,
    T_BEGIN = 267,
    T_END = 268,
    T_IF = 269,
    T_FI = 270,
    T_ELSE = 271,
    T_ELIF = 272,
    T_ON = 273,
    T_HOST = 274,
    T_FROM = 275,
    T_AS = 276,
    T_DO = 277,
    T_DONE = 278,
    T_POLL = 279,
    T_MATCHES = 280,
    T_FNMATCHES = 281,
    T_MXMATCHES = 282,
    T_MXFNMATCHES = 283,
    T_WHEN = 284,
    T_PASS = 285,
    T_SET = 286,
    T_CATCH = 287,
    T_TRY = 288,
    T_THROW = 289,
    T_ECHO = 290,
    T_RETURNS = 291,
    T_RETURN = 292,
    T_FUNC = 293,
    T_SWITCH = 294,
    T_CASE = 295,
    T_DEFAULT = 296,
    T_CONST = 297,
    T_FOR = 298,
    T_LOOP = 299,
    T_WHILE = 300,
    T_BREAK = 301,
    T_NEXT = 302,
    T_ARGCOUNT = 303,
    T_ALIAS = 304,
    T_DOTS = 305,
    T_ARGX = 306,
    T_VAPTR = 307,
    T_PRECIOUS = 308,
    T_OR = 309,
    T_AND = 310,
    T_EQ = 311,
    T_NE = 312,
    T_LT = 313,
    T_LE = 314,
    T_GT = 315,
    T_GE = 316,
    T_NOT = 317,
    T_LOGAND = 318,
    T_LOGOR = 319,
    T_LOGXOR = 320,
    T_LOGNOT = 321,
    T_REQUIRE = 322,
    T_IMPORT = 323,
    T_STATIC = 324,
    T_PUBLIC = 325,
    T_MODULE = 326,
    T_BYE = 327,
    T_DCLEX = 328,
    T_SHL = 329,
    T_SHR = 330,
    T_SED = 331,
    T_COMPOSE = 332,
    T_MODBEG = 333,
    T_MODEND = 334,
    T_STRING = 335,
    T_SYMBOL = 336,
    T_IDENTIFIER = 337,
    T_ARG = 338,
    T_NUMBER = 339,
    T_BACKREF = 340,
    T_BUILTIN = 341,
    T_FUNCTION = 342,
    T_TYPE = 343,
    T_TYPECAST = 344,
    T_VARIABLE = 345,
    T_BOGUS = 346,
    T_UMINUS = 347
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 298 "gram.y" /* yacc.c:1909  */

	struct literal *literal;
	struct stmtlist stmtlist;
	NODE *node;
	struct return_node ret;
	struct poll_data poll;
	struct pollarg {
		int kw;
		NODE *expr;
	} pollarg;
	struct arglist {
		NODE *head;
		NODE *tail;
		size_t count;
	} arglist;
	long number;
	const struct builtin *builtin;
	struct variable *var;
	enum smtp_state state;
	struct {
		int qualifier;
	} matchtype;
	data_type_t type;
	struct parmtype *parm;
	struct parmlist {
		struct parmtype *head, *tail;
		size_t count;
		size_t optcount;
		int varargs;
	} parmlist;
	enum lexical_context tie_in;
	struct function *function;
	struct {
		struct valist *head, *tail;
	} valist_list;
	struct {
		int all;
		struct valist *valist;
	} catchlist;
	struct valist *valist;
	struct value value;
	struct {
		struct case_stmt *head, *tail;
	} case_list ;
	struct case_stmt *case_stmt;
	struct loop_node loop;
	struct {
		int code;
	} progspecial;
	struct enumlist {
		struct constant *cv;
		struct enumlist *prev;
	} enumlist;
	mu_list_t list;
	struct import_rule_list import_rule_list;
	char *string;

#line 211 "gram.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_GRAM_H_INCLUDED  */
