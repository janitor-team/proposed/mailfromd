#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "rate.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



void
#line 19
bi_rate(eval_environ_t env)
#line 19

#line 19

#line 19 "rate.bi"
{
#line 19
	
#line 19

#line 19
        char *  key;
#line 19
        long  interval;
#line 19
        long  mincnt;
#line 19
        long  threshold;
#line 19
        
#line 19
        long __bi_argcnt;
#line 19
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 19
        get_string_arg(env, 1, &key);
#line 19
        get_numeric_arg(env, 2, &interval);
#line 19
        if (__bi_argcnt > 2)
#line 19
                get_numeric_arg(env, 3, &mincnt);
#line 19
        if (__bi_argcnt > 3)
#line 19
                get_numeric_arg(env, 4, &threshold);
#line 19
        
#line 19
        adjust_stack(env, __bi_argcnt + 1);
#line 19

#line 19

#line 19
	if (builtin_module_trace(BUILTIN_IDX_rate))
#line 19
		prog_trace(env, "rate %s %lu %lu %lu",key, interval, ((__bi_argcnt > 2) ? mincnt : 0), ((__bi_argcnt > 3) ? threshold : 0));;

{
	long ret;

		if (!(get_rate(key, &ret, interval,
#line 24
			   ((__bi_argcnt > 2) ? mincnt : 0),
#line 24
			   ((__bi_argcnt > 3) ? threshold : 0)) == mf_success))
#line 24
		(
#line 24
	env_throw_bi(env, mfe_dbfailure, "rate", _("cannot get rate for %s"),key)
#line 24
)
#line 28
;

	
#line 30
do {
#line 30
  push(env, (STKVAL)(mft_number)(ret));
#line 30
  goto endlab;
#line 30
} while (0);
}
endlab:
#line 32
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 32
	return;
#line 32
}

void
#line 34
bi_tbf_rate(eval_environ_t env)
#line 34

#line 34

#line 34 "rate.bi"
{
#line 34
	
#line 34

#line 34
        char *  key;
#line 34
        long  cost;
#line 34
        long  interval;
#line 34
        long  burst_size;
#line 34
        
#line 34

#line 34
        get_string_arg(env, 0, &key);
#line 34
        get_numeric_arg(env, 1, &cost);
#line 34
        get_numeric_arg(env, 2, &interval);
#line 34
        get_numeric_arg(env, 3, &burst_size);
#line 34
        
#line 34
        adjust_stack(env, 4);
#line 34

#line 34

#line 34
	if (builtin_module_trace(BUILTIN_IDX_rate))
#line 34
		prog_trace(env, "tbf_rate %s %lu %lu %lu",key, cost, interval, burst_size);;

{
        int result;
		
		if (!(check_tbf_rate(key, &result, cost, interval,
#line 39
				 burst_size) == mf_success))
#line 39
		(
#line 39
	env_throw_bi(env, mfe_dbfailure, "tbf_rate", _("cannot check TBF rate for %s"),key)
#line 39
)
#line 42
;
	
	
#line 44
do {
#line 44
  push(env, (STKVAL)(mft_number)(result));
#line 44
  goto endlab;
#line 44
} while (0);
}
endlab:
#line 46
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 46
	return;
#line 46
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
rate_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 19 "rate.bi"
va_builtin_install_ex("rate", bi_rate, 0, dtype_number, 4, 2, 0|0, dtype_string, dtype_number, dtype_number, dtype_number);
#line 34 "rate.bi"
va_builtin_install_ex("tbf_rate", bi_tbf_rate, 0, dtype_number, 4, 0, 0|0, dtype_string, dtype_number, dtype_number, dtype_number);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

