/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include "msg.h"

MF_STATE(body)
MF_DEFUN(body_string, STRING, POINTER text, NUMBER length)
{
	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(text, length);
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

MF_STATE(body)
MF_DEFUN(body_has_nulls, NUMBER, POINTER text, NUMBER length)
{
	MF_RETURN(memchr(text, 0, length) != NULL);
}
END
	
/* number current_message() */
MF_STATE(eom)
MF_CAPTURE
MF_DEFUN(current_message, NUMBER)
{
	MF_RETURN(bi_get_current_message(env, NULL));
}
END

MF_DEFUN(replbody, VOID, STRING text)
{
	env_msgmod_append(env, body_repl, "BODY", text, 0);
}
END

MF_DEFUN(replbody_fd, VOID, NUMBER fd)
{
	env_msgmod_append(env, body_repl_fd, "BODYFD", NULL, _bi_io_fd(env, fd, 0));
}
END

