/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2009-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#ifndef ENAMETOOLONG
# define ENAMETOOLONG 0
#endif

#ifndef INITIAL_HOSTNAME_LENGTH
# define INITIAL_HOSTNAME_LENGTH 34
#endif

#ifndef INITIAL_DOMAINNAME_LENGTH
# define INITIAL_DOMAINNAME_LENGTH 34
#endif

/* string gethostname()

   This implementation is based on xgethostname by Jim Meyering
*/
MF_DEFUN(gethostname, STRING, OPTIONAL, NUMBER dns)
{
	size_t size = INITIAL_HOSTNAME_LENGTH;
	
	MF_OBSTACK_BEGIN();
	while (1) {
		size_t nsize;
		size_t size_1;
		char *hostname;

		MF_OBSTACK_GROW(NULL, size);
		hostname = MF_OBSTACK_BASE();

                /* Use SIZE_1 here rather than SIZE to work around the bug in
		   SunOS 5.5's gethostname whereby it NUL-terminates HOSTNAME
		   even when the name is as long as the supplied buffer.  */
		size_1 = size - 1;
		hostname[size_1 - 1] = '\0';
		errno = 0;

		if (gethostname(hostname, size_1) == 0)	{
			if (!hostname[size_1 - 1])
				break;
		} else if (errno != 0
			   && errno != ENAMETOOLONG && errno != EINVAL
			   /* OSX/Darwin does this when the buffer is not
			      large enough */
			   && errno != ENOMEM) {
			int saved_errno = errno;
			MF_OBSTACK_CANCEL();
			MF_THROW(mfe_failure,
				 "%s", mu_strerror(saved_errno));
		}

		nsize = size * 2;
		if (nsize <= size) {
			MF_OBSTACK_CANCEL();
			MF_THROW(mfe_failure,
				 "%s", mu_strerror(ENOMEM));
		}
		size = nsize;
	}

	if (MF_OPTVAL(dns)) {
		struct hostent *hp;
		char *ptr = MF_OBSTACK_BASE;
		
		hp = gethostbyname(ptr);
		if (hp) {
			size_t nlen = strlen(hp->h_name);
			if (nlen >= size)
				MF_OBSTACK_GROW(NULL,
						nlen - size + 1);
			strcpy(ptr, hp->h_name);
		}
	}
	
	MF_RETURN_OBSTACK();
}
END

/* string getdomainname()

   This implementation is based on xgetdomainname by Jim Meyering
*/
MF_DEFUN(getdomainname, STRING)
{
	size_t size = INITIAL_DOMAINNAME_LENGTH;
	
	MF_OBSTACK_BEGIN();
	while (1) {
		size_t nsize;
		size_t size_1;
		char *domainname;
		int rc;
		
		MF_OBSTACK_GROW(NULL, size);
		domainname = MF_OBSTACK_BASE();

		size_1 = size - 1;
		domainname[size_1 - 1] = '\0';
		errno = 0;

		rc = getdomainname(domainname, size);
		if (rc >= 0 && domainname[size_1] == '\0')
			break;
		else if (rc < 0 && errno != EINVAL) {
			int saved_errno = errno;
			MF_OBSTACK_CANCEL();
			MF_THROW(mfe_failure,
				 "%s", mu_strerror(saved_errno));
		}

		nsize = size * 2;
		if (nsize <= size) {
			MF_OBSTACK_CANCEL();
			MF_THROW(mfe_failure,
				 "%s", mu_strerror(ENOMEM));
		}
		size = nsize;
	}
	MF_RETURN_OBSTACK();
}
END

