/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2020-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
MF_COND(WITH_GEOIP2)
#include <inttypes.h>
#include <maxminddb.h>

#define DEFAULT_DIR  "/usr/share/GeoIP"
#define DEFAULT_FILE "GeoLite2-City.mmdb"
#define GEOIP2_DEFAULT_DATABASE DEFAULT_DIR "/" DEFAULT_FILE
	
struct geoip2_storage {
	char *dbname;
	MMDB_s db;
	int is_open;
};

static void *
geoip2_alloc(void)
{
	struct geoip2_storage *gs = mu_zalloc(sizeof(*gs));
	gs->is_open = 0;
	return gs;
}

static void
geoip2_close(struct geoip2_storage *gs)
{
	if (gs->is_open) {
		MMDB_close(&gs->db);
		gs->is_open = 0;
	}
}

static void
geoip2_destroy(void *data)
{
	struct geoip2_storage *gs = data;
	geoip2_close(gs);
	free(gs->dbname);
	free(gs);
}

MF_DECLARE_DATA(GEOIP2, geoip2_alloc, geoip2_destroy)

static struct geoip2_storage *
geoip2_open(eval_environ_t env)
{
	int rc;
	struct geoip2_storage *gs = MF_GET_DATA;
	if (!gs->is_open) {
		if (!gs->dbname)
			gs->dbname = mu_strdup(GEOIP2_DEFAULT_DATABASE);
		rc = MMDB_open(gs->dbname, MMDB_MODE_MMAP, &gs->db);
		MF_ASSERT(rc == MMDB_SUCCESS,
			  mfe_failure,
			  "can't open database \"%s\": %s",
			  gs->dbname, MMDB_strerror(rc));
		gs->is_open = 1;
	}
	return gs;
}

MF_DEFUN(geoip2_dbname, STRING)
{
	struct geoip2_storage *gs = MF_GET_DATA;
	MF_RETURN(gs->dbname ? gs->dbname : GEOIP2_DEFAULT_DATABASE);
}
END

MF_DEFUN(geoip2_open, VOID, STRING name)
{
	struct geoip2_storage *gs = MF_GET_DATA;
	geoip2_close(gs);
	free(gs->dbname);
	gs->dbname = mu_strdup(name);
	geoip2_open(env);
}
END

static void
conv_utf_string(eval_environ_t env, MMDB_entry_data_s *dptr)
{
	MF_OBSTACK_GROW((void*)dptr->utf8_string, dptr->data_size);
}

static void
conv_uint16(eval_environ_t env, MMDB_entry_data_s *dptr)
{
	heap_obstack_sprintf(env, "%" PRIu16, dptr->uint16);
}

static void
conv_uint32(eval_environ_t env, MMDB_entry_data_s *dptr)
{
	heap_obstack_sprintf(env, "%" PRIu32, dptr->uint32);
}

static void
conv_int32(eval_environ_t env, MMDB_entry_data_s *dptr)
{
	heap_obstack_sprintf(env, "%" PRIi32, dptr->int32);
}

static void
conv_bool(eval_environ_t env, MMDB_entry_data_s *dptr)
{
	heap_obstack_sprintf(env, "%01d", dptr->boolean ? 1 : 0);
}

static void
conv_double(eval_environ_t env, MMDB_entry_data_s *dptr)
{
	heap_obstack_sprintf(env, "%g", dptr->double_value);
}

static void
conv_float(eval_environ_t env, MMDB_entry_data_s *dptr)
{
	heap_obstack_sprintf(env, "%g", dptr->float_value);
}

static void (*entry_conv[]) (eval_environ_t, MMDB_entry_data_s *) = {
        [MMDB_DATA_TYPE_UTF8_STRING] = conv_utf_string,
        [MMDB_DATA_TYPE_UINT16]      = conv_uint16,
        [MMDB_DATA_TYPE_UINT32]      = conv_uint32,
        [MMDB_DATA_TYPE_INT32]       = conv_int32,
        [MMDB_DATA_TYPE_BOOLEAN]     = conv_bool,
        [MMDB_DATA_TYPE_DOUBLE]      = conv_double,
        [MMDB_DATA_TYPE_FLOAT]       = conv_float
};

MF_DEFUN(geoip2_get, STRING, STRING ip, STRING pathstr)
{
	struct geoip2_storage *gs = geoip2_open(env);
	MMDB_lookup_result_s result;
        int mmdb_error;
	int gai_error;
	MMDB_entry_data_s entry_data;
	int rc;
	struct mu_wordsplit ws;
	
	result = MMDB_lookup_string(&gs->db, ip, &gai_error, &mmdb_error);
	MF_ASSERT(gai_error == 0,
		  mfe_failure,
		  "%s: %s",
		  ip, gai_strerror(gai_error));
	
	MF_ASSERT(mmdb_error == MMDB_SUCCESS,
		  mfe_failure,
		  "%s: %s: %s",
		  pathstr, ip, MMDB_strerror(mmdb_error));

	MF_ASSERT(result.found_entry != 0,
		  mfe_not_found,
		  _("IP not found in the database"));

	ws.ws_delim = ".";
	rc = mu_wordsplit(pathstr, &ws, MU_WRDSF_DELIM);
	if (rc != 0) {
		char const *errstr = mu_wordsplit_strerror(&ws);
		mu_wordsplit_free(&ws);
		MF_THROW(mfe_failure,
			 "can't split string %s: %s",
			 pathstr, errstr);
	}
	
	rc = MMDB_aget_value(&result.entry, &entry_data,
			     (const char * const* const) ws.ws_wordv);
	mu_wordsplit_free(&ws);

	MF_ASSERT(rc == MMDB_SUCCESS,
		  (rc == MMDB_LOOKUP_PATH_DOES_NOT_MATCH_DATA_ERROR ||
		   rc == MMDB_INVALID_LOOKUP_PATH_ERROR)
		     ? mfe_range : mfe_failure,
		  "%s %s: MMDB_aget_value %s",
		  pathstr, ip, MMDB_strerror(rc));

	if (!entry_data.has_data)
		MF_RETURN("");
	
	MF_ASSERT(entry_data.type >= 0
		  && entry_data.type <= sizeof (entry_conv) / sizeof (entry_conv[0])
		  && entry_conv[entry_data.type],
		  mfe_failure,
		  "%s: can't format %s of type %d",
		  ip, pathstr, entry_data.type);

	MF_OBSTACK_BEGIN();
	MF_OBSTACK_PRINTF("%s", "string result:");
	entry_conv[entry_data.type] (env, &entry_data);
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();	
}
END

struct object_type {
	enum { OBJ_MAP, OBJ_ARRAY } type;
	size_t count;
	unsigned level;
	struct object_type *prev;
};

static inline void
object_type_push(struct object_type **otp, int type, size_t count)
{
	struct object_type *t = mu_alloc(sizeof(*t));
	t->type = type;
	t->count = count;
	t->prev = *otp;
	t->level = t->prev ? t->prev->level + 1 : 1;
	*otp = t;
}

static inline void
object_type_pop(struct object_type **otp)
{
	struct object_type *t = *otp;
	*otp = t->prev;
	free(t);
}

MF_DEFUN(geoip2_get_json, STRING, STRING ip, OPTIONAL, NUMBER indent)
{
	struct geoip2_storage *gs = geoip2_open(env);
	MMDB_lookup_result_s result;
        int mmdb_error;
	int gai_error;
	int rc;
	MMDB_entry_data_list_s *data_list, *p;
	struct object_type *type = NULL;
	int iskey = 1;
	size_t i;
	char *indent_str = NULL;
	
	result = MMDB_lookup_string(&gs->db, ip, &gai_error, &mmdb_error);
	MF_ASSERT(gai_error == 0,
		  mfe_failure,
		  "%s: %s",
		  ip, gai_strerror(gai_error));
	
	MF_ASSERT(mmdb_error == MMDB_SUCCESS,
		  mfe_failure,
		  "%s: %s",
		  ip, MMDB_strerror(mmdb_error));

	MF_ASSERT(result.found_entry != 0,
		  mfe_not_found,
		  _("IP not found in the database"));

	rc = MMDB_get_entry_data_list(&result.entry, &data_list);
	MF_ASSERT(rc == MMDB_SUCCESS,
		  mfe_failure,
		  "%s: MMDB_aget_value %s",
		  ip, MMDB_strerror(rc));

	/*MMDB_dump_entry_data_list(stdout, data_list, 4);*/

	indent = MF_OPTVAL(indent, 0);
	if (indent) {
		indent_str = mu_alloc(indent+1);
		memset(indent_str, ' ', indent);
		indent_str[indent] = 0;
	}
	
	MF_OBSTACK_BEGIN();
	for (p = data_list; p; p = p->next) {
		if (iskey && type && indent_str) {
			MF_OBSTACK_1GROW('\n');
			for (i = 0; i < type->level; i++)
				MF_OBSTACK_GROW(indent_str);
		}

		if (type == NULL && p->entry_data.type != MMDB_DATA_TYPE_MAP) {
			MF_OBSTACK_CANCEL();
			free(indent_str);
			MMDB_free_entry_data_list(data_list);
			MF_THROW(mfe_failure,
				 "%s",
				 _("malformed data list"));
		}
		
		switch (p->entry_data.type) {
		case MMDB_DATA_TYPE_MAP:
			object_type_push(&type, OBJ_MAP, p->entry_data.data_size);
			MF_OBSTACK_1GROW('{');
			iskey = 1;
			continue;
			
		case MMDB_DATA_TYPE_ARRAY:
			object_type_push(&type, OBJ_ARRAY, p->entry_data.data_size);
			MF_OBSTACK_1GROW('[');
			continue;
			
		case MMDB_DATA_TYPE_UTF8_STRING:
			MF_OBSTACK_1GROW('"');
			MF_OBSTACK_GROW((char*)p->entry_data.utf8_string, p->entry_data.data_size);
			MF_OBSTACK_1GROW('"');
			if (iskey)
				MF_OBSTACK_1GROW(':');
			break;
			
		case MMDB_DATA_TYPE_DOUBLE:
			MF_OBSTACK_PRINTF("%g", p->entry_data.double_value);
			break;
			
		case MMDB_DATA_TYPE_BYTES:
			MF_OBSTACK_1GROW('[');
			for (i = 0; i < p->entry_data.data_size; i++) {
				if (i) MF_OBSTACK_1GROW([<','>]);
				MF_OBSTACK_GROW("%d", p->entry_data.bytes[i]);
			}
			MF_OBSTACK_1GROW(']');
			break;
			
		case MMDB_DATA_TYPE_UINT16:
			MF_OBSTACK_PRINTF("%u", p->entry_data.uint16);
			break;
			
		case MMDB_DATA_TYPE_UINT32:
			MF_OBSTACK_PRINTF("%" PRIu32, p->entry_data.uint32);
			break;
			
		case MMDB_DATA_TYPE_INT32:
			MF_OBSTACK_PRINTF("%" PRIi32, p->entry_data.int32);
			break;
			
		case MMDB_DATA_TYPE_UINT64:
			MF_OBSTACK_PRINTF("%" PRIu64, p->entry_data.uint64);
			break;
			
		case MMDB_DATA_TYPE_UINT128:
			MF_OBSTACK_GROW("'N/A");//FIXME
			break;
			
		case MMDB_DATA_TYPE_BOOLEAN:
			MF_OBSTACK_GROW(p->entry_data.boolean ? "true" : "false");
			break;
					
		case MMDB_DATA_TYPE_FLOAT:
			MF_OBSTACK_PRINTF("%g", p->entry_data.float_value);
			break;

		default:
			MF_OBSTACK_CANCEL();
			free(indent_str);
			MMDB_free_entry_data_list(data_list);
			MF_THROW(mfe_failure,
				 _("unsupported MMDB data type %d"),
				 p->entry_data.type);
		}

		if (type) {
			iskey = !iskey;
			if (iskey == 1) {
				while (type && --type->count == 0) {
					if (indent_str) {
						MF_OBSTACK_1GROW('\n');
						for (i = 1; i < type->level; i++)
							MF_OBSTACK_GROW(indent_str);
					}
					MF_OBSTACK_1GROW(type->type == OBJ_MAP
							 ? '}' : ']');
					object_type_pop(&type);
				}
				if (type) {
					MF_OBSTACK_1GROW([<','>]);
				}
			}
		}
	}
	MF_OBSTACK_1GROW(0);
	free(indent_str);
	MMDB_free_entry_data_list(data_list);

	if (type) {
		while (type)
			object_type_pop(&type);
		MF_THROW(mfe_failure,
			 _("malformed data list: reported and actual number of keys differ"));
	}
	
	MF_RETURN_OBSTACK();	
}
END
