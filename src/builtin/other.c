#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 51 "other.bi"
static mu_debug_handle_t debug_handle;
#line 994 "../../src/builtin/snarf.m4"

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "other.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include "mflib/_register.h"

static int
valid_user_p(eval_environ_t env, const char *name)
{
	int rc;
	struct mu_auth_data *auth_data = NULL;

	rc = mu_get_auth(&auth_data, mu_auth_key_name, name);
	mu_auth_data_free(auth_data);
	switch (rc) {
	case 0:
		rc = 1;
		break;
		
	case MU_ERR_AUTH_FAILURE:
		rc = 0;
		break;

	case EAGAIN:
                (
#line 39
	env_throw_bi(env, mfe_temp_failure, NULL, _("temporary failure querying for username %s"),name)
#line 39
);
#line 42
		break;

	default:
                (
#line 45
	env_throw_bi(env, mfe_failure, NULL, _("failure querying for username %s"),name)
#line 45
);
#line 48
		break;
	}

	
#line 51 "other.bi"

#line 51
mu_debug(debug_handle, MU_DEBUG_TRACE1,("Checking user %s: %s", name, rc ? "true" : "false"));
#line 53
	return rc;
}

void
#line 56
bi_validuser(eval_environ_t env)
#line 56

#line 56

#line 56 "other.bi"
{
#line 56
	
#line 56

#line 56
        char *  name;
#line 56
        
#line 56

#line 56
        get_string_arg(env, 0, &name);
#line 56
        
#line 56
        adjust_stack(env, 1);
#line 56

#line 56

#line 56
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 56
		prog_trace(env, "validuser %s",name);;
#line 56

{
	
#line 58
do {
#line 58
  push(env, (STKVAL)(mft_number)(valid_user_p(env, name)));
#line 58
  goto endlab;
#line 58
} while (0);
}
endlab:
#line 60
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 60
	return;
#line 60
}

void
#line 62
bi_interval(eval_environ_t env)
#line 62

#line 62

#line 62 "other.bi"
{
#line 62
	
#line 62

#line 62
        char *  str;
#line 62
        
#line 62

#line 62
        get_string_arg(env, 0, &str);
#line 62
        
#line 62
        adjust_stack(env, 1);
#line 62

#line 62

#line 62
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 62
		prog_trace(env, "interval %s",str);;
#line 62

{
       time_t t;
       const char *endp;

       	if (!(parse_time_interval(str, &t, &endp) == 0))
#line 67
		(
#line 67
	env_throw_bi(env, mfe_invtime, "interval", _("unrecognized time format (near `%s')"),endp)
#line 67
)
#line 69
;
       
#line 70
do {
#line 70
  push(env, (STKVAL)(mft_number)(t));
#line 70
  goto endlab;
#line 70
} while (0);
}
endlab:
#line 72
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 72
	return;
#line 72
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
other_init_builtin(void)
#line 994
{
#line 994
		debug_handle = mu_debug_register_category("bi_other");
#line 994

#line 994
	#line 56 "other.bi"
va_builtin_install_ex("validuser", bi_validuser, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 62 "other.bi"
va_builtin_install_ex("interval", bi_interval, 0, dtype_number, 1, 0, 0|0, dtype_string);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

