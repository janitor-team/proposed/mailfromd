#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "mmq.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2010-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



/* Clear the list of message modification requests. This function undoes
   the effect of the following functions, if they had been called
   previously: rcpt_add, rcpt_delete, header_add, header_insert,
   header_delete, header_replace, replbody, quarantine */
    
void
#line 24
bi_mmq_purge(eval_environ_t env)
#line 24

#line 24

#line 24 "mmq.bi"
{
#line 24
	
#line 24

#line 24
        
#line 24

#line 24
        
#line 24
        adjust_stack(env, 0);
#line 24

#line 24

#line 24
	if (builtin_module_trace(BUILTIN_IDX_mmq))
#line 24
		prog_trace(env, "mmq_purge");;
#line 24

{
	env_msgmod_clear(env);
}

#line 28
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 28
	return;
#line 28
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
mmq_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 24 "mmq.bi"
va_builtin_install_ex("mmq_purge", bi_mmq_purge, 0, dtype_unspecified, 0, 0, 0|0, dtype_unspecified);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

