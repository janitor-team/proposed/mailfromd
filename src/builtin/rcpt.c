#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "rcpt.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



void
#line 19
bi_rcpt_add(eval_environ_t env)
#line 19

#line 19

#line 19 "rcpt.bi"
{
#line 19
	
#line 19

#line 19
        char *  addr;
#line 19
        
#line 19

#line 19
        get_string_arg(env, 0, &addr);
#line 19
        
#line 19
        adjust_stack(env, 1);
#line 19

#line 19

#line 19
	if (builtin_module_trace(BUILTIN_IDX_rcpt))
#line 19
		prog_trace(env, "rcpt_add %s",addr);;
#line 19

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s %s",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(rcpt_add),
	      addr);
	env_msgmod_append(env, rcpt_add, addr, NULL, 0);
}

#line 32
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 32
	return;
#line 32
}

void
#line 34
bi_rcpt_delete(eval_environ_t env)
#line 34

#line 34

#line 34 "rcpt.bi"
{
#line 34
	
#line 34

#line 34
        char *  addr;
#line 34
        
#line 34

#line 34
        get_string_arg(env, 0, &addr);
#line 34
        
#line 34
        adjust_stack(env, 1);
#line 34

#line 34

#line 34
	if (builtin_module_trace(BUILTIN_IDX_rcpt))
#line 34
		prog_trace(env, "rcpt_delete %s",addr);;
#line 34

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s %s",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(rcpt_delete),
	      addr);
	env_msgmod_append(env, rcpt_delete, addr, NULL, 0);
}

#line 47
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 47
	return;
#line 47
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
rcpt_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 19 "rcpt.bi"
va_builtin_install_ex("rcpt_add", bi_rcpt_add, 0, dtype_unspecified, 1, 0, 0|0, dtype_string);
#line 34 "rcpt.bi"
va_builtin_install_ex("rcpt_delete", bi_rcpt_delete, 0, dtype_unspecified, 1, 0, 0|0, dtype_string);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

