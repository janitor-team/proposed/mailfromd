/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include "msg.h"

struct error_closure {
	size_t msglen;
	char *msgstr;
};

#define ERROR_CLOSURE_INITIALIZER { 0, NULL }

static void
errfunc(int ec, char const *errmsg, char const *input, int start, int end,
	void *closure)
{
	struct error_closure *cl = closure;
	mu_asnprintf(&cl->msgstr, &cl->msglen, "%s, near %.*s",
		     errmsg, end - start, input + start);
}

void
msgstr_cleanup(void *ptr)
{
	free(ptr);
}

MF_DSEXP
MF_DEFUN(filter_string, STRING, STRING input, STRING filter_pipe)
{
	int rc;
	mu_stream_t in, temp;
	struct error_closure err = ERROR_CLOSURE_INITIALIZER;
	size_t buflen;
	char *buf;
	size_t n;
	
	rc = mu_static_memory_stream_create(&in, input, strlen(input));
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_static_memory_stream_create: %s",
		  mu_strerror(rc));

	MF_DCL_CLEANUP(CLEANUP_ALWAYS, err.msgstr, msgstr_cleanup);
	
	rc = mfl_filter_pipe_create(&temp, in,
				    MU_FILTER_ENCODE,
				    MU_STREAM_READ,
				    filter_pipe,
				    NULL,
				    errfunc,
				    &err);
	mu_stream_unref(in);
	if (rc == 0) {
		in = temp;
	} else {
		MF_THROW(mfe_failure, "%s", err.msgstr);
	}
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, in, _builtin_stream_cleanup);

	buflen = strlen(input);
	buf = MF_ALLOC_HEAP_TEMP(buflen);
	MF_OBSTACK_BEGIN();
	
	while ((rc = mu_stream_read(in, buf, buflen, &n)) == 0 && n > 0) {
		MF_OBSTACK_GROW(buf, n);
	}

	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_stream_read: %s",
		  mu_strerror(rc));

	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(filter_fd, VOID, NUMBER src_fd, NUMBER dst_fd, STRING filter_pipe)
{
	int rc;
	mu_stream_t src, dst, flt;
	int yes = 1;
	struct error_closure err = ERROR_CLOSURE_INITIALIZER;
	
	rc = mu_fd_stream_create(&dst, NULL, _bi_io_fd(env, dst_fd, 1),
				 MU_STREAM_WRITE);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_fd_stream_create(dest): %s",
		  mu_strerror(rc));
	mu_stream_ioctl(dst, MU_IOCTL_FD, MU_IOCTL_FD_SET_BORROW, &yes);
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, dst, _builtin_stream_cleanup);
	
	rc = mu_fd_stream_create(&src, NULL, _bi_io_fd(env, src_fd, 0),
				 MU_STREAM_READ);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_fd_stream_create(source): %s",
		  mu_strerror(rc));
	mu_stream_ioctl(dst, MU_IOCTL_FD, MU_IOCTL_FD_SET_BORROW, &yes);

	MF_DCL_CLEANUP(CLEANUP_ALWAYS, err.msgstr, msgstr_cleanup);
	rc = mfl_filter_pipe_create(&flt, src,
				    MU_FILTER_ENCODE,
				    MU_STREAM_READ,
				    filter_pipe,
				    NULL,
				    errfunc,
				    &err);
	mu_stream_unref(src);
	if (rc == 0) {
		src = flt;
	} else {
		MF_THROW(mfe_failure, "%s", err.msgstr);
	}
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, src, _builtin_stream_cleanup);

	rc = mu_stream_copy(dst, src, 0, NULL);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_stream_copy: %s",
		  mu_strerror(rc));
}
END
