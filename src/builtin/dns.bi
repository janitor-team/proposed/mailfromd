/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <limits.h>
#include "srvcfg.h"
#include "global.h"

MF_DEFUN(primitive_hostname, STRING, STRING string)
{
	char *hbuf;
	mf_status stat;

	stat = resolve_ipstr(string, &hbuf);
	MF_ASSERT(stat == mf_success,
		  mf_status_to_exception(stat),
		  _("cannot resolve IP %s"),
		  string);
	
	pushs(env, hbuf);
	free(hbuf);
}
END

MF_DEFUN(primitive_resolve, STRING, STRING string, OPTIONAL, STRING domain)
{
	char *ipstr;
	mf_status stat;
	
	if (MF_OPTVAL(domain,"")[0]) {
		stat = resolve_ipstr_domain(string, domain, &ipstr);
		MF_ASSERT(stat == mf_success,
			  mf_status_to_exception(stat),
			  _("cannot resolve %s.%s"), string, domain);
	} else {
		stat = resolve_hostname(string, &ipstr);
		MF_ASSERT(stat == mf_success,
			  mf_status_to_exception(stat),
			  _("cannot resolve %s"), string);
	}
	pushs(env, ipstr);
	free(ipstr);
}
END

MF_DEFUN(primitive_hasmx, NUMBER, STRING string)
{
	struct dns_reply repl;
	mf_status mxstat;

	mxstat = dns_to_mf_status(mx_lookup(string, 0, &repl));
	
	MF_ASSERT(mxstat == mf_success || mxstat == mf_not_found,
		  mf_status_to_exception(mxstat),
		  _("cannot get MX records for %s"),
		  string);
	if (mxstat == mf_success) {
		dns_reply_free(&repl);
		MF_RETURN(1);
	}
	MF_RETURN(0);
}
END

static dns_status
resolve_host(const char *string, struct dns_reply *reply)
{
	struct in_addr addr;

	if (inet_aton(string, &addr)) {
		dns_reply_init(reply, dns_reply_ip, 1);
		reply->data.ip[0] = addr.s_addr;
		return dns_success;
	}
	return a_lookup(string, reply);
}

static int
dns_replies_intersect(struct dns_reply const *a, struct dns_reply const *b)
{
	int i, j;
	
	if (a->type == b->type && a->type == dns_reply_ip) {
		for (i = 0; i < a->count; i++) 
			for (j = 0; j < b->count; j++) 
				if (a->data.ip[i] == b->data.ip[j])
					return 1;
	}
	return 0;
}

MF_DEFUN(primitive_ismx, NUMBER, STRING domain, STRING ipstr)
{
	struct dns_reply areply, mxreply;
	dns_status status;
	int rc = 0;

	status = resolve_host(ipstr, &areply);
	MF_ASSERT(status == dns_success,
		  mf_status_to_exception(dns_to_mf_status(status)), 
                  _("cannot resolve host name %s"), ipstr);
	status = mx_lookup(domain, 1, &mxreply);
	if (status != dns_success) {
		dns_reply_free(&areply);
		MF_THROW(mf_status_to_exception(dns_to_mf_status(status)),
			 _("cannot get MXs for %s"), domain);
	}
	rc = dns_replies_intersect(&areply, &mxreply);
	dns_reply_free(&areply);
	dns_reply_free(&mxreply);
	MF_RETURN(rc);
}
END

MF_DEFUN(relayed, NUMBER, STRING s)
{
	MF_RETURN(relayed_domain_p(s));
}
END

MF_DEFUN(ptr_validate, NUMBER, STRING s)
{
	int rc, res;
	switch (rc = ptr_validate(s, NULL)) {
	case dns_success:
		res = 1;
		break;
	case dns_not_found:
		res = 0;
		break;
	default:
		MF_THROW(mf_status_to_exception(dns_to_mf_status(rc)),
			 _("failed to get PTR record for %s"), s);
	}
	MF_RETURN(res);
}
END

MF_DEFUN(primitive_hasns, NUMBER, STRING dom)
{
	struct dns_reply repl;
	mf_status stat = dns_to_mf_status(ns_lookup(dom, 0, &repl));
	MF_ASSERT(stat == mf_success || stat == mf_not_found,
		  mf_status_to_exception(stat),
		  _("cannot get NS records for %s"),
		  dom);
	if (stat == mf_success) {
		dns_reply_free(&repl);
		MF_RETURN(1);
	}
	MF_RETURN(0);
}
END

static int
cmp_ip(void const *a, void const *b)
{
	GACOPYZ_UINT32_T ipa = ntohl(*(GACOPYZ_UINT32_T const *)a);
	GACOPYZ_UINT32_T ipb = ntohl(*(GACOPYZ_UINT32_T const *)b);
	if (ipa < ipb)
		return -1;
	if (ipa > ipb)
		return 1;
	return 0;
}

static int
cmp_str(void const *a, void const *b)
{
	char * const *stra = a;
	char * const *strb = b;
	return strcmp(*stra, *strb);
}

static int
cmp_hostname(const void *a, const void *b)
{
	return strcasecmp(*(const char**) a, *(const char**) b);
}

typedef long DNS_REPLY_COUNT;
#define DNS_REPLY_MAX LONG_MAX

struct dns_reply_storage {
	DNS_REPLY_COUNT reply_count;
	size_t reply_max;
	struct dns_reply *reply_tab;
};

static inline int
dns_reply_entry_unused(struct dns_reply_storage *rs, DNS_REPLY_COUNT n)
{
	return n == -1 || rs->reply_tab[n].type == -1;
}

static inline void
dns_reply_entry_release(struct dns_reply_storage *rs, DNS_REPLY_COUNT n)
{
	rs->reply_tab[n].type = -1;
}

static inline struct dns_reply *
dns_reply_entry_locate(struct dns_reply_storage *rs, DNS_REPLY_COUNT n)
{
	if (n < 0 || n >= rs->reply_count || dns_reply_entry_unused(rs, n))
		return NULL;
	return &rs->reply_tab[n];
}

static void *
dns_reply_storage_alloc(void)
{
	struct dns_reply_storage *rs = mu_alloc(sizeof(*rs));
	rs->reply_count = 0;
	rs->reply_max = 0;
	rs->reply_tab = NULL;
	return rs;
}

static void
dns_reply_storage_destroy(void *data)
{
	struct dns_reply_storage *rs = data;
	DNS_REPLY_COUNT i;

	for (i = 0; i < rs->reply_count; i++) {
		if (!dns_reply_entry_unused(rs, i)) {
			dns_reply_free(&rs->reply_tab[i]);
			dns_reply_entry_release(rs, i);
		}
	}
	free(rs->reply_tab);
	free(rs);
}

MF_DECLARE_DATA(DNS, dns_reply_storage_alloc, dns_reply_storage_destroy)

static inline DNS_REPLY_COUNT
dns_reply_entry_alloc(struct dns_reply_storage *rs,
		      struct dns_reply **return_reply)
{
	DNS_REPLY_COUNT i;
	
	for (i = 0; i < rs->reply_count; i++) {
		if (dns_reply_entry_unused(rs, i)) {
			*return_reply = &rs->reply_tab[i];
			return i;
		}
	}
	if (rs->reply_count == DNS_REPLY_MAX)
		return -1;
	if (rs->reply_count == rs->reply_max) {
		size_t n = rs->reply_max;
		rs->reply_tab = mu_2nrealloc(rs->reply_tab, &rs->reply_max,
					     sizeof(rs->reply_tab[0]));
		for (; n < rs->reply_max; n++)
			dns_reply_entry_release(rs, n);
	}
	*return_reply = &rs->reply_tab[rs->reply_count];
	return rs->reply_count++;
}

MF_DEFUN(dns_reply_release, VOID, NUMBER n)
{
	struct dns_reply_storage *repl = MF_GET_DATA;
	if (!dns_reply_entry_unused(repl, n)) {
		dns_reply_free(&repl->reply_tab[n]);
		dns_reply_entry_release(repl, n);
	}
}
END

MF_DEFUN(dns_reply_count, NUMBER, NUMBER n)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	struct dns_reply *reply;
	
	if (n == -1)
		MF_RETURN(0);
	if (n < 0)
		MF_THROW(mfe_failure,
			 _("invalid DNS reply number: %ld"), n);
	reply = dns_reply_entry_locate(rs, n);
	if (!reply)
		MF_THROW(mfe_failure,
			 _("no such reply: %ld"), n);
	MF_RETURN(reply->count);
}
END

MF_DEFUN(dns_reply_string, STRING, NUMBER n, NUMBER i)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	struct dns_reply *reply = dns_reply_entry_locate(rs, n);
	if (!reply)
		MF_THROW(mfe_failure,
			 _("no such reply: %ld"), n);
	if (i < 0 || i >= reply->count)
		MF_THROW(mfe_range,
			 _("index out of range: %ld"), i);
	if (reply->type == dns_reply_ip) {
		struct in_addr addr;
		addr.s_addr = reply->data.ip[i];
		MF_RETURN(inet_ntoa(addr));
	}
	MF_RETURN(reply->data.str[i]);
}
END

MF_DEFUN(dns_reply_ip, STRING, NUMBER n, NUMBER i)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	struct dns_reply *reply = dns_reply_entry_locate(rs, n);
	if (!reply)
		MF_THROW(mfe_failure,
			 _("no such reply: %ld"), n);
	if (i < 0 || i >= reply->count)
		MF_THROW(mfe_range,
			 _("index out of range: %ld"), i);
	if (reply->type == dns_reply_str) {
		MF_THROW(mfe_failure,
			 _("can't use dns_reply_ip on string replies"));
	}
	MF_RETURN(reply->data.str[i]);
}
END

enum {
	DNS_TYPE_A = 1,
	DNS_TYPE_NS = 2,
	DNS_TYPE_PTR = 12,	
	DNS_TYPE_MX = 15,
	DNS_TYPE_TXT = 16,
};

static char *dns_type_name[] = {
	[DNS_TYPE_A] = "a",
	[DNS_TYPE_NS] = "ns",
	[DNS_TYPE_PTR] = "ptr",	
	[DNS_TYPE_MX] = "mx",
	[DNS_TYPE_TXT] = "txt"
};

MF_DEFUN(dns_query, NUMBER, NUMBER type, STRING domain, OPTIONAL, NUMBER sort,
	 NUMBER resolve)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	dns_status dnsstat;
	mf_status stat;
	DNS_REPLY_COUNT ret;
	struct dns_reply *reply;
	int (*cmpfun)(const void *, const void *) = NULL;
	struct in_addr addr;
	
	ret = dns_reply_entry_alloc(rs, &reply);
	MF_ASSERT(ret >= 0,
		  mfe_failure,
		  _("DNS reply table full"));
	
	switch (type) {
	case DNS_TYPE_PTR:
		MF_ASSERT(inet_aton(domain, &addr),
			  mfe_invip,
			  _("invalid IP: %s"), domain);
		dnsstat = ptr_lookup(addr, reply);
		cmpfun = cmp_hostname;
		break;
		
	case DNS_TYPE_A:
		dnsstat = resolve_host(domain, reply);
		break;
		
	case DNS_TYPE_TXT:
		dnsstat = txt_lookup(domain, reply);
		break;
		
	case DNS_TYPE_MX:
		dnsstat = mx_lookup(domain, MF_OPTVAL(resolve), reply);
		break;
		
	case DNS_TYPE_NS:
		dnsstat = ns_lookup(domain, MF_OPTVAL(resolve), reply);
		break;

	default:
		dns_reply_entry_release(rs, ret);		
		MF_THROW(mfe_failure,
			 _("unsupported type: %ld"), type);
	}
	
	stat = dns_to_mf_status(dnsstat);

	if (!mf_resolved(stat)) {
		dns_reply_entry_release(rs, ret);
		MF_THROW(mf_status_to_exception(stat),
			 _("cannot get %s records for %s"),
			 dns_type_name[type], domain);
	}
	if (stat == mf_not_found) {
		dns_reply_entry_release(rs, ret);
		MF_RETURN(-1);
	}
	if (MF_OPTVAL(sort)) {
		size_t entry_size;

		if (!cmpfun) {
			if (reply->type == dns_reply_str) {
				cmpfun = cmp_str;
			} else {
				cmpfun = cmp_ip;
			}
		}
		if (reply->type == dns_reply_str) {
			entry_size = sizeof(reply->data.str[0]);
		} else {
			entry_size = sizeof(reply->data.ip[0]);
		}
		
		qsort(reply->data.ptr, reply->count, entry_size, cmpfun);
	}
	MF_RETURN(ret);
}
END
