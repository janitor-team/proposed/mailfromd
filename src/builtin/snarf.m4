m4_divert(-1)m4_dnl -*- m4 -*-
# This file is part of Mailfromd. 
# Copyright (C) 2006-2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

m4_changequote([<,>])
m4_changecom(/*,*/)

/* This qualifier is used when declaring C variables corresponding to
 * MFL STRING parameters.  If the MFL function is declared with MF_DSEXP
 * attribute (i.e. if it can cause dataseg expansion), __mf_dataseg is
 * defined to MFL_DATASEG (volatile), exempting the corresponding variable
 * from optimizations.
 */
m4_define([<__mf_dataseg>])

/* The MF_DSEXP attribute indicates that the MF_DEFUN that follows it
 * can cause dataseg expansion.
 */
m4_define([<MF_DSEXP>],[<m4_define([<__mf_dataseg>],[<MFL_DATASEG>])>])

/* Flags that will be passed to va_builtin_install_ex. */
m4_define([<__MF_DEFUN_FLAGS__>],0)

/* __mf_add_defun_flag(F)
 * ----------------------
 * Add F to the current value of __MF_DEFUN_FLAGS__ via logical OR.
 */ 
m4_define([<__mf_add_defun_flag>],m4_dnl
[<m4_define([<__MF_DEFUN_FLAGS__>],m4_ifelse(__MF_DEFUN_FLAGS__,0,$1,[<__MF_DEFUN_FLAGS__>]|$1))>])

/* __MF_DSEXP_REQ - an auxiliary macro which checks, whether __mf_dataseg
 * is empty.  If so, it issues a diagnostic message.
 *
 * The intent is to warn the programmer that the function in question can
 * result in dataseg being expanded.  The warning is suppressed by MF_DSEXP
 * attribute (for MF_DEFUNs) or by the use of MF_DSEXP_SUPPRESS macro (for
 * C functions).
 */
m4_define([<__MF_DSEXP_REQ>],[<m4_dnl
m4_ifelse(__mf_dataseg,[<>],[<m4_ifdef([<__MF_FUNCTION__>],[<m4_dnl
m4_errprint(m4___file__:m4___line__: Function __MF_FUNCTION__ should be defined with [<MF_DSEXP>] attribute
)>],m4_dnl
[<m4_errprint(m4___file__:m4___line__: this call can cause dataseg expansion; consider using [<MF_DSEXP_SUPPRESS>]
)>])>])>])

/* __mf_define_fun(NAME) - Defines a macro NAME, which, when invoked,
 * produces a warning if __mf_dataseg is not defined, and expands
 * to the literal NAME.
 */
m4_define([<__mf_define_fun>],[<m4_ifelse([<$#>],[<0>],[<[<$0>]>],
[<_$0([<$1>],[<$>][<*>])>])>])
m4_define([<___mf_define_fun>],[<
m4_define([<$1>],[<__MF_DSEXP_REQ()
[<$1>]([<$2>])>])>])

/* MF_DSEXP_SUPPRESS(NAME, DEFN)
 * -----------------------------
 * Suppresses dataseg expansion warnings within DEFN and redeclares NAME to
 * produce a warning if used in a context which is not safe for dataseg
 * expansion.
 */
m4_define([<MF_DSEXP_SUPPRESS>],[<m4_pushdef([<__mf_dataseg>],[</**/>])
$2
m4_popdef([<__mf_dataseg>])
__mf_define_fun([<$1>])>])

/* MF_MODULE_NAME()
 * ----------------
 * Expand to the name of this module.
 */
m4_pushdef([<MF_MODULE_NAME>])

/* MF_BUILTIN_MODULE([NAME])
 * -------------------------
 * Start MFL built-in module text.  Optional NAME is the name of the module.
 * If not supplied, the name is defined as the base name of the module file.
 */
m4_define([<MF_BUILTIN_MODULE>],m4_dnl
[<m4_pushdef([<MF_MODULE_NAME>],m4_dnl
m4_ifelse([<$1>],,[<m4_dnl
m4_patsubst(m4___file__,\(.*/\)?\(.*\)\.bi,\2)>],[<$1>]))>])m4_dnl

/* MF_MODULE_DEBUG_NAME
 * --------------------
 * Expand to the "debug" name for the MODULE.
 */
m4_define([<MF_MODULE_DEBUG_NAME>],[<m4_dnl
[<bi_>]m4_translit(MF_MODULE_NAME,[<A-Z->],[<a-z_>])>])

/* MF_MODULE_IDX()
 * ---------------
 * Expand to the BUILTIN_IDX_ constant for the current module.
 */
m4_define([<MF_MODULE_IDX>],[<m4_dnl
[<BUILTIN_IDX_>]MF_MODULE_NAME()>])

/* __mf_argtype(arg)
 * -----------------
 * Expand to Mailfromd value type code corresponding to ARG:
 *
 *   __mf_argtype(STRING) => dtype_string
 *   __mf_argtype(NUMBER) => dtype_number
 *   __mf_argtype(POINTER) => dtype_pointer
 *   __mf_argtype(anything) => dtype_unspecified
 */
m4_define([<__mf_argtype>],m4_dnl
[<m4_ifelse($1,STRING,dtype_string,$1,NUMBER,dtype_number,m4_dnl
$1,POINTER,dtype_pointer,m4_dnl
dtype_unspecified)>])

/* mf_argtype(TYPE NAME)
 * ---------------------
 * Expand to the Mailfromd value type code corresponding to
 * TYPE. See __mf_argtype above.
 */
m4_define([<mf_argtype>],m4_dnl
[<__mf_argtype(m4_patsubst([<$1>],[<[ \t].*>],))>])

/* mf_typelist(ARGLIST)
 * --------------------
 * Convert ARGLIST into a list of corresponding Mailfromd value type codes.
 * E.g.:
 *   mf_typelist(STRING a, NUMBER b) => dtype_string, dtype_number
 */
m4_define([<mf_typelist>],m4_dnl
[<m4_ifelse($#, 1, [<mf_argtype($1)>], m4_dnl,
$1, [<OPTIONAL>], [<mf_typelist(m4_shift($@))>],
[<mf_argtype($1), >]m4_dnl
[<mf_typelist(m4_shift($@))>])>])

/* __mf_has_optarg(ARGS...)
 * ------------------------
 * Expand to 1 if ARGS contain OPTIONAL keyword, and to 0 otherwise
 */
m4_define([<__mf_has_optarg>],m4_dnl
[<m4_ifelse($1, , 0,$1,[<OPTIONAL>], 1,[<m4_dnl
__mf_has_optarg(m4_shift($@))>])>])

/* __mf_c_type(TYPE)
 * -----------------
 * Expand to a C type corresponding to the Mailfromd TYPE
 */
m4_define([<__mf_c_type>],m4_dnl
[<m4_ifelse($1,STRING,char * __mf_dataseg,$1,NUMBER,long ,$1,POINTER,void * __mf_dataseg, [<UNKNOWN TYPE $1>])>])

/* __mf_c_getarg(TYPE)
 * -------------------
 * Expand to the get_(.*)_arg function call for the given MFL TYPE
 */
m4_define([<__mf_c_getarg>],m4_dnl
[<m4_ifelse($1,STRING,get_string_arg,$1,NUMBER,get_numeric_arg,m4_dnl
$1,POINTER,get_pointer_arg, ERROR )>])

/* mf_c_argdcl(TYPE NAME)
 * ----------------------
 * Translate MFL declaration "TYPE NAME" to the corresponding C one:
 * 
 *   mf_c_argdcl(STRING str) => char *str
 */
m4_define([<mf_c_argdcl>],m4_dnl
[<m4_regexp([<$1>],[<\(\w+\)\W+\(\w+\)>],[<__mf_c_type(\1)>] \2)>])

/* mf_c_arginit(TYPE NAME, NUM)
 * ----------------------------
 * Translate MFL declaration "TYPE NAME" to the corresponding C initialization:
 * 
 *   mf_c_arginit(STRING str, NUM) => get_string_arg(env, NUM, &str)
 */
m4_define([<mf_c_arginit>],m4_dnl
[<m4_regexp([<$1>],[<\(\w+\)\W+\(\w+\)>],[<__mf_c_getarg(\1)(env, $2, &\2)>])>])

/* __mf_c_argdcl_list(NARG, LIST)
 * ------------------------------
 * Translate MFL declaration list to a set of corresponding C variable
 * declarations.
 * For more details, see mf_c_arglist below.
 */
m4_define([<__mf_c_argdcl_list>],m4_dnl
[<m4_ifelse($2, , ,$2,[<OPTIONAL>],[<m4_dnl
__mf_c_argdcl_list($1, m4_shift(m4_shift($@)))>],
[<mf_c_argdcl($2);
        __mf_c_argdcl_list(m4_incr($1), m4_shift(m4_shift($@)))>])>])

/* __mf_c_arginit_list(NARG, OPT, LIST)
 * ------------------------------------
 * Translate MFL declaration list to a set of corresponding C variable
 * initializations.
 *
 * OPT is 0 when processing mandatory parameters and 1 when processing
 * optional ones.
 *
 * For more details, see mf_c_arglist below.
 */
m4_define([<__mf_c_arginit_list>],m4_dnl
[<m4_ifelse($3, , ,$3,[<OPTIONAL>],[<m4_dnl
__mf_c_arginit_list($1, 1, m4_shift(m4_shift(m4_shift($@))))>],
[<m4_ifelse($2,1,[<if (__bi_argcnt > m4_decr($1))
                >])m4_dnl
mf_c_arginit($3, m4_eval($1));
        __mf_c_arginit_list(m4_incr($1), $2, m4_shift(m4_shift(m4_shift($@))))>])>])

/* mf_c_arglist(LIST)
 * ------------------
 * Translate MFL declaration list to a set of corresponding C variable
 * declarations with initializations.
 * Insert an instruction to adjust the stack parameters after obtaining the
 * variables.
 * E.g.:
 *   mf_c_arglist(STRING a, NUMBER n) =>
 *         char *a;
 *         long n; 
 *         get_string_arg(env, 0, &a);
 *         get_numeric_arg(env, 1, &n);
 *         adjust_stack(env, 2);
 *
 * Or, if the builtin takes optional parameters:
 *
 *  mf_c_arglist(STRING a, NUMBER n, OPTIONAL, STRING s) =>
 *         char *a;
 *         long n;
 *         long __bi_argcnt;
 *         get_numeric_arg(env, 0, &__bi_argcnt);
 *         get_string_arg(env, 1, &a);
 *         get_numeric_arg(env, 2, &n);
 *         if (__bi_argcnt > 2)
 *                 get_string_arg(env, 3, &s);
 *         adjust_stack(env, __bi_argcnt + 1);
 */
m4_define([<mf_c_arglist>],m4_dnl
[<
m4_pushdef([<__ARG1__>], m4_ifelse(__MF_VARARGS__,1,1,[<__mf_has_optarg($@)>]))
        __mf_c_argdcl_list(__ARG1__, $@)
m4_ifelse(__ARG1__,0,,[<        long __bi_argcnt;
        get_numeric_arg(env, 0, &__bi_argcnt);>])
        __mf_c_arginit_list(__ARG1__, 0, $@)
        adjust_stack(env, m4_ifelse(__ARG1__,0,mf_argcount($@),__bi_argcnt + 1));
m4_popdef([<__ARG1__>])m4_dnl
>])

/* __mf_printf_type(TYPE)
 * ----------------------
 * Translate the MFL data type TYPE to the printf conversion specification
 * suitable for outputting it.
 */
m4_define([<__mf_printf_type>],m4_dnl
[<m4_ifelse($1,STRING,s,$1,NUMBER,lu,$1,POINTER,p,%?)>])

/* mf_printf_macro(TYPE NAME)
 * --------------------------
 * Translate TYPE to the printf conversion
 */
m4_define([<mf_printf_macro>],m4_dnl
[<%[<>]m4_regexp([<$1>],[<\(\w+\)\W+.*>],[<__mf_printf_type(\1)>])>])

/* mf_printf_list(LIST)
 * --------------------
 * Convert the list of MFL variable declarations to a space-delimited
 * list of printf conversion specifications:
 *   mf_printf_list(STRING a, NUMBER b) =>  %s %lu
 * Notice, that the expansion begins with the space character.
 */ 
m4_define([<mf_printf_list>],m4_dnl
[<m4_ifelse($1,,,$1,[<OPTIONAL>],m4_dnl
[<mf_printf_list(m4_shift($@))>],m4_dnl
[< mf_printf_macro($1)[<>]mf_printf_list(m4_shift($@))>])>])

/* __mf_argname(TYPE NAME)
 * -----------------------
 * Expand to NAME
 */ 
m4_define([<__mf_argname>],m4_dnl
[<m4_regexp($1,[<\w+\W+\(\w+\)>],\1)>])

/* mf_argnames(LIST)
 * -----------------
 * Extract names from the Mailfromd declaration list:
 *  mf_argnames(STRING a, NUMBER b) => a, b
 */ 
m4_define([<mf_argnames>],m4_dnl
[<m4_ifelse($#,1, [<__mf_argname($1)>],m4_dnl
$1,[<OPTIONAL>],[<mf_argnames(m4_shift($@))>],m4_dnl
[<__mf_argname($1), mf_argnames(m4_shift($@))>])>])

/* __mf_defined_argname(OPT, TYPE NAME)
 * ------------------------------------
 * Same as __mf_argname, but wrap the argument into MF_OPTVAL if OPT is 1.
 */ 
m4_define([<__mf_defined_argname>],m4_dnl
[<m4_ifelse($1,1,[<m4_dnl
MF_OPTVAL(__mf_argname($2),m4_ifelse(mf_argtype($2),[<dtype_string>],"",0))>],m4_dnl
[<__mf_argname($2)>])>])

/* __mf_defined_argnames(OPT, LIST)
 * --------------------------------
 * Same as mf_argnames, if OPT is 0.
 * Otherwise, if OPT is 1, expands to list of MF_OPTVAL constructs with
 * consecutive parameter names as arguments.
 * After encountering the OPTIONAL keyword, changes OPT to 1.
 */ 
m4_define([<__mf_defined_argnames>],m4_dnl
[<m4_ifelse($#,2, [<__mf_defined_argname($1,$2)>],m4_dnl
$2,[<OPTIONAL>],[<__mf_defined_argnames(1,m4_shift(m4_shift($@)))>],m4_dnl
[<__mf_defined_argname($1,$2), m4_dnl
__mf_defined_argnames($1,m4_shift(m4_shift($@)))>])>])

/* mf_defined_argnames(LIST)
 * -------------------------
 * Same as mf_argnames, but arguments after the OPTIONAL keywords are
 * protected by MF_OPTVAL
 */ 
m4_define([<mf_defined_argnames>],m4_dnl
[<__mf_defined_argnames(0,$@)>])

/* __mf_argpos(POS,NEEDLE,STACK...)
 * --------------------------------
 * Expands to position at which NEEDLE occurs in STACK
 * Arguments:
 *  POS    -  Current position
 *  NEEDLE -  String to find
 *  STACK  -  Argument list
 * Example:
 *  __mf_argpos(0, x, a, b, x) => 2
 */ 
m4_define([<__mf_argpos>],m4_dnl
[<m4_ifelse($2,$3,$1,$3,,[<m4_errprint(m4___file__:m4___line__: No such variable >]$2[<
)>],[<__mf_argpos(m4_incr($1), $2, m4_shift(m4_shift(m4_shift($@))))>])>])

/* mf_argpos(ARG, TYPE1 ARG1, TYPE2 ARG2 ...)
 * ------------------------------------------
 * Expand to the (zero-based) position of ARG in the argument list:
 *
 *  mf_argpos(x, STRING a, NUMBER b, OPTIONAL, STRING x) => 3
 */
m4_define([<mf_argpos>],m4_dnl
[<__mf_argpos(0, $1, mf_argnames(m4_shift($@)))>])

/* __mf_defined(NAME, ARGS...)
 * ---------------------------
 * Scan ARGS... for the definition of the built-in function parameter NAME,
 * and expand to a C conditional expression that yields true if it is defined.
 * ARGS are parameter declarations in the form:
 *      TYPE NAME
 */ 
m4_define([<__mf_defined>],
[<m4_ifelse(__mf_has_optarg($@),0,1,m4_dnl
[<__bi_argcnt > mf_argpos($1,__MF_ARGLIST__)>])>])

/* MF_DEFINED(NAME)
 * ----------------
 * Expand to a C conditional expression that yields true if the parameter
 * NAME is defined.
 *
 *  __MF_ARGLIST__ => STRING a, NUMBER b, OPTIONAL, STRING x
 *  MF_DEFINED(x) => (__bi_argcnt > 2)
 *
 *  __MF_ARGLIST__ => STRING a, NUMBER b, STRING x
 *  MF_DEFINED(x) => (1)
 */
m4_define([<MF_DEFINED>],
[<m4_ifdef([<__MF_ARGLIST__>],([<__mf_defined($1, __MF_ARGLIST__)>]),m4_dnl
[<m4_errprint(m4___file__:m4___line__: [<MF_DEFINED used out of functional context
>])>])>])

/* MF_OPTVAL(NAME[, DEFVAL])
 * -------------------------
 * If the parameter NAME is defined, expand to its value, otherwise expand
 * to DEFVAL or 0
 */ 
m4_define([<MF_OPTVAL>],
[<m4_ifdef([<__MF_ARGLIST__>],m4_dnl
[<(MF_DEFINED($1) ? $1 : m4_ifelse([<$2>],,0,$2))>],m4_dnl
[<m4_errprint(m4___file__:m4___line__: [<MF_OPTVAL used out of functional context
>])>])>])

/* __mf_check_end()
 * ----------------
 * Signal error if the previous MF_DEFUN statement was not properly closed
 * with END
 */ 
m4_define([<__mf_check_end>],m4_dnl
[<m4_ifdef([<__MF_FUNCTION__>],m4_dnl
[<m4_errprint(m4___file__:m4___line__: Function '>]__MF_FUNCTION__[<' was not closed
)
m4_popdef([<__MF_FUNCTION__>])
m4_define([<__mf_error_code>],1)>])>])

/* MF_STATE(state)
 * ---------------
 * Declare next MF_DEFUN as valid only in the given state.
 * The state argument is any valid milter state, as declared in
 * enum smtp_state (see mailfromd.h around line 74--87), but without the
 * `smtp_state_' prefix.
 *
 * Multiple occurrences of MF_STATE accumulate.
 */
m4_define([<MF_STATE>],
[<m4_ifdef([<__MF_STATE__>],m4_dnl
[<m4_define([<__MF_STATE__>],__MF_STATE__[< | STATMASK(smtp_state_$1)>])>],m4_dnl
[<m4_define([<__MF_STATE__>],[<STATMASK(smtp_state_$1)>])>])>])

/* MF_CAPTURE([str])
 * -----------------
 * Declare next MF_DEFUN as requiring message capturing.
 * The form with the STR argument can be used only in MF_STATE(eom)
 * functions (FIXME: this should be enforced at compile time).  In that
 * case a reference to the capture stream is stored in STR.
 */
m4_define([<MF_CAPTURE>],m4_dnl
[<__mf_add_defun_flag([<MFD_BUILTIN_CAPTURE>])[<>]m4_dnl
m4_ifelse([<$1>],,,[<m4_define([<__MF_CAPTURE__>],[<$1>])>])>])

/* env_get_stream()
 * ----------------
 * Prohibit the use of the library function of the same name.
 */ 
m4_define([<env_get_stream>],m4_dnl
[<m4_ifdef([<__MF_FUNCTION__>],m4_dnl
[<m4_ifdef([<__MF_ENV_GET_STREAM_PROHIBIT>],m4_dnl
[<m4_errprint(m4___file__:m4___line__: [<env_get_stream is illegal here>]
)
m4_define([<__mf_error_code>],1)>],[<[<env_get_stream>]($@)>])>],m4_dnl
[<[<env_get_stream>]($@)>])>])

/* mf_optcount(ARGS...)
 * --------------------
 * Return the number of optional arguments in ARGS
 */ 
m4_define([<mf_optcount>],[<m4_dnl
m4_ifelse($#,1,0,$1,[<OPTIONAL>],m4_eval($# - 1),[<mf_optcount(m4_shift($@))>])>])

/* __mf_argcount(COUNT, ARGS...)
 * -----------------------------
 * Auxiliary function for mf_argcount
 *   COUNT is number of arguments counted so far
 *   ARGS are the rest of the arguments
 */
m4_define([<__mf_argcount>],[<m4_dnl
m4_ifelse($#,2,$1,$2,[<OPTIONAL>],[<__mf_argcount($1, m4_shift(m4_shift($@)))>],m4_dnl
[<__mf_argcount(m4_incr($1), m4_shift(m4_shift($@)))>])>])

/* mf_argcount(ARGS...)
 * --------------------
 * Return the number of arguments in ARGS, not counting eventual OPTIONAL
 * modifier.
 * FIXME: same as m4_eval($# - __mf_has_optarg($@))
 */ 
m4_define([<mf_argcount>],[<m4_dnl
m4_ifelse($1,,0,__mf_argcount(1,$@))>])

/* mf_prog_trace(FNAME[, ARGS...])
 * -------------------------------
 * Expand to the prog_trace call for function FNAME with arguments ARGS.
 */ 
m4_define([<mf_prog_trace>],[<m4_dnl
prog_trace(env, "$1[<>]mf_printf_list(m4_shift($@))"m4_dnl
m4_ifelse($2,,,[<,mf_defined_argnames(m4_shift($@))>]));>])

/* __mf_mfl_type(TYPE)
 * -------------------
 * If TYPE is REGFLAGS, add MFD_BUILTIN_REGEX_FLAGS to the value of
 * __MF_DEFUN_FLAGS__ and expand to NUMBER.  Otherwise, expand to
 * TYPE.
 */
m4_define([<__mf_mfl_type>],
[<m4_ifelse($1,REGFLAGS,[<m4_dnl
__mf_add_defun_flag([<MFD_BUILTIN_REGEX_FLAGS>])m4_dnl
NUMBER>],[<$1>])>])

/* __mf_arglist_init(ARGS)
 * -----------------------
 * ARGS is the argument declaration list from MF_DEFUN.  The macro
 * expands to the ARGS list with the type of its first element fixed,
 * if necessary.  That is, if the first argument ha type designator
 * REGFLAGS, it is changed to NUMBER and MFD_BUILTIN_REGEX_FLAGS is
 * set in __MF_DEFUN_FLAGS__.  Otherwise, themacro expands to ARGS
 * unchanged.
 */
m4_define([<__mf_arglist_init>],m4_dnl
[<m4_ifelse([<$1>],OPTIONAL,[<$@>],[<m4_dnl
m4_regexp([<$1>],[<\(\w+\)\W+\(\w+\)>],[<__mf_mfl_type(\1) \2>]) m4_dnl
m4_ifelse([<$2>],,,[<, m4_shift($@)>])>])>])

/* __mf_defun(VARARG, NAME, RETTYPE, ARGS...)
 * ------------------------------------------
 * Begin a built-in function declaration.
 * Arguments:
 *   VARARG
 *     Initial value for the FLAGS argument to the va_builtin_install_ex
 *     function.  Actually, MFD_BUILTIN_VARIADIC if NAME is a variadic
 *     function, and 0 otherwise.
 *   NAME
 *     Name of the function.
 *   RETTYPE
 *     Return type (STRING or NUMBER).
 *   ARGS
 *     List of arguments with types.
 */ 
m4_define([<__mf_defun>],m4_dnl
[<__mf_check_end[<>]m4_dnl
void
bi_$2(eval_environ_t env)
m4_pushdef([<__MF_FUNCTION__>], $2)m4_dnl
m4_pushdef([<__MF_RETTYPE__>], $3)m4_dnl
m4_pushdef([<__MF_ARGLIST__>], [<__mf_arglist_init(m4_shift(m4_shift(m4_shift($*))))>])
m4_ifelse($3,STRING,[<MF_DSEXP>])
m4_divert(1)m4_dnl
va_builtin_install_ex("$2", bi_$2,m4_dnl
 m4_ifdef([<__MF_STATE__>],__MF_STATE__,0),m4_dnl
 __mf_argtype($3),m4_dnl
 mf_argcount(__MF_ARGLIST__),m4_dnl
 mf_optcount(__MF_ARGLIST__),m4_dnl
 __MF_DEFUN_FLAGS__|$1,m4_dnl
 mf_typelist(__MF_ARGLIST__));
m4_divert(2)m4_dnl
{m4_ifdef([<__MF_CAPTURE__>],[<
	  mu_stream_t __MF_CAPTURE__;
>])
	mf_c_arglist(__MF_ARGLIST__)
m4_ifdef([<__MF_CAPTURE__>],[<{
	  int rc = env_get_stream(env, &__MF_CAPTURE__);
	  MF_ASSERT(rc == 0, mfe_failure,
	            "cannot obtain capture stream reference: %s",
		    mu_strerror(rc));
m4_define([<__MF_ENV_GET_STREAM_PROHIBIT>])m4_dnl
	}>])
	if (builtin_module_trace(MF_MODULE_IDX))
		mf_prog_trace($2,__MF_ARGLIST__);
>])

/* MF_DEFUN(NAME, RETTYPE, ARGS...)
 * --------------------------------
 * Start a declaration of the built-in function NAME.  The declaration
 * must be terminated with END.
 * Arguments:
 *  NAME      - function name
 *  RETTYPE   - return type
 *  ARGS      - list of argument declarations, each one of the form
 *              TYPE ARGNAME; special argument OPTIONAL begins the list of
 *              optional parameters.
 */
m4_define([<MF_DEFUN>],[<m4_pushdef([<__MF_VARARGS__>], 0)m4_dnl
__mf_defun(0, $@)>])

m4_define([<__mf_defun_varargs>],[<m4_dnl
m4_pushdef([<__MF_VARARGS__>], 1)m4_dnl
m4_ifelse(__mf_has_optarg(m4_shift(m4_shift(m4_shift(m4_shift($@))))),0,m4_dnl
[<__mf_defun($1, m4_shift($@))>],m4_dnl
[<m4_errprint(m4___file__:m4___line__: A variadic function cannot take optional arguments
)
m4_define([<__mf_error_code>],1)>])>])

/* MF_DEFUN_VARARGS(NAME, RETTYPE. [TYPE PARAM...])
 * ------------------------------------------------
 * Start the declaration of the built-in variadic function NAME.
 * The declarationl must be terminated with END.
 * Arguments:
 *  NAME       - function name
 *  RETTYPE    - return type
 *  TYPE PARAM - declarations of mandatory parameters. 
 * Actual parameters can be retrieved using MF_VA_ARG(), which see,
 * See also MF_VA_START and MF_VA_END
 */
m4_define([<MF_DEFUN_VARARGS>],[<__mf_defun_varargs(MFD_BUILTIN_VARIADIC,$@)>])

/* MF_DEFUN_VARARGS_NO_PROM(NAME, RETTYPE, [TYPE PARAM...])
 * --------------------------------------------------------
 * Same as MF_DEFUN_VARARGS, but actual parameters are not promoted to
 * STRING.
 */
m4_define([<MF_DEFUN_VARARGS_NO_PROM>],[<m4_dnl
__mf_defun_varargs(MFD_BUILTIN_VARIADIC|MFD_BUILTIN_NO_PROMOTE,$@)>])

/* Prevent the use of the `return' statement in defuns.
 */ 
m4_define([<return>],[<m4_dnl
m4_ifdef([<__MF_FUNCTION__>],[<m4_dnl
m4_errprint(m4___file__:m4___line__: [<return used in MF_DEFUN>]
)
m4_define([<__mf_error_code>],1)>],[<[<return>]>])>])

/* MF_RETURN(VALUE[,TYPE])
 * -----------------------
 * Expand to C code for returning VALUE from the current function. If TYPE
 * is supplied, cast value to this type.
 */ 
m4_define([<MF_RETURN>],[<
m4_ifdef([<__MF_VA_START_USED__>],[<m4_dnl
m4_errprint(m4___file__:m4___line__: [<MF_RETURN>] used before [<MF_VA_END>]
)
m4_define([<__mf_error_code>],1)>],[<m4_dnl
do {
m4_pushdef([<__type>],[<m4_ifelse($2,,__MF_RETTYPE__,$2)>])m4_dnl
  m4_ifelse(__type,[<NUMBER>],[<push(env, (STKVAL)(mft_number)($1))>],m4_dnl
__type,[<STRING>],[<pushs(env, $1)>],m4_dnl
__type,[<string>],[<pushs(env, $1)>],m4_dnl
__type,[<STKVAL>],[<push(env, (STKVAL) ($1))>],m4_dnl
__type,,[<m4_errprint(m4___file__:m4___line__: return type undefined
)>],m4_dnl
[<push(env, (STKVAL) ([<mft_>]$2) ($1))>]);
m4_popdef([<__type>])m4_dnl
  goto endlab;
m4_define([<__MF_ENDLAB__>])m4_dnl  
} while (0)>])>])

/* MF_ALLOC_HEAP(OFF, LEN)
 * -----------------------
 * Allocate LEN bytes from the heap.  Return the offset of the allocated
 * space in OFF.
 */ 
m4_define([<MF_ALLOC_HEAP>],[<m4_dnl
__MF_DSEXP_REQ()m4_dnl
(char*) env_data_ref(env, ([<$1>] = heap_reserve(env, $2)))>])

/* MF_ALLOC_HEAP_TEMP(LEN)
 * -----------------------
 * Temporarily allocate LEN bytes from the heap.
 */ 
m4_define([<MF_ALLOC_HEAP_TEMP>],[<__MF_DSEXP_REQ()m4_dnl
mf_c_val(heap_tempspace(env, $1), ptr)>])

/* MF_COPY_STRING(off, string)
 * ---------------------------
 * Copy STRING to the heap.  Return the pointer to the copy.
 */ 
m4_define([<MF_COPY_STRING>],[<m4_dnl
__MF_DSEXP_REQ()m4_dnl
strcpy((char*)env_data_ref(env, $1 = heap_reserve(env, strlen($2) + 1)), $2)>])

/* MF_OBSTACK_BEGIN()
 * ------------------
 * Begin temporary space manipulations.
 * NOTE: No other heap manipulation function can be used between
 * MF_OBSTACK_BEGIN and MF_OBSTACK_CANCEL/MF_RETURN_OBSTACK/MF_OBSTACK_FINISH
 */ 
m4_define([<MF_OBSTACK_BEGIN>],[<heap_obstack_begin(env)>])

m4_define([<MF_OBSTACK_GROW>],[<m4_dnl
m4_ifelse($2,,[<
do {
  char *__s = $1;
  m4_ifelse($#,3,[<$3 = >])heap_obstack_grow(env, __s, strlen(__s));
} while (0)>],[<m4_ifelse($#,3,[<$3 = >])heap_obstack_grow(env, $1, $2)>])>])

m4_define([<MF_OBSTACK_1GROW>],[<m4_dnl
do { char __c = $1; heap_obstack_grow(env, &__c, 1); } while(0)>])

/* MF_OBSTACK_RECLAIM(N)
 * ---------------------
 * Reclaim last N bytes from the obstack.
 * If N is greater than the size allocated for obstack, the function
 * will abort.
 */
m4_define([<MF_OBSTACK_RECLAIM>],[<m4_dnl
heap_obstack_reclaim(env, $1)>])

/* MF_OBSTACK_TRUNCATE(N)
 * ----------------------
 * Truncate the obstack to N bytes.
 * If N equals to the current size of the obstack, it is a no-op.
 * If N is greater than the obstack size S, it is equivalent to
 * MF_OBSTACK_GROW(NULL, N-S).  The newly allocated chunk is not
 * initialized.
 */
m4_define([<MF_OBSTACK_TRUNCATE>],[<m4_dnl
heap_obstack_truncate(env, $1)>])

m4_define([<MF_OBSTACK_PRINTF>],[<heap_obstack_sprintf(env, $*)>])
  
/* MF_OBSTACK_CANCEL()
 * -------------------
 * Cancel temporary heap allocation initiated by MF_OBSTACK_BEGIN
 */ 
m4_define([<MF_OBSTACK_CANCEL>],[<heap_obstack_cancel(env)>])

/* MF_OBSTACK_BASE()
 * -----------------
 * Return a C pointer to the beginning of the currently
 * allocated obstack space.
 */ 
m4_define([<MF_OBSTACK_BASE>],[<heap_obstack_base(env)>])

/* MF_RETURN_OBSTACK()
 * -------------------
 * Relocate and return temporary space 
 */ 
m4_define([<MF_RETURN_OBSTACK>],[<MF_RETURN(heap_obstack_finish(env),[<STKVAL>])>])

/* MF_OBSTACK_FINISH()
 * -------------------
 * Relocate temporary space and return its starting offset.
 */	
m4_define([<MF_OBSTACK_FINISH>],[<heap_obstack_finish(env)>])

/* MF_VA_START()
 * -------------
 * Begin a code section for handling variable number of arguments.
 */
m4_define([<MF_VA_START>],[<
m4_ifelse(__MF_VARARGS__,1,[<m4_dnl
m4_define([<__MF_VA_START_USED__>],m4___file__:m4___line__)m4_dnl
unroll_stack(env, __bi_argcnt + 1)>],
[<m4_errprint(m4___file__:m4___line__: [<MF_VA_START>] used but `__MF_FUNCTION__' does not take variable number of arguments
)m4_dnl
m4_define([<__mf_error_code>],1)>])>])

/* MF_VA_END()
 * -----------
 * End the section started with MF_VA_START
 */
m4_define([<MF_VA_END>],[<m4_dnl
m4_ifdef([<__MF_VA_START_USED__>],[<m4_dnl
m4_undefine([<__MF_VA_START_USED__>])m4_dnl
adjust_stack(env, __bi_argcnt + 1)>],m4_dnl
[<m4_errprint(m4___file__:m4___line__: [<MF_VA_END>] without previous [<MF_VA_START>]
)
m4_define([<__mf_error_code>],1)>])>])

/* __mf_va_count()
 * ---------------
 * Return number of variable arguments passed to the current vararg function
 */
m4_define([<__mf_va_count>],[<m4_dnl
(__bi_argcnt - mf_argcount(__MF_ARGLIST__))>])

/* MF_VA_COUNT()
 * -------------
 * Return actual number of variable arguments passed to the
 * function.  Bail out if the function is not a vararg one.
 */
m4_define([<MF_VA_COUNT>],[<m4_dnl
m4_ifelse(__MF_VARARGS__,1,[<__mf_va_count>],
[<m4_errprint(m4___file__:m4___line__: [<MF_VA_COUNT>] used but `__MF_FUNCTION__' does not take variable number of arguments
)m4_dnl
m4_define([<__mf_error_code>],1)>])>])

/* MF_VA_ARG(N, TYPE, VAR)
 * -----------------------
 * Produce a code for assigning to VAR the Nth
 * argument of the given TYPE in a vararg section.
 */
m4_define([<MF_VA_ARG>],[<m4_dnl
m4_ifdef([<__MF_VA_START_USED__>],m4_dnl
[<m4_pushdef([<__ARGN__>],[<($1+mf_argcount(__MF_ARGLIST__))>])
 ((__bi_argcnt > __ARGN__) ?m4_dnl 
   __mf_c_getarg($2)(env, __ARGN__ + 1, &$3) :m4_dnl
   (MF_THROW(mfe_range, "Argument %u is not supplied", (unsigned) __ARGN__),m4_dnl
(__mf_c_type($2)) 0))m4_dnl
m4_popdef([<__ARGN__>])>],
[<m4_errprint(m4___file__:m4___line__: [<MF_VA_ARG>] without previous [<MF_VA_START>]
)
m4_define([<__mf_error_code>],1)>])>])

/* MF_VAR(NAME,TYPE[,FLAG])
 * ------------------------
 * Declare a global variable NAME of type TYPE.  FLAGS are additional flags
 * (SYM_VOLATILE is always used).
 */ 
m4_define([<MF_VAR>],[<m4_dnl
static size_t $1_loc
m4_divert(1)m4_dnl
	builtin_variable_install("$1", __mf_argtype($2), m4_dnl
[<SYM_VOLATILE>]m4_ifelse($3,,,|$3), &$1_loc);
m4_divert(2)m4_dnl
>])

/* MF_VAR_REF(NAME, TYPE[, VALUE])
 * -------------------------------
 * Reference the global variable NAME.
 * In two-argument form, expand to its value.  In three-arguments form, assign
 * the VALUE to it.
 */ 
m4_define([<MF_VAR_REF>],[<m4_dnl
mf_c_val(*env_data_ref(env, $1_loc),$2) m4_ifelse($3,,,= ($3))>])

m4_define([<MF_VAR_STRING>],[<m4_dnl
(char*)env_data_ref(env, MF_VAR_REF($1, size))>])

/* MF_VAR_SET_STRING(NAME, VALUE)
 * ------------------------------
 * Set variable NAME to the string VALUE
 */
m4_define([<MF_VAR_SET_STRING>],[<
{ size_t __off;
  const char *__s = $2;
  if (__s)
     MF_COPY_STRING(__off, __s);
  else
     __off = 0;
  MF_VAR_REF($1, size, __off); }
>])

/* MF_VAR_INC(NAME)
 * ----------------
 * Increment the value of the global variable NAME
 */ 
m4_define([<MF_VAR_INC>],[<m4_dnl
env_var_inc(env, $1_loc)>])

/* MF_DECLARE_DATA(NAME, INIT [, DESTR, FREECAP])
 * ----------------------------------------------
 * Declare private data for the current module.
 * The data can be accessed using MF_GET_DATA (see below).
 * Arguments:
 *  NAME  - data identifier.
 *  INIT  - initialization function (void init(void))
 *  DESTR - destructor function (void destr(void*))
 *  FREECAP - free capture function (void freecap(void*))
 */ 
m4_define([<MF_DECLARE_DATA>],[<
m4_define([<__MF_PRIV_ID__>],$1_id)
static int __MF_PRIV_ID__;
m4_divert(1)m4_dnl
__MF_PRIV_ID__ = builtin_priv_register($2, m4_dnl
m4_ifelse($3,,NULL,$3),
m4_ifelse($4,,NULL,$4));
m4_divert(2)m4_dnl
>])

/* MF_GET_DATA
 * -----------
 * Return pointer to the private data, declared with MF_DECLARE_DATA
 */ 
m4_define([<MF_GET_DATA>],[<m4_dnl
m4_ifdef([<__MF_PRIV_ID__>],[<m4_dnl
env_get_builtin_priv(m4_ifelse($1,,env,$1),__MF_PRIV_ID__)>],m4_dnl
[<m4_errprint(m4___file__:m4___line__: private data not declared (call [<MF_DECLARE_DATA>] first)
)
m4_define([<__mf_error_code>],1)
>])>])

/* MF_VASTRING(OFF)
 * ----------------
 * Extract string from VAPTR array at offset OFF.
 */ 	
m4_define([<MF_VASTRING>],[<env_vaptr(env, $1)>])

/* MF_THROW(EXCODE, ...)
 * ------------------------
 * Throw exception.  First argument (EXCODE) is the exception code.
 * Rest of arguments supply additional parameters for the env_throw_bi
 * call.
 */ 
m4_define([<MF_THROW>],[<m4_dnl
(
m4_ifdef([<__MF_CAPTURE__>],[<
	mu_stream_unref(__MF_CAPTURE__),
>])m4_dnl
	env_throw_bi(env, $1, m4_ifdef([<__MF_FUNCTION__>],m4_dnl
"__MF_FUNCTION__", NULL), m4_shift($@))
)>])

/* MF_STREAM_TO_MESSAGE(STR)
 * -------------------------
 * Convert mailutils stream STR to a message.
 */
m4_define([<MF_STREAM_TO_MESSAGE>],[<m4_dnl
_builtin_mu_stream_to_message($1, env, m4_ifdef([<__MF_FUNCTION__>],m4_dnl
"__MF_FUNCTION__", NULL))>])

/* MF_ASSERT(COND, EXCEPTION, ...)
 * -------------------------------
 * Throw EXCEPTION unless condition COND is true.  Additional arguments
 * are passed to MF_THROW.
 */ 
m4_define([<MF_ASSERT>],[<m4_dnl
	if (!($1))
		MF_THROW(m4_shift($@))
>])

/* END -- Finish the built-in function declaration, created with MF_DEFUN
 */ 
m4_define([<END>],m4_dnl
[<m4_ifdef([<__MF_ENDLAB__>],[<endlab:>])
m4_ifdef([<__MF_CAPTURE__>],[<
	mu_stream_unref(__MF_CAPTURE__);
>])m4_dnl
m4_undefine([<__MF_ENV_GET_STREAM_PROHIBIT>])m4_dnl
m4_ifdef([<__MF_VA_START_USED__>],[<m4_dnl
m4_errprint(m4___file__:m4___line__: Missing [<MF_VA_END>]
)m4_dnl
m4_errprint(>]__MF_VA_START_USED__[<: This is the location of [<MF_VA_START>]
)m4_dnl
m4_define([<__mf_error_code>],1)m4_dnl
m4_undefine([<__MF_VA_START_USED__>])>])m4_dnl
m4_popdef([<__MF_FUNCTION__>])m4_dnl
m4_popdef([<__MF_RETTYPE__>])m4_dnl
m4_popdef([<__MF_ARGLIST__>])m4_dnl
m4_popdef([<__MF_VARARGS__>])m4_dnl
m4_undefine([<__MF_CAPTURE__>])m4_dnl
m4_undefine([<__MF_STATE__>])m4_dnl
m4_undefine([<__MF_ENDLAB__>])m4_dnl
m4_define([<__MF_DEFUN_FLAGS__>],0)m4_dnl
m4_define([<__mf_dataseg>])m4_dnl
        env_function_cleanup_flush(env, CLEANUP_RETURN);
	return;
}>])

/* MF_DCL_CLEANUP(WHEN, PTR[, FUNC])
 * ---------------------------------
 * Register clean-up function FUNC for data pointer PTR.  FUNC(PTR) will
 * be called if an exception is thrown or MF_CLEANUP(PTR) is explicitly
 * used.  If neither of these occurs, it will be called upon destroying
 * the evaluation environment.
 * Registration can be undone using MF_CLR_CLEANUP.
 */
m4_define([<MF_DCL_CLEANUP>],[<env_function_cleanup_add(env, $1, m4_dnl
$2, m4_ifelse($3,,NULL,$3))>])

/* MF_CLR_CLEANUP(WHEN, PTR)
 * -------------------------
 * Undo registration of the clean-up routine for PTR.
 */
m4_define([<MF_CLR_CLEANUP>],[<env_function_cleanup_del(env, $@)>])

m4_define([<MF_TRANS_PREFIX>],[<_MFL_>])
/* MF_TRANS(NAME)
 * --------------
 * Expands to a struct builtin_const_trans for translating constant NAME
 * between MFL and C.
 * See syslog.bi for an example of its use.
 */
m4_define([<MF_TRANS>],[<{ MF_TRANS_PREFIX[<>]$1, $1 }>])

/* MF_PRAGMA(NAME, MIN, MAX)
 * -------------------------
 * Register a pragma NAME.
 * Arguments:
 *  NAME   - name of the new pragma
 *  MIN    - minimum number of arguments (including pragma name)
 *  MAX    - maximum number of arguments (or 0, if unlimited).
 *
 * The macro must be followed by a C block with the code for handling
 * the pragma. The code can access the following three parameters:
 *  int argc    -  number of actual arguments in argv array;
 *  char **argv -  NULL terminated array of actual arguments;
 *  text        -  actual pragma text, with built-in constants expanded;
 */ 
m4_define([<MF_PRAGMA>],m4_dnl
[<m4_divert(1)m4_dnl
m4_pushdef([<pragma_handler>],[<_pragma_>]m4_translit($1,-,_))
install_pragma("$1", $2, $3, pragma_handler);
m4_divert(2)
[<static void >]pragma_handler[< (int argc, char **argv, const char *text)>]
m4_popdef([<pragma_handler>])m4_dnl
>])

/* MF_COND(SYM)
 * ------------
 * Protect the code below by the `#ifdef SYM' / `#endif'
 */
m4_define([<MF_COND>],[<m4_dnl
m4_pushdef([<__MF_COND_SYMBOL>],$1)m4_dnl
#ifdef __MF_COND_SYMBOL
>])

/* MF_DEBUG(LEV, (ARGS...))
 * ------------------------
 * Expand to a mu_debug call with the debug handle for this built-in
 * module and LEV, (ARGS...) as arguments.
 */
m4_define([<MF_DEBUG>],[<
m4_ifdef([<__MF_DEBUG>],,[<m4_dnl
m4_define([<__MF_DEBUG>],1)m4_dnl
m4_divert(3)m4_dnl
static mu_debug_handle_t debug_handle;
m4_divert(2)>])
[<mu_debug>](debug_handle, $@)>])>])

m4_pushdef([<__MF_INIT_SEQUENCE>])

/* MF_INIT(CODE)
 * -------------
 * Insert CODE into the initialization function for the
 * curent module (MODNAME_init_builtin).
 */ 
m4_define([<MF_INIT>],m4_dnl
 [<m4_pushdef([<__MF_INIT_SEQUENCE>],[<$1>])>])

/* Expand to the generated text upon the end of input.
 */ 
m4_m4wrap([<
m4_ifelse(MF_MODULE_NAME,,[<m4_errprint([<MF_BUILTIN_MODULE is missing
>])
m4_m4exit(1)>])
m4_undefine([<__MF_PRIV_ID__>])m4_dnl
m4_ifdef([<__MF_COND_SYMBOL>],[<#endif /[<>]* __MF_COND_SYMBOL */
>])
void
MF_MODULE_NAME()_init_builtin(void)
{
	m4_ifdef([<__MF_DEBUG>],[<m4_dnl
	debug_handle = mu_debug_register_category("MF_MODULE_DEBUG_NAME");
>])
m4_ifdef([<__MF_COND_SYMBOL>],[<#ifdef __MF_COND_SYMBOL
	pp_define("__MF_COND_SYMBOL");
>])m4_dnl
	m4_undivert(1)
	__MF_INIT_SEQUENCE
m4_popdef([<__MF_INIT_SEQUENCE>])m4_dnl	
m4_ifdef([<__MF_COND_SYMBOL>],[<#endif /[<>]* __MF_COND_SYMBOL */
m4_popdef([<__MF_COND_SYMBOL>])>])m4_dnl
}
m4_divert(0)m4_dnl
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <sys/types.h>

#include "mailfromd.h"
#include "prog.h"
#include "builtin.h"

m4_undivert(3)
m4_undivert(2)
m4_popdef([<MF_MODULE_NAME>])m4_dnl
>])

m4_divert(2)
/* End of snarf.m4 */
