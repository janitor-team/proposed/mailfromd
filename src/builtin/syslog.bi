/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include "syslog.h"
#include "mflib/syslog.h"

static struct builtin_const_trans syslog_prio[] = {
	MF_TRANS(LOG_EMERG),
	MF_TRANS(LOG_ALERT),
	MF_TRANS(LOG_CRIT),
	MF_TRANS(LOG_ERR),
	MF_TRANS(LOG_WARNING),
	MF_TRANS(LOG_NOTICE),
	MF_TRANS(LOG_INFO),
	MF_TRANS(LOG_DEBUG)
};

static struct builtin_const_trans syslog_fac[] = {
	MF_TRANS(LOG_KERN),
	MF_TRANS(LOG_USER),
	MF_TRANS(LOG_MAIL),
	MF_TRANS(LOG_DAEMON),
	MF_TRANS(LOG_AUTH),
	MF_TRANS(LOG_SYSLOG),
	MF_TRANS(LOG_LPR),
	MF_TRANS(LOG_NEWS),
	MF_TRANS(LOG_UUCP),
	MF_TRANS(LOG_CRON),
	MF_TRANS(LOG_AUTHPRIV),
	MF_TRANS(LOG_FTP),
	MF_TRANS(LOG_LOCAL0),
	MF_TRANS(LOG_LOCAL1),
	MF_TRANS(LOG_LOCAL2),
	MF_TRANS(LOG_LOCAL3),
	MF_TRANS(LOG_LOCAL4),
	MF_TRANS(LOG_LOCAL5),
	MF_TRANS(LOG_LOCAL6),
	MF_TRANS(LOG_LOCAL7),
};

MF_DEFUN(syslog, VOID, NUMBER priority, STRING text)
{
	int prio = 0;
	int f = 0;

 	prio = priority & _MFL_LOG_PRIMASK;
	_builtin_const_to_c(syslog_prio, MU_ARRAY_SIZE(syslog_prio),
			    prio, &prio);

	f = priority & ~_MFL_LOG_PRIMASK;
	_builtin_const_to_c(syslog_fac, MU_ARRAY_SIZE(syslog_fac),
			    f, &f);
	logger_text(prio | f, text);
}
END

