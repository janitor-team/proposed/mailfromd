#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "prereq.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



/* #pragma prereq <version> */
#line 20 "prereq.bi"

#line 20
static void _pragma_prereq (int argc, char **argv, const char *text)
#line 20

{
	int res;
	
	if (mf_vercmp(PACKAGE_VERSION, argv[1], &res) < 0) {
		if (res == 0)
			abort();
		parse_error(_("invalid version syntax"));
	} else if (res < 0) {
		parse_error(_("this source file requires %s version %s or later,"),
			    PACKAGE_NAME, argv[1]);
		parse_error(_("but you use version %s"), PACKAGE_VERSION);
	}
}

void
#line 35
bi_vercmp(eval_environ_t env)
#line 35

#line 35

#line 35 "prereq.bi"
{
#line 35
	
#line 35

#line 35
        char *  a;
#line 35
        char *  b;
#line 35
        
#line 35

#line 35
        get_string_arg(env, 0, &a);
#line 35
        get_string_arg(env, 1, &b);
#line 35
        
#line 35
        adjust_stack(env, 2);
#line 35

#line 35

#line 35
	if (builtin_module_trace(BUILTIN_IDX_prereq))
#line 35
		prog_trace(env, "vercmp %s %s",a, b);;
#line 35

{
	int res;

	if (mf_vercmp(a, b, &res) < 0)
		(
#line 40
	env_throw_bi(env, mfe_range, "vercmp", _("not a valid version number: %s (argument %d)"),res == 0 ? a : b,res + 1)
#line 40
);
#line 43
	
#line 43
do {
#line 43
  push(env, (STKVAL)(mft_number)(res));
#line 43
  goto endlab;
#line 43
} while (0);
}
endlab:
#line 45
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 45
	return;
#line 45
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
prereq_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 20 "prereq.bi"

#line 20
install_pragma("prereq", 1, 0, _pragma_prereq);
#line 35 "prereq.bi"
va_builtin_install_ex("vercmp", bi_vercmp, 0, dtype_number, 2, 0, 0|0, dtype_string, dtype_string);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

