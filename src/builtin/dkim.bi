/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2020-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
MF_COND(WITH_DKIM)

#include "dkim.h"
#include "msg.h"

struct msgmod_data {
	mu_message_t msg;
	char const *h_list;
	struct msgmod_closure *e_msgmod;
};

static int
message_replace_body(mu_message_t msg, mu_stream_t stream)
{
	mu_body_t body;
	int rc;

	rc = mu_body_create(&body, msg);
	if (rc) {
		mu_diag_funcall(MU_DIAG_ERROR, "mu_body_create", NULL, rc);
		return rc;
	}

	rc = mu_body_set_stream(body, stream, msg);
	if (rc) {
		mu_body_destroy(&body, msg);
		mu_diag_funcall(MU_DIAG_ERROR, "mu_body_set_stream", NULL, rc);
		return rc;
	}

	rc = mu_message_set_body(msg, body, mu_message_get_owner(msg));
	if (rc) {
		mu_body_destroy(&body, msg);
		mu_diag_funcall(MU_DIAG_ERROR, "mu_message_set_body", NULL, rc);
	}
	return rc;
}

static int
do_msgmod(void *item, void *data)
{
	struct msgmod_closure *msgmod = item;
	struct msgmod_data *md = data;
	mu_header_t hdr;
	mu_stream_t stream;
	int rc;

	MF_DEBUG(MU_DEBUG_TRACE6,
		 ("%s %s: %s %u",
		  msgmod_opcode_str(msgmod->opcode),
		  SP(msgmod->name), SP(msgmod->value), msgmod->idx));

	mu_message_get_header(md->msg, &hdr);

	switch (msgmod->opcode) {
	case header_replace:
		if (!dkim_header_list_match(md->h_list, msgmod->name))
			break;
		if (mu_header_sget_value(hdr, msgmod->name, NULL) == 0) {
			rc = mu_header_insert(hdr, msgmod->name, msgmod->value,
					      NULL,
					      msgmod->idx, MU_HEADER_REPLACE);
			if (rc == MU_ERR_NOENT) {
				rc = mu_header_append(hdr, msgmod->name, msgmod->value);
				if (rc)
					mu_diag_funcall(MU_DIAG_ERROR,
							"mu_header_insert",
							msgmod->name,
							rc);
			} else if (rc) {
				mu_diag_funcall(MU_DIAG_ERROR,
						"mu_header_insert",
						msgmod->name,
						rc);
			}
		} else {
			rc = mu_header_append(hdr, msgmod->name, msgmod->value);
			if (rc)
				mu_diag_funcall(MU_DIAG_ERROR,
						"mu_header_append",
						msgmod->name,
						rc);
		}
		if (rc) {
			md->e_msgmod = msgmod;
			return rc;
		}
		break;

	case header_delete:
		if (!dkim_header_list_match(md->h_list, msgmod->name))
		    break;
		rc = mu_header_remove(hdr, msgmod->name, msgmod->idx);
		if (rc && rc != MU_ERR_NOENT) {
			mu_diag_funcall(MU_DIAG_ERROR,
					"mu_header_remove",
					msgmod->name,
					rc);
			md->e_msgmod = msgmod;
			return rc;
		}
		break;

	case body_repl:
		rc = mu_static_memory_stream_create(&stream, msgmod->value,
						    strlen(msgmod->value));
		if (rc) {
			md->e_msgmod = msgmod;
			return rc;
		}
		rc = message_replace_body(md->msg, stream);
		if (rc) {
			mu_stream_destroy(&stream);
			md->e_msgmod = msgmod;
			return rc;
		}
		break;

	case body_repl_fd:
		rc = mu_fd_stream_create (&stream, "<body_repl_fd>",
					  msgmod->idx,
					  MU_STREAM_READ);
		if (rc) {
			md->e_msgmod = msgmod;
			return rc;
		}
		rc = message_replace_body(md->msg, stream);
		if (rc) {
			mu_stream_destroy(&stream);
			md->e_msgmod = msgmod;
			return rc;
		}
		break;

	case header_add:
	case header_insert:
		if (!dkim_header_list_match(md->h_list, msgmod->name))
		    break;
		md->e_msgmod = msgmod;
		return MU_ERR_USER0;

	case rcpt_add:
	case rcpt_delete:
	case quarantine:
	case set_from:
		break;
	}
	return 0;
}

MF_VAR(dkim_sendmail_commaize, NUMBER);

MF_INIT([<
	 long n = 1;
	 ds_init_variable("dkim_sendmail_commaize", &n);
>])

MF_STATE(eom)
MF_CAPTURE
MF_DEFUN(dkim_sign, VOID, STRING d, STRING s, STRING keyfile,
OPTIONAL,
STRING canon_h, STRING canon_b, STRING headers, STRING algo)
{
	struct dkim_signature sig = {
		.v = DKIM_VERSION,
		.d = d,
		.s = s,
		.canon = { DKIM_CANON_SIMPLE, DKIM_CANON_SIMPLE },
		.q = DKIM_QUERY_METHOD,
		.l = DKIM_LENGTH_ALL
	};
	static char default_headers[] =
		"From:From:"
		"Reply-To:Reply-To:"
		"Subject:Subject:"
		"Date:Date:"
		"To:"
		"Cc:"
		"Resent-Date:"
		"Resent-From:"
		"Resent-To:"
		"Resent-Cc:"
		"In-Reply-To:"
		"References:"
		"List-Id:"
		"List-Help:"
		"List-Unsubscribe:"
		"List-Subscribe:"
		"List-Post:"
		"List-Owner:"
		"List-Archive";
	mu_message_t msg;
	int rc;
	char *sighdr, *p;
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	if (MF_DEFINED(algo)) {
		sig.algo = dkim_algo(algo);
		MF_ASSERT(sig.algo != -1,
			  mfe_failure,
			  _("bad signing algorithm: %s"), algo);
		sig.a = algo;
	} else {
		sig.algo = DKIM_ALGO_DEFAULT;
		sig.a = (char*) dkim_algo_str[sig.algo];
	}
	
	if (MF_DEFINED(canon_h)) {
		sig.canon[0] = dkim_str_to_canon_type(canon_h, NULL);
		if (sig.canon[0] == DKIM_CANON_ERR)
			MF_THROW(mfe_failure,
				 _("bad canonicalization type: %s"), canon_h);
	}
	if (MF_DEFINED(canon_b)) {
		sig.canon[1] = dkim_str_to_canon_type(canon_b, NULL);
		if (sig.canon[1] == DKIM_CANON_ERR)
			MF_THROW(mfe_failure,
				 _("bad canonicalization type: %s"), canon_b);
	}

	sig.h = MF_OPTVAL(headers, default_headers);
	bi_get_current_message(env, &msg);
	if (env_msgmod_count(env)) {
		struct msgmod_data mdat;

		rc = mu_message_create_copy(&mdat.msg, msg);
		if (rc) {
			mu_diag_funcall(MU_DIAG_ERROR,
					"mu_message_create_copy",
					NULL, rc);
			MF_THROW(mfe_failure,
				 "mu_message_create_copy: %s",
				 mu_strerror(rc));
		}
		mdat.h_list = sig.h;
		mdat.e_msgmod = 0;
		rc = env_msgmod_apply(env, do_msgmod, &mdat);
		if (rc) {
			mu_message_unref(mdat.msg);
			if (rc == MU_ERR_USER0) {
				MF_THROW(mfe_badmmq,
					 _("MMQ incompatible with dkim_sign: %s on %s, value %s"),
					 msgmod_opcode_str(mdat.e_msgmod->opcode),
					 mdat.e_msgmod->name,
					 SP(mdat.e_msgmod->value));
			} else if (mdat.e_msgmod) {
				MF_THROW(mfe_failure,
					 _("%s failed on %s, value %s: %s"),
					 msgmod_opcode_str(mdat.e_msgmod->opcode),
					 mdat.e_msgmod->name,
					 SP(mdat.e_msgmod->value),
					 mu_strerror(rc));
			} else
				MF_THROW(mfe_failure,
					 _("error: %s"),
					 mu_strerror(rc));
		}
		msg = mdat.msg;
	} else
		mu_message_ref(msg);

	if (MF_VAR_REF(dkim_sendmail_commaize, int)) {
		mu_message_t sm_msg;
		rc = sendmail_address_normalize(msg, &sm_msg);
		mu_message_unref(msg);
		MF_ASSERT(rc == 0, mfe_failure,
			  _("Sendmail address header postprocessing failed: %s"),
			  mu_strerror(rc));
		msg = sm_msg;
	}

	rc = mfd_dkim_sign(msg, &sig, keyfile, &sighdr);
	mu_message_unref(msg);
	MF_ASSERT(rc == 0, mfe_failure, _("DKIM failed"));

	p = strchr(sighdr, ':');
	*p++ = 0;
	while (mu_isblank(*p))
		p++;

	trace("%s%s:%u: %s %d \"%s: %s\"",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(header_insert),
	      0,
	      sighdr, p);
	env_msgmod_append(env, header_insert, sighdr, p, 0);
	free(sighdr);
}
END

MF_VAR(dkim_explanation_code, NUMBER);
MF_VAR(dkim_explanation, STRING);
MF_VAR(dkim_verified_signature, STRING);
MF_VAR(dkim_signing_algorithm, STRING);

MF_DSEXP
MF_DEFUN(dkim_verify, NUMBER, NUMBER nmsg)
{
	mu_message_t msg = bi_message_from_descr(env, nmsg);
	char *sig;
	char const *algo;
	int result = mfd_dkim_verify(msg, &sig, &algo);
	MF_VAR_REF(dkim_explanation_code, long, result);
	MF_VAR_SET_STRING(dkim_explanation, dkim_explanation_str[result]);
	MF_VAR_SET_STRING(dkim_signing_algorithm, algo);
	if (dkim_result_trans[result] == DKIM_VERIFY_OK) {
		MF_VAR_SET_STRING(dkim_verified_signature, sig);
	} else {
		MF_VAR_SET_STRING(dkim_verified_signature, "");
	}
	MF_RETURN(dkim_result_trans[result]);
}
END
