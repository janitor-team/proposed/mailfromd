#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "geoip.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2009-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifdef WITH_GEOIP
#line 18

#include <GeoIP.h>

void
#line 21
bi_geoip_country_code_by_addr(eval_environ_t env)
#line 21

#line 21

#line 21 "geoip.bi"
{
#line 21
	
#line 21

#line 21
        char * MFL_DATASEG ip;
#line 21
        long  tlc;
#line 21
        
#line 21
        long __bi_argcnt;
#line 21
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 21
        get_string_arg(env, 1, &ip);
#line 21
        if (__bi_argcnt > 1)
#line 21
                get_numeric_arg(env, 2, &tlc);
#line 21
        
#line 21
        adjust_stack(env, __bi_argcnt + 1);
#line 21

#line 21

#line 21
	if (builtin_module_trace(BUILTIN_IDX_geoip))
#line 21
		prog_trace(env, "geoip_country_code_by_addr %s %lu",ip, ((__bi_argcnt > 1) ? tlc : 0));;

{
	GeoIP *gi = GeoIP_new(GEOIP_STANDARD);
	const char *ret;
	
		if (!(gi != NULL))
#line 27
		(
#line 27
	env_throw_bi(env, mfe_failure, "geoip_country_code_by_addr", "GeoIP_new")
#line 27
)
#line 29
;
	ret = ((__bi_argcnt > 1) ? tlc : 0) ? GeoIP_country_code3_by_addr(gi, ip) 
		             : GeoIP_country_code_by_addr(gi, ip);
	GeoIP_delete(gi);
		if (!(ret != NULL && strcmp(ret, "N/A") != 0))
#line 33
		(
#line 33
	env_throw_bi(env, mfe_not_found, "geoip_country_code_by_addr", _("country code not found"))
#line 33
)
#line 35
;
	
#line 36
do {
#line 36
  pushs(env, ret);
#line 36
  goto endlab;
#line 36
} while (0);
}
endlab:
#line 38
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 38
	return;
#line 38
}

void
#line 40
bi_geoip_country_code_by_name(eval_environ_t env)
#line 40

#line 40

#line 40 "geoip.bi"
{
#line 40
	
#line 40

#line 40
        char * MFL_DATASEG name;
#line 40
        long  tlc;
#line 40
        
#line 40
        long __bi_argcnt;
#line 40
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 40
        get_string_arg(env, 1, &name);
#line 40
        if (__bi_argcnt > 1)
#line 40
                get_numeric_arg(env, 2, &tlc);
#line 40
        
#line 40
        adjust_stack(env, __bi_argcnt + 1);
#line 40

#line 40

#line 40
	if (builtin_module_trace(BUILTIN_IDX_geoip))
#line 40
		prog_trace(env, "geoip_country_code_by_name %s %lu",name, ((__bi_argcnt > 1) ? tlc : 0));;

{
	GeoIP *gi = GeoIP_new(GEOIP_STANDARD);
	const char *ret;
	
		if (!(gi != NULL))
#line 46
		(
#line 46
	env_throw_bi(env, mfe_failure, "geoip_country_code_by_name", "GeoIP_new")
#line 46
)
#line 48
;
	ret = ((__bi_argcnt > 1) ? tlc : 0) ? GeoIP_country_code3_by_name(gi, name)
		             : GeoIP_country_code_by_name(gi, name);
	GeoIP_delete(gi);
		if (!(ret != NULL && strcmp(ret, "N/A") != 0))
#line 52
		(
#line 52
	env_throw_bi(env, mfe_not_found, "geoip_country_code_by_name", _("country code not found"))
#line 52
)
#line 54
;
	
#line 55
do {
#line 55
  pushs(env, ret);
#line 55
  goto endlab;
#line 55
} while (0);
}
endlab:
#line 57
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 57
	return;
#line 57
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994
#endif /* WITH_GEOIP */
#line 994

#line 994
void
#line 994
geoip_init_builtin(void)
#line 994
{
#line 994
	
#line 994
#ifdef WITH_GEOIP
#line 994
	pp_define("WITH_GEOIP");
#line 994
	#line 21 "geoip.bi"
va_builtin_install_ex("geoip_country_code_by_addr", bi_geoip_country_code_by_addr, 0, dtype_string, 2, 1, 0|0, dtype_string, dtype_number);
#line 40 "geoip.bi"
va_builtin_install_ex("geoip_country_code_by_name", bi_geoip_country_code_by_name, 0, dtype_string, 2, 1, 0|0, dtype_string, dtype_number);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
#endif /* WITH_GEOIP */
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

