/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2010-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

MF_STATE(eom)
MF_DEFUN(progress, VOID)
{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      "PROGRESS");
	gacopyz_progress(env_get_context(env));
}
END

