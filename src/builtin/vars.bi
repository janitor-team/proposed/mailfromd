/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include "filenames.h" /* For DEFAULT_FROM_ADDRESS */
MF_VAR(rcpt_count, NUMBER);
MF_VAR(milter_client_family, NUMBER, SYM_PRECIOUS);
MF_VAR(milter_client_address, STRING, SYM_PRECIOUS);
MF_VAR(milter_server_family, NUMBER, SYM_PRECIOUS);
MF_VAR(milter_server_address, STRING, SYM_PRECIOUS);
MF_VAR(milter_server_id, STRING, SYM_PRECIOUS);

/* Functions to access %rcpt_count */
unsigned long
get_rcpt_count(eval_environ_t env)
{
 	return MF_VAR_REF(rcpt_count, long);
}

void
clear_rcpt_count(eval_environ_t env)
{
	MF_VAR_REF(rcpt_count, long, 0);
}

void
incr_rcpt_count(eval_environ_t env)
{
	MF_VAR_INC(rcpt_count);
}

/* define_milter_address name */
m4_define([<define_milter_address>],[<
MF_DSEXP_SUPPRESS([<set_milter_$1_address>],
[<void
set_milter_$1_address(eval_environ_t env, milter_sockaddr_t *addr,
		      socklen_t len)
{
	char *path;
	
	switch (addr->sa.sa_family) {
	case PF_INET:
		MF_VAR_REF(milter_$1_family, long, MFAM_INET);
		MF_VAR_SET_STRING(milter_$1_address,
				  inet_ntoa(addr->sin.sin_addr));
		break;

#ifdef GACOPYZ_IPV6
	case PF_INET6: {
		char hostbuf[NI_MAXHOST];

		MF_VAR_REF(milter_$1_family, long, MFAM_INET6);
		if (getnameinfo(&addr->sa, sizeof(addr->sin6),
				hostbuf, sizeof hostbuf,
				NULL, 0,
				NI_NUMERICHOST|NI_NUMERICSERV))
			hostbuf[0] = 0;
		MF_VAR_SET_STRING(milter_$1_address, hostbuf);
		break;
	}
#endif
		
	case PF_UNIX:
		MF_VAR_REF(milter_$1_family, long, MFAM_UNIX);
		if (len == sizeof (addr->sa.sa_family))
			path = "";
		else
			path = addr->sunix.sun_path;
		MF_VAR_SET_STRING(milter_$1_address, path);
		break;
		
	default:
		/* FIXME */
		MF_VAR_REF(milter_$1_family, long, (long)addr->sa.sa_family);
	}
}
>])>])
	  
define_milter_address(server)
define_milter_address(client)

MF_DSEXP_SUPPRESS([<set_milter_server_id>],[<
void
set_milter_server_id(eval_environ_t env, const char *id)
{
	MF_VAR_SET_STRING(milter_server_id, id ? id : "");
}
>])

