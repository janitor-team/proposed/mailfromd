/* Debugging API for MFL.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
#include "mflib/_register.h"
#include "srvcfg.h"

MF_DEFUN(debug_level, NUMBER, OPTIONAL, STRING modname)
{
	mu_debug_level_t level;
	char *name = MF_OPTVAL(modname, NULL);
	size_t len = name ? strlen (name) : 0;
	
	MF_ASSERT(mu_debug_category_level(name, len, &level) == 0,
		  mfe_range,
		  _("invalid module name: %s"), name);
	MF_RETURN(level);
}
END

MF_DEFUN(debug_spec, STRING, OPTIONAL, STRING modnames, NUMBER showunset)
{
	mu_stream_t str;
	int rc;
	char *names = MF_OPTVAL(modnames, NULL);
	
	rc = mu_memory_stream_create(&str, MU_STREAM_RDWR);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "cannot create stream: %s", mu_strerror(rc));
	MF_OBSTACK_BEGIN();
	rc = mu_debug_format_spec(str, (names && names[0]) ? names : NULL,
				  MF_OPTVAL(showunset, 0));
	if (rc == 0) {
		mu_off_t size;
		char *s;
		size_t n;
		
		mu_stream_seek(str, 0, MU_SEEK_SET, NULL);
		mu_stream_size(str, &size);
		MF_OBSTACK_GROW(NULL, size + 1, s);
		rc = mu_stream_read(str, s, size, &n);
		if (rc == 0)
			s[n] = 0;
	}
	
	mu_stream_destroy(&str);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s", mu_strerror(rc));
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(debug, VOID, STRING spec)
{
	struct mu_locus_range loc = MU_LOCUS_RANGE_INITIALIZER;

        env_get_locus(env, &loc);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_LOCUS_RANGE, &loc);
	mu_debug_parse_spec(spec);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_LOCUS_RANGE, NULL);
}
END

MF_DEFUN(program_trace, VOID, STRING name)
{
	enable_prog_trace(name);
}
END

MF_DEFUN(cancel_program_trace, VOID, STRING name)
{
	disable_prog_trace(name);
}
END	

MF_DEFUN(stack_trace, VOID)
{
	runtime_stack_trace(env);
}
END

MF_DEFUN(_reg, NUMBER, NUMBER what)
{
	prog_counter_t regval = env_register_read(env, what);
	MF_RETURN(regval);
}
END

MF_DEFUN(_expand_dataseg, VOID, NUMBER words)
{
	MF_ASSERT(expand_dataseg(env, words) == 0,
		  mfe_failure,
		  _("out of stack space; increase #pragma stacksize"));
}
END

MF_DEFUN(_wd, VOID, OPTIONAL, NUMBER seconds)
{
	mu_wd(MF_OPTVAL(seconds));
}
END

MF_DEFUN(callout_transcript, NUMBER, OPTIONAL, NUMBER val)
{
	int oldval = smtp_transcript;
	if (MF_DEFINED(val))
		smtp_transcript = val;
	MF_RETURN(oldval);
}
END

MF_INIT([<
/* Declare mailutils_set_debug_level as an alias to 'debug'.
   I do that manually, because there is no m4 magic for that
   (so far it is the only built-in alias). */
      va_builtin_install_ex("mailutils_set_debug_level",
			    bi_debug, 0, dtype_unspecified, 1, 0, 0,
			    dtype_string);
>])

