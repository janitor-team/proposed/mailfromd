/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
#include "msg.h"

/* number current_header_count([string name]) */
MF_STATE(eoh)
MF_STATE(body)
MF_STATE(eom)
MF_CAPTURE
MF_DEFUN(current_header_count, NUMBER, OPTIONAL, STRING name)
{
	mu_header_t hdr;
	size_t total;
	size_t count;
	int rc;

	rc = env_get_header(env, &hdr);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "env_get_header: %s",
		  mu_strerror(rc));
	
	rc = mu_header_get_field_count(hdr, &total);
	
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_header_get_field_count: %s",
		  mu_strerror(rc));
	if (MF_DEFINED(name)) {
		size_t i;
		count = 0;
		for (i = 1; i <= total; i++) {
			const char *sptr;
			if (mu_header_sget_field_name(hdr, i, &sptr) == 0
			    && strcasecmp(sptr, name) == 0)
				count++;
		}
	} else
		count = total;
	MF_RETURN(count);
}
END

/* string current_header_nth_name(number index) */
MF_STATE(eoh)
MF_STATE(body)
MF_STATE(eom)
MF_CAPTURE
MF_DEFUN(current_header_nth_name, STRING, NUMBER index)
{
	mu_header_t hdr;
	const char *sptr;
	int rc;

	rc = env_get_header(env, &hdr);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "env_get_header: %s",
		  mu_strerror(rc));
	
	rc = mu_header_sget_field_name(hdr, index, &sptr);

	if (rc == MU_ERR_NOENT)
		MF_THROW(mfe_not_found,
			 _("requested item (#%ld) not found"),
			 index);
	else if (rc != 0)
		MF_THROW(mfe_failure,
			 "mu_header_sget_field_name: %s",
			 mu_strerror(rc));
	MF_RETURN(sptr);
}
END

/* string current_header_nth_value(number index) */
MF_STATE(eoh)
MF_STATE(body)
MF_STATE(eom)
MF_CAPTURE
MF_DEFUN(current_header_nth_value, STRING, NUMBER index)
{
	mu_header_t hdr;
	const char *sptr;
	int rc;

	rc = env_get_header(env, &hdr);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "env_get_header: %s",
		  mu_strerror(rc));
	
	rc = mu_header_sget_field_value(hdr, index, &sptr);

	if (rc == MU_ERR_NOENT)
		MF_THROW(mfe_not_found,
			 _("requested item (#%ld) not found"),
			 index);
	else if (rc != 0)
		MF_THROW(mfe_failure,
			 "mu_header_sget_field_name: %s",
			 mu_strerror(rc));
	MF_RETURN(sptr);
}
END

/* string current_header(string name[, number index]) */
MF_STATE(eoh)
MF_STATE(body)	
MF_STATE(eom)
MF_CAPTURE
MF_DEFUN(current_header, STRING, STRING name, OPTIONAL, NUMBER index)
{
	mu_header_t hdr;
	const char *sptr;
	int rc;

	rc = env_get_header(env, &hdr);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "env_get_header: %s",
		  mu_strerror(rc));
	
	rc = mu_header_sget_value_n(hdr, name, MF_OPTVAL(index, 1), &sptr);

	if (rc == MU_ERR_NOENT)
		MF_THROW(mfe_not_found,
			 _("requested item (%s,%ld) not found"),
			 name, MF_OPTVAL(index, 1));
	else if (rc != 0)
		MF_THROW(mfe_failure,
			 "mu_header_sget_field_name: %s",
			 mu_strerror(rc));
	MF_RETURN(sptr);
}
END

