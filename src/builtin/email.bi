/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2010-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
#include "mflib/email.h"

MF_DEFUN(domainpart, STRING, STRING str)
{
	char *p = strchr(str, '@');
	MF_RETURN(p ? p+1 : str);
}
END

MF_DEFUN(localpart, STRING, STRING str)
{
	char *p = strchr(str, '@');

	if (p) {
		MF_OBSTACK_BEGIN();
		MF_OBSTACK_GROW(str, p - str);
		MF_OBSTACK_1GROW(0);
		MF_RETURN_OBSTACK();
	} else
		MF_RETURN(str);
}
END

MF_DEFUN(dequote, STRING, STRING str)
{
	size_t len = strlen(str);
	if (len > 1 && str[0] == '<' && str[len-1] == '>') {
		MF_OBSTACK_BEGIN();
		MF_OBSTACK_GROW(str + 1, len - 2);
		MF_OBSTACK_1GROW(0);
		MF_RETURN_OBSTACK();
	} else
		MF_RETURN(str);
}
END

MF_DEFUN(email_map, NUMBER, STRING str)
{
	mu_address_t addr;
	int f = 0;
	int rc = mu_address_create_hint(&addr, str, NULL, 0);
	if (rc)
		MF_RETURN(0);
	if (addr->next)
		f |= EMAIL_MULTIPLE;
	if (addr->comments)
		f |= EMAIL_COMMENTS;
	if (addr->personal)
		f |= EMAIL_PERSONAL;
	if (addr->local_part)
		f |= EMAIL_LOCAL;
	if (addr->domain)
		f |= EMAIL_DOMAIN;
	if (addr->route)
		f |= EMAIL_ROUTE;
	mu_address_destroy(&addr);
	MF_RETURN(f);
}
END

