#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "ctype.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */


static size_t ctype_mismatch_loc
#line 18 "ctype.bi"
;

#line 32



#line 34
void
#line 34
bi_isalnum(eval_environ_t env)
#line 34

#line 34

#line 34 "ctype.bi"
{
#line 34
	
#line 34

#line 34
        char *  str;
#line 34
        
#line 34

#line 34
        get_string_arg(env, 0, &str);
#line 34
        
#line 34
        adjust_stack(env, 1);
#line 34

#line 34

#line 34
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 34
		prog_trace(env, "isalnum %s",str);;
#line 34

#line 34
{
#line 34
	int i;
#line 34
	for (i = 0; str[i]; i++)
#line 34
		if (!mu_isalnum(str[i])) {
#line 34
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 34
			
#line 34
do {
#line 34
  push(env, (STKVAL)(mft_number)(0));
#line 34
  goto endlab;
#line 34
} while (0);
#line 34
	        }
#line 34
	
#line 34
do {
#line 34
  push(env, (STKVAL)(mft_number)(1));
#line 34
  goto endlab;
#line 34
} while (0);
#line 34
}
#line 34
endlab:
#line 34
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 34
	return;
#line 34
}
#line 34


#line 35
void
#line 35
bi_isalpha(eval_environ_t env)
#line 35

#line 35

#line 35 "ctype.bi"
{
#line 35
	
#line 35

#line 35
        char *  str;
#line 35
        
#line 35

#line 35
        get_string_arg(env, 0, &str);
#line 35
        
#line 35
        adjust_stack(env, 1);
#line 35

#line 35

#line 35
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 35
		prog_trace(env, "isalpha %s",str);;
#line 35

#line 35
{
#line 35
	int i;
#line 35
	for (i = 0; str[i]; i++)
#line 35
		if (!mu_isalpha(str[i])) {
#line 35
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 35
			
#line 35
do {
#line 35
  push(env, (STKVAL)(mft_number)(0));
#line 35
  goto endlab;
#line 35
} while (0);
#line 35
	        }
#line 35
	
#line 35
do {
#line 35
  push(env, (STKVAL)(mft_number)(1));
#line 35
  goto endlab;
#line 35
} while (0);
#line 35
}
#line 35
endlab:
#line 35
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 35
	return;
#line 35
}
#line 35


#line 36
void
#line 36
bi_isascii(eval_environ_t env)
#line 36

#line 36

#line 36 "ctype.bi"
{
#line 36
	
#line 36

#line 36
        char *  str;
#line 36
        
#line 36

#line 36
        get_string_arg(env, 0, &str);
#line 36
        
#line 36
        adjust_stack(env, 1);
#line 36

#line 36

#line 36
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 36
		prog_trace(env, "isascii %s",str);;
#line 36

#line 36
{
#line 36
	int i;
#line 36
	for (i = 0; str[i]; i++)
#line 36
		if (!mu_isascii(str[i])) {
#line 36
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 36
			
#line 36
do {
#line 36
  push(env, (STKVAL)(mft_number)(0));
#line 36
  goto endlab;
#line 36
} while (0);
#line 36
	        }
#line 36
	
#line 36
do {
#line 36
  push(env, (STKVAL)(mft_number)(1));
#line 36
  goto endlab;
#line 36
} while (0);
#line 36
}
#line 36
endlab:
#line 36
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 36
	return;
#line 36
}
#line 36


#line 37
void
#line 37
bi_isblank(eval_environ_t env)
#line 37

#line 37

#line 37 "ctype.bi"
{
#line 37
	
#line 37

#line 37
        char *  str;
#line 37
        
#line 37

#line 37
        get_string_arg(env, 0, &str);
#line 37
        
#line 37
        adjust_stack(env, 1);
#line 37

#line 37

#line 37
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 37
		prog_trace(env, "isblank %s",str);;
#line 37

#line 37
{
#line 37
	int i;
#line 37
	for (i = 0; str[i]; i++)
#line 37
		if (!mu_isblank(str[i])) {
#line 37
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 37
			
#line 37
do {
#line 37
  push(env, (STKVAL)(mft_number)(0));
#line 37
  goto endlab;
#line 37
} while (0);
#line 37
	        }
#line 37
	
#line 37
do {
#line 37
  push(env, (STKVAL)(mft_number)(1));
#line 37
  goto endlab;
#line 37
} while (0);
#line 37
}
#line 37
endlab:
#line 37
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 37
	return;
#line 37
}
#line 37


#line 38
void
#line 38
bi_iscntrl(eval_environ_t env)
#line 38

#line 38

#line 38 "ctype.bi"
{
#line 38
	
#line 38

#line 38
        char *  str;
#line 38
        
#line 38

#line 38
        get_string_arg(env, 0, &str);
#line 38
        
#line 38
        adjust_stack(env, 1);
#line 38

#line 38

#line 38
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 38
		prog_trace(env, "iscntrl %s",str);;
#line 38

#line 38
{
#line 38
	int i;
#line 38
	for (i = 0; str[i]; i++)
#line 38
		if (!mu_iscntrl(str[i])) {
#line 38
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 38
			
#line 38
do {
#line 38
  push(env, (STKVAL)(mft_number)(0));
#line 38
  goto endlab;
#line 38
} while (0);
#line 38
	        }
#line 38
	
#line 38
do {
#line 38
  push(env, (STKVAL)(mft_number)(1));
#line 38
  goto endlab;
#line 38
} while (0);
#line 38
}
#line 38
endlab:
#line 38
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 38
	return;
#line 38
}
#line 38


#line 39
void
#line 39
bi_isdigit(eval_environ_t env)
#line 39

#line 39

#line 39 "ctype.bi"
{
#line 39
	
#line 39

#line 39
        char *  str;
#line 39
        
#line 39

#line 39
        get_string_arg(env, 0, &str);
#line 39
        
#line 39
        adjust_stack(env, 1);
#line 39

#line 39

#line 39
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 39
		prog_trace(env, "isdigit %s",str);;
#line 39

#line 39
{
#line 39
	int i;
#line 39
	for (i = 0; str[i]; i++)
#line 39
		if (!mu_isdigit(str[i])) {
#line 39
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 39
			
#line 39
do {
#line 39
  push(env, (STKVAL)(mft_number)(0));
#line 39
  goto endlab;
#line 39
} while (0);
#line 39
	        }
#line 39
	
#line 39
do {
#line 39
  push(env, (STKVAL)(mft_number)(1));
#line 39
  goto endlab;
#line 39
} while (0);
#line 39
}
#line 39
endlab:
#line 39
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 39
	return;
#line 39
}
#line 39


#line 40
void
#line 40
bi_isgraph(eval_environ_t env)
#line 40

#line 40

#line 40 "ctype.bi"
{
#line 40
	
#line 40

#line 40
        char *  str;
#line 40
        
#line 40

#line 40
        get_string_arg(env, 0, &str);
#line 40
        
#line 40
        adjust_stack(env, 1);
#line 40

#line 40

#line 40
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 40
		prog_trace(env, "isgraph %s",str);;
#line 40

#line 40
{
#line 40
	int i;
#line 40
	for (i = 0; str[i]; i++)
#line 40
		if (!mu_isgraph(str[i])) {
#line 40
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 40
			
#line 40
do {
#line 40
  push(env, (STKVAL)(mft_number)(0));
#line 40
  goto endlab;
#line 40
} while (0);
#line 40
	        }
#line 40
	
#line 40
do {
#line 40
  push(env, (STKVAL)(mft_number)(1));
#line 40
  goto endlab;
#line 40
} while (0);
#line 40
}
#line 40
endlab:
#line 40
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 40
	return;
#line 40
}
#line 40


#line 41
void
#line 41
bi_islower(eval_environ_t env)
#line 41

#line 41

#line 41 "ctype.bi"
{
#line 41
	
#line 41

#line 41
        char *  str;
#line 41
        
#line 41

#line 41
        get_string_arg(env, 0, &str);
#line 41
        
#line 41
        adjust_stack(env, 1);
#line 41

#line 41

#line 41
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 41
		prog_trace(env, "islower %s",str);;
#line 41

#line 41
{
#line 41
	int i;
#line 41
	for (i = 0; str[i]; i++)
#line 41
		if (!mu_islower(str[i])) {
#line 41
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 41
			
#line 41
do {
#line 41
  push(env, (STKVAL)(mft_number)(0));
#line 41
  goto endlab;
#line 41
} while (0);
#line 41
	        }
#line 41
	
#line 41
do {
#line 41
  push(env, (STKVAL)(mft_number)(1));
#line 41
  goto endlab;
#line 41
} while (0);
#line 41
}
#line 41
endlab:
#line 41
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 41
	return;
#line 41
}
#line 41


#line 42
void
#line 42
bi_isprint(eval_environ_t env)
#line 42

#line 42

#line 42 "ctype.bi"
{
#line 42
	
#line 42

#line 42
        char *  str;
#line 42
        
#line 42

#line 42
        get_string_arg(env, 0, &str);
#line 42
        
#line 42
        adjust_stack(env, 1);
#line 42

#line 42

#line 42
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 42
		prog_trace(env, "isprint %s",str);;
#line 42

#line 42
{
#line 42
	int i;
#line 42
	for (i = 0; str[i]; i++)
#line 42
		if (!mu_isprint(str[i])) {
#line 42
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 42
			
#line 42
do {
#line 42
  push(env, (STKVAL)(mft_number)(0));
#line 42
  goto endlab;
#line 42
} while (0);
#line 42
	        }
#line 42
	
#line 42
do {
#line 42
  push(env, (STKVAL)(mft_number)(1));
#line 42
  goto endlab;
#line 42
} while (0);
#line 42
}
#line 42
endlab:
#line 42
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 42
	return;
#line 42
}
#line 42


#line 43
void
#line 43
bi_ispunct(eval_environ_t env)
#line 43

#line 43

#line 43 "ctype.bi"
{
#line 43
	
#line 43

#line 43
        char *  str;
#line 43
        
#line 43

#line 43
        get_string_arg(env, 0, &str);
#line 43
        
#line 43
        adjust_stack(env, 1);
#line 43

#line 43

#line 43
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 43
		prog_trace(env, "ispunct %s",str);;
#line 43

#line 43
{
#line 43
	int i;
#line 43
	for (i = 0; str[i]; i++)
#line 43
		if (!mu_ispunct(str[i])) {
#line 43
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 43
			
#line 43
do {
#line 43
  push(env, (STKVAL)(mft_number)(0));
#line 43
  goto endlab;
#line 43
} while (0);
#line 43
	        }
#line 43
	
#line 43
do {
#line 43
  push(env, (STKVAL)(mft_number)(1));
#line 43
  goto endlab;
#line 43
} while (0);
#line 43
}
#line 43
endlab:
#line 43
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 43
	return;
#line 43
}
#line 43


#line 44
void
#line 44
bi_isspace(eval_environ_t env)
#line 44

#line 44

#line 44 "ctype.bi"
{
#line 44
	
#line 44

#line 44
        char *  str;
#line 44
        
#line 44

#line 44
        get_string_arg(env, 0, &str);
#line 44
        
#line 44
        adjust_stack(env, 1);
#line 44

#line 44

#line 44
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 44
		prog_trace(env, "isspace %s",str);;
#line 44

#line 44
{
#line 44
	int i;
#line 44
	for (i = 0; str[i]; i++)
#line 44
		if (!mu_isspace(str[i])) {
#line 44
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 44
			
#line 44
do {
#line 44
  push(env, (STKVAL)(mft_number)(0));
#line 44
  goto endlab;
#line 44
} while (0);
#line 44
	        }
#line 44
	
#line 44
do {
#line 44
  push(env, (STKVAL)(mft_number)(1));
#line 44
  goto endlab;
#line 44
} while (0);
#line 44
}
#line 44
endlab:
#line 44
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 44
	return;
#line 44
}
#line 44


#line 45
void
#line 45
bi_isupper(eval_environ_t env)
#line 45

#line 45

#line 45 "ctype.bi"
{
#line 45
	
#line 45

#line 45
        char *  str;
#line 45
        
#line 45

#line 45
        get_string_arg(env, 0, &str);
#line 45
        
#line 45
        adjust_stack(env, 1);
#line 45

#line 45

#line 45
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 45
		prog_trace(env, "isupper %s",str);;
#line 45

#line 45
{
#line 45
	int i;
#line 45
	for (i = 0; str[i]; i++)
#line 45
		if (!mu_isupper(str[i])) {
#line 45
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 45
			
#line 45
do {
#line 45
  push(env, (STKVAL)(mft_number)(0));
#line 45
  goto endlab;
#line 45
} while (0);
#line 45
	        }
#line 45
	
#line 45
do {
#line 45
  push(env, (STKVAL)(mft_number)(1));
#line 45
  goto endlab;
#line 45
} while (0);
#line 45
}
#line 45
endlab:
#line 45
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 45
	return;
#line 45
}
#line 45


#line 46
void
#line 46
bi_isxdigit(eval_environ_t env)
#line 46

#line 46

#line 46 "ctype.bi"
{
#line 46
	
#line 46

#line 46
        char *  str;
#line 46
        
#line 46

#line 46
        get_string_arg(env, 0, &str);
#line 46
        
#line 46
        adjust_stack(env, 1);
#line 46

#line 46

#line 46
	if (builtin_module_trace(BUILTIN_IDX_ctype))
#line 46
		prog_trace(env, "isxdigit %s",str);;
#line 46

#line 46
{
#line 46
	int i;
#line 46
	for (i = 0; str[i]; i++)
#line 46
		if (!mu_isxdigit(str[i])) {
#line 46
			mf_c_val(*env_data_ref(env, ctype_mismatch_loc),int) = (i);
#line 46
			
#line 46
do {
#line 46
  push(env, (STKVAL)(mft_number)(0));
#line 46
  goto endlab;
#line 46
} while (0);
#line 46
	        }
#line 46
	
#line 46
do {
#line 46
  push(env, (STKVAL)(mft_number)(1));
#line 46
  goto endlab;
#line 46
} while (0);
#line 46
}
#line 46
endlab:
#line 46
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 46
	return;
#line 46
}
#line 46


#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
ctype_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 18 "ctype.bi"
	builtin_variable_install("ctype_mismatch", dtype_number, SYM_VOLATILE, &ctype_mismatch_loc);
#line 34 "ctype.bi"
va_builtin_install_ex("isalnum", bi_isalnum, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 35 "ctype.bi"
va_builtin_install_ex("isalpha", bi_isalpha, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 36 "ctype.bi"
va_builtin_install_ex("isascii", bi_isascii, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 37 "ctype.bi"
va_builtin_install_ex("isblank", bi_isblank, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 38 "ctype.bi"
va_builtin_install_ex("iscntrl", bi_iscntrl, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 39 "ctype.bi"
va_builtin_install_ex("isdigit", bi_isdigit, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 40 "ctype.bi"
va_builtin_install_ex("isgraph", bi_isgraph, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 41 "ctype.bi"
va_builtin_install_ex("islower", bi_islower, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 42 "ctype.bi"
va_builtin_install_ex("isprint", bi_isprint, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 43 "ctype.bi"
va_builtin_install_ex("ispunct", bi_ispunct, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 44 "ctype.bi"
va_builtin_install_ex("isspace", bi_isspace, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 45 "ctype.bi"
va_builtin_install_ex("isupper", bi_isupper, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 46 "ctype.bi"
va_builtin_install_ex("isxdigit", bi_isxdigit, 0, dtype_number, 1, 0, 0|0, dtype_string);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

