#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "callout.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/* Run-time support for callout functions */

#include "filenames.h"
#include "callout.h"

static size_t ehlo_domain_loc
#line 22 "callout.bi"
;
static size_t mailfrom_address_loc
#line 23 "callout.bi"
;

int provide_callout;

/* #pragma provide-callout */
#line 28 "callout.bi"

#line 28
static void _pragma_provide_callout (int argc, char **argv, const char *text)
#line 28

{
	provide_callout = 1;
}

void
#line 33
bi_default_callout_server_url(eval_environ_t env)
#line 33

#line 33

#line 33 "callout.bi"
{
#line 33
	
#line 33

#line 33
        
#line 33

#line 33
        
#line 33
        adjust_stack(env, 0);
#line 33

#line 33

#line 33
	if (builtin_module_trace(BUILTIN_IDX_callout))
#line 33
		prog_trace(env, "default_callout_server_url");;
#line 33

{
	
#line 35
do {
#line 35
  pushs(env, callout_server_url ?
#line 35
		  callout_server_url : DEFAULT_CALLOUT_SOCKET);
#line 35
  goto endlab;
#line 35
} while (0);
#line 37
}
endlab:
#line 38
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 38
	return;
#line 38
}

 
#line 46
	


#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
callout_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 22 "callout.bi"
	builtin_variable_install("ehlo_domain", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &ehlo_domain_loc);
#line 23 "callout.bi"
	builtin_variable_install("mailfrom_address", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &mailfrom_address_loc);
#line 28 "callout.bi"

#line 28
install_pragma("provide-callout", 1, 1, _pragma_provide_callout);
#line 33 "callout.bi"
va_builtin_install_ex("default_callout_server_url", bi_default_callout_server_url, 0, dtype_string, 0, 0, 0|0, dtype_unspecified);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
	 if (ehlo_domain)
#line 994
		 ds_init_variable("ehlo_domain", ehlo_domain);
#line 994
	 if (mailfrom_address)
#line 994
		 ds_init_variable("mailfrom_address", mailfrom_address);
#line 994

#line 994
}
#line 994 "../../src/builtin/snarf.m4"

