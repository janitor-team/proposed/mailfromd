/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2009-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

static void
return_passwd(eval_environ_t env, struct passwd *pw)
{
	char buf[NUMERIC_BUFSIZE_BOUND];
	
	MF_OBSTACK_GROW(pw->pw_name);
	MF_OBSTACK_1GROW(':');
	MF_OBSTACK_GROW(pw->pw_passwd);
	MF_OBSTACK_1GROW(':');
	snprintf(buf, sizeof(buf), "%lu", (unsigned long) pw->pw_uid);
	MF_OBSTACK_GROW(buf);
	MF_OBSTACK_1GROW(':');
	snprintf(buf, sizeof(buf), "%lu", (unsigned long) pw->pw_gid);
	MF_OBSTACK_GROW(buf);
	MF_OBSTACK_1GROW(':');
	MF_OBSTACK_GROW(pw->pw_gecos);
	MF_OBSTACK_1GROW(':');
	MF_OBSTACK_GROW(pw->pw_dir);
	MF_OBSTACK_1GROW(':');
	MF_OBSTACK_GROW(pw->pw_shell);
	MF_OBSTACK_1GROW(0);
}

MF_DEFUN(mappwnam, NUMBER, STRING name)
{
	struct passwd *pw = getpwnam(name);
	MF_RETURN(pw != NULL);
}
END

MF_DEFUN(getpwnam, STRING, STRING name)
{
	struct passwd *pw = getpwnam(name);
	MF_ASSERT(pw != NULL,
		  mfe_not_found,
		  _("%s: user not found"), name);
	MF_OBSTACK_BEGIN();
	return_passwd(env, pw);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(mappwuid, NUMBER, NUMBER uid)
{
	struct passwd *pw = getpwuid(uid);
	MF_RETURN(pw != NULL);
}
END

MF_DEFUN(getpwuid, STRING, NUMBER uid)
{
	struct passwd *pw = getpwuid(uid);
	MF_ASSERT(pw != NULL,
		  mfe_not_found,
		  _("%lu: uid not found"), (unsigned long) uid);
	MF_OBSTACK_BEGIN();
	return_passwd(env, pw);
	MF_RETURN_OBSTACK();
}
END

