/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#define FMT_ALTPOS         0x01 
#define FMT_ALTERNATE      0x02
#define FMT_PADZERO        0x04
#define FMT_ADJUST_LEFT    0x08
#define FMT_SPACEPFX       0x10
#define FMT_SIGNPFX        0x20

typedef enum {
	fmts_copy,      /* Copy char as is */
	fmts_pos,       /* Expect argument position -- %_2$ */
	fmts_flags,     /* Expect flags -- %2$_# */
	fmts_width,     /* Expect width -- %2$#_8 or %2$#_* */
	fmts_width_arg, /* Expect width argument position -- %2$#*_1$ */
	fmts_prec,      /* Expect precision */ 
	fmts_prec_arg,  /* Expect precision argument position */
	fmts_conv       /* Expect conversion specifier */
} printf_format_state;

static int
get_num(const char *p, int i, unsigned *pn)
{
	unsigned n = 0;

	for (; p[i] && mu_isdigit(p[i]); i++) 
		n = n * 10 + p[i] - '0';
	*pn = n;
	return i;
}

#define __MF_MAX(a,b) ((a)>(b) ? (a) : (b))
#define SPRINTF_BUF_SIZE (__MF_MAX(3*sizeof(long), NUMERIC_BUFSIZE_BOUND) + 2)

MF_DEFUN_VARARGS_NO_PROM(sprintf, STRING, STRING format)
{
	int i = 0;
	int cur = 0;
	int start;
	char buf[SPRINTF_BUF_SIZE];
	printf_format_state state = fmts_copy;
	int flags = 0;
	unsigned width = 0;
	unsigned prec = 0;
	unsigned argnum;

	MF_OBSTACK_BEGIN();
	MF_VA_START();
	while (format[cur]) {
		unsigned n;
		char *str;
		long num;
		int negative;
		char fmtbuf[] = { '%', 'x', 0 };
		
		switch (state) {
		case fmts_copy:
			/* Expect `%', and copy all the rest verbatim */
			if (format[cur] == '%') {
				start = cur;
				state = fmts_pos;
				flags = 0;
				width = 0;
				prec = 0;
			} else
				MF_OBSTACK_1GROW(format[cur]);
			cur++;
			break;
				
		case fmts_pos:
			/* Expect '%' or an argument position -- %_% or %_2$ */
			if (format[cur] == '%') {
				MF_OBSTACK_1GROW('%');
				cur++;
				state = fmts_copy;
				break;
			}
			if (mu_isdigit(format[cur])) {
				int pos = get_num(format, cur, &n);
				if (format[pos] == '$') {
					argnum = n - 1;
					flags |= FMT_ALTPOS;
					cur = pos + 1;
				}
			}
			state = fmts_flags;    
			break;    
				
		case fmts_flags:
			/* Expect flags -- %2$_# */
			switch (format[cur]) {
			case '#':
				flags |= FMT_ALTERNATE;
				cur++;
				break;
				
			case '0':
				flags |= FMT_PADZERO;
				cur++;
				break;
				
			case '-':
				flags |= FMT_ADJUST_LEFT;
				cur++;
				break;
				
			case ' ':
				flags |= FMT_SPACEPFX;
				cur++;
				break;
				
			case '+':
				flags |= FMT_SIGNPFX;
				cur++;
				break;

			default:
				state = fmts_width;
			}
			break;
			
		case fmts_width:
			/* Expect width -- %2$#_8 or %2$#_* */
			if (mu_isdigit(format[cur])) {
				cur = get_num(format, cur, &width);
				state = fmts_prec;
			} else if (format[cur] == '*') {
				cur++;
				state = fmts_width_arg;
			} else
				state = fmts_prec;
			break;
			
		case fmts_width_arg:
			/* Expect width argument position -- %2$#*_1$ */
			state = fmts_prec;
			if (mu_isdigit(format[cur])) {
				int pos = get_num(format, cur, &n);
				if (format[pos] == '$') {
					MF_VA_ARG(n-1, NUMBER, num);
					cur = pos + 1;
					if (num < 0) {
						flags |= FMT_SPACEPFX;
						num = - num;
					}
					width = (unsigned) num;
					break;
				}
			}
			MF_VA_ARG(i, NUMBER, num);
			i++;
			if (num < 0) {
				/* A negative field width is taken
				   as a `-' flag followed by a positive field width. */
				flags |= FMT_SPACEPFX;
				num = - num;
			}
			width = (unsigned) num;
			break;
			
		case fmts_prec:
			/* Expect precision -- %2$#*1$_. */
			state = fmts_conv;
			if (format[cur] == '.') {
				cur++;
				if (mu_isdigit(format[cur])) {
					cur = get_num(format, cur, &prec);
				} else if (format[cur] == '*') {
					cur++;
					state = fmts_prec_arg;
				}
			} 
			break;
			
		case fmts_prec_arg:
                        /* Expect precision argument position --
			                %2$#*1$.*_3$ */
			state = fmts_conv;
			if (mu_isdigit(format[cur])) {
				int pos = get_num(format, cur, &n);
				if (format[pos] == '$') {
					MF_VA_ARG(n-1, NUMBER, num);
					if (num > 0)
						prec = (unsigned) num;
					cur = pos + 1;
					break;
				}
			}
			MF_VA_ARG(i, NUMBER, num);
			i++;
			if (num > 0)
				prec = (unsigned) num;
			break;
			
		case fmts_conv:       /* Expect conversion specifier */
			if (!(flags & FMT_ALTPOS))
				argnum = i++;
			switch (format[cur]) {
			case 's':
				MF_VA_ARG(argnum, STRING, str);
				n = strlen(str);
				if (prec && prec < n)
					n = prec;
				if (width) {
					char *q, *s;
					if (n > width)
						width = n;
					q = s = MF_ALLOC_HEAP_TEMP(width + 1);
					q[width] = 0;
					memset(q, ' ', width);
					if (!(flags & FMT_ADJUST_LEFT)
					    && n < width) {
						s = q + width - n;
					} 
					memcpy(s, str, n);
					str = q;
					n = width;
				}
				MF_OBSTACK_GROW(str, n);
				break;

			case 'i':
			case 'd':
				MF_VA_ARG(argnum, NUMBER, num);
				if (num < 0) {
					negative = 1;
					num = - num;
				} else
					negative = 0;
				/* If a precision is given with a
				   numeric conversion, the 0 flag is ignored.
				 */
				/* A - overrides a 0 if both are given. */
				if (prec || (flags & FMT_ADJUST_LEFT))
					flags &= ~FMT_PADZERO;
				snprintf(buf+1, sizeof(buf)-1, "%ld", num);
				str = buf + 1;
				n = strlen(str);
				if (prec && prec > n) {
					memmove(str + prec - n, str, n + 1);
					memset(str, '0', prec - n);
				}
				
				if (flags & FMT_SIGNPFX) {
					buf[0] = negative ? '-' : '+';
					str = buf;
				} else if (flags & FMT_SPACEPFX) {
					buf[0] = negative ? '-' : ' ';
					str = buf;
				} else if (negative) {
					buf[0] = '-';
					str = buf;
				} else
					str = buf + 1;
				n = strlen(str);

				if (width && width > n) {
					char *q;
					MF_OBSTACK_GROW(NULL, width, q);
					memset(q,
					       (flags & FMT_PADZERO) ?
					         '0' : ' ',
					       width);
					if (flags & FMT_ADJUST_LEFT) 
						memcpy(q, str, n);
					else {
						if ((flags & FMT_PADZERO) &&
						    str == buf) {
							q[0] = *str++;
							n--;
						}
						memcpy(q + width - n, str, n);
					}
				} else
					MF_OBSTACK_GROW(str, n);
				break;

			case 'u':
				MF_VA_ARG(argnum, NUMBER, num);
				/* If a precision is given with a
				   numeric conversion, the 0 flag is ignored.
				*/
				/* A - overrides a 0 if both are given.*/
				if (prec || (flags & FMT_ADJUST_LEFT))
					flags &= ~FMT_PADZERO;
				snprintf(buf, sizeof(buf), "%lu", num);
				str = buf;
				n = strlen(str);
				if (prec && prec > n) {
					memmove(str + prec - n, str, n + 1);
					memset(str, '0', prec - n);
					n = prec;
				}
				
				if (width && width > n) {
					char *q;
					MF_OBSTACK_GROW(NULL, width, q);
					memset(q,
					       (flags & FMT_PADZERO) ?
					         '0' : ' ',
					       width);
					if (flags & FMT_ADJUST_LEFT) 
						memcpy(q, str, n);
					else 
						memcpy(q + width - n, str, n);
				} else
					MF_OBSTACK_GROW(str, n);
				break;

			case 'x':
			case 'X':
				MF_VA_ARG(argnum, NUMBER, num);
				/* If a precision is given with a
				   numeric conversion, the 0 flag is ignored.
				*/
				/* A - overrides a 0 if both are given.*/
				if (prec || (flags & FMT_ADJUST_LEFT))
					flags &= ~FMT_PADZERO;
				fmtbuf[1] = format[cur];
				snprintf(buf+2, sizeof(buf)-2, fmtbuf, num);
				str = buf + 2;
				n = strlen(str);
				if (prec && prec > n) {
					memmove(str + prec - n, str, n + 1);
					memset(str, '0', prec - n);
					n = prec;
				}

				if (flags & FMT_ALTERNATE) {
					*--str = format[cur];
					*--str = '0';
					n += 2;
				}
				
				if (width && width > n) {
					char *q;
					MF_OBSTACK_GROW(NULL, width, q);
					memset(q,
					       (flags & FMT_PADZERO) ?
					         '0' : ' ',
					       width);
					if (flags & FMT_ADJUST_LEFT) 
						memcpy(q, str, n);
					else {
						if (flags & FMT_ALTERNATE
						    && flags & FMT_PADZERO) {
							q[0] = *str++;
							q[1] = *str++;
							n -= 2;
						}
						memcpy(q + width - n, str, n);
					}
				} else
					MF_OBSTACK_GROW(str, n);
				break;
				
			case 'o':
				MF_VA_ARG(argnum, NUMBER, num);
				/* If a precision is given with a
				   numeric conversion, the 0 flag is ignored.
				*/
				/* A - overrides a 0 if both are given.*/
				if (prec || (flags & FMT_ADJUST_LEFT))
					flags &= ~FMT_PADZERO;
				snprintf(buf+1, sizeof(buf)-1, "%lo", num);
				str = buf + 2;
				n = strlen(str);
				if (prec && prec > n) {
					memmove(str + prec - n, str, n + 1);
					memset(str, '0', prec - n);
				}

				if ((flags & FMT_ALTERNATE) && *str != '0') {
					*--str = '0';
					n++;
				}
				
				if (width && width > n) {
					char *q;
					MF_OBSTACK_GROW(NULL, width, q);
					memset(q,
					       (flags & FMT_PADZERO) ?
					         '0' : ' ',
					       width);
					if (flags & FMT_ADJUST_LEFT) 
						memcpy(q, str, n);
					else 
						memcpy(q + width - n, str, n);
				} else
					MF_OBSTACK_GROW(str, n);
				break;
				
			default:
				MF_OBSTACK_GROW(&format[start],
						cur - start + 1);
			}
			
			cur++;
			state = fmts_copy;
		}
	}
	MF_OBSTACK_1GROW(0);
	MF_VA_END();
	MF_RETURN_OBSTACK();
}
END

