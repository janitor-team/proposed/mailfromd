/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
MF_VAR(ctype_mismatch, NUMBER);

m4_define([<MF_DEFUN_CTYPE>],[<
MF_DEFUN($1, NUMBER, STRING str)
{
	int i;
	for (i = 0; str[i]; i++)
		if (!mu_$1(str[i])) {
			MF_VAR_REF(ctype_mismatch, int, i);
			MF_RETURN(0);
	        }
	MF_RETURN(1);
}
END
>])

MF_DEFUN_CTYPE(isalnum)
MF_DEFUN_CTYPE(isalpha)
MF_DEFUN_CTYPE(isascii)
MF_DEFUN_CTYPE(isblank)
MF_DEFUN_CTYPE(iscntrl)
MF_DEFUN_CTYPE(isdigit)
MF_DEFUN_CTYPE(isgraph)
MF_DEFUN_CTYPE(islower)
MF_DEFUN_CTYPE(isprint)
MF_DEFUN_CTYPE(ispunct)
MF_DEFUN_CTYPE(isspace)
MF_DEFUN_CTYPE(isupper)
MF_DEFUN_CTYPE(isxdigit)

