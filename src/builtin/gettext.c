#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "gettext.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



void
#line 19
bi_bindtextdomain(eval_environ_t env)
#line 19

#line 19

#line 19 "gettext.bi"
{
#line 19
	
#line 19

#line 19
        char * MFL_DATASEG domain;
#line 19
        char * MFL_DATASEG dirname;
#line 19
        
#line 19

#line 19
        get_string_arg(env, 0, &domain);
#line 19
        get_string_arg(env, 1, &dirname);
#line 19
        
#line 19
        adjust_stack(env, 2);
#line 19

#line 19

#line 19
	if (builtin_module_trace(BUILTIN_IDX_gettext))
#line 19
		prog_trace(env, "bindtextdomain %s %s",domain, dirname);;
#line 19

{
	const char *s = bindtextdomain(domain[0] ? domain : NULL, dirname);
		if (!(s != NULL))
#line 22
		(
#line 22
	env_throw_bi(env, mfe_failure, "bindtextdomain", "bindtextdomain failed: %s",mu_strerror(errno))
#line 22
)
#line 24
;
	
#line 25
do {
#line 25
  pushs(env, s);
#line 25
  goto endlab;
#line 25
} while (0);
}
endlab:
#line 27
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 27
	return;
#line 27
}

void
#line 29
bi_dgettext(eval_environ_t env)
#line 29

#line 29

#line 29 "gettext.bi"
{
#line 29
	
#line 29

#line 29
        char * MFL_DATASEG domain;
#line 29
        char * MFL_DATASEG msgid;
#line 29
        
#line 29

#line 29
        get_string_arg(env, 0, &domain);
#line 29
        get_string_arg(env, 1, &msgid);
#line 29
        
#line 29
        adjust_stack(env, 2);
#line 29

#line 29

#line 29
	if (builtin_module_trace(BUILTIN_IDX_gettext))
#line 29
		prog_trace(env, "dgettext %s %s",domain, msgid);;
#line 29

{
	
#line 31
do {
#line 31
  pushs(env, dgettext(domain, msgid));
#line 31
  goto endlab;
#line 31
} while (0);
}
endlab:
#line 33
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 33
	return;
#line 33
}

void
#line 35
bi_dngettext(eval_environ_t env)
#line 35

#line 35

#line 35 "gettext.bi"
{
#line 35
	
#line 35

#line 35
        char * MFL_DATASEG domain;
#line 35
        char * MFL_DATASEG msgid;
#line 35
        char * MFL_DATASEG msgid_plural;
#line 35
        long  n;
#line 35
        
#line 35

#line 35
        get_string_arg(env, 0, &domain);
#line 35
        get_string_arg(env, 1, &msgid);
#line 35
        get_string_arg(env, 2, &msgid_plural);
#line 35
        get_numeric_arg(env, 3, &n);
#line 35
        
#line 35
        adjust_stack(env, 4);
#line 35

#line 35

#line 35
	if (builtin_module_trace(BUILTIN_IDX_gettext))
#line 35
		prog_trace(env, "dngettext %s %s %s %lu",domain, msgid, msgid_plural, n);;

{
	
#line 38
do {
#line 38
  pushs(env, dngettext(domain, msgid, msgid_plural, n));
#line 38
  goto endlab;
#line 38
} while (0);
}
endlab:
#line 40
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 40
	return;
#line 40
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
gettext_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 19 "gettext.bi"
va_builtin_install_ex("bindtextdomain", bi_bindtextdomain, 0, dtype_string, 2, 0, 0|0, dtype_string, dtype_string);
#line 29 "gettext.bi"
va_builtin_install_ex("dgettext", bi_dgettext, 0, dtype_string, 2, 0, 0|0, dtype_string, dtype_string);
#line 35 "gettext.bi"
va_builtin_install_ex("dngettext", bi_dngettext, 0, dtype_string, 4, 0, 0|0, dtype_string, dtype_string, dtype_string, dtype_number);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

