/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2015-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

MF_DEFUN(set_from, VOID, STRING addr, OPTIONAL, STRING args)
{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s %s",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(set_from),
	      addr);
	env_msgmod_append(env, set_from, addr, MF_OPTVAL(args, NULL), 0);
}
END

