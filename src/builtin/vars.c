#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "vars.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include "filenames.h" /* For DEFAULT_FROM_ADDRESS */
static size_t rcpt_count_loc
#line 20 "vars.bi"
;
static size_t milter_client_family_loc
#line 21 "vars.bi"
;
static size_t milter_client_address_loc
#line 22 "vars.bi"
;
static size_t milter_server_family_loc
#line 23 "vars.bi"
;
static size_t milter_server_address_loc
#line 24 "vars.bi"
;
static size_t milter_server_id_loc
#line 25 "vars.bi"
;

/* Functions to access %rcpt_count */
unsigned long
get_rcpt_count(eval_environ_t env)
{
 	return mf_c_val(*env_data_ref(env, rcpt_count_loc),long) ;
}

void
clear_rcpt_count(eval_environ_t env)
{
	mf_c_val(*env_data_ref(env, rcpt_count_loc),long) = (0);
}

void
incr_rcpt_count(eval_environ_t env)
{
	env_var_inc(env, rcpt_count_loc);
}

/* define_milter_address name */
#line 91

	  

#line 93

#line 93
void
#line 93
set_milter_server_address(eval_environ_t env, milter_sockaddr_t *addr,
#line 93
		      socklen_t len)
#line 93
{
#line 93
	char *path;
#line 93
	
#line 93
	switch (addr->sa.sa_family) {
#line 93
	case PF_INET:
#line 93
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = (MFAM_INET);
#line 93
		
#line 93
{ size_t __off;
#line 93
  const char *__s = inet_ntoa(addr->sin.sin_addr);
#line 93
  if (__s)
#line 93
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 93
  else
#line 93
     __off = 0;
#line 93
  mf_c_val(*env_data_ref(env, milter_server_address_loc),size) = (__off); }
#line 93
;
#line 93
		break;
#line 93

#line 93
#ifdef GACOPYZ_IPV6
#line 93
	case PF_INET6: {
#line 93
		char hostbuf[NI_MAXHOST];
#line 93

#line 93
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = (MFAM_INET6);
#line 93
		if (getnameinfo(&addr->sa, sizeof(addr->sin6),
#line 93
				hostbuf, sizeof hostbuf,
#line 93
				NULL, 0,
#line 93
				NI_NUMERICHOST|NI_NUMERICSERV))
#line 93
			hostbuf[0] = 0;
#line 93
		
#line 93
{ size_t __off;
#line 93
  const char *__s = hostbuf;
#line 93
  if (__s)
#line 93
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 93
  else
#line 93
     __off = 0;
#line 93
  mf_c_val(*env_data_ref(env, milter_server_address_loc),size) = (__off); }
#line 93
;
#line 93
		break;
#line 93
	}
#line 93
#endif
#line 93
		
#line 93
	case PF_UNIX:
#line 93
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = (MFAM_UNIX);
#line 93
		if (len == sizeof (addr->sa.sa_family))
#line 93
			path = "";
#line 93
		else
#line 93
			path = addr->sunix.sun_path;
#line 93
		
#line 93
{ size_t __off;
#line 93
  const char *__s = path;
#line 93
  if (__s)
#line 93
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 93
  else
#line 93
     __off = 0;
#line 93
  mf_c_val(*env_data_ref(env, milter_server_address_loc),size) = (__off); }
#line 93
;
#line 93
		break;
#line 93
		
#line 93
	default:
#line 93
		/* FIXME */
#line 93
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = ((long)addr->sa.sa_family);
#line 93
	}
#line 93
}
#line 93

#line 93

#line 93

#line 93


#line 94

#line 94
void
#line 94
set_milter_client_address(eval_environ_t env, milter_sockaddr_t *addr,
#line 94
		      socklen_t len)
#line 94
{
#line 94
	char *path;
#line 94
	
#line 94
	switch (addr->sa.sa_family) {
#line 94
	case PF_INET:
#line 94
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = (MFAM_INET);
#line 94
		
#line 94
{ size_t __off;
#line 94
  const char *__s = inet_ntoa(addr->sin.sin_addr);
#line 94
  if (__s)
#line 94
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 94
  else
#line 94
     __off = 0;
#line 94
  mf_c_val(*env_data_ref(env, milter_client_address_loc),size) = (__off); }
#line 94
;
#line 94
		break;
#line 94

#line 94
#ifdef GACOPYZ_IPV6
#line 94
	case PF_INET6: {
#line 94
		char hostbuf[NI_MAXHOST];
#line 94

#line 94
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = (MFAM_INET6);
#line 94
		if (getnameinfo(&addr->sa, sizeof(addr->sin6),
#line 94
				hostbuf, sizeof hostbuf,
#line 94
				NULL, 0,
#line 94
				NI_NUMERICHOST|NI_NUMERICSERV))
#line 94
			hostbuf[0] = 0;
#line 94
		
#line 94
{ size_t __off;
#line 94
  const char *__s = hostbuf;
#line 94
  if (__s)
#line 94
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 94
  else
#line 94
     __off = 0;
#line 94
  mf_c_val(*env_data_ref(env, milter_client_address_loc),size) = (__off); }
#line 94
;
#line 94
		break;
#line 94
	}
#line 94
#endif
#line 94
		
#line 94
	case PF_UNIX:
#line 94
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = (MFAM_UNIX);
#line 94
		if (len == sizeof (addr->sa.sa_family))
#line 94
			path = "";
#line 94
		else
#line 94
			path = addr->sunix.sun_path;
#line 94
		
#line 94
{ size_t __off;
#line 94
  const char *__s = path;
#line 94
  if (__s)
#line 94
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 94
  else
#line 94
     __off = 0;
#line 94
  mf_c_val(*env_data_ref(env, milter_client_address_loc),size) = (__off); }
#line 94
;
#line 94
		break;
#line 94
		
#line 94
	default:
#line 94
		/* FIXME */
#line 94
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = ((long)addr->sa.sa_family);
#line 94
	}
#line 94
}
#line 94

#line 94

#line 94

#line 94



#line 96

#line 96
void
#line 96
set_milter_server_id(eval_environ_t env, const char *id)
#line 96
{
#line 96
	
#line 96
{ size_t __off;
#line 96
  const char *__s = id ? id : "";
#line 96
  if (__s)
#line 96
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 96
  else
#line 96
     __off = 0;
#line 96
  mf_c_val(*env_data_ref(env, milter_server_id_loc),size) = (__off); }
#line 96
;
#line 96
}
#line 96

#line 96

#line 96

#line 102


#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
vars_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 20 "vars.bi"
	builtin_variable_install("rcpt_count", dtype_number, SYM_VOLATILE, &rcpt_count_loc);
#line 21 "vars.bi"
	builtin_variable_install("milter_client_family", dtype_number, SYM_VOLATILE|SYM_PRECIOUS, &milter_client_family_loc);
#line 22 "vars.bi"
	builtin_variable_install("milter_client_address", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &milter_client_address_loc);
#line 23 "vars.bi"
	builtin_variable_install("milter_server_family", dtype_number, SYM_VOLATILE|SYM_PRECIOUS, &milter_server_family_loc);
#line 24 "vars.bi"
	builtin_variable_install("milter_server_address", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &milter_server_address_loc);
#line 25 "vars.bi"
	builtin_variable_install("milter_server_id", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &milter_server_id_loc);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

