/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

/* #pragma miltermacros <handler> <name> [name...] */
MF_PRAGMA(miltermacros, 3, 0)
{
	enum smtp_state state;
	int i = 1;
	
	state = string_to_state(argv[i]);
	if (state == smtp_state_none) {
		parse_error(_("unknown smtp state tag: %s"), argv[i]);
		return;
	}
	for (i++; i < argc; i++)
		register_macro(state, argv[i]);
}

MF_DEFUN(getmacro, STRING, STRING name)
{
	const char *s = env_get_macro(env, name);
	MF_ASSERT(s,
		  mfe_macroundef, _("macro not defined: %s"), 
		  name);
	MF_RETURN(s);
}
END

MF_DEFUN(macro_defined, NUMBER, STRING name)
{
	int ret = !!env_get_macro(env, name);
	MF_RETURN(ret);
}
END

