#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "string.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



void
#line 19
bi_toupper(eval_environ_t env)
#line 19

#line 19

#line 19 "string.bi"
{
#line 19
	
#line 19

#line 19
        char * MFL_DATASEG string;
#line 19
        
#line 19

#line 19
        get_string_arg(env, 0, &string);
#line 19
        
#line 19
        adjust_stack(env, 1);
#line 19

#line 19

#line 19
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 19
		prog_trace(env, "toupper %s",string);;
#line 19

{
	size_t off;
	char *s = strcpy((char*)env_data_ref(env, off = heap_reserve(env, strlen(string) + 1)), string);
	char *p;
	for (p = s; *p; p++)
		*p = toupper(*p);
	
#line 26
do {
#line 26
  push(env, (STKVAL) (mft_size) (off));
#line 26
  goto endlab;
#line 26
} while (0);
}
endlab:
#line 28
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 28
	return;
#line 28
}

void
#line 30
bi_tolower(eval_environ_t env)
#line 30

#line 30

#line 30 "string.bi"
{
#line 30
	
#line 30

#line 30
        char * MFL_DATASEG string;
#line 30
        
#line 30

#line 30
        get_string_arg(env, 0, &string);
#line 30
        
#line 30
        adjust_stack(env, 1);
#line 30

#line 30

#line 30
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 30
		prog_trace(env, "tolower %s",string);;
#line 30

{
	size_t off;
	char *s = strcpy((char*)env_data_ref(env, off = heap_reserve(env, strlen(string) + 1)), string);
	char *p;
	for (p = s; *p; p++)
		*p = tolower(*p);
	
#line 37
do {
#line 37
  push(env, (STKVAL) (mft_size) (off));
#line 37
  goto endlab;
#line 37
} while (0);
}
endlab:
#line 39
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 39
	return;
#line 39
}

void
#line 41
bi_length(eval_environ_t env)
#line 41

#line 41

#line 41 "string.bi"
{
#line 41
	
#line 41

#line 41
        char *  string;
#line 41
        
#line 41

#line 41
        get_string_arg(env, 0, &string);
#line 41
        
#line 41
        adjust_stack(env, 1);
#line 41

#line 41

#line 41
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 41
		prog_trace(env, "length %s",string);;
#line 41

{
	
#line 43
do {
#line 43
  push(env, (STKVAL)(mft_number)(strlen(string)));
#line 43
  goto endlab;
#line 43
} while (0);
}
endlab:
#line 45
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 45
	return;
#line 45
}

void
#line 47
bi_substring(eval_environ_t env)
#line 47

#line 47

#line 47 "string.bi"
{
#line 47
	
#line 47

#line 47
        char * MFL_DATASEG string;
#line 47
        long  start;
#line 47
        long  end;
#line 47
        
#line 47

#line 47
        get_string_arg(env, 0, &string);
#line 47
        get_numeric_arg(env, 1, &start);
#line 47
        get_numeric_arg(env, 2, &end);
#line 47
        
#line 47
        adjust_stack(env, 3);
#line 47

#line 47

#line 47
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 47
		prog_trace(env, "substring %s %lu %lu",string, start, end);;
#line 47

{
	long len = strlen(string);
	
	if (end < 0)
		end = len + end;
	if (end < start) {
		long t = end;
		end = start;
		start = t;
	}

		if (!(start < len && end < len))
#line 59
		(
#line 59
	env_throw_bi(env, mfe_range, "substring", _("argument out of range"))
#line 59
)
;
	
	len = end - start + 1;
	heap_obstack_begin(env);
	heap_obstack_grow(env, string + start, len);
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 66
do {
#line 66
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 66
  goto endlab;
#line 66
} while (0);
}
endlab:
#line 68
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 68
	return;
#line 68
}

void
#line 70
bi_substr(eval_environ_t env)
#line 70

#line 70

#line 70 "string.bi"
{
#line 70
	
#line 70

#line 70
        char * MFL_DATASEG string;
#line 70
        long  start;
#line 70
        long  nbytes;
#line 70
        
#line 70
        long __bi_argcnt;
#line 70
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 70
        get_string_arg(env, 1, &string);
#line 70
        get_numeric_arg(env, 2, &start);
#line 70
        if (__bi_argcnt > 2)
#line 70
                get_numeric_arg(env, 3, &nbytes);
#line 70
        
#line 70
        adjust_stack(env, __bi_argcnt + 1);
#line 70

#line 70

#line 70
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 70
		prog_trace(env, "substr %s %lu %lu",string, start, ((__bi_argcnt > 2) ? nbytes : 0));;
#line 70

{
	long len = strlen(string);
	
		if (!(start >= 0))
#line 74
		(
#line 74
	env_throw_bi(env, mfe_range, "substr", _("argument out of range: start=%ld"),start)
#line 74
)
;
	if (!(__bi_argcnt > 2))
		nbytes = len - start;
		if (!(nbytes >= 0))
#line 78
		(
#line 78
	env_throw_bi(env, mfe_range, "substr", _("argument out of range: start=%ld, len=%ld"),start,len)
#line 78
)
;

	heap_obstack_begin(env);
	heap_obstack_grow(env, string + start, nbytes);
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 84
do {
#line 84
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 84
  goto endlab;
#line 84
} while (0);
}
endlab:
#line 86
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 86
	return;
#line 86
}

void
#line 88
bi_index(eval_environ_t env)
#line 88

#line 88

#line 88 "string.bi"
{
#line 88
	
#line 88

#line 88
        char *  str;
#line 88
        char *  sub;
#line 88
        long  from;
#line 88
        
#line 88
        long __bi_argcnt;
#line 88
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 88
        get_string_arg(env, 1, &str);
#line 88
        get_string_arg(env, 2, &sub);
#line 88
        if (__bi_argcnt > 2)
#line 88
                get_numeric_arg(env, 3, &from);
#line 88
        
#line 88
        adjust_stack(env, __bi_argcnt + 1);
#line 88

#line 88

#line 88
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 88
		prog_trace(env, "index %s %s %lu",str, sub, ((__bi_argcnt > 2) ? from : 0));;
#line 88

{
	long start = 0;
	char *p;

	if ((__bi_argcnt > 2))
		start = from;
		if (!(start >= 0 && start <= strlen(str)))
#line 95
		(
#line 95
	env_throw_bi(env, mfe_range, "index", _("argument out of range: start=%ld"),start)
#line 95
)
;
	p = strstr(str + start, sub);
	
#line 98
do {
#line 98
  push(env, (STKVAL)(mft_number)(p ? p - str : -1));
#line 98
  goto endlab;
#line 98
} while (0);
}
endlab:
#line 100
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 100
	return;
#line 100
}


void
#line 103
bi_rindex(eval_environ_t env)
#line 103

#line 103

#line 103 "string.bi"
{
#line 103
	
#line 103

#line 103
        char * MFL_DATASEG str;
#line 103
        char * MFL_DATASEG sub;
#line 103
        long  from;
#line 103
        
#line 103
        long __bi_argcnt;
#line 103
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 103
        get_string_arg(env, 1, &str);
#line 103
        get_string_arg(env, 2, &sub);
#line 103
        if (__bi_argcnt > 2)
#line 103
                get_numeric_arg(env, 3, &from);
#line 103
        
#line 103
        adjust_stack(env, __bi_argcnt + 1);
#line 103

#line 103

#line 103
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 103
		prog_trace(env, "rindex %s %s %lu",str, sub, ((__bi_argcnt > 2) ? from : 0));;
#line 103

{
	size_t start = 0;
	int slen, xlen, rc;
	char *p, *s, *x;
	char *temp;

	slen = strlen(str);
	xlen = strlen(sub);
	temp = mf_c_val(heap_tempspace(env, xlen + slen + 2), ptr);
	s = temp;
	x = s + slen + 1;
	
	if ((__bi_argcnt > 2)) {
			if (!(from >=0 && from <= slen))
#line 117
		(
#line 117
	env_throw_bi(env, mfe_range, "rindex", _("argument out of range: start=%ld"),from)
#line 117
)
;
		start = slen - from;
	}
	
	/* Reverse str */                     
#define REV(v,s,l)                                         \
	l = strlen(s);                                     \
	v[l] = 0;                                          \
	for (p = v + l - 1; p >= v; p--, s++)              \
		*p = *s;

	REV(s,str,slen);
	REV(x,sub,xlen);

	p = strstr(s + start, x);
	if (p)
		rc = slen - (p - s + xlen);
	else
		rc = -1;
	
	
#line 138
do {
#line 138
  push(env, (STKVAL)(mft_number)(rc));
#line 138
  goto endlab;
#line 138
} while (0);
}
endlab:
#line 140
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 140
	return;
#line 140
}

void
#line 142
bi_revstr(eval_environ_t env)
#line 142

#line 142

#line 142 "string.bi"
{
#line 142
	
#line 142

#line 142
        char * MFL_DATASEG str;
#line 142
        
#line 142

#line 142
        get_string_arg(env, 0, &str);
#line 142
        
#line 142
        adjust_stack(env, 1);
#line 142

#line 142

#line 142
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 142
		prog_trace(env, "revstr %s",str);;
#line 142

{
	int len = strlen(str);
	char *p;
	char *s;

	heap_obstack_begin(env);
	s = heap_obstack_grow(env, NULL, len);
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	for (p = s + len - 1; p >= s; p--, str++)
		*p = *str;
	
#line 153
do {
#line 153
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 153
  goto endlab;
#line 153
} while (0);
}
endlab:
#line 155
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 155
	return;
#line 155
}

void
#line 157
bi_replstr(eval_environ_t env)
#line 157

#line 157

#line 157 "string.bi"
{
#line 157
	
#line 157

#line 157
        char * MFL_DATASEG text;
#line 157
        long  count;
#line 157
        
#line 157

#line 157
        get_string_arg(env, 0, &text);
#line 157
        get_numeric_arg(env, 1, &count);
#line 157
        
#line 157
        adjust_stack(env, 2);
#line 157

#line 157

#line 157
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 157
		prog_trace(env, "replstr %s %lu",text, count);;
#line 157

{
	size_t len, i;

		if (!(count >= 0))
#line 161
		(
#line 161
	env_throw_bi(env, mfe_range, "replstr", _("argument out of range: count=%ld"),count)
#line 161
)
;
	len = strlen(text);
	if (count == 0 || len == 0)
		
#line 165
do {
#line 165
  pushs(env, "");
#line 165
  goto endlab;
#line 165
} while (0);

	heap_obstack_begin(env);
	for (i = 0; i < count; i++)
		heap_obstack_grow(env, text, len);
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 171
do {
#line 171
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 171
  goto endlab;
#line 171
} while (0);
}
endlab:
#line 173
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 173
	return;
#line 173
}

void
#line 175
bi_message_header_decode(eval_environ_t env)
#line 175

#line 175

#line 175 "string.bi"
{
#line 175
	
#line 175

#line 175
        char * MFL_DATASEG text;
#line 175
        char * MFL_DATASEG charset;
#line 175
        
#line 175
        long __bi_argcnt;
#line 175
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 175
        get_string_arg(env, 1, &text);
#line 175
        if (__bi_argcnt > 1)
#line 175
                get_string_arg(env, 2, &charset);
#line 175
        
#line 175
        adjust_stack(env, __bi_argcnt + 1);
#line 175

#line 175

#line 175
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 175
		prog_trace(env, "message_header_decode %s %s",text, ((__bi_argcnt > 1) ? charset : ""));;
#line 175

{
	char *p;
	int rc = mu_rfc2047_decode (((__bi_argcnt > 1) ? charset : "utf-8"), text, &p);
		if (!(rc == 0))
#line 179
		(
#line 179
	env_throw_bi(env, mfe_failure, "message_header_decode", _("error decoding string: %s"),mu_strerror(rc))
#line 179
)
#line 182
;
	pushs(env, p);
	free(p);
}

#line 186
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 186
	return;
#line 186
}

void
#line 188
bi_message_header_encode(eval_environ_t env)
#line 188

#line 188

#line 188 "string.bi"
{
#line 188
	
#line 188

#line 188
        char * MFL_DATASEG text;
#line 188
        char * MFL_DATASEG encoding;
#line 188
        char * MFL_DATASEG charset;
#line 188
        
#line 188
        long __bi_argcnt;
#line 188
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 188
        get_string_arg(env, 1, &text);
#line 188
        if (__bi_argcnt > 1)
#line 188
                get_string_arg(env, 2, &encoding);
#line 188
        if (__bi_argcnt > 2)
#line 188
                get_string_arg(env, 3, &charset);
#line 188
        
#line 188
        adjust_stack(env, __bi_argcnt + 1);
#line 188

#line 188

#line 188
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 188
		prog_trace(env, "message_header_encode %s %s %s",text, ((__bi_argcnt > 1) ? encoding : ""), ((__bi_argcnt > 2) ? charset : ""));;

{
	char *p;
	int rc = mu_rfc2047_encode (((__bi_argcnt > 2) ? charset : "utf-8"),
				    ((__bi_argcnt > 1) ? encoding : "quoted-printable"),
				    text, &p);
		if (!(rc == 0))
#line 195
		(
#line 195
	env_throw_bi(env, mfe_failure, "message_header_encode", _("error encoding string: %s"),mu_strerror(rc))
#line 195
)
#line 198
;
	pushs(env, p);
	free(p);
}

#line 202
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 202
	return;
#line 202
}

void
#line 204
bi_unfold(eval_environ_t env)
#line 204

#line 204

#line 204 "string.bi"
{
#line 204
	
#line 204

#line 204
        char * MFL_DATASEG text;
#line 204
        
#line 204

#line 204
        get_string_arg(env, 0, &text);
#line 204
        
#line 204
        adjust_stack(env, 1);
#line 204

#line 204

#line 204
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 204
		prog_trace(env, "unfold %s",text);;
#line 204

{
	size_t off;
	char *s = strcpy((char*)env_data_ref(env, off = heap_reserve(env, strlen(text) + 1)), text);
	mu_string_unfold(s, NULL);
	
#line 209
do {
#line 209
  push(env, (STKVAL) (mft_size) (off));
#line 209
  goto endlab;
#line 209
} while (0);
}
endlab:
#line 211
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 211
	return;
#line 211
}

void
#line 213
bi_escape(eval_environ_t env)
#line 213

#line 213

#line 213 "string.bi"
{
#line 213
	
#line 213

#line 213
        char * MFL_DATASEG text;
#line 213
        char * MFL_DATASEG chars;
#line 213
        
#line 213
        long __bi_argcnt;
#line 213
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 213
        get_string_arg(env, 1, &text);
#line 213
        if (__bi_argcnt > 1)
#line 213
                get_string_arg(env, 2, &chars);
#line 213
        
#line 213
        adjust_stack(env, __bi_argcnt + 1);
#line 213

#line 213

#line 213
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 213
		prog_trace(env, "escape %s %s",text, ((__bi_argcnt > 1) ? chars : ""));;
#line 213

{
	char *charmap = ((__bi_argcnt > 1) ? chars : "\\\"");
	
	if (text[strcspn(text, charmap)] == 0)
		
#line 218
do {
#line 218
  pushs(env, text);
#line 218
  goto endlab;
#line 218
} while (0);
	
	heap_obstack_begin(env);
	for (;;) {
		size_t len = strcspn(text, charmap);
		if (len)
			heap_obstack_grow(env, text, len);
		if (text[len]) {
			do { char __c = '\\'; heap_obstack_grow(env, &__c, 1); } while(0);
			do { char __c = text[len]; heap_obstack_grow(env, &__c, 1); } while(0);
			text += len + 1;
		} else
			break;
	}
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 233
do {
#line 233
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 233
  goto endlab;
#line 233
} while (0);
}
endlab:
#line 235
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 235
	return;
#line 235
}

void
#line 237
bi_qr(eval_environ_t env)
#line 237

#line 237

#line 237 "string.bi"
{
#line 237
	
#line 237

#line 237
        long  regex_flags;
#line 237
        char * MFL_DATASEG text;
#line 237
        char * MFL_DATASEG delim;
#line 237
        
#line 237
        long __bi_argcnt;
#line 237
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 237
        get_numeric_arg(env, 1, &regex_flags);
#line 237
        get_string_arg(env, 2, &text);
#line 237
        if (__bi_argcnt > 2)
#line 237
                get_string_arg(env, 3, &delim);
#line 237
        
#line 237
        adjust_stack(env, __bi_argcnt + 1);
#line 237

#line 237

#line 237
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 237
		prog_trace(env, "qr %lu %s %s",regex_flags, text, ((__bi_argcnt > 2) ? delim : ""));;
#line 237

{
	static char *qc[] = {
		/* Basic regular expressions */
		"\\+*?[]",
		/* Extended regular expressions */
		"\\+*?[](){}"
	};
	char charmap[13];
	
	strcpy(charmap, qc[!!(regex_flags & REG_EXTENDED)]);
	
	if ((__bi_argcnt > 2)) {
		char *p = charmap + strlen(charmap);
		*p++ = *delim++;
		if (*delim)
			*p++ = *delim;
		*p = 0;
	}
	
	if (text[strcspn(text, charmap)] == 0)
		
#line 258
do {
#line 258
  pushs(env, text);
#line 258
  goto endlab;
#line 258
} while (0);
	heap_obstack_begin(env);
	for (;;) {
		size_t len = strcspn(text, charmap);
		if (len)
			heap_obstack_grow(env, text, len);
		if (text[len]) {
			do { char __c = '\\'; heap_obstack_grow(env, &__c, 1); } while(0);
			do { char __c = text[len]; heap_obstack_grow(env, &__c, 1); } while(0);
			text += len + 1;
		} else
			break;
	}
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 272
do {
#line 272
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 272
  goto endlab;
#line 272
} while (0);	
}
endlab:
#line 274
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 274
	return;
#line 274
}

void
#line 276
bi_unescape(eval_environ_t env)
#line 276

#line 276

#line 276 "string.bi"
{
#line 276
	
#line 276

#line 276
        char * MFL_DATASEG text;
#line 276
        
#line 276

#line 276
        get_string_arg(env, 0, &text);
#line 276
        
#line 276
        adjust_stack(env, 1);
#line 276

#line 276

#line 276
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 276
		prog_trace(env, "unescape %s",text);;
#line 276

{
	heap_obstack_begin(env);
	for (;;) {
		size_t len = strcspn(text, "\\");
		if (len)
			heap_obstack_grow(env, text, len);
		if (text[len]) {
			char c = text[len + 1];
			if (!c)
				break;
			do { char __c = text[len+1]; heap_obstack_grow(env, &__c, 1); } while(0);
			text += len + 2;
		} else
			break;
	}
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 293
do {
#line 293
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 293
  goto endlab;
#line 293
} while (0);
}
endlab:
#line 295
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 295
	return;
#line 295
}

void
#line 297
bi_ltrim(eval_environ_t env)
#line 297

#line 297

#line 297 "string.bi"
{
#line 297
	
#line 297

#line 297
        char * MFL_DATASEG input;
#line 297
        char * MFL_DATASEG cset;
#line 297
        
#line 297
        long __bi_argcnt;
#line 297
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 297
        get_string_arg(env, 1, &input);
#line 297
        if (__bi_argcnt > 1)
#line 297
                get_string_arg(env, 2, &cset);
#line 297
        
#line 297
        adjust_stack(env, __bi_argcnt + 1);
#line 297

#line 297

#line 297
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 297
		prog_trace(env, "ltrim %s %s",input, ((__bi_argcnt > 1) ? cset : ""));;
#line 297

{
	char *p = ((__bi_argcnt > 1) ? cset : " \t\r\n\f");
	for (; *input && strchr(p, *input); input++);
	
#line 301
do {
#line 301
  pushs(env, input);
#line 301
  goto endlab;
#line 301
} while (0);
}
endlab:
#line 303
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 303
	return;
#line 303
}

void
#line 305
bi_rtrim(eval_environ_t env)
#line 305

#line 305

#line 305 "string.bi"
{
#line 305
	
#line 305

#line 305
        char * MFL_DATASEG input;
#line 305
        char * MFL_DATASEG cset;
#line 305
        
#line 305
        long __bi_argcnt;
#line 305
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 305
        get_string_arg(env, 1, &input);
#line 305
        if (__bi_argcnt > 1)
#line 305
                get_string_arg(env, 2, &cset);
#line 305
        
#line 305
        adjust_stack(env, __bi_argcnt + 1);
#line 305

#line 305

#line 305
	if (builtin_module_trace(BUILTIN_IDX_string))
#line 305
		prog_trace(env, "rtrim %s %s",input, ((__bi_argcnt > 1) ? cset : ""));;
#line 305

{
	char *p = ((__bi_argcnt > 1) ? cset : " \t\r\n\f");
	size_t len = strlen(input);

	for (; len > 0 && strchr(p, input[len-1]); len--);
	heap_obstack_begin(env);
	heap_obstack_grow(env, input, len);
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 314
do {
#line 314
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 314
  goto endlab;
#line 314
} while (0);
}
endlab:
#line 316
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 316
	return;
#line 316
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
string_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 19 "string.bi"
va_builtin_install_ex("toupper", bi_toupper, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 30 "string.bi"
va_builtin_install_ex("tolower", bi_tolower, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 41 "string.bi"
va_builtin_install_ex("length", bi_length, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 47 "string.bi"
va_builtin_install_ex("substring", bi_substring, 0, dtype_string, 3, 0, 0|0, dtype_string, dtype_number, dtype_number);
#line 70 "string.bi"
va_builtin_install_ex("substr", bi_substr, 0, dtype_string, 3, 1, 0|0, dtype_string, dtype_number, dtype_number);
#line 88 "string.bi"
va_builtin_install_ex("index", bi_index, 0, dtype_number, 3, 1, 0|0, dtype_string, dtype_string, dtype_number);
#line 103 "string.bi"
va_builtin_install_ex("rindex", bi_rindex, 0, dtype_number, 3, 1, 0|0, dtype_string, dtype_string, dtype_number);
#line 142 "string.bi"
va_builtin_install_ex("revstr", bi_revstr, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 157 "string.bi"
va_builtin_install_ex("replstr", bi_replstr, 0, dtype_string, 2, 0, 0|0, dtype_string, dtype_number);
#line 175 "string.bi"
va_builtin_install_ex("message_header_decode", bi_message_header_decode, 0, dtype_string, 2, 1, 0|0, dtype_string, dtype_string);
#line 188 "string.bi"
va_builtin_install_ex("message_header_encode", bi_message_header_encode, 0, dtype_string, 3, 2, 0|0, dtype_string, dtype_string, dtype_string);
#line 204 "string.bi"
va_builtin_install_ex("unfold", bi_unfold, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 213 "string.bi"
va_builtin_install_ex("escape", bi_escape, 0, dtype_string, 2, 1, 0|0, dtype_string, dtype_string);
#line 237 "string.bi"
va_builtin_install_ex("qr", bi_qr, 0, dtype_string, 3, 1, MFD_BUILTIN_REGEX_FLAGS|MFD_BUILTIN_REGEX_FLAGS|0, dtype_number, dtype_string, dtype_string);
#line 276 "string.bi"
va_builtin_install_ex("unescape", bi_unescape, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 297 "string.bi"
va_builtin_install_ex("ltrim", bi_ltrim, 0, dtype_string, 2, 1, 0|0, dtype_string, dtype_string);
#line 305 "string.bi"
va_builtin_install_ex("rtrim", bi_rtrim, 0, dtype_string, 2, 1, 0|0, dtype_string, dtype_string);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

