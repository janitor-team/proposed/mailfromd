/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

struct mf_mbox;
struct mf_message;

struct mf_mbox {
	mu_mailbox_t mbox;
	mu_list_t /* of struct mf_message */ msglist;
};

#define MF_MSG_MAILBOX    0
#define MF_MSG_CURRENT    1
#define MF_MSG_STANDALONE 2

struct mf_message {
	mu_message_t msg;
	mu_stream_t mstr;
	mu_header_t hdr;
/*	FIXME: mu_stream_t hstr;? */
	mu_body_t body;
	mu_stream_t bstr;
	char *buf;
	size_t bufsize;
	int type;
	mu_list_t /* of struct mf_message */ msglist;
};

void bi_close_message(struct mf_message *msg);
int bi_message_register(eval_environ_t env, mu_list_t list, mu_message_t msg,
			int type);
int bi_get_current_message(eval_environ_t env, mu_message_t *msg);
mu_message_t bi_message_from_descr(eval_environ_t env, int md);

int _bi_io_fd(eval_environ_t env, int fd, int what);
