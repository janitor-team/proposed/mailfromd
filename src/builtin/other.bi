/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include "mflib/_register.h"

static int
valid_user_p(eval_environ_t env, const char *name)
{
	int rc;
	struct mu_auth_data *auth_data = NULL;

	rc = mu_get_auth(&auth_data, mu_auth_key_name, name);
	mu_auth_data_free(auth_data);
	switch (rc) {
	case 0:
		rc = 1;
		break;
		
	case MU_ERR_AUTH_FAILURE:
		rc = 0;
		break;

	case EAGAIN:
                MF_THROW(mfe_temp_failure,
		         _("temporary failure querying for username %s"),
			 name);
		break;

	default:
                MF_THROW(mfe_failure,
		         _("failure querying for username %s"),
			 name);
		break;
	}

	MF_DEBUG(MU_DEBUG_TRACE1, 
                 ("Checking user %s: %s", name, rc ? "true" : "false"));
	return rc;
}

MF_DEFUN(validuser, NUMBER, STRING name)
{
	MF_RETURN(valid_user_p(env, name));
}
END

MF_DEFUN(interval, NUMBER, STRING str)
{
       time_t t;
       const char *endp;

       MF_ASSERT(parse_time_interval(str, &t, &endp) == 0,
	         mfe_invtime,
		 _("unrecognized time format (near `%s')"), endp);
       MF_RETURN(t);
}
END

