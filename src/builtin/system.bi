/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include <sys/utsname.h>
#include <mflib/status.h>

MF_DEFUN(system, NUMBER, STRING str)
{
	MF_RETURN(system(str));
}
END

MF_DEFUN(time, NUMBER)
{
	time_t t = time(NULL);
	MF_RETURN(t);
}
END

MF_DEFUN(sleep, VOID, NUMBER seconds, OPTIONAL, NUMBER useconds)
{
	struct timeval tv;
	
	tv.tv_sec = seconds;
	tv.tv_usec = MF_OPTVAL(useconds);
	/* FIXME: signals? */
	select(0, NULL, NULL, NULL, &tv);
}
END

MF_DEFUN(strftime, STRING, STRING fmt, NUMBER timestamp, OPTIONAL, NUMBER gmt)
{
	time_t t = timestamp;
	struct tm *tm = MF_OPTVAL(gmt) ? gmtime(&t) : localtime(&t);
	size_t size = strlen(fmt);

	if (size == 0)
		MF_RETURN("");

	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(NULL, size);

	for (;;) {
		size_t n;

		n = strftime(MF_OBSTACK_BASE(), size, fmt, tm);
		if (n > 0) {
			MF_OBSTACK_TRUNCATE(n + 1);
			break;
		}

		n = (size + 1) / 2;
		MF_OBSTACK_GROW(NULL, n);
		size += n;

	}
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(umask, NUMBER, NUMBER mask)
{
	mode_t mode = umask(mask);
	MF_RETURN(mode);
}
END

/* Interface to the system uname call.
   
   Format sequences are:
   %s   sysname
   %n   nodename
   %r   release
   %v   version
   %m   machine
*/
MF_DEFUN(uname, STRING, STRING fmt)
{
	struct utsname ubuf;

	uname(&ubuf);
	MF_OBSTACK_BEGIN();
	while (*fmt) {
		if (*fmt == '%') {
			switch (*++fmt) {
			case 's':
				MF_OBSTACK_GROW(ubuf.sysname,
						strlen(ubuf.sysname));
				break;
			case 'n':
				MF_OBSTACK_GROW(ubuf.nodename,
						strlen(ubuf.nodename));
				break;
			case 'r':
				MF_OBSTACK_GROW(ubuf.release,
						strlen(ubuf.release));
				break;
			case 'v':
				MF_OBSTACK_GROW(ubuf.version,
						strlen(ubuf.version));
				break;
			case 'm':
				MF_OBSTACK_GROW(ubuf.machine,
						strlen(ubuf.machine));
				break;
			case '%':
				MF_OBSTACK_1GROW('%');
				break;
			default:
				MF_OBSTACK_1GROW('%');
				MF_OBSTACK_1GROW(*fmt);
			}
			fmt++;
		} else {
			MF_OBSTACK_1GROW(*fmt);
			fmt++;
		}
	}
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

static struct builtin_const_trans access_modes[] = {
	MF_TRANS(R_OK),
	MF_TRANS(W_OK),
	MF_TRANS(X_OK),
	MF_TRANS(F_OK)
};

MF_DEFUN(access, NUMBER, STRING pathname, NUMBER mode)
{
	int c_mode;
	
	MF_ASSERT(_builtin_const_to_c(access_modes,
				      MU_ARRAY_SIZE(access_modes),
				      mode,
				      &c_mode) == 0,
		  mfe_failure,
		  "bad access mode");
	MF_RETURN(access(pathname, c_mode) == 0);
}
END

MF_DEFUN(getenv, STRING, STRING name)
{
	char *p = getenv(name);
	MF_ASSERT(p != NULL,
		  mfe_not_found,
		  "%s: environment variable not defined",
		  name);
	MF_RETURN(p);
}
END

MF_DEFUN(unlink, VOID, STRING name)
{
	int rc = unlink(name);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "cannot unlink %s: %s", name, mu_strerror(errno));
}
END
		  
