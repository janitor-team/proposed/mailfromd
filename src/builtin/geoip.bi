/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2009-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
MF_COND(WITH_GEOIP)
#include <GeoIP.h>

MF_DEFUN(geoip_country_code_by_addr, STRING, STRING ip,
	 OPTIONAL, NUMBER tlc)
{
	GeoIP *gi = GeoIP_new(GEOIP_STANDARD);
	const char *ret;
	
	MF_ASSERT(gi != NULL,
		  mfe_failure,
		  "GeoIP_new");
	ret = MF_OPTVAL(tlc) ? GeoIP_country_code3_by_addr(gi, ip) 
		             : GeoIP_country_code_by_addr(gi, ip);
	GeoIP_delete(gi);
	MF_ASSERT(ret != NULL && strcmp(ret, "N/A") != 0,
		  mfe_not_found,
		  _("country code not found"));
	MF_RETURN(ret);
}
END

MF_DEFUN(geoip_country_code_by_name, STRING, STRING name,
	 OPTIONAL, NUMBER tlc)
{
	GeoIP *gi = GeoIP_new(GEOIP_STANDARD);
	const char *ret;
	
	MF_ASSERT(gi != NULL,
		  mfe_failure,
		  "GeoIP_new");
	ret = MF_OPTVAL(tlc) ? GeoIP_country_code3_by_name(gi, name)
		             : GeoIP_country_code_by_name(gi, name);
	GeoIP_delete(gi);
	MF_ASSERT(ret != NULL && strcmp(ret, "N/A") != 0,
		  mfe_not_found,
		  _("country code not found"));
	MF_RETURN(ret);
}
END

