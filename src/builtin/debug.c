#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "debug.bi"
/* Debugging API for MFL.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "mflib/_register.h"
#include "srvcfg.h"

void
#line 21
bi_debug_level(eval_environ_t env)
#line 21

#line 21

#line 21 "debug.bi"
{
#line 21
	
#line 21

#line 21
        char *  modname;
#line 21
        
#line 21
        long __bi_argcnt;
#line 21
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 21
        if (__bi_argcnt > 0)
#line 21
                get_string_arg(env, 1, &modname);
#line 21
        
#line 21
        adjust_stack(env, __bi_argcnt + 1);
#line 21

#line 21

#line 21
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 21
		prog_trace(env, "debug_level %s",((__bi_argcnt > 0) ? modname : ""));;
#line 21

{
	mu_debug_level_t level;
	char *name = ((__bi_argcnt > 0) ? modname : NULL);
	size_t len = name ? strlen (name) : 0;
	
		if (!(mu_debug_category_level(name, len, &level) == 0))
#line 27
		(
#line 27
	env_throw_bi(env, mfe_range, "debug_level", _("invalid module name: %s"),name)
#line 27
)
#line 29
;
	
#line 30
do {
#line 30
  push(env, (STKVAL)(mft_number)(level));
#line 30
  goto endlab;
#line 30
} while (0);
}
endlab:
#line 32
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 32
	return;
#line 32
}

void
#line 34
bi_debug_spec(eval_environ_t env)
#line 34

#line 34

#line 34 "debug.bi"
{
#line 34
	
#line 34

#line 34
        char * MFL_DATASEG modnames;
#line 34
        long  showunset;
#line 34
        
#line 34
        long __bi_argcnt;
#line 34
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 34
        if (__bi_argcnt > 0)
#line 34
                get_string_arg(env, 1, &modnames);
#line 34
        if (__bi_argcnt > 1)
#line 34
                get_numeric_arg(env, 2, &showunset);
#line 34
        
#line 34
        adjust_stack(env, __bi_argcnt + 1);
#line 34

#line 34

#line 34
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 34
		prog_trace(env, "debug_spec %s %lu",((__bi_argcnt > 0) ? modnames : ""), ((__bi_argcnt > 1) ? showunset : 0));;
#line 34

{
	mu_stream_t str;
	int rc;
	char *names = ((__bi_argcnt > 0) ? modnames : NULL);
	
	rc = mu_memory_stream_create(&str, MU_STREAM_RDWR);
		if (!(rc == 0))
#line 41
		(
#line 41
	env_throw_bi(env, mfe_failure, "debug_spec", "cannot create stream: %s",mu_strerror(rc))
#line 41
)
#line 43
;
	heap_obstack_begin(env);
	rc = mu_debug_format_spec(str, (names && names[0]) ? names : NULL,
				  ((__bi_argcnt > 1) ? showunset : 0));
	if (rc == 0) {
		mu_off_t size;
		char *s;
		size_t n;
		
		mu_stream_seek(str, 0, MU_SEEK_SET, NULL);
		mu_stream_size(str, &size);
		s = heap_obstack_grow(env, NULL, size + 1);
		rc = mu_stream_read(str, s, size, &n);
		if (rc == 0)
			s[n] = 0;
	}
	
	mu_stream_destroy(&str);
		if (!(rc == 0))
#line 61
		(
#line 61
	env_throw_bi(env, mfe_failure, "debug_spec", "%s",mu_strerror(rc))
#line 61
)
#line 63
;
	
#line 64
do {
#line 64
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 64
  goto endlab;
#line 64
} while (0);
}
endlab:
#line 66
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 66
	return;
#line 66
}

void
#line 68
bi_debug(eval_environ_t env)
#line 68

#line 68

#line 68 "debug.bi"
{
#line 68
	
#line 68

#line 68
        char *  spec;
#line 68
        
#line 68

#line 68
        get_string_arg(env, 0, &spec);
#line 68
        
#line 68
        adjust_stack(env, 1);
#line 68

#line 68

#line 68
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 68
		prog_trace(env, "debug %s",spec);;
#line 68

{
	struct mu_locus_range loc = MU_LOCUS_RANGE_INITIALIZER;

        env_get_locus(env, &loc);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_LOCUS_RANGE, &loc);
	mu_debug_parse_spec(spec);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_LOCUS_RANGE, NULL);
}

#line 79
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 79
	return;
#line 79
}

void
#line 81
bi_program_trace(eval_environ_t env)
#line 81

#line 81

#line 81 "debug.bi"
{
#line 81
	
#line 81

#line 81
        char *  name;
#line 81
        
#line 81

#line 81
        get_string_arg(env, 0, &name);
#line 81
        
#line 81
        adjust_stack(env, 1);
#line 81

#line 81

#line 81
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 81
		prog_trace(env, "program_trace %s",name);;
#line 81

{
	enable_prog_trace(name);
}

#line 85
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 85
	return;
#line 85
}

void
#line 87
bi_cancel_program_trace(eval_environ_t env)
#line 87

#line 87

#line 87 "debug.bi"
{
#line 87
	
#line 87

#line 87
        char *  name;
#line 87
        
#line 87

#line 87
        get_string_arg(env, 0, &name);
#line 87
        
#line 87
        adjust_stack(env, 1);
#line 87

#line 87

#line 87
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 87
		prog_trace(env, "cancel_program_trace %s",name);;
#line 87

{
	disable_prog_trace(name);
}

#line 91
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 91
	return;
#line 91
}	

void
#line 93
bi_stack_trace(eval_environ_t env)
#line 93

#line 93

#line 93 "debug.bi"
{
#line 93
	
#line 93

#line 93
        
#line 93

#line 93
        
#line 93
        adjust_stack(env, 0);
#line 93

#line 93

#line 93
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 93
		prog_trace(env, "stack_trace");;
#line 93

{
	runtime_stack_trace(env);
}

#line 97
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 97
	return;
#line 97
}

void
#line 99
bi__reg(eval_environ_t env)
#line 99

#line 99

#line 99 "debug.bi"
{
#line 99
	
#line 99

#line 99
        long  what;
#line 99
        
#line 99

#line 99
        get_numeric_arg(env, 0, &what);
#line 99
        
#line 99
        adjust_stack(env, 1);
#line 99

#line 99

#line 99
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 99
		prog_trace(env, "_reg %lu",what);;
#line 99

{
	prog_counter_t regval = env_register_read(env, what);
	
#line 102
do {
#line 102
  push(env, (STKVAL)(mft_number)(regval));
#line 102
  goto endlab;
#line 102
} while (0);
}
endlab:
#line 104
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 104
	return;
#line 104
}

void
#line 106
bi__expand_dataseg(eval_environ_t env)
#line 106

#line 106

#line 106 "debug.bi"
{
#line 106
	
#line 106

#line 106
        long  words;
#line 106
        
#line 106

#line 106
        get_numeric_arg(env, 0, &words);
#line 106
        
#line 106
        adjust_stack(env, 1);
#line 106

#line 106

#line 106
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 106
		prog_trace(env, "_expand_dataseg %lu",words);;
#line 106

{
		if (!(expand_dataseg(env, words) == 0))
#line 108
		(
#line 108
	env_throw_bi(env, mfe_failure, "_expand_dataseg", _("out of stack space; increase #pragma stacksize"))
#line 108
)
#line 110
;
}

#line 112
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 112
	return;
#line 112
}

void
#line 114
bi__wd(eval_environ_t env)
#line 114

#line 114

#line 114 "debug.bi"
{
#line 114
	
#line 114

#line 114
        long  seconds;
#line 114
        
#line 114
        long __bi_argcnt;
#line 114
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 114
        if (__bi_argcnt > 0)
#line 114
                get_numeric_arg(env, 1, &seconds);
#line 114
        
#line 114
        adjust_stack(env, __bi_argcnt + 1);
#line 114

#line 114

#line 114
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 114
		prog_trace(env, "_wd %lu",((__bi_argcnt > 0) ? seconds : 0));;
#line 114

{
	mu_wd(((__bi_argcnt > 0) ? seconds : 0));
}

#line 118
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 118
	return;
#line 118
}

void
#line 120
bi_callout_transcript(eval_environ_t env)
#line 120

#line 120

#line 120 "debug.bi"
{
#line 120
	
#line 120

#line 120
        long  val;
#line 120
        
#line 120
        long __bi_argcnt;
#line 120
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 120
        if (__bi_argcnt > 0)
#line 120
                get_numeric_arg(env, 1, &val);
#line 120
        
#line 120
        adjust_stack(env, __bi_argcnt + 1);
#line 120

#line 120

#line 120
	if (builtin_module_trace(BUILTIN_IDX_debug))
#line 120
		prog_trace(env, "callout_transcript %lu",((__bi_argcnt > 0) ? val : 0));;
#line 120

{
	int oldval = smtp_transcript;
	if ((__bi_argcnt > 0))
		smtp_transcript = val;
	
#line 125
do {
#line 125
  push(env, (STKVAL)(mft_number)(oldval));
#line 125
  goto endlab;
#line 125
} while (0);
}
endlab:
#line 127
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 127
	return;
#line 127
}

 
#line 137

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
debug_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 21 "debug.bi"
va_builtin_install_ex("debug_level", bi_debug_level, 0, dtype_number, 1, 1, 0|0, dtype_string);
#line 34 "debug.bi"
va_builtin_install_ex("debug_spec", bi_debug_spec, 0, dtype_string, 2, 2, 0|0, dtype_string, dtype_number);
#line 68 "debug.bi"
va_builtin_install_ex("debug", bi_debug, 0, dtype_unspecified, 1, 0, 0|0, dtype_string);
#line 81 "debug.bi"
va_builtin_install_ex("program_trace", bi_program_trace, 0, dtype_unspecified, 1, 0, 0|0, dtype_string);
#line 87 "debug.bi"
va_builtin_install_ex("cancel_program_trace", bi_cancel_program_trace, 0, dtype_unspecified, 1, 0, 0|0, dtype_string);
#line 93 "debug.bi"
va_builtin_install_ex("stack_trace", bi_stack_trace, 0, dtype_unspecified, 0, 0, 0|0, dtype_unspecified);
#line 99 "debug.bi"
va_builtin_install_ex("_reg", bi__reg, 0, dtype_number, 1, 0, 0|0, dtype_number);
#line 106 "debug.bi"
va_builtin_install_ex("_expand_dataseg", bi__expand_dataseg, 0, dtype_unspecified, 1, 0, 0|0, dtype_number);
#line 114 "debug.bi"
va_builtin_install_ex("_wd", bi__wd, 0, dtype_unspecified, 1, 1, 0|0, dtype_number);
#line 120 "debug.bi"
va_builtin_install_ex("callout_transcript", bi_callout_transcript, 0, dtype_number, 1, 1, 0|0, dtype_number);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
/* Declare mailutils_set_debug_level as an alias to 'debug'.
   I do that manually, because there is no m4 magic for that
   (so far it is the only built-in alias). */
#line 994
      va_builtin_install_ex("mailutils_set_debug_level",
#line 994
			    bi_debug, 0, dtype_unspecified, 1, 0, 0,
#line 994
			    dtype_string);
#line 994

#line 994
}
#line 994 "../../src/builtin/snarf.m4"

