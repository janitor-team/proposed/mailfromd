/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

MF_DEFUN(header_add, VOID, STRING name, STRING value, OPTIONAL, NUMBER idx)
{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	if (MF_DEFINED(idx)) {
		trace("%s%s:%u: %s %ld \"%s: %s\"",
		      mailfromd_msgid(env_get_context(env)),
		      locus.beg.mu_file, locus.beg.mu_line,
		      msgmod_opcode_str(header_insert),
		      idx,
		      name, value);
		env_msgmod_append(env, header_insert, name, value, idx);
	} else {
		trace("%s%s:%u: %s \"%s: %s\"",
		      mailfromd_msgid(env_get_context(env)),
		      locus.beg.mu_file, locus.beg.mu_line,
		      msgmod_opcode_str(header_add),
		      name, value);
		env_msgmod_append(env, header_add, name, value, 1);
	}
}
END

MF_DEFUN(header_insert, VOID, STRING name, STRING value, NUMBER idx)
{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s %ld \"%s: %s\"",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(header_insert),
	      idx,
	      name, value);
	env_msgmod_append(env, header_insert, name, value, idx);
}
END

MF_DEFUN(header_delete, VOID, STRING name, OPTIONAL, NUMBER idx)
{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s \"%s\" (%lu)",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(header_delete),
	      name, MF_OPTVAL(idx, 1));
	env_msgmod_append(env, header_delete, name, NULL, MF_OPTVAL(idx, 1));
}
END

MF_DEFUN(header_replace, VOID, STRING name, STRING value, OPTIONAL, NUMBER idx)
{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s \"%s: %s\" (%lu)",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(header_replace),
	      name, value, MF_OPTVAL(idx, 1));
	env_msgmod_append(env, header_replace, name, value, MF_OPTVAL(idx, 1));
}
END

