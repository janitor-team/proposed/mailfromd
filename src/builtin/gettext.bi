/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

MF_DEFUN(bindtextdomain, STRING, STRING domain, STRING dirname)
{
	const char *s = bindtextdomain(domain[0] ? domain : NULL, dirname);
	MF_ASSERT(s != NULL,
		  mfe_failure,
		  "bindtextdomain failed: %s", mu_strerror(errno));
	MF_RETURN(s);
}
END

MF_DEFUN(dgettext, STRING, STRING domain, STRING msgid)
{
	MF_RETURN(dgettext(domain, msgid));
}
END

MF_DEFUN(dngettext, STRING, STRING domain,
         STRING msgid, STRING msgid_plural, NUMBER n)
{
	MF_RETURN(dngettext(domain, msgid, msgid_plural, n));
}
END

