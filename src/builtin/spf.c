#line 994 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 994
#ifdef HAVE_CONFIG_H
#line 994
# include <config.h>
#line 994
#endif
#line 994
#include <sys/types.h>
#line 994

#line 994
#include "mailfromd.h"
#line 994
#include "prog.h"
#line 994
#include "builtin.h"
#line 994

#line 994

#line 1034 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "spf.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include "spf.h"

static size_t spf_explanation_loc
#line 21 "spf.bi"
;
static size_t spf_mechanism_loc
#line 22 "spf.bi"
;
static size_t spf_explanation_prefix_loc
#line 23 "spf.bi"
;
       

#line 25

#line 25
static void
#line 25
update_spf_vars(eval_environ_t env, spf_answer_t *ans)
#line 25
{
#line 25
	size_t i;
#line 25
	
#line 25
	
#line 25
{ size_t __off;
#line 25
  const char *__s = ans->exp_text;
#line 25
  if (__s)
#line 25
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 25
  else
#line 25
     __off = 0;
#line 25
  mf_c_val(*env_data_ref(env, spf_explanation_loc),size) = (__off); }
#line 25
;
#line 25
	heap_obstack_begin(env);
#line 25
	if (ans->mechn) {
#line 25
		
#line 25
do {
#line 25
  char *__s = ans->mechv[0];
#line 25
  heap_obstack_grow(env, __s, strlen(__s));
#line 25
} while (0);
#line 25
		for (i = 1; i < ans->mechn; i++) {
#line 25
			do { char __c = ' '; heap_obstack_grow(env, &__c, 1); } while(0);
#line 25
			
#line 25
do {
#line 25
  char *__s = ans->mechv[i];
#line 25
  heap_obstack_grow(env, __s, strlen(__s));
#line 25
} while (0);
#line 25
		}
#line 25
	}
#line 25
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
#line 25
	mf_c_val(*env_data_ref(env, spf_mechanism_loc),size) = (mf_c_val(heap_obstack_finish(env), size));
#line 25
}
#line 25

#line 25

#line 25

#line 43



void
#line 46
bi_spf_check_host(eval_environ_t env)
#line 46

#line 46

#line 46 "spf.bi"
{
#line 46
	
#line 46

#line 46
        char * MFL_DATASEG ip;
#line 46
        char * MFL_DATASEG domain;
#line 46
        char * MFL_DATASEG sender;
#line 46
        char * MFL_DATASEG helo_domain;
#line 46
        char * MFL_DATASEG my_domain;
#line 46
        
#line 46

#line 46
        get_string_arg(env, 0, &ip);
#line 46
        get_string_arg(env, 1, &domain);
#line 46
        get_string_arg(env, 2, &sender);
#line 46
        get_string_arg(env, 3, &helo_domain);
#line 46
        get_string_arg(env, 4, &my_domain);
#line 46
        
#line 46
        adjust_stack(env, 5);
#line 46

#line 46

#line 46
	if (builtin_module_trace(BUILTIN_IDX_spf))
#line 46
		prog_trace(env, "spf_check_host %s %s %s %s %s",ip, domain, sender, helo_domain, my_domain);;

{
	spf_result res;
	spf_query_t q;
	spf_answer_t ans;

	q.ipstr = ip;
	q.domain = domain;
	q.sender = sender;
	q.helo_domain = helo_domain;
	q.my_domain = my_domain;
	q.exp_prefix = (char*)env_data_ref(env, mf_c_val(*env_data_ref(env, spf_explanation_prefix_loc),size) );
	res = spf_check_host(&q, &ans);
	
#line 60
update_spf_vars(env,&ans);
	spf_answer_free(&ans);
	
#line 62
do {
#line 62
  push(env, (STKVAL)(mft_number)(res));
#line 62
  goto endlab;
#line 62
} while (0);
}
endlab:
#line 64
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 64
	return;
#line 64
}


void
#line 67
bi_spf_test_record(eval_environ_t env)
#line 67

#line 67

#line 67 "spf.bi"
{
#line 67
	
#line 67

#line 67
        char * MFL_DATASEG record;
#line 67
        char * MFL_DATASEG ip;
#line 67
        char * MFL_DATASEG domain;
#line 67
        char * MFL_DATASEG sender;
#line 67
        char * MFL_DATASEG helo_domain;
#line 67
        char * MFL_DATASEG my_domain;
#line 67
        
#line 67

#line 67
        get_string_arg(env, 0, &record);
#line 67
        get_string_arg(env, 1, &ip);
#line 67
        get_string_arg(env, 2, &domain);
#line 67
        get_string_arg(env, 3, &sender);
#line 67
        get_string_arg(env, 4, &helo_domain);
#line 67
        get_string_arg(env, 5, &my_domain);
#line 67
        
#line 67
        adjust_stack(env, 6);
#line 67

#line 67

#line 67
	if (builtin_module_trace(BUILTIN_IDX_spf))
#line 67
		prog_trace(env, "spf_test_record %s %s %s %s %s %s",record, ip, domain, sender, helo_domain, my_domain);;
#line 69

{
	spf_result res;
	spf_query_t q;
	spf_answer_t ans;

	q.ipstr = ip;
	q.domain = domain;
	q.sender = sender;
	q.helo_domain = helo_domain;
	q.my_domain = my_domain;
	q.exp_prefix = (char*)env_data_ref(env, mf_c_val(*env_data_ref(env, spf_explanation_prefix_loc),size) );
	res = spf_test_record(record, &q, &ans);
	
#line 82
update_spf_vars(env,&ans);
	spf_answer_free(&ans);
	
#line 84
do {
#line 84
  push(env, (STKVAL)(mft_number)(res));
#line 84
  goto endlab;
#line 84
} while (0);
}
endlab:
#line 86
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 86
	return;
#line 86
}

#line 994 "../../src/builtin/snarf.m4"

#line 994

#line 994

#line 994
void
#line 994
spf_init_builtin(void)
#line 994
{
#line 994
	
#line 994
	#line 21 "spf.bi"
	builtin_variable_install("spf_explanation", dtype_string, SYM_VOLATILE, &spf_explanation_loc);
#line 22 "spf.bi"
	builtin_variable_install("spf_mechanism", dtype_string, SYM_VOLATILE, &spf_mechanism_loc);
#line 23 "spf.bi"
	builtin_variable_install("spf_explanation_prefix", dtype_string, SYM_VOLATILE, &spf_explanation_prefix_loc);
#line 46 "spf.bi"
va_builtin_install_ex("spf_check_host", bi_spf_check_host, 0, dtype_number, 5, 0, 0|0, dtype_string, dtype_string, dtype_string, dtype_string, dtype_string);
#line 67 "spf.bi"
va_builtin_install_ex("spf_test_record", bi_spf_test_record, 0, dtype_number, 6, 0, 0|0, dtype_string, dtype_string, dtype_string, dtype_string, dtype_string, dtype_string);

#line 994 "../../src/builtin/snarf.m4"
	
#line 994
}
#line 994 "../../src/builtin/snarf.m4"

