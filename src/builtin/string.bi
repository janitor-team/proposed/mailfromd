/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

MF_DEFUN(toupper, STRING, STRING string)
{
	size_t off;
	char *s = MF_COPY_STRING(off, string);
	char *p;
	for (p = s; *p; p++)
		*p = toupper(*p);
	MF_RETURN(off, size);
}
END

MF_DEFUN(tolower, STRING, STRING string)
{
	size_t off;
	char *s = MF_COPY_STRING(off, string);
	char *p;
	for (p = s; *p; p++)
		*p = tolower(*p);
	MF_RETURN(off, size);
}
END

MF_DEFUN(length, NUMBER, STRING string)
{
	MF_RETURN(strlen(string));
}
END

MF_DEFUN(substring, STRING, STRING string, NUMBER start, NUMBER end)
{
	long len = strlen(string);
	
	if (end < 0)
		end = len + end;
	if (end < start) {
		long t = end;
		end = start;
		start = t;
	}

	MF_ASSERT(start < len && end < len, mfe_range,
		  _("argument out of range"));
	
	len = end - start + 1;
	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(string + start, len);
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(substr, STRING, STRING string, NUMBER start, OPTIONAL, NUMBER nbytes)
{
	long len = strlen(string);
	
	MF_ASSERT(start >= 0, mfe_range,
		  _("argument out of range: start=%ld"), start);
	if (!MF_DEFINED(nbytes))
		nbytes = len - start;
	MF_ASSERT(nbytes >= 0, mfe_range, 
	          _("argument out of range: start=%ld, len=%ld"), start, len);

	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(string + start, nbytes);
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(index, NUMBER, STRING str, STRING sub, OPTIONAL, NUMBER from)
{
	long start = 0;
	char *p;

	if (MF_DEFINED(from))
		start = from;
	MF_ASSERT(start >= 0 && start <= strlen(str), mfe_range,
		  _("argument out of range: start=%ld"), start);
	p = strstr(str + start, sub);
	MF_RETURN(p ? p - str : -1);
}
END

MF_DSEXP
MF_DEFUN(rindex, NUMBER, STRING str, STRING sub, OPTIONAL, NUMBER from)
{
	size_t start = 0;
	int slen, xlen, rc;
	char *p, *s, *x;
	char *temp;

	slen = strlen(str);
	xlen = strlen(sub);
	temp = MF_ALLOC_HEAP_TEMP(xlen + slen + 2);
	s = temp;
	x = s + slen + 1;
	
	if (MF_DEFINED(from)) {
		MF_ASSERT(from >=0 && from <= slen, mfe_range,
			  _("argument out of range: start=%ld"), from);
		start = slen - from;
	}
	
	/* Reverse str */                     
#define REV(v,s,l)                                         \
	l = strlen(s);                                     \
	v[l] = 0;                                          \
	for (p = v + l - 1; p >= v; p--, s++)              \
		*p = *s;

	REV(s,str,slen);
	REV(x,sub,xlen);

	p = strstr(s + start, x);
	if (p)
		rc = slen - (p - s + xlen);
	else
		rc = -1;
	
	MF_RETURN(rc);
}
END

MF_DEFUN(revstr, STRING, STRING str)
{
	int len = strlen(str);
	char *p;
	char *s;

	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(NULL, len, s);
	MF_OBSTACK_1GROW(0);
	for (p = s + len - 1; p >= s; p--, str++)
		*p = *str;
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(replstr, STRING, STRING text, NUMBER count)
{
	size_t len, i;

	MF_ASSERT(count >= 0, mfe_range,
		  _("argument out of range: count=%ld"), count);
	len = strlen(text);
	if (count == 0 || len == 0)
		MF_RETURN("");

	MF_OBSTACK_BEGIN();
	for (i = 0; i < count; i++)
		MF_OBSTACK_GROW(text, len);
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(message_header_decode, STRING, STRING text, OPTIONAL, STRING charset)
{
	char *p;
	int rc = mu_rfc2047_decode (MF_OPTVAL(charset, "utf-8"), text, &p);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  _("error decoding string: %s"),
		  mu_strerror(rc));
	pushs(env, p);
	free(p);
}
END

MF_DEFUN(message_header_encode, STRING, STRING text, OPTIONAL,
	 STRING encoding, STRING charset)
{
	char *p;
	int rc = mu_rfc2047_encode (MF_OPTVAL(charset, "utf-8"),
				    MF_OPTVAL(encoding, "quoted-printable"),
				    text, &p);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  _("error encoding string: %s"),
		  mu_strerror(rc));
	pushs(env, p);
	free(p);
}
END

MF_DEFUN(unfold, STRING, STRING text)
{
	size_t off;
	char *s = MF_COPY_STRING(off, text);
	mu_string_unfold(s, NULL);
	MF_RETURN(off, size);
}
END

MF_DEFUN(escape, STRING, STRING text, OPTIONAL, STRING chars)
{
	char *charmap = MF_OPTVAL(chars, "\\\"");
	
	if (text[strcspn(text, charmap)] == 0)
		MF_RETURN(text);
	
	MF_OBSTACK_BEGIN();
	for (;;) {
		size_t len = strcspn(text, charmap);
		if (len)
			MF_OBSTACK_GROW(text, len);
		if (text[len]) {
			MF_OBSTACK_1GROW('\\');
			MF_OBSTACK_1GROW(text[len]);
			text += len + 1;
		} else
			break;
	}
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(qr, STRING, REGFLAGS regex_flags, STRING text, OPTIONAL, STRING delim)
{
	static char *qc[] = {
		/* Basic regular expressions */
		"\\+*?[]",
		/* Extended regular expressions */
		"\\+*?[](){}"
	};
	char charmap[13];
	
	strcpy(charmap, qc[!!(regex_flags & REG_EXTENDED)]);
	
	if (MF_DEFINED(delim)) {
		char *p = charmap + strlen(charmap);
		*p++ = *delim++;
		if (*delim)
			*p++ = *delim;
		*p = 0;
	}
	
	if (text[strcspn(text, charmap)] == 0)
		MF_RETURN(text);
	MF_OBSTACK_BEGIN();
	for (;;) {
		size_t len = strcspn(text, charmap);
		if (len)
			MF_OBSTACK_GROW(text, len);
		if (text[len]) {
			MF_OBSTACK_1GROW('\\');
			MF_OBSTACK_1GROW(text[len]);
			text += len + 1;
		} else
			break;
	}
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();	
}
END

MF_DEFUN(unescape, STRING, STRING text)
{
	MF_OBSTACK_BEGIN();
	for (;;) {
		size_t len = strcspn(text, "\\");
		if (len)
			MF_OBSTACK_GROW(text, len);
		if (text[len]) {
			char c = text[len + 1];
			if (!c)
				break;
			MF_OBSTACK_1GROW(text[len+1]);
			text += len + 2;
		} else
			break;
	}
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(ltrim, STRING, STRING input, OPTIONAL, STRING cset)
{
	char *p = MF_OPTVAL(cset, " \t\r\n\f");
	for (; *input && strchr(p, *input); input++);
	MF_RETURN(input);
}
END

MF_DEFUN(rtrim, STRING, STRING input, OPTIONAL, STRING cset)
{
	char *p = MF_OPTVAL(cset, " \t\r\n\f");
	size_t len = strlen(input);

	for (; len > 0 && strchr(p, input[len-1]); len--);
	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(input, len);
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();
}
END

