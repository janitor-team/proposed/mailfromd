/* -*- buffer-read-only: t -*- vi: set ro: */
/* THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT. */

enum node_type {
    node_type_noop,
    node_type_string,
    node_type_symbol,
    node_type_number,
    node_type_if,
    node_type_bin,
    node_type_un,
    node_type_result,
    node_type_header,
    node_type_builtin,
    node_type_concat,
    node_type_variable,
    node_type_asgn,
    node_type_arg,
    node_type_argx,
    node_type_vaptr,
    node_type_regex,
    node_type_regcomp,
    node_type_catch,
    node_type_try,
    node_type_throw,
    node_type_echo,
    node_type_return,
    node_type_call,
    node_type_switch,
    node_type_next,
    node_type_break,
    node_type_loop,
    node_type_backref,
    node_type_cast,
    node_type_funcdecl,
    node_type_progdecl,
    node_type_offset,
    node_type_sedcomp,
    node_type_sed,
    max_node_type
};
