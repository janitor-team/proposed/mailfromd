/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "gram.y" /* yacc.c:339  */

/* This file is part of Mailfromd.
   Copyright (C) 2005-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <fnmatch.h>
#include <netdb.h>
#include <limits.h>
#include "mailfromd.h"
#include <mailutils/yyloc.h>	
#include "prog.h"
#include "optab.h"

static void free_node(NODE *node); 
static void set_poll_arg(struct poll_data *poll, int kw, NODE *expr);
static int codegen(prog_counter_t *pc, NODE *node, struct exmask *exmask,
		   int finalize, size_t nautos);
static void mark(NODE *node);
static void dataseg_layout(void);
static void regex_layout(void);
static int optimize_tree(NODE *node);
static void compile_tree(NODE *node);
static NODE *reverse(NODE *in);
static size_t nodelistlength(NODE *list);
static NODE *function_call(struct function *function, size_t count,
                           NODE *subtree);
static NODE *declare_function(struct function *func,
		              struct mu_locus_range const *loc,
                              size_t nautos);
static data_type_t node_type(NODE *node);
static NODE *cast_arg_list(NODE *args, size_t parmc, data_type_t *parmtype,
			   int disable_prom);
void add_xref(struct variable *var, struct mu_locus_range const *locus);
static struct variable *vardecl(const char *name, data_type_t type,
                                storage_class_t sc,
		                struct mu_locus_range const *loc);
static struct variable *externdecl(const char *name, struct value *value,
		                   struct mu_locus_range const *loc);
static int initialize_variable(struct variable *var, struct value *val,
                               struct mu_locus_range const *locus);
static void apply_deferred_init(void);
static NODE *create_asgn_node(struct variable *var, NODE *expr,
		              struct mu_locus_range const *loc);

NODE *defined_parm(struct variable *var, struct mu_locus_range const *locus);
 
static void register_auto(struct variable *var);
static void unregister_auto(struct variable *var);
static size_t forget_autos(size_t nparam, size_t prev, size_t hidden_arg);
static void optimize(NODE *node);

static NODE *root_node[smtp_state_count];
prog_counter_t entry_point[smtp_state_count];

#define PS_BEGIN 0
#define PS_END   1 

int regex_flags;        /* Should default to REG_NOSUB ? */
unsigned error_count;   /* Number of detected errors */
size_t variable_count = 0;  /* Number of variables in the data segment. */
size_t precious_count = 0;

static enum smtp_state state_tag; /* State tag of the currently processed
                                      PROG */

static struct function *func;   /* The currently compiled function */
static prog_counter_t jump_pc;  /* Pointer to the chain of jmp instructions */

/* Outermost context decides how to code exits from catches.
   Innermost context is used to access parameters. */
enum lexical_context outer_context, inner_context;

size_t catch_nesting;  /* Nesting level for catch statements */

static struct stmtlist genstmt;     /* List of generated statements */

/* State handlers and their positional parameters */ 
struct state_parms {
        int cnt;                  /* Number or positional parameters */
        data_type_t types[4];   /* Their data types */
} state_parms[] = {
        { 0,  }, /* smtp_state_none */
        { 0, }, /* smtp_state_begin */
        { 4, { dtype_string, dtype_number, dtype_number, dtype_string } },
        /* smtp_state_connect */
        { 1, { dtype_string } },  /* smtp_state_helo */
        { 2, { dtype_string, dtype_string } },  /* smtp_state_envfrom */
        { 2, { dtype_string, dtype_string } },  /* smtp_state_envrcpt */
        { 0, },  /* smtp_state_data */
        { 2, { dtype_string, dtype_string } }, /* smtp_state_header */
        { 0,  }, /* smtp_state_eoh */
        { 2, { dtype_pointer, dtype_number } }, /* smtp_state_body */
        { 0, }, /* smtp_state_eom */
        { 0, }  /* smtp_state_end */
};

struct parmtype {
        struct parmtype *next;
        data_type_t type;
};

static int
parmcount_none()
{
        return 0;
}

static data_type_t
parmtype_none(int n)
{
        return dtype_unspecified;
}

static int
parmcount_handler()
{
        return state_parms[state_tag].cnt;
}

static data_type_t
parmtype_handler(int n)
{
        return state_parms[state_tag].types[n-1];
}

static int
parmcount_catch()
{
        return 2;
}

static data_type_t
parmtype_catch(int n)
{
        switch (n) {
        case 1:
                return dtype_number;
        case 2:
                return dtype_string;
        }
        abort();
}

static int
parmcount_function()
{
        return func->parmcount;
}

static data_type_t
parmtype_function(int n)
{
        if (func->varargs && n > func->parmcount)
                return dtype_string;
        return func->parmtype[n-1];
}

struct parminfo {
        int (*parmcount)(void);
        data_type_t (*parmtype)(int n);
} parminfo[] = {
        { parmcount_none, parmtype_none },
        { parmcount_handler, parmtype_handler },
        { parmcount_catch, parmtype_catch },
        { parmcount_function, parmtype_function },
};

#define PARMCOUNT() parminfo[inner_context].parmcount()
#define PARMTYPE(n) parminfo[inner_context].parmtype(n)
#define FUNC_HIDDEN_ARGS(f) (((f)->optcount || (f)->varargs) ? 1 : 0)


data_type_t
string_to_type(const char *s)
{
        if (strcmp(s, "n") == 0)
                return dtype_number;
        else if (strcmp(s, "s") == 0)
                return dtype_string;
        else
                return dtype_unspecified;
}

const char *
type_to_string(data_type_t t)
{
        switch (t) {
        case dtype_number:
                return "number";
        case dtype_string:
                return "string";
        case dtype_unspecified:
                return "unspecified";
        case dtype_pointer:
                return "pointer";
        default:
                abort();
        }
}

static int
check_func_usage(struct function *fp, struct mu_locus_range const *locus)
{
	switch (outer_context) {
	case context_handler:
		if (fp->statemask && !(STATMASK(state_tag) & fp->statemask)) {
                        parse_error_locus(locus,
                                          _("function `%s' cannot be used in "
					    "prog `%s'"),
				    fp->sym.name,
				    state_to_string(state_tag));
			return 1;
		}
		break;
		
	case context_function:
		func->statemask |= fp->statemask;
		break;

	default:
		break;
	}
	return 0;
}

static int
check_builtin_usage(const struct builtin *bp,
		    struct mu_locus_range const *locus)
{
	switch (outer_context) {
	case context_handler:
		if (bp->statemask && !(STATMASK(state_tag) & bp->statemask)) {
			parse_error_locus(locus,
					  _("built-in function `%s' cannot be used in "
					    "prog `%s'"),
					  bp->name,
					  state_to_string(state_tag));
			return 1;
		}
		break;
		
	case context_function:
		func->statemask |= bp->statemask;
		break;
		
	default:
		break;
	}
			
	if (bp->flags & MFD_BUILTIN_CAPTURE)
		capture_on();
	return 0;
}

static void jump_fixup(prog_counter_t pos, prog_counter_t endpos);

#define LITERAL_TEXT(lit) ((lit) ? (lit)->text : NULL)
#define LITERAL_OFF(lit) ((lit) ? (lit)->off : 0)

static int
_create_alias(void *item, void *data)
{
        struct literal *lit = item;
        struct function *fun = data;
        /* FIXME: Ideally we should pass a locus of `lit' instead of
           fun->locus. However, its only purpose is for use in redefinition
           diagnostic messages and these are never produced, because the
           grammar catches these errors earlier. This might change in
           the future.  2008-09-14 */
        install_alias(lit->text, fun, &fun->sym.locus);
        return 0;
}

#line 356 "gram.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "gram.h".  */
#ifndef YY_YY_GRAM_H_INCLUDED
# define YY_YY_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 294 "gram.y" /* yacc.c:355  */

#define YYLTYPE struct mu_locus_range

#line 390 "gram.c" /* yacc.c:355  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    T_ACCEPT = 258,
    T_REJECT = 259,
    T_TEMPFAIL = 260,
    T_CONTINUE = 261,
    T_DISCARD = 262,
    T_ADD = 263,
    T_REPLACE = 264,
    T_DELETE = 265,
    T_PROG = 266,
    T_BEGIN = 267,
    T_END = 268,
    T_IF = 269,
    T_FI = 270,
    T_ELSE = 271,
    T_ELIF = 272,
    T_ON = 273,
    T_HOST = 274,
    T_FROM = 275,
    T_AS = 276,
    T_DO = 277,
    T_DONE = 278,
    T_POLL = 279,
    T_MATCHES = 280,
    T_FNMATCHES = 281,
    T_MXMATCHES = 282,
    T_MXFNMATCHES = 283,
    T_WHEN = 284,
    T_PASS = 285,
    T_SET = 286,
    T_CATCH = 287,
    T_TRY = 288,
    T_THROW = 289,
    T_ECHO = 290,
    T_RETURNS = 291,
    T_RETURN = 292,
    T_FUNC = 293,
    T_SWITCH = 294,
    T_CASE = 295,
    T_DEFAULT = 296,
    T_CONST = 297,
    T_FOR = 298,
    T_LOOP = 299,
    T_WHILE = 300,
    T_BREAK = 301,
    T_NEXT = 302,
    T_ARGCOUNT = 303,
    T_ALIAS = 304,
    T_DOTS = 305,
    T_ARGX = 306,
    T_VAPTR = 307,
    T_PRECIOUS = 308,
    T_OR = 309,
    T_AND = 310,
    T_EQ = 311,
    T_NE = 312,
    T_LT = 313,
    T_LE = 314,
    T_GT = 315,
    T_GE = 316,
    T_NOT = 317,
    T_LOGAND = 318,
    T_LOGOR = 319,
    T_LOGXOR = 320,
    T_LOGNOT = 321,
    T_REQUIRE = 322,
    T_IMPORT = 323,
    T_STATIC = 324,
    T_PUBLIC = 325,
    T_MODULE = 326,
    T_BYE = 327,
    T_DCLEX = 328,
    T_SHL = 329,
    T_SHR = 330,
    T_SED = 331,
    T_COMPOSE = 332,
    T_MODBEG = 333,
    T_MODEND = 334,
    T_STRING = 335,
    T_SYMBOL = 336,
    T_IDENTIFIER = 337,
    T_ARG = 338,
    T_NUMBER = 339,
    T_BACKREF = 340,
    T_BUILTIN = 341,
    T_FUNCTION = 342,
    T_TYPE = 343,
    T_TYPECAST = 344,
    T_VARIABLE = 345,
    T_BOGUS = 346,
    T_UMINUS = 347
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 298 "gram.y" /* yacc.c:355  */

	struct literal *literal;
	struct stmtlist stmtlist;
	NODE *node;
	struct return_node ret;
	struct poll_data poll;
	struct pollarg {
		int kw;
		NODE *expr;
	} pollarg;
	struct arglist {
		NODE *head;
		NODE *tail;
		size_t count;
	} arglist;
	long number;
	const struct builtin *builtin;
	struct variable *var;
	enum smtp_state state;
	struct {
		int qualifier;
	} matchtype;
	data_type_t type;
	struct parmtype *parm;
	struct parmlist {
		struct parmtype *head, *tail;
		size_t count;
		size_t optcount;
		int varargs;
	} parmlist;
	enum lexical_context tie_in;
	struct function *function;
	struct {
		struct valist *head, *tail;
	} valist_list;
	struct {
		int all;
		struct valist *valist;
	} catchlist;
	struct valist *valist;
	struct value value;
	struct {
		struct case_stmt *head, *tail;
	} case_list ;
	struct case_stmt *case_stmt;
	struct loop_node loop;
	struct {
		int code;
	} progspecial;
	struct enumlist {
		struct constant *cv;
		struct enumlist *prev;
	} enumlist;
	mu_list_t list;
	struct import_rule_list import_rule_list;
	char *string;

#line 553 "gram.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_GRAM_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 584 "gram.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  33
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1606

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  105
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  93
/* YYNRULES -- Number of rules.  */
#define YYNRULES  233
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  398

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   347

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    97,     2,     2,
     100,   101,    95,    93,    99,    94,    92,    96,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   103,   102,
       2,     2,     2,     2,   104,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    98
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   487,   487,   513,   514,   520,   524,   525,   536,   549,
     553,   561,   564,   567,   578,   596,   600,   606,   607,   613,
     618,   626,   627,   630,   641,   667,   685,   689,   693,   699,
     705,   713,   714,   720,   734,   771,   799,   802,   811,   815,
     819,   826,   831,   844,   858,   898,   922,   930,   948,   958,
     964,   995,   999,  1002,  1003,  1008,  1023,  1028,  1031,  1038,
    1047,  1048,  1055,  1064,  1075,  1078,  1081,  1086,  1093,  1100,
    1103,  1109,  1121,  1127,  1140,  1141,  1142,  1143,  1144,  1145,
    1146,  1147,  1155,  1156,  1157,  1160,  1184,  1190,  1202,  1215,
    1223,  1227,  1235,  1243,  1249,  1257,  1263,  1271,  1277,  1285,
    1286,  1295,  1302,  1309,  1319,  1322,  1325,  1332,  1340,  1348,
    1357,  1370,  1393,  1394,  1395,  1398,  1408,  1411,  1418,  1424,
    1469,  1473,  1480,  1489,  1500,  1506,  1517,  1522,  1529,  1542,
    1546,  1552,  1556,  1565,  1576,  1583,  1586,  1590,  1593,  1596,
    1616,  1646,  1651,  1656,  1664,  1667,  1673,  1687,  1712,  1718,
    1725,  1732,  1739,  1746,  1753,  1760,  1774,  1782,  1789,  1796,
    1800,  1803,  1806,  1807,  1814,  1820,  1827,  1834,  1841,  1848,
    1855,  1862,  1869,  1876,  1885,  1890,  1894,  1898,  1899,  1905,
    1909,  1917,  1921,  1926,  1930,  1931,  1935,  1941,  1946,  1949,
    1950,  1958,  1963,  1966,  1970,  1974,  2001,  2002,  2008,  2042,
    2057,  2097,  2105,  2115,  2121,  2130,  2134,  2140,  2148,  2156,
    2164,  2163,  2194,  2198,  2205,  2221,  2233,  2252,  2280,  2339,
    2347,  2353,  2359,  2368,  2377,  2383,  2388,  2395,  2400,  2405,
    2410,  2418,  2422,  2430
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "\"accept\"", "\"reject\"",
  "\"tempfail\"", "\"continue\"", "\"discard\"", "\"add\"", "\"replace\"",
  "\"delete\"", "\"prog\"", "\"begin\"", "\"end\"", "\"if\"", "\"fi\"",
  "\"else\"", "\"elif\"", "\"on\"", "\"host\"", "\"from\"", "\"as\"",
  "\"do\"", "\"done\"", "\"poll\"", "\"matches\"", "\"fnmatches\"",
  "\"mx matches\"", "\"mx fnmatches\"", "\"when\"", "\"pass\"", "\"set\"",
  "\"catch\"", "\"try\"", "\"throw\"", "\"echo\"", "\"returns\"",
  "\"return\"", "\"func\"", "\"switch\"", "\"case\"", "\"default\"",
  "\"const\"", "\"for\"", "\"loop\"", "\"while\"", "\"break\"", "\"next\"",
  "\"$#\"", "\"alias\"", "\"...\"", "\"$(n)\"", "\"vaptr\"",
  "\"precious\"", "\"or\"", "\"and\"", "\"==\"", "\"!=\"", "\"<\"",
  "\"<=\"", "\">\"", "\">=\"", "\"!\"", "\"&\"", "\"|\"", "\"^\"", "\"~\"",
  "\"require\"", "\"import\"", "\"static\"", "\"public\"", "\"module\"",
  "\"bye\"", "\"dclex\"", "\"<<\"", "\">>\"", "\"sed\"",
  "\"composed string\"", "T_MODBEG", "T_MODEND", "\"string\"",
  "\"MTA macro\"", "\"identifier\"", "\"$n\"", "\"number\"",
  "\"back reference\"", "\"builtin function\"", "\"function\"",
  "\"data type\"", "\"typecast\"", "\"variable\"", "T_BOGUS", "'.'", "'+'",
  "'-'", "'*'", "'/'", "'%'", "T_UMINUS", "','", "'('", "')'", "';'",
  "':'", "'@'", "$accept", "input", "program", "decllist", "modcntl",
  "opt_moddecl", "moddecl", "require", "bye", "imports", "literal", "decl",
  "progspecial", "varname", "vardecl", "qualifiers", "qualifier",
  "constdecl", "qualconst", "constdefn", "enumlist", "exdecl", "fundecl",
  "parmdecl", "params", "opt_parmlist", "parmlist", "fparmlist", "parm",
  "aliasdecl", "aliases", "alias", "retdecl", "state_ident", "stmtlist",
  "stmt", "asgn", "autodcl", "action", "sendmail_action",
  "maybe_xcode_expr", "header_action", "maybe_triplet", "triplet", "code",
  "xcode", "condition", "if_cond", "else_cond", "case_cond",
  "cond_branches", "cond_branch", "valist", "value", "string", "matches",
  "fnmatches", "loopstmt", "loop_ident", "opt_ident", "opt_loop_parms",
  "loop_parm_list", "loop_parm", "opt_while", "jumpstmt", "expr",
  "maybe_expr", "simp_expr", "atom_expr", "atom", "strcat", "strval",
  "argref", "paren_argref", "funcall", "arglist", "variable", "catch",
  "try_block", "simple_catch", "@1", "catchlist", "throw", "return",
  "bool_expr", "on_cond", "on", "do", "pollstmt", "pollarglist", "pollarg",
  "branches", "branch", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,    46,    43,    45,    42,    47,    37,   347,    44,
      40,    41,    59,    58,    64
};
# endif

#define YYPACT_NINF -287

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-287)))

#define YYTABLE_NINF -234

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-234)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     640,   -61,  -287,  -287,    33,    70,    33,    33,   -55,    48,
    -287,   581,  -287,   316,    -1,  -287,    58,  -287,   103,  -287,
       0,  -287,  -287,    62,  -287,  -287,    30,  -287,  -287,  1411,
    -287,    89,  -287,  -287,  -287,  -287,    50,    79,  1250,    70,
    -287,  -287,  -287,  -287,    70,  -287,    70,  1411,  -287,  1250,
      33,  -287,    71,   110,  1411,  1477,    95,  -287,  -287,  -287,
    -287,  -287,  -287,   104,   123,   129,  -287,  -287,  1477,  1477,
    1411,    72,   712,  1000,  -287,  -287,    87,  -287,  -287,  -287,
    -287,  -287,   116,  -287,  -287,   316,  -287,   153,   -54,   -51,
    -287,  -287,    67,    67,    67,  1411,  -287,  -287,    70,    45,
     226,    67,  1411,  1411,  1411,    70,    70,    70,    70,   183,
    -287,   837,  -287,  -287,  -287,  -287,  -287,  -287,  -287,  -287,
    -287,  -287,  -287,  -287,  -287,   222,  -287,  -287,  -287,  -287,
      -8,   170,   250,  1411,  1411,  -287,     3,   712,   896,   -28,
    -287,  1411,    63,  -287,  -287,   831,  -287,  1411,  1323,  1367,
    1411,  -287,  -287,   205,  -287,  -287,  -287,  -287,  -287,  1411,
    1411,  1411,  1411,  1411,  1411,  1411,  1411,  1411,  1411,  1477,
    1477,  1477,  1477,  1477,  1477,  1477,  1477,  1477,  1477,  1477,
    -287,  -287,    76,  -287,  -287,  -287,  1455,  1411,  -287,  1411,
    -287,  -287,  -287,  -287,  1411,  1411,  -287,   712,  1250,  1411,
    -287,   219,  -287,   253,  1250,  1411,   712,   712,   417,  -287,
     478,  -287,  -287,  -287,  1411,  -287,  -287,  -287,  1279,   254,
     254,   -33,  1250,   712,  -287,  1411,  -287,  -287,  -287,    33,
     359,   177,   712,   108,  -287,   127,  -287,   139,   476,  -287,
     772,   831,   890,   890,   279,   279,   279,   279,   890,   890,
     445,  1176,  1509,   315,   315,   237,    31,    31,  -287,  -287,
    -287,   201,   190,  1411,   712,   712,   184,   185,   712,   712,
     541,   712,    67,  -287,   955,   712,   143,  1250,  1411,   360,
     265,   193,  -287,   712,  1411,  1411,  1411,  1411,  1292,    81,
    -287,  -287,   260,   260,  -287,    70,   194,  -287,   195,   144,
    -287,  -287,  1014,  -287,  -287,  -287,  1411,  -287,  -287,  -287,
    -287,  -287,   220,   712,  1455,  1455,  1250,  1411,   290,  -287,
    1250,  -287,    67,   204,    16,  -287,   419,   712,  1250,   718,
     712,   712,   712,   712,    81,  -287,    67,    15,  -287,   159,
    -287,   259,   221,   -21,  -287,   712,   223,   215,  -287,  -287,
     224,  1073,  1250,  -287,  1132,   -12,  1250,  -287,  -287,  1191,
    -287,   -11,  -287,  -287,  -287,    70,   282,   259,  -287,   236,
    -287,  -287,  -287,   241,  1411,  1411,   541,  -287,  1250,   600,
     275,  1250,  -287,   257,  -287,  -287,  -287,   248,   249,  -287,
     659,  1411,  -287,   778,  -287,  -287,  -287,   712
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      36,     0,    29,    30,     0,     0,     0,     0,     0,     0,
       2,    36,     6,    36,     9,     5,     0,    26,     0,    27,
       0,    28,    71,     0,    21,    22,     0,    31,    32,     0,
      15,     0,    49,     1,     8,     7,    36,    11,    36,     0,
      43,    38,    39,    40,     0,    37,     0,     0,    41,    36,
       0,   185,     0,     0,     0,     0,     0,   192,   191,   181,
     194,   182,   183,     0,     0,     0,   205,   206,     0,     0,
       0,     0,    35,   159,   162,   177,   188,   189,   184,   174,
     193,    13,     0,    18,     4,    36,    12,   104,   104,   104,
      97,    98,     0,     0,     0,     0,   220,    90,     0,     0,
       0,     0,     0,   215,     0,   135,   135,   135,     0,     0,
      82,    36,    72,    76,    77,    75,    88,    89,    74,   112,
     113,    83,    84,    81,    78,     0,   207,    79,    80,   114,
       0,     0,     0,    33,    45,    46,     0,    44,    36,     0,
      19,     0,     0,   196,   187,   148,   180,     0,     0,     0,
       0,   179,   178,     0,   186,   129,   131,   130,   132,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     190,    14,    36,   110,    92,   105,   106,   160,    93,   160,
      95,   126,   127,   128,     0,     0,   103,   217,    36,     0,
     212,   213,   124,     0,    36,     0,    91,   216,     0,   136,
      36,   134,   146,   147,    86,    24,    73,   208,     0,     0,
       0,    51,    36,    34,    42,    47,    48,    23,    16,     0,
       0,     0,   203,     0,   199,     0,   202,     0,     0,   175,
     157,   158,   149,   150,   151,   152,   153,   154,   155,   156,
     169,   170,   171,   172,   173,   164,   163,   165,   166,   167,
     168,     0,   182,   107,   109,   161,     0,     0,   101,   102,
      36,    85,     0,   210,    36,   214,     0,    36,     0,    36,
       0,   138,   139,    87,     0,     0,     0,     0,   222,   224,
     225,   221,     0,     0,    54,    63,     0,    52,     0,    57,
      53,    58,    36,    20,   195,   197,     0,   200,   198,   201,
     176,    10,     0,   108,   160,   160,    36,     0,     0,   125,
      36,   209,     0,     0,     0,   120,    36,   143,    36,    36,
     228,   230,   229,   227,   223,   226,     0,     0,   231,     0,
      62,    64,     0,     0,    25,   204,     0,     0,   100,    99,
       0,    36,    36,   115,    36,     0,    36,   119,   121,    36,
     140,     0,   219,   232,   218,     0,    69,    65,    66,    60,
      55,    61,    59,     0,   160,   160,    36,   211,    36,    36,
     144,    36,    68,     0,    50,    67,   111,     0,     0,   117,
      36,     0,   133,    36,    70,    94,    96,   145
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -287,  -287,  -287,    -2,    -4,  -287,   305,  -287,   171,  -287,
      -5,     4,  -287,     8,  -287,    23,   321,    24,  -287,   -37,
    -287,  -287,  -287,  -287,  -287,  -287,    12,    14,    17,  -287,
    -287,   -10,  -287,  -287,   -30,   -80,  -287,  -287,  -287,  -287,
      44,  -287,    91,  -287,  -287,   175,  -287,  -287,     1,  -287,
    -287,    47,  -250,   -95,   109,  -287,  -287,  -287,  -287,   162,
    -287,  -287,    43,  -287,  -287,   -29,  -177,    42,  -287,  -287,
    -287,   299,   -48,  -287,   -35,   102,  -287,  -287,  -287,   251,
    -287,  -287,  -287,  -287,    64,  -287,  -287,   160,  -287,   100,
    -269,    86,  -286
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     9,    10,    11,    12,    85,    13,    14,    84,   139,
      26,    15,    16,   209,    17,   109,    45,   110,    20,    48,
     136,    21,   132,   296,   297,   298,   299,   300,   301,   366,
     367,   368,   384,    23,   279,   112,   113,   114,   115,   116,
     347,   117,   184,   185,   186,   348,   118,   119,   318,   120,
     324,   325,   201,   193,   194,   167,   168,   121,   210,   211,
     280,   281,   282,   392,   122,   265,   349,    73,    74,    75,
      76,    77,    78,   144,    79,   233,    80,   124,   125,   126,
     320,   203,   127,   128,   198,   129,   130,   292,   220,   289,
     290,   337,   338
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      72,    30,    31,   123,   202,   143,   205,    34,   111,   135,
     266,    36,   267,    29,   123,    35,   218,   294,   137,   138,
     335,    22,    46,    18,    19,   145,   224,    32,    47,   371,
     183,   216,    34,   183,    18,    19,    18,    19,   362,   357,
      35,   153,   272,   272,   336,   140,   187,   131,    33,   189,
     -17,   363,   133,   363,   134,   295,   322,   323,   216,    18,
      19,     1,     2,     3,   228,   335,   197,   295,    56,   -56,
       4,   229,   355,   206,   207,   208,   123,    37,    63,    64,
      38,     5,    27,   182,    49,    27,   361,     1,     2,     3,
      28,   378,   381,    28,   231,   219,     4,   146,    50,   226,
     284,   285,   286,   123,   223,   137,   199,     5,    18,    19,
     151,   152,   230,    24,    52,    25,   214,     6,   232,   232,
     232,   238,    83,     8,   287,   191,   177,   178,   179,   192,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     200,    39,    41,     6,   225,    40,    60,   191,    83,     8,
       7,   192,    27,    66,    67,   -17,    41,   264,    42,    43,
      28,    52,   154,   123,    57,   268,   269,    58,   270,   123,
     271,   141,    42,    43,   274,   123,   275,   319,    34,   188,
     190,    81,   364,   322,   323,   283,    35,   123,   336,   288,
     216,    44,   302,    60,   216,   147,   137,   387,   388,   216,
      66,    67,   195,   196,   148,    18,    19,   306,   181,   307,
     142,   250,   251,   252,   253,   254,   255,   256,   257,   258,
     259,   260,   216,   149,   303,    40,   306,   202,   308,   150,
     155,   156,   157,   158,   313,   123,    41,   183,   306,   123,
     309,   202,   123,   343,   123,   -60,   216,   326,   204,   327,
     235,   237,    42,    43,    99,   330,   331,   332,   333,   159,
     160,   161,   162,   163,   164,   165,   166,   123,   212,   213,
     221,   216,   222,   272,   216,   273,   291,   345,   305,   216,
     311,   123,   312,   314,   315,   123,   351,   328,   197,   336,
     354,   123,   329,   123,   123,   341,   216,   342,   359,   216,
     169,   170,   171,   340,   346,   353,   239,   356,   365,   295,
     216,   172,   173,   216,   374,   373,   123,   123,   383,   123,
     391,   123,   376,   375,   123,   386,   379,     1,     2,     3,
     175,   176,   177,   178,   179,   343,     4,  -234,  -234,  -234,
    -234,   123,    86,   123,   123,   394,   123,     5,   390,   395,
     396,   393,    82,   261,   369,   123,   370,   385,   123,   350,
     372,   263,   397,    87,    88,    89,    90,    91,    92,    93,
      94,   358,   360,   382,    95,   180,   217,   389,    96,   339,
     293,   352,  -141,     6,   155,   156,   157,   158,   334,     8,
      97,    98,    99,   100,   101,   102,     0,   103,     0,   104,
       0,     0,     0,     0,   105,     0,   106,   107,   175,   176,
     177,   178,   179,   159,   160,   161,   162,   163,   164,   165,
     166,     0,    87,    88,    89,    90,    91,    92,    93,    94,
       0,     0,     0,    95,     0,     0,    56,    96,     0,   276,
       0,  -142,   155,   156,   157,   158,    63,    64,   108,    97,
      98,    99,   100,   101,   102,     0,   103,     0,   104,  -141,
     304,     0,     0,   105,     0,   106,   107,     0,     0,     0,
       0,   159,   160,   161,   162,   163,   164,   165,   166,     0,
       0,    87,    88,    89,    90,    91,    92,    93,    94,     0,
       0,     0,    95,     0,     0,    56,    96,     0,     0,     0,
    -137,   155,   156,   157,   158,    63,    64,   108,    97,    98,
      99,   100,   101,   102,     0,   103,     0,   104,  -142,   172,
     173,   277,   105,   278,   106,   107,     0,     0,     0,     0,
     159,   160,   161,   162,   163,   164,   165,   166,   175,   176,
     177,   178,   179,     0,    87,    88,    89,    90,    91,    92,
      93,    94,     0,     0,    56,    95,  -116,   316,   317,    96,
       0,     0,     0,     0,    63,    64,   108,     0,     0,     0,
       0,    97,    98,    99,   100,   101,   102,   310,   103,     0,
     104,    -3,     0,     0,     0,   105,     0,   106,   107,     0,
       0,     0,     1,     2,     3,     0,     0,     0,     0,     0,
       0,     4,     0,    87,    88,    89,    90,    91,    92,    93,
      94,     0,     5,     0,    95,     0,     0,    56,    96,     0,
       0,     0,     0,  -123,     0,     0,     0,    63,    64,   108,
      97,    98,    99,   100,   101,   102,     0,   103,     0,   104,
    -123,  -123,     0,     0,   105,     0,   106,   107,     6,     0,
       0,     1,     2,     3,     8,     0,     0,     0,     0,     0,
       4,     0,    87,    88,    89,    90,    91,    92,    93,    94,
       0,     5,     0,    95,     0,     0,    56,    96,     0,     0,
       0,     0,  -122,     0,     0,     0,    63,    64,   108,    97,
      98,    99,   100,   101,   102,     0,   103,     0,   104,  -122,
    -122,     0,     0,   105,     0,   106,   107,     6,     0,     0,
       0,     7,     0,     8,     0,     0,     0,     0,     0,     0,
       0,    87,    88,    89,    90,    91,    92,    93,    94,     0,
       0,     0,    95,     0,     0,    56,    96,   155,   156,   157,
     158,     0,     0,     0,     0,    63,    64,   108,    97,    98,
      99,   100,   101,   102,     0,   103,     0,   104,     0,     0,
       0,   277,   105,   278,   106,   107,   159,   160,   161,   162,
     163,   164,   165,   166,     0,     0,     0,     0,     0,     0,
       0,    87,    88,    89,    90,    91,    92,    93,    94,     0,
       0,     0,    95,     0,    56,     0,    96,   155,   156,   157,
     158,  -233,     0,     0,    63,    64,   108,  -233,    97,    98,
      99,   100,   101,   102,     0,   103,     0,   104,     0,     0,
       0,     0,   105,     0,   106,   107,     0,   160,   161,   162,
     163,   164,   165,   166,     0,     0,     0,     0,     0,     0,
      87,    88,    89,    90,    91,    92,    93,    94,     0,     0,
       0,    95,     0,     0,    56,    96,   155,   156,   157,   158,
     215,     0,     0,     0,    63,    64,   108,    97,    98,    99,
     100,   101,   102,     0,   103,     0,   104,     0,     0,     0,
       0,   105,     0,   106,   107,     0,     0,   161,   162,   163,
     164,   165,   166,     0,     0,     0,     0,     0,     0,    87,
      88,    89,    90,    91,    92,    93,    94,     0,     0,     0,
      95,     0,     0,    56,    96,  -234,  -234,  -234,  -234,   227,
       0,     0,     0,    63,    64,   108,    97,    98,    99,   100,
     101,   102,     0,   103,     0,   104,     0,     0,     0,     0,
     105,     0,   106,   107,     0,     0,  -234,  -234,   163,   164,
     165,   166,     0,     0,     0,     0,     0,     0,    87,    88,
      89,    90,    91,    92,    93,    94,     0,     0,     0,    95,
       0,     0,    56,    96,     0,     0,     0,     0,   321,     0,
       0,     0,    63,    64,   108,    97,    98,    99,   100,   101,
     102,     0,   103,     0,   104,     0,     0,     0,     0,   105,
       0,   106,   107,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    87,    88,    89,
      90,    91,    92,    93,    94,     0,     0,     0,    95,     0,
       0,    56,    96,     0,     0,     0,     0,   344,     0,     0,
       0,    63,    64,   108,    97,    98,    99,   100,   101,   102,
       0,   103,     0,   104,     0,     0,     0,     0,   105,     0,
     106,   107,     0,   169,   170,   171,     0,     0,     0,     0,
       0,     0,     0,     0,   172,   173,    87,    88,    89,    90,
      91,    92,    93,    94,     0,     0,     0,    95,  -118,     0,
      56,    96,   174,   175,   176,   177,   178,   179,     0,     0,
      63,    64,   108,    97,    98,    99,   100,   101,   102,     0,
     103,     0,   104,     0,     0,     0,     0,   105,     0,   106,
     107,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    87,    88,    89,    90,    91,
      92,    93,    94,     0,     0,     0,    95,     0,     0,    56,
      96,     0,     0,     0,     0,   377,     0,     0,     0,    63,
      64,   108,    97,    98,    99,   100,   101,   102,     0,   103,
       0,   104,     0,     0,     0,     0,   105,     0,   106,   107,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    87,    88,    89,    90,    91,    92,
      93,    94,     0,     0,     0,    95,     0,     0,    56,    96,
       0,     0,     0,     0,   380,     0,     0,     0,    63,    64,
     108,    97,    98,    99,   100,   101,   102,     0,   103,     0,
     104,     0,     0,     0,     0,   105,     0,   106,   107,   169,
       0,   171,     0,     0,     0,     0,     0,     0,     0,     0,
     172,   173,     0,    87,    88,    89,    90,    91,    92,    93,
      94,     0,     0,     0,    95,     0,     0,    56,    96,   175,
     176,   177,   178,   179,     0,     0,     0,    63,    64,   108,
      97,    98,    99,   100,   101,   102,     0,   103,     0,   104,
       0,     0,     0,     0,   105,     0,   106,   107,   284,   285,
     286,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   284,   285,   286,     0,     0,     0,   155,   156,   157,
     158,     0,   287,     0,     0,     0,    56,    51,     0,     0,
      52,    53,     0,     0,     0,   287,    63,    64,   108,     0,
       0,    54,     0,     0,     0,    55,   159,   160,   161,   162,
     163,   164,   165,   166,     0,    56,    57,     0,     0,    58,
      59,     0,    60,    61,    62,    63,    64,     0,    65,    66,
      67,    51,    68,    69,    52,    53,     0,     0,     0,    70,
       0,     0,     0,    71,     0,    54,     0,     0,     0,    55,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    56,
      57,     0,     0,    58,    59,     0,    60,    61,    62,    63,
      64,     0,    65,    66,    67,    51,    68,    69,    52,    53,
       0,     0,     0,    70,   234,     0,     0,    71,     0,    54,
       0,     0,     0,    55,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    56,    57,     0,     0,    58,    59,     0,
      60,    61,    62,    63,    64,     0,    65,    66,    67,    51,
      68,    69,    52,    53,     0,     0,     0,    70,   236,     0,
       0,    71,     0,    54,     0,     0,     0,    55,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    56,    57,     0,
       0,    58,    59,     0,    60,    61,    62,    63,    64,     0,
      65,    66,    67,    51,    68,    69,    52,    53,     0,     0,
       0,    70,     0,     0,     0,    71,     0,    54,     0,     0,
       0,    55,     0,     0,     0,    51,     0,     0,    52,    53,
       0,    56,    57,     0,     0,    58,    59,     0,    60,   262,
      62,    63,    64,    55,    65,    66,    67,     0,    68,    69,
       0,     0,     0,    56,    57,    70,     0,    58,    59,    71,
      60,    61,    62,    63,    64,     0,    65,    66,    67,     0,
      68,    69,   169,     0,     0,     0,     0,    70,     0,     0,
       0,    71,     0,   172,   173,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   175,   176,   177,   178,   179
};

static const yytype_int16 yycheck[] =
{
      29,     6,     7,    38,    99,    53,   101,    11,    38,    46,
     187,    13,   189,     5,    49,    11,    24,    50,    47,    49,
     289,    82,    22,     0,     0,    54,    23,    82,    20,    50,
      84,   111,    36,    84,    11,    11,    13,    13,    23,    23,
      36,    70,    54,    54,    29,    50,   100,    39,     0,   100,
       0,   337,    44,   339,    46,    88,    40,    41,   138,    36,
      36,    11,    12,    13,    92,   334,    95,    88,    76,   102,
      20,    99,   322,   102,   103,   104,   111,    78,    86,    87,
      22,    31,    82,    85,    22,    82,   336,    11,    12,    13,
      90,   103,   103,    90,   142,   130,    20,    55,    68,   136,
      19,    20,    21,   138,   133,   134,    98,    31,    85,    85,
      68,    69,   141,    80,    51,    82,   108,    67,   147,   148,
     149,   150,    72,    73,    43,    80,    95,    96,    97,    84,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
      95,    38,    53,    67,   136,    42,    83,    80,    72,    73,
      71,    84,    82,    90,    91,    79,    53,   186,    69,    70,
      90,    51,    90,   198,    77,   194,   195,    80,   198,   204,
     199,   100,    69,    70,   204,   210,   205,   272,   182,    88,
      89,    92,    23,    40,    41,   214,   182,   222,    29,   218,
     270,    88,   222,    83,   274,   100,   225,   374,   375,   279,
      90,    91,    93,    94,   100,   182,   182,    99,    92,   101,
     100,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   302,   100,   229,    42,    99,   322,   101,   100,
      25,    26,    27,    28,   263,   270,    53,    84,    99,   274,
     101,   336,   277,    99,   279,   101,   326,   277,    22,   278,
     148,   149,    69,    70,    32,   284,   285,   286,   287,    54,
      55,    56,    57,    58,    59,    60,    61,   302,   106,   107,
     100,   351,    22,    54,   354,    22,    22,   306,   101,   359,
      79,   316,    92,    99,    99,   320,   316,    22,   317,    29,
     320,   326,    99,   328,   329,   101,   376,   102,   328,   379,
      63,    64,    65,   295,    84,    15,   101,   103,    49,    88,
     390,    74,    75,   393,    99,    92,   351,   352,    36,   354,
      45,   356,   352,    99,   359,    84,   356,    11,    12,    13,
      93,    94,    95,    96,    97,    99,    20,    58,    59,    60,
      61,   376,    37,   378,   379,    88,   381,    31,   378,   101,
     101,   381,    31,   182,   342,   390,   342,   367,   393,   315,
     343,   186,   391,     3,     4,     5,     6,     7,     8,     9,
      10,   324,   329,   365,    14,    76,   125,   376,    18,   293,
     220,   317,    22,    67,    25,    26,    27,    28,   288,    73,
      30,    31,    32,    33,    34,    35,    -1,    37,    -1,    39,
      -1,    -1,    -1,    -1,    44,    -1,    46,    47,    93,    94,
      95,    96,    97,    54,    55,    56,    57,    58,    59,    60,
      61,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      -1,    -1,    -1,    14,    -1,    -1,    76,    18,    -1,    22,
      -1,    22,    25,    26,    27,    28,    86,    87,    88,    30,
      31,    32,    33,    34,    35,    -1,    37,    -1,    39,    99,
     101,    -1,    -1,    44,    -1,    46,    47,    -1,    -1,    -1,
      -1,    54,    55,    56,    57,    58,    59,    60,    61,    -1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    14,    -1,    -1,    76,    18,    -1,    -1,    -1,
      22,    25,    26,    27,    28,    86,    87,    88,    30,    31,
      32,    33,    34,    35,    -1,    37,    -1,    39,    99,    74,
      75,    43,    44,    45,    46,    47,    -1,    -1,    -1,    -1,
      54,    55,    56,    57,    58,    59,    60,    61,    93,    94,
      95,    96,    97,    -1,     3,     4,     5,     6,     7,     8,
       9,    10,    -1,    -1,    76,    14,    15,    16,    17,    18,
      -1,    -1,    -1,    -1,    86,    87,    88,    -1,    -1,    -1,
      -1,    30,    31,    32,    33,    34,    35,   101,    37,    -1,
      39,     0,    -1,    -1,    -1,    44,    -1,    46,    47,    -1,
      -1,    -1,    11,    12,    13,    -1,    -1,    -1,    -1,    -1,
      -1,    20,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    -1,    31,    -1,    14,    -1,    -1,    76,    18,    -1,
      -1,    -1,    -1,    23,    -1,    -1,    -1,    86,    87,    88,
      30,    31,    32,    33,    34,    35,    -1,    37,    -1,    39,
      40,    41,    -1,    -1,    44,    -1,    46,    47,    67,    -1,
      -1,    11,    12,    13,    73,    -1,    -1,    -1,    -1,    -1,
      20,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      -1,    31,    -1,    14,    -1,    -1,    76,    18,    -1,    -1,
      -1,    -1,    23,    -1,    -1,    -1,    86,    87,    88,    30,
      31,    32,    33,    34,    35,    -1,    37,    -1,    39,    40,
      41,    -1,    -1,    44,    -1,    46,    47,    67,    -1,    -1,
      -1,    71,    -1,    73,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    14,    -1,    -1,    76,    18,    25,    26,    27,
      28,    -1,    -1,    -1,    -1,    86,    87,    88,    30,    31,
      32,    33,    34,    35,    -1,    37,    -1,    39,    -1,    -1,
      -1,    43,    44,    45,    46,    47,    54,    55,    56,    57,
      58,    59,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    14,    -1,    76,    -1,    18,    25,    26,    27,
      28,    23,    -1,    -1,    86,    87,    88,    29,    30,    31,
      32,    33,    34,    35,    -1,    37,    -1,    39,    -1,    -1,
      -1,    -1,    44,    -1,    46,    47,    -1,    55,    56,    57,
      58,    59,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,
       3,     4,     5,     6,     7,     8,     9,    10,    -1,    -1,
      -1,    14,    -1,    -1,    76,    18,    25,    26,    27,    28,
      23,    -1,    -1,    -1,    86,    87,    88,    30,    31,    32,
      33,    34,    35,    -1,    37,    -1,    39,    -1,    -1,    -1,
      -1,    44,    -1,    46,    47,    -1,    -1,    56,    57,    58,
      59,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,     3,
       4,     5,     6,     7,     8,     9,    10,    -1,    -1,    -1,
      14,    -1,    -1,    76,    18,    25,    26,    27,    28,    23,
      -1,    -1,    -1,    86,    87,    88,    30,    31,    32,    33,
      34,    35,    -1,    37,    -1,    39,    -1,    -1,    -1,    -1,
      44,    -1,    46,    47,    -1,    -1,    56,    57,    58,    59,
      60,    61,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,
       5,     6,     7,     8,     9,    10,    -1,    -1,    -1,    14,
      -1,    -1,    76,    18,    -1,    -1,    -1,    -1,    23,    -1,
      -1,    -1,    86,    87,    88,    30,    31,    32,    33,    34,
      35,    -1,    37,    -1,    39,    -1,    -1,    -1,    -1,    44,
      -1,    46,    47,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,
       6,     7,     8,     9,    10,    -1,    -1,    -1,    14,    -1,
      -1,    76,    18,    -1,    -1,    -1,    -1,    23,    -1,    -1,
      -1,    86,    87,    88,    30,    31,    32,    33,    34,    35,
      -1,    37,    -1,    39,    -1,    -1,    -1,    -1,    44,    -1,
      46,    47,    -1,    63,    64,    65,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    74,    75,     3,     4,     5,     6,
       7,     8,     9,    10,    -1,    -1,    -1,    14,    15,    -1,
      76,    18,    92,    93,    94,    95,    96,    97,    -1,    -1,
      86,    87,    88,    30,    31,    32,    33,    34,    35,    -1,
      37,    -1,    39,    -1,    -1,    -1,    -1,    44,    -1,    46,
      47,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,     7,
       8,     9,    10,    -1,    -1,    -1,    14,    -1,    -1,    76,
      18,    -1,    -1,    -1,    -1,    23,    -1,    -1,    -1,    86,
      87,    88,    30,    31,    32,    33,    34,    35,    -1,    37,
      -1,    39,    -1,    -1,    -1,    -1,    44,    -1,    46,    47,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     3,     4,     5,     6,     7,     8,
       9,    10,    -1,    -1,    -1,    14,    -1,    -1,    76,    18,
      -1,    -1,    -1,    -1,    23,    -1,    -1,    -1,    86,    87,
      88,    30,    31,    32,    33,    34,    35,    -1,    37,    -1,
      39,    -1,    -1,    -1,    -1,    44,    -1,    46,    47,    63,
      -1,    65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      74,    75,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    -1,    -1,    -1,    14,    -1,    -1,    76,    18,    93,
      94,    95,    96,    97,    -1,    -1,    -1,    86,    87,    88,
      30,    31,    32,    33,    34,    35,    -1,    37,    -1,    39,
      -1,    -1,    -1,    -1,    44,    -1,    46,    47,    19,    20,
      21,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    19,    20,    21,    -1,    -1,    -1,    25,    26,    27,
      28,    -1,    43,    -1,    -1,    -1,    76,    48,    -1,    -1,
      51,    52,    -1,    -1,    -1,    43,    86,    87,    88,    -1,
      -1,    62,    -1,    -1,    -1,    66,    54,    55,    56,    57,
      58,    59,    60,    61,    -1,    76,    77,    -1,    -1,    80,
      81,    -1,    83,    84,    85,    86,    87,    -1,    89,    90,
      91,    48,    93,    94,    51,    52,    -1,    -1,    -1,   100,
      -1,    -1,    -1,   104,    -1,    62,    -1,    -1,    -1,    66,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    76,
      77,    -1,    -1,    80,    81,    -1,    83,    84,    85,    86,
      87,    -1,    89,    90,    91,    48,    93,    94,    51,    52,
      -1,    -1,    -1,   100,   101,    -1,    -1,   104,    -1,    62,
      -1,    -1,    -1,    66,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    76,    77,    -1,    -1,    80,    81,    -1,
      83,    84,    85,    86,    87,    -1,    89,    90,    91,    48,
      93,    94,    51,    52,    -1,    -1,    -1,   100,   101,    -1,
      -1,   104,    -1,    62,    -1,    -1,    -1,    66,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    76,    77,    -1,
      -1,    80,    81,    -1,    83,    84,    85,    86,    87,    -1,
      89,    90,    91,    48,    93,    94,    51,    52,    -1,    -1,
      -1,   100,    -1,    -1,    -1,   104,    -1,    62,    -1,    -1,
      -1,    66,    -1,    -1,    -1,    48,    -1,    -1,    51,    52,
      -1,    76,    77,    -1,    -1,    80,    81,    -1,    83,    84,
      85,    86,    87,    66,    89,    90,    91,    -1,    93,    94,
      -1,    -1,    -1,    76,    77,   100,    -1,    80,    81,   104,
      83,    84,    85,    86,    87,    -1,    89,    90,    91,    -1,
      93,    94,    63,    -1,    -1,    -1,    -1,   100,    -1,    -1,
      -1,   104,    -1,    74,    75,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    93,    94,    95,    96,    97
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    11,    12,    13,    20,    31,    67,    71,    73,   106,
     107,   108,   109,   111,   112,   116,   117,   119,   120,   122,
     123,   126,    82,   138,    80,    82,   115,    82,    90,   118,
     115,   115,    82,     0,   109,   116,   108,    78,    22,    38,
      42,    53,    69,    70,    88,   121,    22,   118,   124,    22,
      68,    48,    51,    52,    62,    66,    76,    77,    80,    81,
      83,    84,    85,    86,    87,    89,    90,    91,    93,    94,
     100,   104,   170,   172,   173,   174,   175,   176,   177,   179,
     181,    92,   121,    72,   113,   110,   111,     3,     4,     5,
       6,     7,     8,     9,    10,    14,    18,    30,    31,    32,
      33,    34,    35,    37,    39,    44,    46,    47,    88,   120,
     122,   139,   140,   141,   142,   143,   144,   146,   151,   152,
     154,   162,   169,   179,   182,   183,   184,   187,   188,   190,
     191,   118,   127,   118,   118,   124,   125,   170,   139,   114,
     115,   100,   100,   177,   178,   170,   172,   100,   100,   100,
     100,   172,   172,   170,    90,    25,    26,    27,    28,    54,
      55,    56,    57,    58,    59,    60,    61,   160,   161,    63,
      64,    65,    74,    75,    92,    93,    94,    95,    96,    97,
     176,    92,   108,    84,   147,   148,   149,   100,   147,   100,
     147,    80,    84,   158,   159,   159,   159,   170,   189,   118,
      95,   157,   158,   186,    22,   158,   170,   170,   170,   118,
     163,   164,   164,   164,   118,    23,   140,   184,    24,   179,
     193,   100,    22,   170,    23,   118,   124,    23,    92,    99,
     170,   177,   170,   180,   101,   180,   101,   180,   170,   101,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     172,   172,   172,   172,   172,   172,   172,   172,   172,   172,
     172,   113,    84,   150,   170,   170,   171,   171,   170,   170,
     139,   170,    54,    22,   139,   170,    22,    43,    45,   139,
     165,   166,   167,   170,    19,    20,    21,    43,   170,   194,
     195,    22,   192,   192,    50,    88,   128,   129,   130,   131,
     132,   133,   139,   115,   101,   101,    99,   101,   101,   101,
     101,    79,    92,   170,    99,    99,    16,    17,   153,   158,
     185,    23,    40,    41,   155,   156,   139,   170,    22,    99,
     170,   170,   170,   170,   194,   195,    29,   196,   197,   196,
     118,   101,   102,    99,    23,   170,    84,   145,   150,   171,
     145,   139,   189,    15,   139,   157,   103,    23,   156,   139,
     167,   157,    23,   197,    23,    49,   134,   135,   136,   131,
     132,    50,   133,    92,    99,    99,   139,    23,   103,   139,
      23,   103,   118,    36,   137,   136,    84,   171,   171,   153,
     139,    45,   168,   139,    88,   101,   101,   170
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   105,   106,   107,   107,   108,   108,   108,   108,   109,
     109,   110,   110,   111,   111,   112,   112,   113,   113,   114,
     114,   115,   115,   116,   116,   116,   116,   116,   116,   117,
     117,   118,   118,   119,   119,   119,   120,   120,   121,   121,
     121,   122,   122,   123,   124,   125,   125,   125,   125,   126,
     127,   128,   128,   129,   129,   129,   130,   130,   131,   131,
     132,   132,   133,   133,   134,   134,   135,   135,   136,   137,
     137,   138,   139,   139,   140,   140,   140,   140,   140,   140,
     140,   140,   140,   140,   140,   141,   142,   142,   143,   143,
     143,   143,   144,   144,   144,   144,   144,   144,   144,   145,
     145,   146,   146,   146,   147,   147,   148,   148,   148,   148,
     149,   150,   151,   151,   151,   152,   153,   153,   153,   154,
     155,   155,   156,   156,   157,   157,   158,   158,   159,   160,
     160,   161,   161,   162,   163,   164,   164,   165,   165,   166,
     166,   167,   167,   167,   168,   168,   169,   169,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     171,   171,   172,   172,   172,   172,   172,   172,   172,   172,
     172,   172,   172,   172,   173,   173,   173,   173,   173,   173,
     173,   174,   174,   174,   174,   174,   174,   174,   174,   175,
     175,   176,   176,   177,   177,   177,   178,   178,   179,   179,
     179,   179,   179,   180,   180,   181,   181,   182,   182,   183,
     185,   184,   186,   186,   187,   188,   188,   189,   190,   190,
     191,   192,   193,   193,   193,   194,   194,   195,   195,   195,
     195,   196,   196,   197
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     3,     1,     1,     2,     2,     1,
       6,     0,     1,     3,     4,     2,     5,     0,     1,     1,
       3,     1,     1,     5,     4,     6,     1,     1,     1,     1,
       1,     1,     1,     3,     4,     3,     0,     2,     1,     1,
       1,     2,     4,     2,     2,     1,     1,     2,     2,     2,
       6,     0,     1,     1,     1,     3,     0,     1,     1,     3,
       1,     3,     2,     1,     0,     1,     1,     2,     2,     0,
       2,     1,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     3,     2,     3,     1,     1,
       1,     2,     2,     2,     8,     2,     8,     1,     1,     1,
       1,     3,     3,     2,     0,     1,     1,     2,     3,     2,
       1,     5,     1,     1,     1,     5,     0,     4,     2,     5,
       1,     2,     4,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     7,     1,     0,     1,     0,     1,     1,
       3,     1,     2,     2,     0,     2,     2,     2,     2,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     1,
       0,     1,     1,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     1,     3,     4,     1,     2,     2,
       2,     1,     1,     1,     1,     1,     2,     2,     1,     1,
       2,     1,     1,     1,     1,     4,     1,     3,     4,     3,
       4,     4,     3,     1,     3,     1,     1,     1,     2,     4,
       0,     6,     1,     1,     3,     1,     2,     1,     5,     5,
       1,     1,     2,     3,     2,     1,     2,     2,     2,     2,
       2,     1,     2,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Location data for the lookahead symbol.  */
YYLTYPE yylloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 488 "gram.y" /* yacc.c:1646  */
    {
                     if (error_count == 0) {
                             if (genstmt.head) {
                                     genstmt.tail->next = (yyvsp[0].stmtlist).head;
                                     genstmt.tail = (yyvsp[0].stmtlist).tail;
                             } else
                                     genstmt = (yyvsp[0].stmtlist);
                             optimize_tree(genstmt.head);
                             if (error_count)
                                     YYERROR;
                             mark(genstmt.head);
                             if (error_count)
                                     YYERROR;
                             apply_deferred_init();
			     fixup_exceptions();
                             dataseg_layout();
                             if (optimization_level)
                                     regex_layout();
                             compile_tree(genstmt.head);
                             if (!optimization_level)
                                     regex_layout();
                     }
             }
#line 2343 "gram.c" /* yacc.c:1646  */
    break;

  case 4:
#line 515 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist) = (yyvsp[-1].stmtlist);
             }
#line 2351 "gram.c" /* yacc.c:1646  */
    break;

  case 5:
#line 521 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist).head = (yyval.stmtlist).tail = (yyvsp[0].node);
             }
#line 2359 "gram.c" /* yacc.c:1646  */
    break;

  case 7:
#line 526 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].node)) {
                             if ((yyvsp[-1].stmtlist).tail) 
                                     (yyvsp[-1].stmtlist).tail->next = (yyvsp[0].node);
                             else
                                     (yyvsp[-1].stmtlist).head = (yyvsp[0].node);
                             (yyvsp[-1].stmtlist).tail = (yyvsp[0].node);
                     }
                     (yyval.stmtlist) = (yyvsp[-1].stmtlist);
             }
#line 2374 "gram.c" /* yacc.c:1646  */
    break;

  case 8:
#line 537 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].stmtlist).tail) {
                             if ((yyvsp[-1].stmtlist).tail) {
                                     (yyval.stmtlist).head = (yyvsp[-1].stmtlist).head;
                                     (yyvsp[-1].stmtlist).tail->next = (yyvsp[0].stmtlist).head;
                                     (yyval.stmtlist).tail = (yyvsp[0].stmtlist).tail;
                             } else
                                     (yyval.stmtlist) = (yyvsp[0].stmtlist);
                     } 
             }
#line 2389 "gram.c" /* yacc.c:1646  */
    break;

  case 9:
#line 550 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist).head = (yyval.stmtlist).tail = NULL;
             }
#line 2397 "gram.c" /* yacc.c:1646  */
    break;

  case 10:
#line 554 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist) = (yyvsp[-2].stmtlist);
                     pop_top_module();
             }
#line 2406 "gram.c" /* yacc.c:1646  */
    break;

  case 11:
#line 561 "gram.y" /* yacc.c:1646  */
    {
                     parse_warning(_("missing module declaration"));
             }
#line 2414 "gram.c" /* yacc.c:1646  */
    break;

  case 13:
#line 568 "gram.y" /* yacc.c:1646  */
    {
		     if (top_module->dclname) {
			     struct mu_locus_range lr;
			     lr.beg = (yylsp[-2]).beg;
			     lr.end = (yylsp[0]).end;
                             parse_error_locus(&lr,
                                               _("duplicate module declaration"));
		     } else
			     top_module->dclname = (yyvsp[-1].literal)->text;
	     }
#line 2429 "gram.c" /* yacc.c:1646  */
    break;

  case 14:
#line 579 "gram.y" /* yacc.c:1646  */
    {
		     if (top_module->dclname) {
			     struct mu_locus_range lr;
			     lr.beg = (yylsp[-3]).beg;
			     lr.end = (yylsp[0]).end;
                             parse_error_locus(&lr,
                                               _("duplicate module declaration"));
		     } else {
			     if (((yyvsp[-1].number) & (SYM_STATIC|SYM_PUBLIC)) == 0)
                                     parse_error_locus(&(yylsp[-1]),
                                                       _("invalid module declaration"));
			     top_module->dclname = (yyvsp[-2].literal)->text;
			     top_module->flags = (yyvsp[-1].number);
		     }
	     }
#line 2449 "gram.c" /* yacc.c:1646  */
    break;

  case 15:
#line 597 "gram.y" /* yacc.c:1646  */
    {
                     require_module((yyvsp[0].literal)->text, NULL);
             }
#line 2457 "gram.c" /* yacc.c:1646  */
    break;

  case 16:
#line 601 "gram.y" /* yacc.c:1646  */
    {
                     require_module((yyvsp[-3].literal)->text, (yyvsp[-1].import_rule_list).head);
             }
#line 2465 "gram.c" /* yacc.c:1646  */
    break;

  case 18:
#line 608 "gram.y" /* yacc.c:1646  */
    {
                     lex_bye();
             }
#line 2473 "gram.c" /* yacc.c:1646  */
    break;

  case 19:
#line 614 "gram.y" /* yacc.c:1646  */
    {
                     struct import_rule *rule = import_rule_create((yyvsp[0].literal));
                     (yyval.import_rule_list).head = (yyval.import_rule_list).tail = rule;
             }
#line 2482 "gram.c" /* yacc.c:1646  */
    break;

  case 20:
#line 619 "gram.y" /* yacc.c:1646  */
    {
                     struct import_rule *rule = import_rule_create((yyvsp[0].literal));
                     (yyval.import_rule_list).tail->next = rule;
                     (yyval.import_rule_list).tail = rule;
             }
#line 2492 "gram.c" /* yacc.c:1646  */
    break;

  case 23:
#line 631 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_progdecl, &(yylsp[-4]));
		     (yyval.node)->v.progdecl.tag = (yyvsp[-3].state);
		     (yyval.node)->v.progdecl.auto_count = forget_autos(PARMCOUNT(),
							      0, 0);
		     (yyval.node)->v.progdecl.tree = (yyvsp[-1].stmtlist).head;
		     
		     outer_context = inner_context = context_none;
		     state_tag = smtp_state_none;
	     }
#line 2507 "gram.c" /* yacc.c:1646  */
    break;

  case 24:
#line 642 "gram.y" /* yacc.c:1646  */
    {
		     static NODE *progspecial[2];
		     NODE *np = progspecial[(yyvsp[-3].progspecial).code];
		     
		     if (!np) {
                             np = alloc_node(node_type_progdecl, &(yylsp[-3]));
			     (yyval.node) = progspecial[(yyvsp[-3].progspecial).code] = np;
			     np->v.progdecl.tag = state_tag;
			     np->v.progdecl.tree = (yyvsp[-1].stmtlist).head;
			     np->v.progdecl.auto_count = 0;
		     } else {
			     NODE *cur;
			     for (cur = np->v.progdecl.tree; cur->next;
				  cur = cur->next)
				     ;
			     cur->next = (yyvsp[-1].stmtlist).head;
			     (yyval.node) = NULL;
		     }
				     
		     np->v.progdecl.auto_count = forget_autos(PARMCOUNT(),
							      np->v.progdecl.auto_count,
							      0);
		     outer_context = inner_context = context_none;
		     state_tag = smtp_state_none;
	     }
#line 2537 "gram.c" /* yacc.c:1646  */
    break;

  case 25:
#line 668 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[-5].number) & SYM_PRECIOUS)
                             parse_error_locus(&(yylsp[-4]),
					       _("`precious' used with func"));
		     if (((yyvsp[-5].number) & (SYM_STATIC|SYM_PUBLIC)) ==
			 (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-4]),
					       _("`static' and `public' "
						 "used together"));
		     func->sym.flags = (yyvsp[-5].number);
		     func->node = (yyvsp[-1].stmtlist).head;
                     (yyval.node) = declare_function(func, &(yylsp[-4]),
			   forget_autos(PARMCOUNT() + FUNC_HIDDEN_ARGS(func),
					0, FUNC_HIDDEN_ARGS(func)));
		     outer_context = inner_context = context_none;
		     func = NULL;
	     }
#line 2559 "gram.c" /* yacc.c:1646  */
    break;

  case 26:
#line 686 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = NULL;
	     }
#line 2567 "gram.c" /* yacc.c:1646  */
    break;

  case 27:
#line 690 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = NULL;
	     }
#line 2575 "gram.c" /* yacc.c:1646  */
    break;

  case 28:
#line 694 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = NULL;
	     }
#line 2583 "gram.c" /* yacc.c:1646  */
    break;

  case 29:
#line 700 "gram.y" /* yacc.c:1646  */
    {
		     state_tag = smtp_state_begin;
		     outer_context = inner_context = context_handler;
		     (yyval.progspecial).code = PS_BEGIN;
	     }
#line 2593 "gram.c" /* yacc.c:1646  */
    break;

  case 30:
#line 706 "gram.y" /* yacc.c:1646  */
    {
		     state_tag = smtp_state_end;
		     outer_context = inner_context = context_handler;
		     (yyval.progspecial).code = PS_END;
	     }
#line 2603 "gram.c" /* yacc.c:1646  */
    break;

  case 32:
#line 715 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.literal) = literal_lookup((yyvsp[0].var)->sym.name);
	     }
#line 2611 "gram.c" /* yacc.c:1646  */
    break;

  case 33:
#line 721 "gram.y" /* yacc.c:1646  */
    {
		     struct variable *var;
		     
		     if (((yyvsp[-2].number) & (SYM_STATIC|SYM_PUBLIC))
			 == (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-2]),
                                               _("`static' and `public' "
						 "used together"));
		     var = vardecl((yyvsp[0].literal)->text, (yyvsp[-1].type), storage_extern, &(yylsp[0]));
		     if (!var) 
			     YYERROR;
		     var->sym.flags |= (yyvsp[-2].number);
	     }
#line 2629 "gram.c" /* yacc.c:1646  */
    break;

  case 34:
#line 735 "gram.y" /* yacc.c:1646  */
    {
                     struct value value;
                     struct variable *var;

		     if (((yyvsp[-3].number) & (SYM_STATIC|SYM_PUBLIC))
			 == (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-3]),
                                               _("`static' and `public' "
						 "used together"));
		     
                     var = vardecl((yyvsp[-1].literal)->text, (yyvsp[-2].type), storage_extern, &(yylsp[-2]));
                     if (!var)
                             YYERROR;
                     var->sym.flags |= (yyvsp[-3].number);

                     if (optimization_level)
                             optimize((yyvsp[0].node));
                     value.type = node_type((yyvsp[0].node));

                     switch ((yyvsp[0].node)->type) {
                     case node_type_string:
                             value.v.literal = (yyvsp[0].node)->v.literal;
                             break;

                     case node_type_number:
                             value.v.number = (yyvsp[0].node)->v.number;
                             break;

                     default:
                             yyerror("initializer element is not constant");
                             YYERROR;

		     }
		     if (initialize_variable(var, &value, &(yylsp[-1])))
			     YYERROR;
	     }
#line 2670 "gram.c" /* yacc.c:1646  */
    break;

  case 35:
#line 773 "gram.y" /* yacc.c:1646  */
    {
		     struct value value;
		     
		     if (optimization_level)
			     optimize((yyvsp[0].node));
		     value.type = node_type((yyvsp[0].node));
		     
		     switch ((yyvsp[0].node)->type) {
		     case node_type_string:
			     value.v.literal = (yyvsp[0].node)->v.literal;
			     break;

		     case node_type_number:
			     value.v.number = (yyvsp[0].node)->v.number;
			     break;

		     default:
			     yyerror("initializer element is not constant");
			     YYERROR;
		     }
                     if (!externdecl((yyvsp[-1].literal)->text, &value, &(yylsp[-1])))
			     YYERROR;
	     }
#line 2698 "gram.c" /* yacc.c:1646  */
    break;

  case 36:
#line 799 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = 0;
             }
#line 2706 "gram.c" /* yacc.c:1646  */
    break;

  case 37:
#line 803 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[-1].number) & (yyvsp[0].number))
                             parse_warning(_("duplicate `%s'"),
                                           symbit_to_qualifier((yyvsp[0].number)));
                     (yyval.number) = (yyvsp[-1].number) | (yyvsp[0].number);
             }
#line 2717 "gram.c" /* yacc.c:1646  */
    break;

  case 38:
#line 812 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = SYM_PRECIOUS;
             }
#line 2725 "gram.c" /* yacc.c:1646  */
    break;

  case 39:
#line 816 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = SYM_STATIC;
             }
#line 2733 "gram.c" /* yacc.c:1646  */
    break;

  case 40:
#line 820 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = SYM_PUBLIC;
             }
#line 2741 "gram.c" /* yacc.c:1646  */
    break;

  case 41:
#line 827 "gram.y" /* yacc.c:1646  */
    {
		     (yyvsp[0].enumlist).cv->sym.flags = (yyvsp[-1].number);
		     (yyval.node) = NULL;
	     }
#line 2750 "gram.c" /* yacc.c:1646  */
    break;

  case 42:
#line 832 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist;
		     while ((elist = (yyvsp[-1].enumlist).prev)) {
			     elist->cv->sym.flags = (yyvsp[-3].number);
			     elist = elist->prev;
			     free((yyvsp[-1].enumlist).prev);
			     (yyvsp[-1].enumlist).prev = elist;
		     }
		     (yyval.node) = NULL;
	     }
#line 2765 "gram.c" /* yacc.c:1646  */
    break;

  case 43:
#line 845 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[-1].number) & SYM_PRECIOUS)
                             parse_error_locus(&(yylsp[-1]),
					       _("`precious' used with const"));
		     if (((yyvsp[-1].number) & (SYM_STATIC|SYM_PUBLIC)) ==
			 (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-1]),
					       _("`static' and `public' "
						 "used together"));
		     (yyval.number) = (yyvsp[-1].number);
	     }
#line 2781 "gram.c" /* yacc.c:1646  */
    break;

  case 44:
#line 860 "gram.y" /* yacc.c:1646  */
    {
		     struct value value;
		     struct variable *pvar;

		     /* FIXME: This is necessary because constants can be
			referred to the same way as variables. */
		     if (pvar = variable_lookup((yyvsp[-1].literal)->text)) {
			     parse_warning(_("constant name `%s' clashes with a variable name"),
					   (yyvsp[-1].literal)->text);
			     parse_warning_locus(&pvar->sym.locus,
						 _("this is the location of the "
						   "previous definition"));
		     }
			     
		     if (optimization_level)
			     optimize((yyvsp[0].node));
		     
		     switch ((yyvsp[0].node)->type) {
		     case node_type_string:
			     value.type = dtype_string;
			     value.v.literal = (yyvsp[0].node)->v.literal;
			     break;

		     case node_type_number:
			     value.type = dtype_number;
			     value.v.number = (yyvsp[0].node)->v.number;
			     break;
			     
		     default:
			     yyerror(_("initializer element is not constant"));
			     YYERROR;
		     }

		     (yyval.enumlist).cv = define_constant((yyvsp[-1].literal)->text, &value, 0, &(yylsp[-1]));
		     (yyval.enumlist).prev =  NULL;
	     }
#line 2822 "gram.c" /* yacc.c:1646  */
    break;

  case 45:
#line 899 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist;
		     struct value value;
		     struct variable *pvar;

		     if (pvar = variable_lookup((yyvsp[0].literal)->text)) {
			     parse_warning(_("constant name `%s' clashes with a variable name"),
					   (yyvsp[0].literal)->text);
			     parse_warning_locus(&pvar->sym.locus,
						 _("this is the location of the "
						   "previous definition"));
		     }

		     value.type = dtype_number;
		     value.v.number = 0;

		     elist = mu_alloc(sizeof(*elist));
		     
		     elist->cv = define_constant((yyvsp[0].literal)->text, &value, 0, &(yylsp[0]));
		     elist->prev = NULL;
		     (yyval.enumlist).cv = NULL;
		     (yyval.enumlist).prev = elist;
	     }
#line 2850 "gram.c" /* yacc.c:1646  */
    break;

  case 46:
#line 923 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist = mu_alloc(sizeof(*elist));
		     elist->cv = (yyvsp[0].enumlist).cv;
		     elist->prev = NULL;
		     (yyval.enumlist).cv = NULL;
		     (yyval.enumlist).prev = elist;
	     }
#line 2862 "gram.c" /* yacc.c:1646  */
    break;

  case 47:
#line 931 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[-1].enumlist).prev->cv->value.type == dtype_number) {
			     struct enumlist *elist = mu_alloc(sizeof(*elist));
			     struct value value;

			     value.type = dtype_number;
			     value.v.number = (yyvsp[-1].enumlist).prev->cv->value.v.number + 1;
			     
			     elist->cv = define_constant((yyvsp[0].literal)->text, &value, 0,
							 &(yylsp[0]));
			     elist->prev = (yyvsp[-1].enumlist).prev;
			     (yyval.enumlist).prev = elist;
		     } else {
			     yyerror(_("initializer element is not numeric"));
			     YYERROR;
		     }
	     }
#line 2884 "gram.c" /* yacc.c:1646  */
    break;

  case 48:
#line 949 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist = mu_alloc(sizeof(*elist));
		     elist->cv = (yyvsp[0].enumlist).cv;
		     elist->prev = (yyvsp[-1].enumlist).prev;
		     (yyvsp[-1].enumlist).prev = elist;
		     (yyval.enumlist) = (yyvsp[-1].enumlist);
	     }
#line 2896 "gram.c" /* yacc.c:1646  */
    break;

  case 49:
#line 959 "gram.y" /* yacc.c:1646  */
    {
		     define_exception((yyvsp[0].literal), &(yylsp[-1]));
	     }
#line 2904 "gram.c" /* yacc.c:1646  */
    break;

  case 50:
#line 965 "gram.y" /* yacc.c:1646  */
    {
		     data_type_t *ptypes = NULL;
		     
		     if ((yyvsp[-3].parmlist).count) {
			     int i;
			     struct parmtype *p;
			     
			     ptypes = mu_alloc((yyvsp[-3].parmlist).count * sizeof *ptypes);
			     for (i = 0, p = (yyvsp[-3].parmlist).head; p; i++) {
				     struct parmtype *next = p->next;
				     ptypes[i] = p->type;
				     free(p);
				     p = next;
			     }
		     }

		     (yyval.function) = func = function_install((yyvsp[-5].literal)->text,
						  (yyvsp[-3].parmlist).count, (yyvsp[-3].parmlist).optcount,
						  (yyvsp[-3].parmlist).varargs,
						  ptypes, (yyvsp[0].type),
                                                  &(yylsp[-5]));
		     if ((yyvsp[-1].list)) {
			     mu_list_foreach((yyvsp[-1].list), _create_alias, (yyval.function));
			     mu_list_destroy(&(yyvsp[-1].list));
		     }
		     outer_context = inner_context = context_function;
	     }
#line 2936 "gram.c" /* yacc.c:1646  */
    break;

  case 51:
#line 995 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.parmlist).count = (yyval.parmlist).optcount = 0;
                     (yyval.parmlist).varargs = 0;
             }
#line 2945 "gram.c" /* yacc.c:1646  */
    break;

  case 54:
#line 1004 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.parmlist).count = (yyval.parmlist).optcount = 0;
                     (yyval.parmlist).varargs = 1;
             }
#line 2954 "gram.c" /* yacc.c:1646  */
    break;

  case 55:
#line 1009 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].parmlist).count += (yyvsp[0].parmlist).count;
                     (yyvsp[-2].parmlist).optcount = (yyvsp[0].parmlist).count;
                     (yyvsp[-2].parmlist).varargs = (yyvsp[0].parmlist).varargs;
                     if ((yyvsp[-2].parmlist).tail) 
                             (yyvsp[-2].parmlist).tail->next = (yyvsp[0].parmlist).head;
                     else
                             (yyvsp[-2].parmlist).head = (yyvsp[0].parmlist).head;
                     (yyvsp[-2].parmlist).tail = (yyvsp[0].parmlist).tail;
                     (yyval.parmlist) = (yyvsp[-2].parmlist);
             }
#line 2970 "gram.c" /* yacc.c:1646  */
    break;

  case 56:
#line 1023 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.parmlist).count = 0;
                     (yyval.parmlist).varargs = 0;
                     (yyval.parmlist).head = (yyval.parmlist).tail = NULL;
             }
#line 2980 "gram.c" /* yacc.c:1646  */
    break;

  case 58:
#line 1032 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.parmlist).count = 1;
		     (yyval.parmlist).varargs = 0;
                     (yyval.parmlist).optcount = 0;
		     (yyval.parmlist).head = (yyval.parmlist).tail = (yyvsp[0].parm);
	     }
#line 2991 "gram.c" /* yacc.c:1646  */
    break;

  case 59:
#line 1039 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].parmlist).count++;
                     (yyvsp[-2].parmlist).tail->next = (yyvsp[0].parm);
                     (yyvsp[-2].parmlist).tail = (yyvsp[0].parm);
                     (yyval.parmlist) = (yyvsp[-2].parmlist);
             }
#line 3002 "gram.c" /* yacc.c:1646  */
    break;

  case 61:
#line 1049 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].parmlist).varargs = 1;
                     (yyval.parmlist) = (yyvsp[-2].parmlist);
             }
#line 3011 "gram.c" /* yacc.c:1646  */
    break;

  case 62:
#line 1056 "gram.y" /* yacc.c:1646  */
    {
		     if (!vardecl((yyvsp[0].literal)->text, (yyvsp[-1].type), storage_param, &(yylsp[0])))
			     YYERROR;
		     (yyval.parm) = mu_alloc(sizeof *(yyval.parm));
		     (yyval.parm)->next = NULL;
		     (yyval.parm)->type = (yyvsp[-1].type);
	     }
#line 3023 "gram.c" /* yacc.c:1646  */
    break;

  case 63:
#line 1065 "gram.y" /* yacc.c:1646  */
    {
		     parse_warning_locus(&(yylsp[0]),
					 _("unnamed formal parameters are deprecated"));
		     (yyval.parm) = mu_alloc(sizeof *(yyval.parm));
		     (yyval.parm)->next = NULL;
		     (yyval.parm)->type = (yyvsp[0].type);
	     }
#line 3035 "gram.c" /* yacc.c:1646  */
    break;

  case 64:
#line 1075 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.list) = NULL;
             }
#line 3043 "gram.c" /* yacc.c:1646  */
    break;

  case 66:
#line 1082 "gram.y" /* yacc.c:1646  */
    {
                     mu_list_create(&(yyval.list));
                     mu_list_append((yyval.list), (yyvsp[0].literal));
             }
#line 3052 "gram.c" /* yacc.c:1646  */
    break;

  case 67:
#line 1087 "gram.y" /* yacc.c:1646  */
    {
                     mu_list_append((yyvsp[-1].list), (yyvsp[0].literal));
                     (yyval.list) = (yyvsp[-1].list);
             }
#line 3061 "gram.c" /* yacc.c:1646  */
    break;

  case 68:
#line 1094 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.literal) = (yyvsp[0].literal);
             }
#line 3069 "gram.c" /* yacc.c:1646  */
    break;

  case 69:
#line 1100 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.type) = dtype_unspecified;
             }
#line 3077 "gram.c" /* yacc.c:1646  */
    break;

  case 70:
#line 1104 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.type) = (yyvsp[0].type);
	     }
#line 3085 "gram.c" /* yacc.c:1646  */
    break;

  case 71:
#line 1110 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.state) = string_to_state((yyvsp[0].literal)->text);
		     if ((yyval.state) == smtp_state_none)
                             parse_error_locus(&(yylsp[0]),
					       _("unknown smtp state tag: %s"),
					       (yyvsp[0].literal)->text);
		     state_tag = (yyval.state);
		     outer_context = inner_context = context_handler;
	     }
#line 3099 "gram.c" /* yacc.c:1646  */
    break;

  case 72:
#line 1122 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].node)) 
                             (yyvsp[0].node)->next = NULL;
                     (yyval.stmtlist).head = (yyval.stmtlist).tail = (yyvsp[0].node);
             }
#line 3109 "gram.c" /* yacc.c:1646  */
    break;

  case 73:
#line 1128 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].node)) {
                             if ((yyvsp[-1].stmtlist).tail) 
                                     (yyvsp[-1].stmtlist).tail->next = (yyvsp[0].node);
                             else
                                     (yyvsp[-1].stmtlist).head = (yyvsp[0].node);
                             (yyvsp[-1].stmtlist).tail = (yyvsp[0].node);
                     }
                     (yyval.stmtlist) = (yyvsp[-1].stmtlist);
             }
#line 3124 "gram.c" /* yacc.c:1646  */
    break;

  case 81:
#line 1148 "gram.y" /* yacc.c:1646  */
    {
		     if (node_type((yyvsp[0].node)) != dtype_unspecified)
			     parse_warning(_("return from %s is ignored"),
					   (yyvsp[0].node)->type == node_type_builtin ?
					   (yyvsp[0].node)->v.builtin.builtin->name :
					   (yyvsp[0].node)->v.call.func->sym.name);
	     }
#line 3136 "gram.c" /* yacc.c:1646  */
    break;

  case 85:
#line 1161 "gram.y" /* yacc.c:1646  */
    {
		     struct variable *var;
		     data_type_t t = node_type((yyvsp[0].node));

		     if (t == dtype_unspecified) {
                             parse_error_locus(&(yylsp[0]),
					   _("unspecified value not ignored as "
					     "it should be"));
			     YYERROR;
		     }

		     var = variable_lookup((yyvsp[-1].literal)->text);
		     if (!var) {
                             var = vardecl((yyvsp[-1].literal)->text, t, storage_auto, &(yylsp[-1]));
			     if (!var) 
				     YYERROR;
		     }
                     (yyval.node) = create_asgn_node(var, (yyvsp[0].node), &(yylsp[-2]));
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 3162 "gram.c" /* yacc.c:1646  */
    break;

  case 86:
#line 1185 "gram.y" /* yacc.c:1646  */
    {
		     if (!vardecl((yyvsp[0].literal)->text, (yyvsp[-1].type), storage_auto, &(yylsp[0])))
			     YYERROR;
		     (yyval.node) = NULL;
	     }
#line 3172 "gram.c" /* yacc.c:1646  */
    break;

  case 87:
#line 1191 "gram.y" /* yacc.c:1646  */
    {
		     struct variable *var = vardecl((yyvsp[-1].literal)->text, (yyvsp[-2].type),
						    storage_auto, &(yylsp[-1]));
		     if (!var)
			     YYERROR;
                     (yyval.node) = create_asgn_node(var, (yyvsp[0].node), &(yylsp[-2]));
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 3186 "gram.c" /* yacc.c:1646  */
    break;

  case 88:
#line 1203 "gram.y" /* yacc.c:1646  */
    {
		     if (inner_context == context_handler) {
			     if (state_tag == smtp_state_begin)
                                     parse_error_locus(&(yylsp[0]),
                                                _("Sendmail action is not "
						   "allowed in begin block"));
			     else if (state_tag == smtp_state_end)
                                     parse_error_locus(&(yylsp[0]),
                                               _("Sendmail action is not "
						   "allowed in end block"));
		     }
	     }
#line 3203 "gram.c" /* yacc.c:1646  */
    break;

  case 89:
#line 1216 "gram.y" /* yacc.c:1646  */
    {
		     if (inner_context == context_handler
			 && state_tag == smtp_state_end)
                            parse_error_locus(&(yylsp[0]),
                                          _("header action is not allowed "
					   "in end block"));
	     }
#line 3215 "gram.c" /* yacc.c:1646  */
    break;

  case 90:
#line 1224 "gram.y" /* yacc.c:1646  */
    {
                    (yyval.node) = alloc_node(node_type_noop, &(yylsp[0]));
	     }
#line 3223 "gram.c" /* yacc.c:1646  */
    break;

  case 91:
#line 1228 "gram.y" /* yacc.c:1646  */
    {
                    (yyval.node) = alloc_node(node_type_echo, &(yylsp[-1]));
		    (yyval.node)->v.node = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3232 "gram.c" /* yacc.c:1646  */
    break;

  case 92:
#line 1236 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[0].ret).code || (yyvsp[0].ret).xcode || (yyvsp[0].ret).message)
			     parse_warning(_("arguments are ignored for accept"));
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-1]));
		     (yyval.node)->v.ret = (yyvsp[0].ret);
		     (yyval.node)->v.ret.stat = SMFIS_ACCEPT;
	     }
#line 3244 "gram.c" /* yacc.c:1646  */
    break;

  case 93:
#line 1244 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-1]));
		     (yyval.node)->v.ret = (yyvsp[0].ret);
		     (yyval.node)->v.ret.stat = SMFIS_REJECT;
	     }
#line 3254 "gram.c" /* yacc.c:1646  */
    break;

  case 94:
#line 1250 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-7]));
		     (yyval.node)->v.ret.stat = SMFIS_REJECT;
		     (yyval.node)->v.ret.code = (yyvsp[-5].node) ? cast_to(dtype_string, (yyvsp[-5].node)) : NULL;
		     (yyval.node)->v.ret.xcode = (yyvsp[-3].node) ? cast_to(dtype_string, (yyvsp[-3].node)) : NULL;
		     (yyval.node)->v.ret.message = (yyvsp[-1].node) ? cast_to(dtype_string, (yyvsp[-1].node)) : NULL;
	     }
#line 3266 "gram.c" /* yacc.c:1646  */
    break;

  case 95:
#line 1258 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-1]));
		     (yyval.node)->v.ret = (yyvsp[0].ret);
		     (yyval.node)->v.ret.stat = SMFIS_TEMPFAIL;
	     }
#line 3276 "gram.c" /* yacc.c:1646  */
    break;

  case 96:
#line 1264 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-7]));
		     (yyval.node)->v.ret.stat = SMFIS_TEMPFAIL;
		     (yyval.node)->v.ret.code = (yyvsp[-5].node) ? cast_to(dtype_string, (yyvsp[-5].node)) : NULL;
		     (yyval.node)->v.ret.xcode = (yyvsp[-3].node) ? cast_to(dtype_string, (yyvsp[-3].node)) : NULL;
		     (yyval.node)->v.ret.message = (yyvsp[-1].node) ? cast_to(dtype_string, (yyvsp[-1].node)) : NULL;
	     }
#line 3288 "gram.c" /* yacc.c:1646  */
    break;

  case 97:
#line 1272 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[0]));
		     memset(&(yyval.node)->v.ret, 0, sizeof (yyval.node)->v.ret);
		     (yyval.node)->v.ret.stat = SMFIS_CONTINUE;
	     }
#line 3298 "gram.c" /* yacc.c:1646  */
    break;

  case 98:
#line 1278 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[0]));
		     memset(&(yyval.node)->v.ret, 0, sizeof (yyval.node)->v.ret);
		     (yyval.node)->v.ret.stat = SMFIS_DISCARD;
	     }
#line 3308 "gram.c" /* yacc.c:1646  */
    break;

  case 100:
#line 1287 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.node)->v.literal = (yyvsp[0].literal);
	     }
#line 3317 "gram.c" /* yacc.c:1646  */
    break;

  case 101:
#line 1296 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_header, &(yylsp[-2]));
		     (yyval.node)->v.hdr.opcode = header_add;
		     (yyval.node)->v.hdr.name = (yyvsp[-1].literal);
		     (yyval.node)->v.hdr.value = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3328 "gram.c" /* yacc.c:1646  */
    break;

  case 102:
#line 1303 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_header, &(yylsp[-2]));
		     (yyval.node)->v.hdr.opcode = header_replace;
		     (yyval.node)->v.hdr.name = (yyvsp[-1].literal);
		     (yyval.node)->v.hdr.value = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3339 "gram.c" /* yacc.c:1646  */
    break;

  case 103:
#line 1310 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_header, &(yylsp[-1]));
		     (yyval.node)->v.hdr.opcode = header_delete;
		     (yyval.node)->v.hdr.name = (yyvsp[0].literal);
		     (yyval.node)->v.hdr.value = NULL;
	     }
#line 3350 "gram.c" /* yacc.c:1646  */
    break;

  case 104:
#line 1319 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.ret), 0, sizeof (yyval.ret));
             }
#line 3358 "gram.c" /* yacc.c:1646  */
    break;

  case 106:
#line 1326 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.ret).code->v.literal = (yyvsp[0].literal);
		     (yyval.ret).xcode = NULL;
		     (yyval.ret).message = NULL;
	     }
#line 3369 "gram.c" /* yacc.c:1646  */
    break;

  case 107:
#line 1333 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[-1]));
		     (yyval.ret).code->v.literal = (yyvsp[-1].literal);
                     (yyval.ret).xcode = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.ret).xcode->v.literal = (yyvsp[0].literal);
		     (yyval.ret).message = NULL;
	     }
#line 3381 "gram.c" /* yacc.c:1646  */
    break;

  case 108:
#line 1341 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[-2]));
		     (yyval.ret).code->v.literal = (yyvsp[-2].literal);
                     (yyval.ret).xcode = alloc_node(node_type_string, &(yylsp[-1]));
		     (yyval.ret).xcode->v.literal = (yyvsp[-1].literal);
		     (yyval.ret).message = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3393 "gram.c" /* yacc.c:1646  */
    break;

  case 109:
#line 1349 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[-1]));
		     (yyval.ret).code->v.literal = (yyvsp[-1].literal);
		     (yyval.ret).xcode = NULL;
		     (yyval.ret).message = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3404 "gram.c" /* yacc.c:1646  */
    break;

  case 110:
#line 1358 "gram.y" /* yacc.c:1646  */
    {
                     char buf[4];

                     if ((yyvsp[0].number) < 200 || (yyvsp[0].number) > 599) {
                             yyerror(_("invalid SMTP reply code"));
                             buf[0] = 0;
                     } else
                             snprintf(buf, sizeof(buf), "%lu", (yyvsp[0].number));
                     (yyval.literal) = string_alloc(buf, strlen(buf));
             }
#line 3419 "gram.c" /* yacc.c:1646  */
    break;

  case 111:
#line 1371 "gram.y" /* yacc.c:1646  */
    {
                     char buf[sizeof("5.999.999")];

                     /* RFC 1893:
                        The syntax of the new status codes is defined as:

                        status-code = class "." subject "." detail
                        class = "2"/"4"/"5"
                        subject = 1*3digit
                        detail = 1*3digit
                     */                        
                     if (((yyvsp[-4].number) != 2 && (yyvsp[-4].number) != 4 && (yyvsp[-4].number) !=5)
                         || (yyvsp[-2].number) > 999 || (yyvsp[0].number) > 999) {
                             yyerror(_("invalid extended reply code"));
                             buf[0] = 0;
                     } else
                             snprintf(buf, sizeof(buf), "%lu.%lu.%lu",
                                      (yyvsp[-4].number), (yyvsp[-2].number), (yyvsp[0].number));
                     (yyval.literal) = string_alloc(buf, strlen(buf));
             }
#line 3444 "gram.c" /* yacc.c:1646  */
    break;

  case 115:
#line 1399 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_if, &(yylsp[-4]));
		     (yyval.node)->v.cond.cond = (yyvsp[-3].node);
		     (yyval.node)->v.cond.if_true = (yyvsp[-2].stmtlist).head;
		     (yyval.node)->v.cond.if_false = (yyvsp[-1].node);
	     }
#line 3455 "gram.c" /* yacc.c:1646  */
    break;

  case 116:
#line 1408 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = NULL;
             }
#line 3463 "gram.c" /* yacc.c:1646  */
    break;

  case 117:
#line 1412 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_if, &(yylsp[-3]));
		     (yyval.node)->v.cond.cond = (yyvsp[-2].node);
		     (yyval.node)->v.cond.if_true = (yyvsp[-1].stmtlist).head;
		     (yyval.node)->v.cond.if_false = (yyvsp[0].node);
	     }
#line 3474 "gram.c" /* yacc.c:1646  */
    break;

  case 118:
#line 1419 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[0].stmtlist).head;
             }
#line 3482 "gram.c" /* yacc.c:1646  */
    break;

  case 119:
#line 1425 "gram.y" /* yacc.c:1646  */
    {
		     struct case_stmt *defcase = NULL, *pcase, *prev;
		     
                     (yyval.node) = alloc_node(node_type_switch, &(yylsp[-4]));
		     (yyval.node)->v.switch_stmt.node = (yyvsp[-3].node);
		     
		     /* Make sure there is only one default case and
			place it at the beginning of the list */
		     pcase = (yyvsp[-1].case_list).head;
		     if (!pcase->valist) {
			     defcase = pcase;
			     (yyvsp[-1].case_list).head = (yyvsp[-1].case_list).head->next;
		     }
		     prev = pcase;
		     pcase = pcase->next;
		     while (pcase) {
			     if (!pcase->valist) {
				     if (defcase) {
					     parse_error_locus(&pcase->locus,
					      _("duplicate default statement"));
					     parse_error_locus(&defcase->locus,
					      _("previously defined here"));
					     YYERROR;
				     }
				     defcase = pcase;
				     prev->next = pcase->next;
			     } else
				     prev = pcase;
			     pcase = pcase->next;
		     }

		     if (!defcase) {
			     defcase = mu_alloc(sizeof *defcase);
			     mu_locus_range_init(&defcase->locus);
                             mu_locus_range_copy(&defcase->locus, &(yylsp[0]));
			     defcase->valist = NULL;
			     defcase->node = alloc_node(node_type_noop,
							&defcase->locus);
		     }
		     defcase->next = (yyvsp[-1].case_list).head;
		     (yyval.node)->v.switch_stmt.cases = defcase;
	     }
#line 3529 "gram.c" /* yacc.c:1646  */
    break;

  case 120:
#line 1470 "gram.y" /* yacc.c:1646  */
    {
                       (yyval.case_list).head = (yyval.case_list).tail = (yyvsp[0].case_stmt);
               }
#line 3537 "gram.c" /* yacc.c:1646  */
    break;

  case 121:
#line 1474 "gram.y" /* yacc.c:1646  */
    {
                       (yyval.case_list).tail->next = (yyvsp[0].case_stmt);
                       (yyval.case_list).tail = (yyvsp[0].case_stmt);
               }
#line 3546 "gram.c" /* yacc.c:1646  */
    break;

  case 122:
#line 1481 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.case_stmt) = mu_alloc(sizeof *(yyval.case_stmt));
		     (yyval.case_stmt)->next = NULL;
		     mu_locus_range_init (&(yyval.case_stmt)->locus);
                     mu_locus_range_copy (&(yyval.case_stmt)->locus, &(yylsp[-3]));
		     (yyval.case_stmt)->valist = (yyvsp[-2].valist_list).head;
		     (yyval.case_stmt)->node = (yyvsp[0].stmtlist).head;
	     }
#line 3559 "gram.c" /* yacc.c:1646  */
    break;

  case 123:
#line 1490 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.case_stmt) = mu_alloc(sizeof *(yyval.case_stmt));
		     (yyval.case_stmt)->next = NULL;
		     mu_locus_range_init (&(yyval.case_stmt)->locus);
                     mu_locus_range_copy (&(yyval.case_stmt)->locus, &(yylsp[-2]));
		     (yyval.case_stmt)->valist = NULL;
		     (yyval.case_stmt)->node = (yyvsp[0].stmtlist).head;
	     }
#line 3572 "gram.c" /* yacc.c:1646  */
    break;

  case 124:
#line 1501 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.valist_list).head = (yyval.valist_list).tail = mu_alloc(sizeof((yyval.valist_list).head[0]));
                     (yyval.valist_list).head->next = NULL;
                     (yyval.valist_list).head->value = (yyvsp[0].value);
             }
#line 3582 "gram.c" /* yacc.c:1646  */
    break;

  case 125:
#line 1507 "gram.y" /* yacc.c:1646  */
    {
                     struct valist *p = mu_alloc(sizeof(*p));
                     p->value = (yyvsp[0].value);
                     p->next = NULL;
                     (yyvsp[-2].valist_list).tail->next = p;
                     (yyvsp[-2].valist_list).tail = p;
                     (yyval.valist_list) = (yyvsp[-2].valist_list);
             }
#line 3595 "gram.c" /* yacc.c:1646  */
    break;

  case 126:
#line 1518 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.value).type = dtype_string;
		     (yyval.value).v.literal = (yyvsp[0].literal);
	     }
#line 3604 "gram.c" /* yacc.c:1646  */
    break;

  case 127:
#line 1523 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.value).type = dtype_number;
                     (yyval.value).v.number = (yyvsp[0].number);
             }
#line 3613 "gram.c" /* yacc.c:1646  */
    break;

  case 128:
#line 1530 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[0].value).type != dtype_string) {
                             parse_error_locus(&(yylsp[0]),
                                         _("expected string, but found %s"),
					 type_to_string((yyvsp[0].value).type));
			     /* Make sure we return something usable: */
			     (yyval.literal) = string_alloc("ERROR", 5);
		     } else
			     (yyval.literal) = (yyvsp[0].value).v.literal; 
	     }
#line 3628 "gram.c" /* yacc.c:1646  */
    break;

  case 129:
#line 1543 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = 0;
	     }
#line 3636 "gram.c" /* yacc.c:1646  */
    break;

  case 130:
#line 1547 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = QUALIFIER_MX;
	     }
#line 3644 "gram.c" /* yacc.c:1646  */
    break;

  case 131:
#line 1553 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = 0;
	     }
#line 3652 "gram.c" /* yacc.c:1646  */
    break;

  case 132:
#line 1557 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = QUALIFIER_MX;
	     }
#line 3660 "gram.c" /* yacc.c:1646  */
    break;

  case 133:
#line 1566 "gram.y" /* yacc.c:1646  */
    {
		     leave_loop();
		     (yyvsp[-4].loop).end_while = (yyvsp[0].node);
                     (yyval.node) = alloc_node(node_type_loop, &(yylsp[-6]));
		     (yyvsp[-4].loop).body = (yyvsp[-2].stmtlist).head;
		     (yyvsp[-4].loop).ident = (yyvsp[-5].literal);
		     (yyval.node)->v.loop = (yyvsp[-4].loop);
	     }
#line 3673 "gram.c" /* yacc.c:1646  */
    break;

  case 134:
#line 1577 "gram.y" /* yacc.c:1646  */
    {
                     enter_loop((yyvsp[0].literal), NULL, NULL);
             }
#line 3681 "gram.c" /* yacc.c:1646  */
    break;

  case 135:
#line 1583 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.literal) = NULL;
	     }
#line 3689 "gram.c" /* yacc.c:1646  */
    break;

  case 137:
#line 1590 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.loop), 0, sizeof (yyval.loop));
             }
#line 3697 "gram.c" /* yacc.c:1646  */
    break;

  case 139:
#line 1597 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.loop), 0, sizeof (yyval.loop));
                     switch ((yyvsp[0].pollarg).kw) {
                     case 0:
                             (yyval.loop).stmt = (yyvsp[0].pollarg).expr;
                             break;

                     case T_FOR:
                             (yyval.loop).for_stmt = (yyvsp[0].pollarg).expr;
                             break;

                     case T_WHILE:
                             (yyval.loop).beg_while = (yyvsp[0].pollarg).expr;
                             break;

                     default:
                             abort();
                     }
             }
#line 3721 "gram.c" /* yacc.c:1646  */
    break;

  case 140:
#line 1617 "gram.y" /* yacc.c:1646  */
    {
		     switch ((yyvsp[0].pollarg).kw) {
		     case 0:
			     if ((yyval.loop).stmt) 
                                     parse_error_locus(&(yylsp[0]),
                                                _("duplicate loop increment"));
			     (yyval.loop).stmt = (yyvsp[0].pollarg).expr;
			     break;

		     case T_FOR:
			     if ((yyval.loop).for_stmt) 
                                     parse_error_locus(&(yylsp[0]),
                                                _("duplicate for statement"));
			     (yyval.loop).for_stmt = (yyvsp[0].pollarg).expr;
			     break;

		     case T_WHILE:
			     if ((yyval.loop).beg_while) 
                                     parse_error_locus(&(yylsp[0]),
                                              _("duplicate while statement"));
			     (yyval.loop).beg_while = (yyvsp[0].pollarg).expr;
			     break;

		     default:
			     abort();
		     }
	     }
#line 3753 "gram.c" /* yacc.c:1646  */
    break;

  case 141:
#line 1647 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = 0;
                     (yyval.pollarg).expr = (yyvsp[0].stmtlist).head;
             }
#line 3762 "gram.c" /* yacc.c:1646  */
    break;

  case 142:
#line 1652 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_FOR;
                     (yyval.pollarg).expr = (yyvsp[0].stmtlist).head;
             }
#line 3771 "gram.c" /* yacc.c:1646  */
    break;

  case 143:
#line 1657 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_WHILE;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 3780 "gram.c" /* yacc.c:1646  */
    break;

  case 144:
#line 1664 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = NULL;
             }
#line 3788 "gram.c" /* yacc.c:1646  */
    break;

  case 145:
#line 1668 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[0].node);
             }
#line 3796 "gram.c" /* yacc.c:1646  */
    break;

  case 146:
#line 1674 "gram.y" /* yacc.c:1646  */
    {
		     if (!within_loop((yyvsp[0].literal))) {
			     if ((yyvsp[0].literal))
                                     parse_error_locus(&(yylsp[0]),
                                                       _("no such loop: %s"), 
				                 (yyvsp[0].literal)->text);
                             parse_error_locus(&(yylsp[-1]),
                                        _("`break' used outside of `loop'"));
			     YYERROR;
		     }
                     (yyval.node) = alloc_node(node_type_break, &(yylsp[-1]));
		     (yyval.node)->v.literal = (yyvsp[0].literal);
	     }
#line 3814 "gram.c" /* yacc.c:1646  */
    break;

  case 147:
#line 1688 "gram.y" /* yacc.c:1646  */
    {
		     if (!within_loop((yyvsp[0].literal))) {
			     if ((yyvsp[0].literal)) {
                                     parse_error_locus(&(yylsp[0]),
                                                       _("no such loop: %s"),
                                                       (yyvsp[0].literal)->text);
                                     parse_error_locus(&(yylsp[-1]),
                                                       _("`next' used outside `loop'"));
				     YYERROR;
			     } else {
                                     parse_error_locus(&(yylsp[-1]),
                                           _("`next' is used outside `loop'; "
                                             "did you mean `pass'?"));
				     YYERROR;
			     }
		     } else {
                             (yyval.node) = alloc_node(node_type_next, &(yylsp[-1]));
			     (yyval.node)->v.literal = (yyvsp[0].literal);
		     }
	     }
#line 3839 "gram.c" /* yacc.c:1646  */
    break;

  case 148:
#line 1713 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_un, &(yylsp[-1]));
		     (yyval.node)->v.un.opcode = unary_not;
		     (yyval.node)->v.un.arg = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3849 "gram.c" /* yacc.c:1646  */
    break;

  case 149:
#line 1719 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_eq;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3860 "gram.c" /* yacc.c:1646  */
    break;

  case 150:
#line 1726 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_ne;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3871 "gram.c" /* yacc.c:1646  */
    break;

  case 151:
#line 1733 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_lt;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3882 "gram.c" /* yacc.c:1646  */
    break;

  case 152:
#line 1740 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_le;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3893 "gram.c" /* yacc.c:1646  */
    break;

  case 153:
#line 1747 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_gt;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3904 "gram.c" /* yacc.c:1646  */
    break;

  case 154:
#line 1754 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_ge;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3915 "gram.c" /* yacc.c:1646  */
    break;

  case 155:
#line 1761 "gram.y" /* yacc.c:1646  */
    {
		     NODE *p;
		     
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_match;
		     (yyval.node)->v.bin.qualifier = (yyvsp[-1].matchtype).qualifier;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_string, (yyvsp[-2].node));
                     (yyval.node)->v.bin.arg[1] = p = alloc_node(node_type_regcomp,
                                                       &(yylsp[-1]));
		     p->v.regcomp_data.expr = cast_to(dtype_string, (yyvsp[0].node));
		     p->v.regcomp_data.flags = regex_flags;
		     p->v.regcomp_data.regind = -1;
	     }
#line 3933 "gram.c" /* yacc.c:1646  */
    break;

  case 156:
#line 1775 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_fnmatch;
		     (yyval.node)->v.bin.qualifier = (yyvsp[-1].matchtype).qualifier;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_string, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3945 "gram.c" /* yacc.c:1646  */
    break;

  case 157:
#line 1783 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_or;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3956 "gram.c" /* yacc.c:1646  */
    break;

  case 158:
#line 1790 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_and;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3967 "gram.c" /* yacc.c:1646  */
    break;

  case 160:
#line 1800 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = NULL;
             }
#line 3975 "gram.c" /* yacc.c:1646  */
    break;

  case 163:
#line 1808 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_add;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3986 "gram.c" /* yacc.c:1646  */
    break;

  case 164:
#line 1815 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_concat, &(yylsp[-1]));
		     (yyval.node)->v.concat.arg[0] = cast_to(dtype_string, (yyvsp[-2].node));
		     (yyval.node)->v.concat.arg[1] = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3996 "gram.c" /* yacc.c:1646  */
    break;

  case 165:
#line 1821 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_sub;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4007 "gram.c" /* yacc.c:1646  */
    break;

  case 166:
#line 1828 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_mul;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4018 "gram.c" /* yacc.c:1646  */
    break;

  case 167:
#line 1835 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_div;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4029 "gram.c" /* yacc.c:1646  */
    break;

  case 168:
#line 1842 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_mod;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4040 "gram.c" /* yacc.c:1646  */
    break;

  case 169:
#line 1849 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_logand;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4051 "gram.c" /* yacc.c:1646  */
    break;

  case 170:
#line 1856 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_logor;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4062 "gram.c" /* yacc.c:1646  */
    break;

  case 171:
#line 1863 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_logxor;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4073 "gram.c" /* yacc.c:1646  */
    break;

  case 172:
#line 1870 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_shl;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4084 "gram.c" /* yacc.c:1646  */
    break;

  case 173:
#line 1877 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_shr;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4095 "gram.c" /* yacc.c:1646  */
    break;

  case 174:
#line 1886 "gram.y" /* yacc.c:1646  */
    {
		     if (node_type((yyvsp[0].node)) == dtype_unspecified)
			     parse_error(_("unspecified value not ignored as it should be"));
	     }
#line 4104 "gram.c" /* yacc.c:1646  */
    break;

  case 175:
#line 1891 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[-1].node);
             }
#line 4112 "gram.c" /* yacc.c:1646  */
    break;

  case 176:
#line 1895 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = cast_to((yyvsp[-3].type), (yyvsp[-1].node));
             }
#line 4120 "gram.c" /* yacc.c:1646  */
    break;

  case 178:
#line 1900 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_un, &(yylsp[-1]));
		     (yyval.node)->v.un.opcode = unary_minus;
		     (yyval.node)->v.un.arg = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4130 "gram.c" /* yacc.c:1646  */
    break;

  case 179:
#line 1906 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[0].node);
             }
#line 4138 "gram.c" /* yacc.c:1646  */
    break;

  case 180:
#line 1910 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_un, &(yylsp[-1]));
		     (yyval.node)->v.un.opcode = unary_lognot;
		     (yyval.node)->v.un.arg = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4148 "gram.c" /* yacc.c:1646  */
    break;

  case 181:
#line 1918 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = create_node_symbol((yyvsp[0].literal), &(yylsp[0]));
	     }
#line 4156 "gram.c" /* yacc.c:1646  */
    break;

  case 182:
#line 1922 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_number, &(yylsp[0]));
		     (yyval.node)->v.number = (yyvsp[0].number);
	     }
#line 4165 "gram.c" /* yacc.c:1646  */
    break;

  case 183:
#line 1927 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = create_node_backref((yyvsp[0].number), &(yylsp[0]));
	     }
#line 4173 "gram.c" /* yacc.c:1646  */
    break;

  case 185:
#line 1932 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = create_node_argcount(&(yylsp[0]));
	     }
#line 4181 "gram.c" /* yacc.c:1646  */
    break;

  case 186:
#line 1936 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_offset, &(yylsp[-1]));
		     (yyval.node)->v.var_ref.variable = (yyvsp[0].var);
		     (yyval.node)->v.var_ref.nframes = catch_nesting;
	     }
#line 4191 "gram.c" /* yacc.c:1646  */
    break;

  case 187:
#line 1942 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_vaptr, &(yylsp[-1]));
		     (yyval.node)->v.node = (yyvsp[0].node);
	     }
#line 4200 "gram.c" /* yacc.c:1646  */
    break;

  case 190:
#line 1951 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_concat, &(yylsp[0]));
		     (yyval.node)->v.concat.arg[0] = (yyvsp[-1].node);
		     (yyval.node)->v.concat.arg[1] = (yyvsp[0].node);
	     }
#line 4210 "gram.c" /* yacc.c:1646  */
    break;

  case 191:
#line 1959 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.node)->v.literal = (yyvsp[0].literal);
	     }
#line 4219 "gram.c" /* yacc.c:1646  */
    break;

  case 193:
#line 1967 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = create_node_variable((yyvsp[0].var), &(yylsp[0]));
	     }
#line 4227 "gram.c" /* yacc.c:1646  */
    break;

  case 194:
#line 1971 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = create_node_arg((yyvsp[0].number), &(yylsp[0]));
 	     }
#line 4235 "gram.c" /* yacc.c:1646  */
    break;

  case 195:
#line 1975 "gram.y" /* yacc.c:1646  */
    {
		     if (outer_context == context_function) {
			     if (func->varargs) {
                                     (yyval.node) = alloc_node(node_type_argx, &(yylsp[-3]));
				     (yyval.node)->v.argx.nargs = PARMCOUNT() +
					     FUNC_HIDDEN_ARGS(func);
				     (yyval.node)->v.argx.node = (yyvsp[-1].node);
			     } else {
                                     (yyval.node) = alloc_node(node_type_noop, &(yylsp[-3]));
                                     parse_error_locus(&(yylsp[-3]),
                                                 _("$(expr) is allowed only "
						   "in a function with "
						   "variable number of "
						   "arguments"));
			     }
		     } else {
                             (yyval.node) = alloc_node(node_type_noop, &(yylsp[-3]));
                             parse_error_locus(&(yylsp[-3]),
                                         _("$(expr) is allowed only "
					   "in a function with "
					   "variable number of "
					   "arguments"));
		     }
	     }
#line 4264 "gram.c" /* yacc.c:1646  */
    break;

  case 197:
#line 2003 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[-1].node);
             }
#line 4272 "gram.c" /* yacc.c:1646  */
    break;

  case 198:
#line 2009 "gram.y" /* yacc.c:1646  */
    {
		     if (check_builtin_usage((yyvsp[-3].builtin), &(yylsp[-3])))
			     YYERROR;

		     if ((yyvsp[-3].builtin)->flags & MFD_BUILTIN_REGEX_FLAGS) {
			     NODE *p = alloc_node(node_type_number, &(yylsp[-3]));
			     p->v.number = regex_flags;
			     p->next = (yyvsp[-1].arglist).head;
			     (yyvsp[-1].arglist).head = p;
			     (yyvsp[-1].arglist).count++;
		     }

		     if ((yyvsp[-1].arglist).count < (yyvsp[-3].builtin)->parmcount - (yyvsp[-3].builtin)->optcount) {
                             parse_error_locus(&(yylsp[-3]),
                                       _("too few arguments in call to `%s'"),
					 (yyvsp[-3].builtin)->name);
			     YYERROR;
		     } else if ((yyvsp[-1].arglist).count > (yyvsp[-3].builtin)->parmcount
				&& !((yyvsp[-3].builtin)->flags & MFD_BUILTIN_VARIADIC)) {
                             parse_error_locus(&(yylsp[-3]),
                                      _("too many arguments in call to `%s'"),
					 (yyvsp[-3].builtin)->name);
			     YYERROR;
		     } else {
                             (yyval.node) = alloc_node(node_type_builtin, &(yylsp[-3]));
			     (yyval.node)->v.builtin.builtin = (yyvsp[-3].builtin);
			     (yyval.node)->v.builtin.args =
					     reverse(cast_arg_list((yyvsp[-1].arglist).head,
 							       (yyvsp[-3].builtin)->parmcount,
							       (yyvsp[-3].builtin)->parmtype,
							       (yyval.node)->v.builtin.builtin->flags & MFD_BUILTIN_NO_PROMOTE));
		     }
	     }
#line 4310 "gram.c" /* yacc.c:1646  */
    break;

  case 199:
#line 2043 "gram.y" /* yacc.c:1646  */
    {
		     if (check_builtin_usage((yyvsp[-2].builtin), &(yylsp[-2])))
			     YYERROR;
		     if ((yyvsp[-2].builtin)->parmcount - (yyvsp[-2].builtin)->optcount) {
                             parse_error_locus(&(yylsp[-2]),
                                       _("too few arguments in call to `%s'"),
					 (yyvsp[-2].builtin)->name);
			     YYERROR;
		     } else {
                             (yyval.node) = alloc_node(node_type_builtin, &(yylsp[-2]));
			     (yyval.node)->v.builtin.builtin = (yyvsp[-2].builtin);
			     (yyval.node)->v.builtin.args = NULL;
		     }
	     }
#line 4329 "gram.c" /* yacc.c:1646  */
    break;

  case 200:
#line 2058 "gram.y" /* yacc.c:1646  */
    {
		     NODE *arg, *expr;

		     if ((yyvsp[-1].arglist).count < 2) {
			     parse_error_locus(&(yylsp[-3]),
					       _("too few arguments in call to `%s'"),
					       "sed");
			     YYERROR;
		     }

		     arg = cast_to(dtype_string, (yyvsp[-1].arglist).head);
		     expr = (yyvsp[-1].arglist).head->next;
		     /* Break the link between the args lest any eventual
			optimizations cause grief later in mark phase. */
		     (yyvsp[-1].arglist).head->next = NULL;

		     do {
			     NODE *comp, *next;
			     struct mu_locus_range lr;

			     comp = alloc_node(node_type_sedcomp, &expr->locus);
			     comp->v.sedcomp.index = next_transform_index();
			     comp->v.sedcomp.expr = cast_to(dtype_string, expr);
			     comp->v.sedcomp.flags = regex_flags;

			     lr.beg = (yylsp[-3]).beg;
			     lr.end = expr->locus.end;

			     (yyval.node) = alloc_node(node_type_sed, &lr);
			     (yyval.node)->v.sed.comp = comp;
			     (yyval.node)->v.sed.arg = arg;

			     next = expr->next;
			     expr->next = NULL;
			     expr = next;

			     arg = (yyval.node);
		     } while (expr);
	     }
#line 4373 "gram.c" /* yacc.c:1646  */
    break;

  case 201:
#line 2098 "gram.y" /* yacc.c:1646  */
    {
                     if (check_func_usage((yyvsp[-3].function), &(yylsp[-3])))
			     YYERROR;
		     (yyval.node) = function_call((yyvsp[-3].function), (yyvsp[-1].arglist).count, (yyvsp[-1].arglist).head);
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 4385 "gram.c" /* yacc.c:1646  */
    break;

  case 202:
#line 2106 "gram.y" /* yacc.c:1646  */
    {
                     if (check_func_usage((yyvsp[-2].function), &(yylsp[-2])))
			     YYERROR;
		     (yyval.node) = function_call((yyvsp[-2].function), 0, NULL);
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 4397 "gram.c" /* yacc.c:1646  */
    break;

  case 203:
#line 2116 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[0].node)->next = NULL;
                     (yyval.arglist).head = (yyval.arglist).tail = (yyvsp[0].node);
                     (yyval.arglist).count = 1;
             }
#line 4407 "gram.c" /* yacc.c:1646  */
    break;

  case 204:
#line 2122 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].arglist).tail->next = (yyvsp[0].node);
                     (yyvsp[-2].arglist).tail = (yyvsp[0].node);
                     (yyvsp[-2].arglist).count++;
                     (yyval.arglist) = (yyvsp[-2].arglist);
             }
#line 4418 "gram.c" /* yacc.c:1646  */
    break;

  case 205:
#line 2131 "gram.y" /* yacc.c:1646  */
    {
		     add_xref((yyvsp[0].var), &(yylsp[0]));
	     }
#line 4426 "gram.c" /* yacc.c:1646  */
    break;

  case 206:
#line 2135 "gram.y" /* yacc.c:1646  */
    {
                     YYERROR;
             }
#line 4434 "gram.c" /* yacc.c:1646  */
    break;

  case 207:
#line 2141 "gram.y" /* yacc.c:1646  */
    {
		     if (outer_context == context_function) {
			     func->exmask->all |= (yyval.node)->v.catch.exmask->all;
			     bitmask_merge(&func->exmask->bm,
					   &(yyval.node)->v.catch.exmask->bm);
		     }
	     }
#line 4446 "gram.c" /* yacc.c:1646  */
    break;

  case 208:
#line 2149 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_try, &(yylsp[-1]));
		     (yyval.node)->v.try.node = (yyvsp[-1].node);
		     (yyval.node)->v.try.catch = (yyvsp[0].node);
	     }
#line 4456 "gram.c" /* yacc.c:1646  */
    break;

  case 209:
#line 2157 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = (yyvsp[-1].stmtlist).head;
	     }
#line 4464 "gram.c" /* yacc.c:1646  */
    break;

  case 210:
#line 2164 "gram.y" /* yacc.c:1646  */
    { (yyval.tie_in) = inner_context;
                   inner_context = context_catch; 
                   catch_nesting++; }
#line 4472 "gram.c" /* yacc.c:1646  */
    break;

  case 211:
#line 2168 "gram.y" /* yacc.c:1646  */
    {
		      int i;
		      struct valist *p;

		      inner_context = (yyvsp[-2].tie_in);
		      catch_nesting--;
                      (yyval.node) = alloc_node(node_type_catch, &(yylsp[-5]));
		      (yyval.node)->v.catch.exmask = exmask_create();
		      (yyval.node)->v.catch.context = outer_context;/*??*/
		      (yyval.node)->v.catch.exmask->all = (yyvsp[-4].catchlist).all;
		      if (!(yyvsp[-4].catchlist).all) {
			      for (i = 0, p = (yyvsp[-4].catchlist).valist; p; p = p->next, i++) {
				      if (p->value.type != dtype_number) {
					      parse_error_locus(&(yylsp[-5]),
								_("expected numeric value, but found `%s'"),
								p->value.v.literal->text);
					      continue;
				      }
				      bitmask_set(&(yyval.node)->v.catch.exmask->bm,
						  p->value.v.number);
			      }
		      }
		      (yyval.node)->v.catch.node = (yyvsp[-1].stmtlist).head;
	      }
#line 4501 "gram.c" /* yacc.c:1646  */
    break;

  case 212:
#line 2195 "gram.y" /* yacc.c:1646  */
    {
		      (yyval.catchlist).all = 1;
	      }
#line 4509 "gram.c" /* yacc.c:1646  */
    break;

  case 213:
#line 2199 "gram.y" /* yacc.c:1646  */
    {
		      (yyval.catchlist).all = 0;
		      (yyval.catchlist).valist = (yyvsp[0].valist_list).head;
	      }
#line 4518 "gram.c" /* yacc.c:1646  */
    break;

  case 214:
#line 2206 "gram.y" /* yacc.c:1646  */
    {
                      (yyval.node) = alloc_node(node_type_throw, &(yylsp[-2]));
		      if ((yyvsp[-1].value).type != dtype_number) 
                              parse_error_locus(&(yylsp[-1]),
                                           _("exception code not a number"));
		      else if ((yyvsp[-1].value).v.number > exception_count)
                              parse_error_locus(&(yylsp[-1]),
                                           _("invalid exception number: %lu"),
					  (yyvsp[-1].value).v.number);

		      (yyval.node)->v.throw.code = (yyvsp[-1].value).v.number;
		      (yyval.node)->v.throw.expr = cast_to(dtype_string, (yyvsp[0].node));
	      }
#line 4536 "gram.c" /* yacc.c:1646  */
    break;

  case 215:
#line 2222 "gram.y" /* yacc.c:1646  */
    {
		      if (!func) 
			      parse_error_locus(&(yylsp[0]),
				      _("`return' outside of a function"));
		      else if (func->rettype != dtype_unspecified) 
			      parse_error_locus(&(yylsp[0]),
				      _("`return' with no value, in function "
				      "returning non-void"));
		      (yyval.node) = alloc_node(node_type_return, &(yylsp[0]));
		      (yyval.node)->v.node = NULL;
	      }
#line 4552 "gram.c" /* yacc.c:1646  */
    break;

  case 216:
#line 2234 "gram.y" /* yacc.c:1646  */
    {
		      if (!func) 
			      parse_error_locus(&(yylsp[-1]),
					_("`return' outside of a function"));
		      else {
			      (yyval.node) = alloc_node(node_type_return, &(yylsp[-1]));
			      if (func->rettype == dtype_unspecified) { 
				      parse_error_locus(&(yylsp[-1]),
				       _("`return' with a value, in function "
				       "returning void"));
				      (yyval.node)->v.node = NULL;
			      }
			      else
				      (yyval.node)->v.node = cast_to(func->rettype, (yyvsp[0].node));
		      }
	      }
#line 4573 "gram.c" /* yacc.c:1646  */
    break;

  case 217:
#line 2253 "gram.y" /* yacc.c:1646  */
    {
		      switch (node_type((yyvsp[0].node))) {
		      case dtype_string:
			      (yyval.node) = alloc_node(node_type_bin, &(yylsp[0]));
			      (yyval.node)->v.bin.opcode = bin_ne;
			      (yyval.node)->v.bin.arg[0] = (yyvsp[0].node);
			      (yyval.node)->v.bin.arg[1] = alloc_node(node_type_string, &(yylsp[0]));
			      (yyval.node)->v.bin.arg[1]->v.literal = literal_lookup("");
			      break;
			      
		      case dtype_number:
		      case dtype_pointer:
			      (yyval.node) = (yyvsp[0].node);
			      break;
			      
		      default:
			      parse_error_locus(&(yylsp[0]),
						_("unspecified data type in conditional expression: please, report"));
			      YYERROR;
		      }
	      }
#line 4599 "gram.c" /* yacc.c:1646  */
    break;

  case 218:
#line 2281 "gram.y" /* yacc.c:1646  */
    {
		     NODE *sel, *np;
		     NODE *head = NULL, *tail;
		     struct function *fp;
		     
		     fp = function_lookup((yyvsp[-3].poll).client_addr ?
					  "strictpoll" : "stdpoll");
		     if (!fp) {
			     parse_error_locus(&(yylsp[-4]),
					       _("`on poll' used without prior `require poll'"));
			     YYERROR;
		     }

		     /* Build argument list */
		     if ((yyvsp[-3].poll).client_addr) {
			     head = tail = (yyvsp[-3].poll).client_addr;
			     tail = (yyvsp[-3].poll).email;
			     if (!tail) {
                                     parse_error_locus(&(yylsp[-3]),
                                          _("recipient address not specified "
                                            "in `on poll' construct"));
				     YYERROR;
			     }
			     tail->next = NULL;
			     head->next = tail;
		     } else
			     head = tail = (yyvsp[-3].poll).email;

		     if ((yyvsp[-3].poll).ehlo)
			     np = (yyvsp[-3].poll).ehlo;
		     else {
			     /* FIXME: Pass NULL? */
                             np = alloc_node(node_type_variable, &(yylsp[-3]));
			     np->v.var_ref.variable =
				     variable_lookup("ehlo_domain");
			     np->v.var_ref.nframes = 0;
		     }
		     tail->next = np;
		     tail = np;

		     if ((yyvsp[-3].poll).mailfrom)
			     np = (yyvsp[-3].poll).mailfrom;
		     else {
			     /* FIXME: Pass NULL? */
                             np = alloc_node(node_type_variable, &(yylsp[-3]));
			     np->v.var_ref.variable =
				     variable_lookup("mailfrom_address");
			     np->v.var_ref.nframes = 0;
		     }
		     tail->next = np;
		     tail = np;

		     sel = function_call(fp, nodelistlength(head), head);

		     (yyval.node) = alloc_node(node_type_switch, &(yylsp[-4]));
		     (yyval.node)->v.switch_stmt.node = sel;
		     (yyval.node)->v.switch_stmt.cases = (yyvsp[-1].case_list).head;
	     }
#line 4662 "gram.c" /* yacc.c:1646  */
    break;

  case 219:
#line 2340 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_switch, &(yylsp[-4]));
		     (yyval.node)->v.switch_stmt.node = (yyvsp[-3].node);
		     (yyval.node)->v.switch_stmt.cases = (yyvsp[-1].case_list).head;
             }
#line 4672 "gram.c" /* yacc.c:1646  */
    break;

  case 220:
#line 2348 "gram.y" /* yacc.c:1646  */
    {
                     tie_in_onblock(1);
             }
#line 4680 "gram.c" /* yacc.c:1646  */
    break;

  case 221:
#line 2354 "gram.y" /* yacc.c:1646  */
    {
                     tie_in_onblock(0);
             }
#line 4688 "gram.c" /* yacc.c:1646  */
    break;

  case 222:
#line 2360 "gram.y" /* yacc.c:1646  */
    {
                     struct pollarg arg;
                     
                     arg.kw = T_FOR;
                     arg.expr = (yyvsp[0].node);
                     memset(&(yyval.poll), 0, sizeof (yyval.poll));
                     set_poll_arg(&(yyval.poll), arg.kw, arg.expr);
             }
#line 4701 "gram.c" /* yacc.c:1646  */
    break;

  case 223:
#line 2369 "gram.y" /* yacc.c:1646  */
    {
                     struct pollarg arg;
                     
                     arg.kw = T_FOR;
                     arg.expr = (yyvsp[-1].node);
                     set_poll_arg(&(yyvsp[0].poll), arg.kw, arg.expr);
                     (yyval.poll) = (yyvsp[0].poll);
             }
#line 4714 "gram.c" /* yacc.c:1646  */
    break;

  case 224:
#line 2378 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.poll) = (yyvsp[0].poll);
             }
#line 4722 "gram.c" /* yacc.c:1646  */
    break;

  case 225:
#line 2384 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.poll), 0, sizeof (yyval.poll));
                     set_poll_arg(&(yyval.poll), (yyvsp[0].pollarg).kw, (yyvsp[0].pollarg).expr);
             }
#line 4731 "gram.c" /* yacc.c:1646  */
    break;

  case 226:
#line 2389 "gram.y" /* yacc.c:1646  */
    {
                     set_poll_arg(&(yyvsp[-1].poll), (yyvsp[0].pollarg).kw, (yyvsp[0].pollarg).expr);
                     (yyval.poll) = (yyvsp[-1].poll);
             }
#line 4740 "gram.c" /* yacc.c:1646  */
    break;

  case 227:
#line 2396 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_FOR;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4749 "gram.c" /* yacc.c:1646  */
    break;

  case 228:
#line 2401 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_HOST;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4758 "gram.c" /* yacc.c:1646  */
    break;

  case 229:
#line 2406 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_AS;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4767 "gram.c" /* yacc.c:1646  */
    break;

  case 230:
#line 2411 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_FROM;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4776 "gram.c" /* yacc.c:1646  */
    break;

  case 231:
#line 2419 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.case_list).head = (yyval.case_list).tail = (yyvsp[0].case_stmt);
             }
#line 4784 "gram.c" /* yacc.c:1646  */
    break;

  case 232:
#line 2423 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-1].case_list).tail->next = (yyvsp[0].case_stmt);
                     (yyvsp[-1].case_list).tail = (yyvsp[0].case_stmt);
                     (yyval.case_list) = (yyvsp[-1].case_list);
             }
#line 4794 "gram.c" /* yacc.c:1646  */
    break;

  case 233:
#line 2431 "gram.y" /* yacc.c:1646  */
    {
		     struct valist *p;

		     for (p = (yyvsp[-2].valist_list).head; p; p = p->next) {
			     if (p->value.type == dtype_string) {
                                     parse_error_locus(&(yylsp[-2]),
                                                       _("invalid data type, "
                                                         "expected number"));
				     /* Try to continue */
				     p->value.type = dtype_number;
				     p->value.v.number = 0;
			     }
		     }
		     
		     (yyval.case_stmt) = mu_alloc(sizeof *(yyval.case_stmt));
		     (yyval.case_stmt)->next = NULL;
		     mu_locus_range_init (&(yyval.case_stmt)->locus);
                     mu_locus_range_copy (&(yyval.case_stmt)->locus, &(yylsp[-3]));
		     (yyval.case_stmt)->valist = (yyvsp[-2].valist_list).head;
		     (yyval.case_stmt)->node = (yyvsp[0].stmtlist).head;
	     }
#line 4820 "gram.c" /* yacc.c:1646  */
    break;


#line 4824 "gram.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 2454 "gram.y" /* yacc.c:1906  */


int
yyerror(char const *s)
{
        parse_error("%s", s);
        return 0;
}

struct stream_state
{
	int mode;
	struct mu_locus_range loc;
	int sevmask;
};

void
stream_state_save(mu_stream_t str, struct stream_state *st)
{
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_GET_MODE, &st->mode);
	mu_locus_range_init(&st->loc);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_GET_LOCUS_RANGE, &st->loc);
	mu_stream_ioctl (mu_strerr, MU_IOCTL_LOGSTREAM,
			 MU_IOCTL_LOGSTREAM_GET_SEVERITY_MASK,
			 &st->sevmask);
}

void
stream_state_restore(mu_stream_t str, struct stream_state *st)
{
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_MODE, &st->mode);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_LOCUS_RANGE, &st->loc);
	mu_stream_ioctl (mu_strerr, MU_IOCTL_LOGSTREAM,
			 MU_IOCTL_LOGSTREAM_SET_SEVERITY_MASK,
			 &st->sevmask);
	mu_locus_range_deinit(&st->loc);
}

int
parse_program(char *name, int ydebug)
{
        int rc;
	struct stream_state st;
	int mode;
	
	stream_state_save(mu_strerr, &st);
	
	mode = st.mode | MU_LOGMODE_LOCUS | MU_LOGMODE_SEVERITY;
	mu_stream_ioctl (mu_strerr, MU_IOCTL_LOGSTREAM,
			 MU_IOCTL_LOGSTREAM_SET_MODE, &mode);
	mode = MU_DEBUG_LEVEL_MASK (MU_DIAG_ERROR);
	mu_stream_ioctl (mu_strerr, MU_IOCTL_LOGSTREAM,
			 MU_IOCTL_LOGSTREAM_SET_SEVERITY_MASK,
			 &mode);
	
        yydebug = ydebug;
        if (lex_new_source(name, 0))
                return -1;
        outer_context = inner_context = context_none;
        catch_nesting = 0;
        code_immediate(NULL, ptr); /* Reserve 0 slot */
        rc = yyparse() + error_count;

	stream_state_restore(mu_strerr, &st);
	
        return rc;
}

static void
alloc_locus(struct mu_locus_range const *locus)
{
	struct literal *lit;
	if (locus->beg.mu_file) {
		lit = string_alloc(locus->beg.mu_file,
				   strlen(locus->beg.mu_file));
		lit->flags |= SYM_REFERENCED;
	}
	if (!mu_locus_point_same_file(&locus->beg, &locus->end)) {
		lit = string_alloc(locus->end.mu_file,
				   strlen(locus->end.mu_file));
		lit->flags |= SYM_REFERENCED;
	}
}

NODE *
alloc_node(enum node_type type, struct mu_locus_range const *locus)
{
        NODE *node = malloc(sizeof(*node));
        if (!node) {
                yyerror("Not enough memory");
                abort();
        }
        node->type = type;
        mu_locus_range_init (&node->locus);
        mu_locus_range_copy (&node->locus, locus);
	alloc_locus(locus);
        node->next = NULL;
        return node;
}

void
free_node(NODE *node)
{
	mu_locus_range_deinit(&node->locus);
        free(node);
}

void
copy_node(NODE *dest, NODE *src)
{
        dest->type = src->type;
        mu_locus_range_copy (&dest->locus, &src->locus);
        dest->v = src->v;
}

void
free_subtree(NODE *node)
{
        /*FIXME*/
}

void
free_parser_data()
{
	/*FIXME*/
}


/* Print parse tree */

static void print_node_list(NODE *node, int indent);
static void print_node_list_reverse(NODE *node, int level);
static void print_node(NODE *node, int indent);
void print_stat(sfsistat stat);
static int dbg_setreply(void *data, char *code, char *xcode, char *message);
static void dbg_msgmod(void *data, struct msgmod_closure *clos);

static void
print_level(int level)
{
        level *= 2;
        printf("%*.*s", level, level, "");
}

static void
print_bin_op(enum bin_opcode opcode)
{
        char *p;
        switch (opcode) {
        case bin_and:
                p = "AND";
                break;
                
        case bin_or:
                p = "OR";
                break;
                
        case bin_eq:
                p = "EQ";
                break;
                
        case bin_ne:
                p = "NE";
                break;
                
        case bin_lt:
                p = "LT";
                break;
                
        case bin_le:
                p = "LE";
                break;
                
        case bin_gt:
                p = "GT";
                break;
                
        case bin_ge:
                p = "GE";
                break;
                
        case bin_match:
                p = "MATCH";
                break;
                
        case bin_fnmatch:
                p = "FNMATCH";
                break;

        case bin_add:
                p = "ADD";
                break;
                
        case bin_sub:
                p = "SUB";
                break;
                
        case bin_mul:
                p = "MUL";
                break;
                
        case bin_div:
                p = "DIV";
                break;

        case bin_mod:
                p = "MOD";
                break;

        case bin_logand:
                p = "LOGAND";
                break;
                
        case bin_logor:
                p = "LOGOR";
                break;
                
        case bin_logxor:
                p = "LOGXOR";
                break;

	case bin_shl:
		p = "SHL";
		break;

	case bin_shr:
		p = "SHR";
		break;
		
        default:
                p = "UNKNOWN_OP";
        }
        printf("%s", p);
}

static void
print_quoted_string(const char *str)
{
        for (; *str; str++) {
                if (mu_isprint(*str))
                        putchar(*str);
                else {
                        putchar('\\');
                        switch (*str) {
                        case '\a':
                                putchar('a');
                                break;
                        case '\b':
                                putchar('b');
                                break;
                        case '\f':
                                putchar('f');
                                break;
                        case '\n':
                                putchar('n');
                                break;
                        case '\r':
                                putchar('r');
                                break;
                        case '\t':
                                putchar('t');
                                break;
                        default:
                                printf("%03o", *str);
                        }
                }
        }
}

struct node_drv {
        void (*print) (NODE *, int);
        void (*mark) (NODE *);
        void (*code) (NODE *, struct mu_locus_range const **);
        void (*optimize) (NODE *);
};

static void traverse_tree(NODE *node);
static void code_node(NODE *node);
static void code_node(NODE *node);
static void optimize_node(NODE *node);
static void optimize(NODE *node);
static void record_switch(struct switch_stmt *sw);

#include "drivers.c"
#include "node-tab.c"

struct node_drv *
find_node_drv(enum node_type type)
{
        if (type >= NELEMS(nodetab)) {
                parse_error(_("INTERNAL ERROR at %s:%d, "
                              "unexpected node type %d"),
                            __FILE__, __LINE__,
                            type);
                abort();
        }
        return nodetab + type;
}

static void
print_node(NODE *node, int level)
{
        struct node_drv *nd = find_node_drv(node->type);
        if (nd->print)
                nd->print(node, level);
}       

static void
print_node_list(NODE *node, int level)
{
        for (; node; node = node->next)
                print_node(node, level);
}

static void
print_node_list_reverse(NODE *node, int level)
{
        if (node) {
                print_node_list_reverse(node->next, level);
                print_node(node, level);
        }
}

int
function_enumerator(void *sym, void *data)
{
        int i;
        struct function *fsym = sym;
        struct function *f =
                (struct function *)symbol_resolve_alias(&fsym->sym);
        struct module *mod = data;

        if (f->sym.module != mod)
                return 0;
        printf("function %s (", f->sym.name);
        for (i = 0; i < f->parmcount; i++) {
                printf("%s", type_to_string(f->parmtype[i]));
                if (i < f->parmcount-1)
                        putchar(',');
        }
        putchar(')');
        if (f->rettype != dtype_unspecified)
                printf(" returns %s", type_to_string(f->rettype));
        printf(":\n");
        print_node_list(f->node, 0);
        printf("END function %s\n", f->sym.name);
        return 0;
}
                
void
print_syntax_tree()
{
        struct module **modv;
        size_t i, modc;

        enum smtp_state tag;

        printf("State handlers:\n");
        printf("---------------\n");
        for (tag = smtp_state_first; tag < smtp_state_count; tag++) {
                if (root_node[tag]) {
                        printf("%s:\n", state_to_string(tag));
                        print_node_list(root_node[tag], 0);
                        putchar('\n');
                }
        }
        printf("User functions:\n");
        printf("---------------\n");
        
        collect_modules(&modv, &modc);
        for (i = 0; i < modc; i++) {
                size_t n;

                n = printf("Module %s (%s)\n", modv[i]->name,
                           modv[i]->file);
                while (n--)
                        putchar('-');
                putchar('\n');
                symtab_enumerate(MODULE_SYMTAB(modv[i], namespace_function),
                                 function_enumerator, modv[i]);
                putchar('\n');
        }
        free(modv);
}


/* Cross-reference support */

struct collect_data {
	mu_opool_t pool;
        size_t count;
};

static int
variable_enumerator(void *item, void *data)
{
        struct variable *var = item;
        struct collect_data *p = data;
        if (var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED)) {
                mu_opool_append(p->pool, &var, sizeof var);
                p->count++;
        }
        return 0;
}

int
print_locus(void *item, void *data)
{
        struct mu_locus_range *loc = item;
        struct mu_locus_range **prev = data;
        int c;

        if (!*prev) {
                *prev = loc;
                printf("%s", loc->beg.mu_file);
                c = ':';
        } else if (mu_locus_point_same_file (&(*prev)->beg, &loc->beg)) {
                *prev = loc;
                printf(", %s", loc->beg.mu_file);
                c = ':';
        } else
                c = ',';
        printf("%c%u", c, loc->beg.mu_line);
        return 0;
}

void
print_xref_var(struct variable *var)
{
        struct mu_locus_range *prev = NULL;
        var = (struct variable *)symbol_resolve_alias(&var->sym);
        printf("%-32.32s %6s %lu ",
               var->sym.name, type_to_string(var->type),
               (unsigned long)var->off);
        mu_list_foreach(var->xref, print_locus, &prev);
        printf("\n");
}

int
vp_comp(const void *a, const void *b)
{
        struct variable * const *va = a, * const *vb = b;
        return strcmp((*va)->sym.name, (*vb)->sym.name);
}

void
print_xref()
{
        struct collect_data cd;
        struct variable **vp;
        size_t i;
        
        mu_opool_create(&cd.pool, MU_OPOOL_ENOMEMABRT);
        cd.count = 0;
        symtab_enumerate(TOP_MODULE_SYMTAB(namespace_variable),
                         variable_enumerator, &cd);
        printf("Cross-references:\n");
        printf("-----------------\n");
        
        vp = mu_opool_finish(cd.pool, NULL);
        qsort(vp, cd.count, sizeof *vp, vp_comp);
        for (i = 0; i < cd.count; i++, vp++) 
                print_xref_var(*vp);
        mu_opool_destroy(&cd.pool);
}


static mu_list_t smtp_macro_list[gacopyz_stage_max];

static enum gacopyz_stage 
smtp_to_gacopyz_stage(enum smtp_state tag)
{
        switch (tag) {
        case smtp_state_connect:
                return gacopyz_stage_conn;
        case smtp_state_helo:
                return gacopyz_stage_helo;
        case smtp_state_envfrom:
                return gacopyz_stage_mail;
        case smtp_state_envrcpt:
                return gacopyz_stage_rcpt;
        case smtp_state_data:
        case smtp_state_header:
        case smtp_state_body:
                return gacopyz_stage_data;
        case smtp_state_eoh:
                return gacopyz_stage_eoh;
        case smtp_state_eom:
                return gacopyz_stage_eom;
        default:
                break;
        }
        return gacopyz_stage_none;
}

static int
compare_macro_names (const void *item, const void *data)
{
	const char *elt;
	size_t elen;
	const char *arg;
	size_t alen;

	elt = item;
	elen = strlen (elt);
	if (elt[0] == '{') {
		elt++;
		elen -= 2;
	}

	arg = data;
	alen = strlen (arg);
	if (arg[0] == '{') {
		arg++;
		alen -= 2;
	}

	if (alen != elen)
		return 1;
	return memcmp (elt, arg, alen);
}

void
register_macro(enum smtp_state state, const char *macro)
{
        enum gacopyz_stage ind = smtp_to_gacopyz_stage(state);
	
        if (ind == gacopyz_stage_none)
                return;
        if (!smtp_macro_list[ind]) {
                mu_list_create(&smtp_macro_list[ind]);
                mu_list_set_comparator(smtp_macro_list[ind],
				       compare_macro_names);
        }
        /* FIXME: MU: 2nd arg should be const? */
        if (mu_list_locate(smtp_macro_list[ind], (void*) macro, NULL)) {
		char *cmacro;
		if (macro[1] == 0 || macro[0] == '{')
			cmacro = mu_strdup (macro);
		else {
			size_t mlen = strlen (macro);
			cmacro = mu_alloc (mlen + 3);
			cmacro[0] = '{';
			memcpy (cmacro + 1, macro, mlen);
			cmacro[mlen + 1] = '}';
			cmacro[mlen + 2] = 0;
		}
                mu_list_append(smtp_macro_list[ind], cmacro);
	}
}

static int
print_macro(void *item, void *data)
{
        int *p = data;
        if (*p) {
                printf(" ");
                *p = 0;
        } else
                printf(", ");
        printf("%s", (char*) item);
        return 0;
}

void
print_used_macros()
{
        enum gacopyz_stage i;
        
        for (i = 0; i < gacopyz_stage_max; i++) {
                if (smtp_macro_list[i]) {
                        int n = 1;
                        printf("%s", gacopyz_stage_name[i]);
                        mu_list_foreach(smtp_macro_list[i], print_macro, &n);
                        printf("\n");
                }
        }
}

struct macro_acc {
        size_t size;
        char *buf;
};

static int
add_macro_size(void *item, void *data)
{
        char *macro = (char*) item;
        struct macro_acc *mp = data;
        mp->size += strlen(macro) + 1;
        return 0;
}

static int
concat_macro(void *item, void *data)
{
        char *macro = (char*) item;
        struct macro_acc *mp = data;
        size_t len = strlen(macro);
        char *pbuf = mp->buf + mp->size;
        memcpy(pbuf, macro, len);
        pbuf[len++] = ' ';
        mp->size += len;
        return 0;
}

char *
get_stage_macro_string(enum gacopyz_stage i)
{
        struct macro_acc acc;
        size_t size;
        mu_list_t list = smtp_macro_list[i];
        if (!list)
                return NULL;
                
        acc.size = 0;
        mu_list_foreach(list, add_macro_size, &acc);
        if (!acc.size)
                return NULL;
        size = acc.size;
                
        acc.size = 0;
        acc.buf = mu_alloc (size);
        mu_list_foreach(list, concat_macro, &acc);
        acc.buf[size-1] = 0;
        return acc.buf;
}


/* Code generation */

static void
code_node(NODE *node)
{
        if (!node) 
                error_count++;
        else {
                static struct mu_locus_range const *old_locus;
                struct node_drv *nd = find_node_drv(node->type);
                if (nd->code)
                        nd->code(node, &old_locus);
        }
}

static void
traverse_tree(NODE *node)
{
        for (; node; node = node->next)
                code_node(node);
}

static void
optimize_node(NODE *node)
{
        if (!node) 
                error_count++;
        else {
                struct node_drv *nd = find_node_drv(node->type);
                if (nd->optimize)
                        nd->optimize(node);
        }
}

static void
optimize(NODE *node)
{
        for (; node; node = node->next)
                optimize_node(node);
}

static int
optimize_tree(NODE *node)
{
        if (optimization_level)
                optimize(node);
        return error_count;
}


static struct switch_stmt *switch_root;

static void
record_switch(struct switch_stmt *sw)
{
        sw->next = switch_root;
        switch_root = sw;
}


static struct exmask *exmask_root;

struct exmask *
exmask_create()
{
	struct exmask *p = mu_alloc(sizeof(*p));
	p->next = exmask_root;
	p->off = 0;
	p->all = 0;
	bitmask_init(&p->bm);
	exmask_root = p;
	return p;
}



static void
mark_node(NODE *node)
{
        if (!node) 
                error_count++;
        else {
                struct node_drv *nd = find_node_drv(node->type);
                if (nd->mark)
                        nd->mark(node);
        }
}

static void
mark(NODE *node)
{
        for (; node; node = node->next)
                mark_node(node);
}
        

static int
codegen(prog_counter_t *pc, NODE *node, struct exmask *exmask,
	int finalize, size_t nautos)
{
	int save_mask;
	
	if (error_count)
		return 1;
	
	*pc = code_get_counter();
	jump_pc = 0;
	if (nautos) {
		code_op(opcode_stkalloc);
		code_immediate(nautos, uint);
	}
	save_mask = exmask && bitmask_nset(&exmask->bm);
	if (save_mask) {
		code_op(opcode_saveex);
		code_exmask(exmask);
	}
	traverse_tree(node);

	jump_fixup(jump_pc, code_get_counter());
	if (save_mask) 
		code_op(opcode_restex);
	
	if (finalize)
		code_op(opcode_nil);
	else
		code_op(opcode_return);
		
	return 0;
}

static void
compile_tree(NODE *node)
{
        struct node_drv *nd;
        struct mu_locus_range const *plocus;
        
        for (; node; node = node->next) {
                switch (node->type) {
                case node_type_progdecl:
                case node_type_funcdecl:
                        nd = find_node_drv(node->type);
                        if (!nd->code)
                                abort();
                        nd->code(node, &plocus);
                        break;
                        
                default:
                        parse_error_locus(&node->locus,
                                          _("INTERNAL ERROR at %s:%d, "
                                            "unexpected node type %d"),
                                          __FILE__, __LINE__,
                                          node->type);
                        break;
                }
        }
}
                



enum regex_mode { regex_enable, regex_disable, regex_set };

static mf_stack_t regex_stack;

void
regex_push()
{
        if (!regex_stack)
                regex_stack = mf_stack_create(sizeof regex_flags, 0);
        mf_stack_push(regex_stack, &regex_flags);
}

void
regex_pop()
{
        if (!regex_stack || mf_stack_pop(regex_stack, &regex_flags))
                parse_error(_("nothing to pop"));
}

static void
pragma_regex(int argc, char **argv, const char *text)
{
        enum regex_mode mode = regex_set;
        int i = 1;

        if (strcmp(argv[i], "push") == 0) {
                regex_push();
                i++;
        } else if (strcmp(argv[i], "pop") == 0) {
                regex_pop();
                i++;
        }

        for (; i < argc; i++) {
                int bit;
                char *p = argv[i];
                switch (p[0]) {
                case '+':
                        mode = regex_enable;
                        p++;
                        break;

                case '-':
                        mode = regex_disable;
                        p++;
                        break;

                case '=':
                        mode = regex_set;
                        p++;
                        break;
                }

                if (strcmp (p, REG_EXTENDED_NAME) == 0)
                        bit = REG_EXTENDED;
                else if (strcmp (p, REG_ICASE_NAME) == 0)
                        bit = REG_ICASE;
                else if (strcmp (p, REG_NEWLINE_NAME) == 0)
                        bit = REG_NEWLINE;
                else {
                        parse_error(_("unknown regexp flag: %s"), p);
                        return;
                }

                switch (mode) {
                case regex_disable:
                        regex_flags &= ~bit;
                        break;
                case regex_enable:
                        regex_flags |= bit;
                        break;
                case regex_set:
                        regex_flags = bit;
                        break;
                }
        }
}       

static int
strtosize(const char *text, size_t *psize)
{
        unsigned long size;
	size_t factor = 1;
        char *p;

        size = strtoul(text, &p, 0);
	if (size == ULONG_MAX && errno == ERANGE) {
                parse_error(_("invalid size: numeric overflow occurred"));
                return 2;
	}
	
        switch (*p) {
        case 't':
        case 'T':
                factor = 1024;
        case 'g':
        case 'G':
                factor *= 1024;
        case 'm':
        case 'M':
                factor *= 1024;
        case 'k':
        case 'K':
                factor *= 1024;
                p++;
                if (*p && (*p == 'b' || *p == 'B'))
                        p++;
                break;

        case 0:
		factor = 1;
                break;
                
        default:
                parse_error(_("invalid size suffix (near %s)"), p);
                return 1;
        }

	if (((size_t)-1) / factor < size) {
                parse_error(_("invalid size: numeric overflow occurred"));
                return 2;
        }
        
        *psize = size * factor;

        return 0;
}

static void
pragma_stacksize(int argc, char **argv, const char *text)
{
        size_t size, incr = stack_expand_incr, max_size = stack_max_size;
        enum stack_expand_policy policy = stack_expand_policy;

        switch (argc) {
        case 4:
                if (strtosize(argv[3], &max_size))
                        return;
        case 3:
                if (strcmp(argv[2], "twice") == 0) 
                        policy = stack_expand_twice;
                else {
                        policy = stack_expand_add;
                                
                        if (strtosize(argv[2], &incr))
                                return;
                }
        case 2:
                if (strtosize(argv[1], &size))
			return;
        }

        stack_size = size;
        stack_expand_incr = incr;
        stack_expand_policy = policy;
        stack_max_size = max_size;
}

void
pragma_setup()
{
	install_pragma("regex", 2, 0, pragma_regex);
	install_pragma("stacksize", 2, 4, pragma_stacksize);
}



/* Test run */
struct sfsistat_tab {
        char *name;
        sfsistat stat;
} sfsistat_tab[] = {
        { "accept", SMFIS_ACCEPT },
        { "continue", SMFIS_CONTINUE },
        { "discard", SMFIS_DISCARD },
        { "reject", SMFIS_REJECT },
        { "tempfail", SMFIS_TEMPFAIL },
        { NULL }
};

const char *
sfsistat_str(sfsistat stat)
{
        struct sfsistat_tab *p;
        for (p = sfsistat_tab; p->name; p++)
                if (p->stat == stat) 
                        return p->name;
        return NULL;
}

void
print_stat(sfsistat stat)
{
        struct sfsistat_tab *p;
        for (p = sfsistat_tab; p->name; p++)
                if (p->stat == stat) {
                        printf("%s", p->name);
                        return;
                }
        printf("%d", stat);
}

const char *
msgmod_opcode_str(enum msgmod_opcode opcode)
{
        switch (opcode) {
        case header_add:
                return "ADD HEADER";
                
        case header_replace:
                return "REPLACE HEADER";

        case header_delete:
                return "DELETE HEADER";

        case header_insert:
                return "INSERT HEADER";
                
        case rcpt_add:
                return "ADD RECIPIENT";

        case rcpt_delete:
                return "DELETE RECIPIENT";

        case quarantine:
                return "QUARANTINE";

        case body_repl:
                return "REPLACE BODY";

	case body_repl_fd:
		return "REPLACE BODY FROM FILE";
		
	case set_from:
		return "SET FROM";
		
        }
        return "UNKNOWN HEADER COMMAND";
}

static int
dbg_setreply(void *data, char *code, char *xcode, char *message)
{
        if (code) {
                printf("SET REPLY %s", code);
                if (xcode)
                        printf(" %s", xcode);
                if (message)
                        printf(" %s", message);
                printf("\n");
        }
        return 0;
}

static void
dbg_msgmod(void *data, struct msgmod_closure *clos)
{
	if (!clos)
		printf("clearing msgmod list\n");
	else
		printf("%s %s: %s %u\n", msgmod_opcode_str(clos->opcode),
		       SP(clos->name), SP(clos->value), clos->idx);
}

static const char *
dbg_dict_getsym (void *data, const char *str)
{
        return dict_getsym ((mu_assoc_t)data, str);
}

void
mailfromd_test(int argc, char **argv)
{
	int i;
	mu_assoc_t dict = NULL;
	eval_environ_t env;
	char *p, *end;
	long n;
	sfsistat status;
	char *args[9] = {0,0,0,0,0,0,0,0,0};
		
	dict_init(&dict);
	env = create_environment(NULL,
				 dbg_dict_getsym, dbg_setreply, dbg_msgmod,
				 dict);
	env_init(env);
	xeval(env, smtp_state_begin);

	env_init(env);
	for (i = 0; i < argc; i++) {
		if (p = strchr(argv[i], '=')) {
			char *ident = argv[i];
			*p++ = 0;
			if (mu_isdigit(*ident) && *ident != 0
				 && ident[1] == 0) 
				args[*ident - '0' - 1] = p;
			else 
				dict_install(dict, argv[i], p);
		}
	}

	for (i = state_parms[test_state].cnt; i--; ) {
		switch (state_parms[test_state].types[i]) {
		case dtype_string:
			env_push_string(env, args[i] ? args[i] : "");
			break;
		case dtype_number:
			if (args[i]) {
				n = strtol(args[i], &end, 0);
				if (*end) 
					mu_error(_("$%d is not a number"),
						 i+1);
			} else
				n = 0;
			env_push_number(env, n);
			break;
		default:
			abort();
		}
	}

	test_message_data_init(env);
	
	env_make_frame(env);

	xeval(env, test_state);
	env_leave_frame(env, state_parms[test_state].cnt);
	env_final_gc(env);

	status = environment_get_status(env);

	env_init(env);
	xeval(env, smtp_state_end);

	printf("State %s: ", state_to_string(test_state));
	print_stat(status);
	printf("\n");
	destroy_environment(env);
}

void
mailfromd_run(prog_counter_t entry_point, int argc, char **argv)
{
	int rc, i;
	mu_assoc_t dict = NULL;
	eval_environ_t env;

	dict_init(&dict);
	env = create_environment(NULL,
				 dbg_dict_getsym, dbg_setreply, dbg_msgmod,
				 dict);

	env_init(env);
	test_message_data_init(env);
	
	env_push_number(env, 0);

	for (i = argc - 1; i >= 0; i--)
		env_push_string(env, argv[i]);
	env_push_number(env, argc);
	
	env_make_frame0(env);
	rc = eval_environment(env, entry_point);
	env_final_gc(env);
	rc = mf_c_val(env_get_reg(env), long);
	destroy_environment(env);
	exit(rc);
}

static struct tagtable {
        char *name;
        enum smtp_state tag;
} tagtable[] = {
        { "none", smtp_state_none },
        { "begin", smtp_state_begin },
        { "connect", smtp_state_connect },
        { "helo", smtp_state_helo },  
        { "envfrom", smtp_state_envfrom }, 
        { "envrcpt", smtp_state_envrcpt }, 
        { "data", smtp_state_data },
        { "header", smtp_state_header }, 
        { "eoh", smtp_state_eoh }, 
        { "body", smtp_state_body }, 
        { "eom", smtp_state_eom },
        { "end", smtp_state_end },
};

enum smtp_state
string_to_state(const char *name)
{
        struct tagtable *p;

        for (p = tagtable; p < tagtable + sizeof tagtable/sizeof tagtable[0];
             p++)
                if (strcasecmp (p->name, name) == 0)
                        return p->tag;

        return smtp_state_none;
}

const char *
state_to_string(enum smtp_state state)
{
        if (state < sizeof tagtable/sizeof tagtable[0])
                return tagtable[state].name;
        abort();
}

static NODE *
_reverse(NODE *list, NODE **root)
{
        NODE *next;

        if (list->next == NULL) {
                *root = list;
                return list;
        }
        next = _reverse(list->next, root);
        next->next = list;
        list->next = NULL;
        return list;
}               

NODE *
reverse(NODE *in)
{
        NODE *root;
        if (!in)
                return in;
        _reverse(in, &root);
        return root;
}

size_t
nodelistlength(NODE *p)
{
	size_t len = 0;
	for (; p; p = p->next)
		len++;
	return len;
}

static NODE *
create_asgn_node(struct variable *var, NODE *expr,
		 struct mu_locus_range const *loc)
{
        NODE *node;
        data_type_t t = node_type(expr);

        if (t == dtype_unspecified) {
                parse_error(_("unspecified value not ignored as it should be"));
                return NULL;
        }
        node = alloc_node(node_type_asgn, loc);
        node->v.asgn.var = var;
        node->v.asgn.nframes = catch_nesting;
        node->v.asgn.node = cast_to(var->type, expr);
	var->initialized = 1;
        return node;
}


NODE *
function_call(struct function *function, size_t count, NODE *subtree)
{
        NODE *np = NULL;
        if (count < function->parmcount - function->optcount) {
                parse_error(_("too few arguments in call to `%s'"),
                            function->sym.name);
        } else if (count > function->parmcount && !function->varargs) {
                parse_error(_("too many arguments in call to `%s'"),
                            function->sym.name);
        } else {
                np = alloc_node(node_type_call, &yylloc);
                np->v.call.func = function;
                np->v.call.args = reverse(cast_arg_list(subtree,
                                                        function->parmcount,
                                                        function->parmtype,
                                                        0));
        }
        return np;
}       

data_type_t
node_type(NODE *node)
{
	switch (node->type) {
	case node_type_string:
	case node_type_symbol:
	case node_type_sed:
	case node_type_concat:
	case node_type_argx:
	case node_type_backref:
		return dtype_string;

	case node_type_number:
	case node_type_bin:
	case node_type_un:
	case node_type_sedcomp:
	case node_type_offset:
	case node_type_vaptr:
		return dtype_number;
		
	case node_type_if:
		return dtype_unspecified;
		
	case node_type_builtin:
		return node->v.builtin.builtin->rettype;
		
	case node_type_variable:
		return node->v.var_ref.variable->type;
		
	case node_type_arg:
		return node->v.arg.data_type;

	case node_type_call:
		return node->v.call.func->rettype;
		
	case node_type_return:
		if (node->v.node)
			return node_type(node->v.node);
		break;

	case node_type_cast:
		return node->v.cast.data_type;

	case node_type_result:
	case node_type_header:
	case node_type_asgn:
	case node_type_regex:
	case node_type_regcomp:
	case node_type_catch:
	case node_type_try:
	case node_type_throw:
	case node_type_echo:
	case node_type_switch:
	case node_type_funcdecl:
	case node_type_progdecl:
	case node_type_noop:
	case node_type_next:
	case node_type_break:
	case node_type_loop:
	case max_node_type:
		break;
	}
	return dtype_unspecified;
}

NODE *
cast_to(data_type_t type, NODE *node)
{
        NODE *np;
        data_type_t ntype = node_type(node);
        
        switch (ntype) {
        case dtype_string:
        case dtype_number:
                if (type == ntype)
                        return node;
                break;
                
        case dtype_pointer:
                if (type == ntype)
                        return node;
                break;
                
        case dtype_unspecified:
                parse_error(_("cannot convert %s to %s"), type_to_string(ntype),
                            type_to_string(type));
                return NULL;

        default:
                abort();
        }
        np = alloc_node(node_type_cast, &yylloc);
        np->v.cast.data_type = type;
        np->v.cast.node = node;
        node->next = NULL;
        return np;
}

NODE *
cast_arg_list(NODE *args, size_t parmc, data_type_t *parmtype, int disable_prom)
{
        NODE *head = NULL, *tail = NULL;
        
        while (args) {
                NODE *next = args->next;
                NODE *p;
                data_type_t type;
                
                if (parmc) {
                        type = *parmtype++;
                        parmc--;
                } else if (disable_prom)
                        type = node_type(args);
                else
                        type = dtype_string;
                
                p = cast_to(type, args);

                if (head)
                        tail->next = p;
                else
                        head = p;
                tail = p;
                args = next;
        }
        return head;
}

void
add_xref(struct variable *var, struct mu_locus_range const *locus)
{
        if (script_dump_xref) {
		/* FIXME: either change type to mu_locus_point, or
		   change print_locus above */
                struct mu_locus_range *elt = mu_zalloc(sizeof *elt);
                if (!var->xref)
                        mu_list_create(&var->xref);
                mu_locus_range_copy(elt, locus);
                mu_list_append(var->xref, elt);
        }
}

struct variable *
vardecl(const char *name, data_type_t type, storage_class_t sc,
        struct mu_locus_range const *loc)
{
        struct variable *var;
        const struct constant *cptr;
	
	if (!loc)
		loc = &yylloc;
        if (type == dtype_unspecified) {
                parse_error(_("cannot define variable of unspecified type"));
                return NULL;
        }
        var = variable_install(name);
        if (var->type == dtype_unspecified) {
                /* the variable has just been added: go straight to
                   initializing it */;
        } else if (sc != var->storage_class) {
                struct variable *vp;
                
                switch (sc) {
                case storage_extern:
                        parse_error(_("INTERNAL ERROR at %s:%d, declaring %s %s"),
                                    __FILE__, __LINE__,
                                    storage_class_str(sc), name);
                        abort();

                case storage_auto:
                        if (var->storage_class == storage_param) {
                                parse_warning_locus(loc,
						    _("automatic variable `%s' "
						      "is shadowing a parameter"),
						    var->sym.name);
                        } else
                                parse_warning_locus(loc,
						    _("automatic variable `%s' "
						      "is shadowing a global"), 
                                              var->sym.name);
                        unregister_auto(var);
                        break;

                case storage_param:
                        parse_warning_locus(loc,
					    _("parameter `%s' is shadowing a "
					      "global"),
					    name);
                }

                /* Do the shadowing */
                vp = variable_replace(var->sym.name, NULL);
                vp->shadowed = var;
                var = vp;
        } else {
                switch (sc) {
                case storage_extern:
                        if (var->type != type) {
                                parse_error_locus(loc,
						  _("redeclaring `%s' as different "
						    "data type"),
						  name);
                                parse_error_locus(&var->sym.locus,
                                            _("this is the location of the "
                                              "previous definition"));
                                return NULL;
                        }
                        break;

                case storage_auto:
                        if (var->type != type) {
                                parse_error_locus(loc,
						  _("redeclaring `%s' as different "
						    "data type"),
						  name);
                                parse_error_locus(&var->sym.locus,
						  _("this is the location of the "
						    "previous definition"));
                                return NULL;
                        } else {
                                parse_error_locus(loc,
						  _("duplicate variable: %s"),
						  name);
                                return NULL;
                        }
                        break;

                case storage_param:
                        parse_error_locus(loc, _("duplicate parameter: %s"),
					  name);
                        return NULL;
                }
        }

        /* FIXME: This is necessary because constants can be
           referred to the same way as variables. */
        if (cptr = constant_lookup(name)) {
                parse_warning_locus(loc,
				    _("variable name `%s' clashes with a constant name"),
                              name);
                parse_warning_locus(&cptr->sym.locus,
                                    _("this is the location of the "
                                      "previous definition"));
        }
        
        var->type = type;
        var->storage_class = sc;
        switch (sc) {
        case storage_extern:
                add_xref(var, loc);
                break;
        case storage_auto:
        case storage_param:
                register_auto(var);
        }
        mu_locus_range_copy(&var->sym.locus, loc);
        return var;
}

static int
cast_value(data_type_t type, struct value *value)
{
        if (type != value->type) {
                char buf[NUMERIC_BUFSIZE_BOUND];
                char *p;
                
                switch (type) {
                default:
                        abort();
                        
                case dtype_string:
                        snprintf(buf, sizeof buf, "%ld", value->v.number);
                        value->v.literal = string_alloc(buf, strlen(buf));
                        break;

                case dtype_number:
                        value->v.number = strtol(value->v.literal->text,
                                                 &p, 10);
                        if (*p) {
                                parse_error(_("cannot convert `%s' to number"),
                                            value->v.literal->text);
                                return 1;
                        }
                        break;
                }
                value->type = type;
        }
        return 0;
}

static struct variable *
externdecl(const char *name, struct value *value,
	   struct mu_locus_range const *loc)
{
        struct variable *var = vardecl(name, value->type, storage_extern, loc);
        if (!var)
                return NULL;
        if (initialize_variable(var, value, loc))
                return NULL;
        return var;
}


struct deferred_decl {
        struct deferred_decl *next;
        struct literal *name;
        struct value value;
	struct mu_locus_range locus;
};

struct deferred_decl *deferred_decl;

void
defer_initialize_variable(const char *arg, const char *val,
			  struct mu_locus_range const *ploc)
{
        struct deferred_decl *p;
        struct literal *name = string_alloc(arg, strlen(arg));
        for (p = deferred_decl; p; p = p->next)
                if (p->name == name) {
                        parse_warning_locus(NULL, _("redefining variable %s"),
                                            name->text);
                        p->value.type = dtype_string;
                        p->value.v.literal = string_alloc(val, strlen(val));
			mu_locus_range_copy (&p->locus, ploc);
                        return;
                }
        p = mu_alloc(sizeof *p);
        p->name = name;
        p->value.type = dtype_string;
        p->value.v.literal = string_alloc(val, strlen(val));
	mu_locus_range_init (&p->locus);
	mu_locus_range_copy (&p->locus, ploc);
        p->next = deferred_decl;
        deferred_decl = p;
}

static void
apply_deferred_init()
{
        struct deferred_decl *p;
        for (p = deferred_decl; p; p = p->next) {
                struct variable *var = variable_lookup(p->name->text);
                if (!var) {
			mu_error(_("<command line>: warning: "
				   "no such variable: %s"),
				 p->name->text);
                        continue;
                }
                if (initialize_variable(var, &p->value, &p->locus))
                        parse_error_locus(&p->locus,
					  _("error initialising variable %s: incompatible types"),
					  p->name->text);
        }
}


struct declvar {
        struct declvar *next;
        struct mu_locus_range locus;
        struct variable *var;
        struct value val;
};

static struct declvar *declvar;

void
set_poll_arg(struct poll_data *poll, int kw, NODE *expr)
{
        switch (kw) {
        case T_FOR:
                poll->email = expr;
                break;
                
        case T_HOST:
                poll->client_addr = expr;
                break;
                
        case T_AS:
                poll->mailfrom = expr;
                break;
                
        case T_FROM:
                poll->ehlo = expr;
                break;
                
        default:
                abort();
        }
}
                
int
initialize_variable(struct variable *var, struct value *val,
                    struct mu_locus_range const *locus)
{
        struct declvar *dv;

        if (cast_value(var->type, val))
                return 1;
        for (dv = declvar; dv; dv = dv->next)
                if (dv->var == var) {
                        if (dv->locus.beg.mu_file) {
                                parse_warning_locus(locus,
                                     _("variable `%s' already initialized"),
                                                    var->sym.name);
                                parse_warning_locus(&dv->locus,
                                     _("this is the location of the "
                                       "previous initialization"));
                        }
			
                        if (locus)
				mu_locus_range_copy (&dv->locus, locus);
                        else
				mu_locus_range_deinit (&dv->locus);
                        dv->val = *val;
                        return 0;
                }

        dv = mu_alloc(sizeof *dv);
        dv->next = declvar;
        dv->var = var;
	mu_locus_range_init (&dv->locus);
	if (locus)
		mu_locus_range_copy (&dv->locus, locus);
        dv->val = *val;
        declvar = dv;
	var->sym.flags |= SYM_INITIALIZED;
        return 0;
}

void
ds_init_variable(const char *name, void *data)
{
        struct declvar *dv;     
        struct variable *var = variable_lookup(name);

        if (!var) {
                mu_error(_("INTERNAL ERROR at %s:%d: variable to be "
                           "initialized is not declared"),
                         __FILE__, __LINE__);
                abort();
        }

        for (dv = declvar; dv; dv = dv->next)
                if (dv->var == var)
                        return;

        dv = mu_alloc(sizeof *dv);
	mu_locus_range_init(&dv->locus);
        dv->var = var;
        dv->next = declvar;
        declvar = dv;

	switch (var->type) {
	case dtype_string:
		dv->val.v.literal = string_alloc(data, strlen(data));
		break;
		
	case dtype_number:
		dv->val.v.number = *(long*)data;
		break;

	default:
                mu_error(_("INTERNAL ERROR at %s:%d: variable to be "
                           "initialized has wrong type"),
                         __FILE__, __LINE__);
		abort ();
	}
	dv->val.type = var->type;
}	

static int
_ds_variable_count_fun(void *sym, void *data)
{
        struct variable *var = sym;

        if ((var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED))
	    && !(var->sym.flags & SYM_PASSTOGGLE)) {
		var->sym.flags |= SYM_PASSTOGGLE;
                variable_count++;
                if (var->type == dtype_string) 
                        dataseg_reloc_count++;
                if (var->sym.flags & SYM_PRECIOUS)
                        precious_count++;
        }
        return 0;
}

static int
_ds_variable_fill_fun(void *sym, void *data)
{
        struct variable *var = sym;
        
        if (var->sym.flags & SYM_PASSTOGGLE) {
		var->sym.flags &= ~SYM_PASSTOGGLE;
                struct variable ***vtabptr = data;
                **vtabptr = var;
                ++*vtabptr;
        }
        return 0;
}

static int
_ds_reloc_fun(void *sym, void *data)
{
        struct variable *var = sym;
        size_t *pi = data;
        
        if ((var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED))
	    && !(var->sym.flags & SYM_PASSTOGGLE)
	    && var->type == dtype_string) {
		var->sym.flags |= SYM_PASSTOGGLE;
                dataseg_reloc[(*pi)++] = var->off;
	}
        return 0;
}

static int
_ds_literal_count_fun(void *sym, void *data)
{
        struct literal *lit = sym;
        size_t *offset = data;
        if (!(lit->flags & SYM_VOLATILE) && (lit->flags & SYM_REFERENCED)) {
                lit->off = *offset;
                *offset += B2STACK(strlen(lit->text) + 1);
        }
        return 0;
}

static int
_ds_literal_copy_fun(void *sym, void *data)
{
        struct literal *lit = sym;
        if (!(lit->flags & SYM_VOLATILE) && (lit->flags & SYM_REFERENCED)) 
                strcpy((char*)(dataseg + lit->off), lit->text);
        return 0;
}

static int
vtab_comp(const void *a, const void *b)
{
        const struct variable *vp1 = *(const struct variable **)a;
        const struct variable *vp2 = *(const struct variable **)b;

        if ((vp1->sym.flags & SYM_PRECIOUS)
            && !(vp2->sym.flags & SYM_PRECIOUS))
                return 1;
        else if ((vp2->sym.flags & SYM_PRECIOUS) 
                 && !(vp1->sym.flags & SYM_PRECIOUS))
                return -1;
        return 0;
}

static int
place_exc(const struct constant *cp, const struct literal *lit, void *data)
{
	STKVAL *tab = data;
	tab[cp->value.v.number] = (STKVAL) lit->off;
	return 0;
}

static void
dataseg_layout()
{
	struct declvar *dv;
	size_t i;
	struct switch_stmt *sw;
	struct variable **vtab, **pvtab;
	struct exmask *exmask;
	
	/* Count used variables and estimate the number of relocations
	   needed */
	dataseg_reloc_count = 0;
	module_symtab_enumerate(namespace_variable,
				_ds_variable_count_fun,
				NULL);

	/* Fill variable pointer array and make sure precious variables
	   occupy its bottom part */
	vtab = mu_calloc(variable_count, sizeof(vtab[0]));
	pvtab = vtab;
	module_symtab_enumerate(namespace_variable,
				_ds_variable_fill_fun,
				&pvtab);
	qsort(vtab, variable_count, sizeof(vtab[0]), vtab_comp);

	/* Compute variable offsets. Offset 0 is reserved for NULL symbol */
	for (i = 0; i < variable_count; i++) {
		vtab[i]->off = i + 1;
		if (vtab[i]->addrptr)
			*vtab[i]->addrptr = vtab[i]->off;
	}

	/* Free the array */
	free(vtab);
	
	/* Mark literals used to initialize variables as referenced */
	for (dv = declvar; dv; dv = dv->next) {
		if ((dv->var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED))
		    && dv->var->type == dtype_string) {
			dv->val.v.literal->flags |= SYM_REFERENCED;
		}
	}

	datasize = variable_count + 1;
	dvarsize = datasize - precious_count;
	
	/* Count referenced literals and adjust the data size */
	symtab_enumerate(stab_literal, _ds_literal_count_fun, &datasize);

	/* Account for switch translation tables */
	for (sw = switch_root; sw; sw = sw->next) {
		sw->off = datasize;
		datasize += sw->tabsize;
	}

	/* Account for exception masks */
	for (exmask = exmask_root; exmask; exmask = exmask->next) {
		exmask->off = datasize;
		if (exmask->all) {
			size_t i;
			for (i = 0; i < exception_count; i++)
				bitmask_set(&exmask->bm, i);
		}
		datasize += exmask->bm.bm_size + 1;
	}

	/* Account for exception name table */
	datasize += exception_count;
	
	/* Allocate data segment and relocation table */
	dataseg = mu_calloc(datasize, sizeof(STKVAL));
	dataseg_reloc = mu_calloc(dataseg_reloc_count, sizeof *dataseg_reloc);

	/* Fill relocation table */
	i = 0;
	module_symtab_enumerate(namespace_variable, _ds_reloc_fun, &i);
	
	/* Initialize variables */
	for (dv = declvar; dv; dv = dv->next) {
		if (dv->var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED)) {
			switch (dv->var->type) {
			case dtype_string:
				dataseg[dv->var->off] =
					(STKVAL) dv->val.v.literal->off;
				break;

			case dtype_number:
				dataseg[dv->var->off] =
					(STKVAL) dv->val.v.number;
				break;

			default:
				abort();
			}
		}
	}	

	/* Place literals */
	symtab_enumerate(stab_literal, _ds_literal_copy_fun, NULL);

	/* Initialize exception masks */
	for (exmask = exmask_root; exmask; exmask = exmask->next) {
		size_t i, off = exmask->off;
		
		dataseg[off++] = (STKVAL) exmask->bm.bm_size;
		for (i = 0; i < exmask->bm.bm_size; i++)
			dataseg[off++] = (STKVAL) exmask->bm.bm_bits[i++];
	}

	/* Initialize exception name table */
	enumerate_exceptions(place_exc, dataseg + EXTABIND);
}


static int
_regex_compile_fun(void *sym, void *data)
{
        struct literal *lit = sym;
        if (lit->regex) {
                struct sym_regex *rp;
                for (rp = lit->regex; rp; rp = rp->next) 
                        register_regex(rp);
        }
        return 0;
}

void
regex_layout()
{
        symtab_enumerate(stab_literal, _regex_compile_fun, NULL);
        finalize_regex();
}


static struct variable *auto_list;

static void
register_auto(struct variable *var)
{
        var->next = auto_list;
        auto_list = var;
}

static void
unregister_auto(struct variable *var)
{
        struct variable *p = auto_list, *prev = NULL;
        while (p) {
                struct variable *next = p->next;
                if (p == var) {
                        if (prev)
                                prev->next = next;
                        else
                                auto_list = next;
                        p->next = NULL;
                        return;
                }
                prev = p;
                p = next;
        }
}

/* FIXME: Redo shadowing via a separate table? */
static size_t
forget_autos(size_t nparam, size_t auto_count, size_t hidden_arg)
{
        size_t param_count = 0;
        struct variable *var = auto_list;
        while (var) {
                struct variable *next = var->next;
                switch (var->storage_class) {
                case storage_auto:
                        var->off = auto_count++;
                        break;
                case storage_param:
                        var->off = nparam - param_count++;
                        var->ord = var->off - (hidden_arg ? 1 : 0) - 1;
                        break;
                default:
                        abort();
                }
                while (var->storage_class != storage_extern) {
                        struct variable *shadowed = var->shadowed;
                        if (!shadowed) {
                                symtab_remove(TOP_MODULE_SYMTAB(namespace_variable),
                                              var->sym.name);
                                break;
                        }
                        var->shadowed = NULL;
                        var = variable_replace(var->sym.name, shadowed);
                }
                var = next;
        }
        auto_list = NULL;
        return auto_count;
}

const char *
storage_class_str(storage_class_t sc)
{
        switch (sc) {
        case storage_extern:
                return "extern";
        case storage_auto:
                return "auto";
        case storage_param:
                return "param";
        }
        return "unknown?";
}


const char *
function_name()
{
        switch (outer_context) {
        case context_function:
                return func->sym.name;
        case context_handler:
                return state_to_string(state_tag);
        default:
                return "";
        }
}

NODE *
declare_function(struct function *func, struct mu_locus_range const *loc,
		 size_t nautos)
{
        NODE *node = alloc_node(node_type_funcdecl, loc);
        node->v.funcdecl.func = func;
        node->v.funcdecl.auto_count = nautos;
        node->v.funcdecl.tree = func->node; 
        return node;
}


NODE *
create_node_variable(struct variable *var, struct mu_locus_range const *locus)
{
        NODE *node;

	variable_check_initialized(var, locus);
	node = alloc_node(node_type_variable, locus);
        node->v.var_ref.variable = var;
        node->v.var_ref.nframes = catch_nesting;
        return node;
}

NODE *
create_node_argcount(struct mu_locus_range const *locus)
{
        NODE *node;
        
        if (outer_context == context_function) {
                if (func->optcount || func->varargs) {
                        node = alloc_node(node_type_arg, locus);
                        node->v.arg.data_type = dtype_number;
                        node->v.arg.number = 1;
                } else {
                        node = alloc_node(node_type_number, locus);
                        node->v.number = parminfo[outer_context].parmcount();
                }
        } else {
                node = alloc_node(node_type_number, locus);
                node->v.number = parminfo[outer_context].parmcount();
        }
        return node;
}

NODE *
create_node_arg(long num, struct mu_locus_range const *locus)
{
        NODE *node;
        
        if (inner_context == context_function && func->varargs)
                ;
        else if (num > PARMCOUNT()) 
                parse_error(_("argument number too high"));
        node = alloc_node(node_type_arg, locus);
        node->v.arg.data_type = PARMTYPE(num);
        node->v.arg.number = num;
        if (inner_context == context_function)
                node->v.arg.number += FUNC_HIDDEN_ARGS(func);
        return node;
}

NODE *
create_node_symbol(struct literal *lit, struct mu_locus_range const *locus)
{
        NODE *node;
        register_macro(state_tag, lit->text);
        node = alloc_node(node_type_symbol, locus);
        node->v.literal = lit;
        return node;
}

NODE *
create_node_backref(long num, struct mu_locus_range const *locus)
{
        NODE *node = alloc_node(node_type_backref, locus);
        node->v.number = num;
        return node;
}

static inline int
variable_is_initialized(struct variable *var)
{
	return var->storage_class != storage_auto || var->initialized;
}

void
variable_check_initialized(struct variable *var,
			   struct mu_locus_range const *loc)
{
	if (!variable_is_initialized(var)) {
		parse_warning_locus(loc,
				    _("use of uninitialized variable '%s'"),
				    var->sym.name);
		var->initialized = 1;
	}
}
