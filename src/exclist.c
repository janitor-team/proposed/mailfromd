/* This file is part of Mailfromd.
   Copyright (C) 2011-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "mailfromd.h"
#include "prog.h"

struct exclist {
	struct exclist *next;
	struct literal *lit;
	const struct constant *cp;
};

static struct exclist *head, *tail;
size_t exception_count = 0;

void
define_exception(struct literal *lit, struct mu_locus_range const *locus)
{
	const struct constant *cp;
	struct value value;
	struct exclist *exdef;
	
	value.type = dtype_number;
	value.v.number = exception_count++;
	cp = define_constant(lit->text, &value, 0, locus);
	lit->flags |= SYM_REFERENCED;
	exdef = mu_alloc(sizeof(*exdef));
	exdef->cp = cp;
	exdef->lit = lit;
	exdef->next = NULL;
	if (tail)
		tail->next = exdef;
	else
		head = exdef;
	tail = exdef;
}

void
fixup_exceptions(void)
{
	char namebuf[128];
	struct mu_locus_range locus = MU_LOCUS_RANGE_INITIALIZER;
	mu_locus_point_set_file(&locus.beg, __FILE__);	
	locus.beg.mu_line = __LINE__;
	while (exception_count < mf_exception_count) {
		snprintf(namebuf, sizeof namebuf, "#%lu",
			 (unsigned long) exception_count);
		define_exception(string_alloc(namebuf, strlen(namebuf)),
				 &locus);
	}
	mu_locus_range_deinit(&locus);
}		

void
enumerate_exceptions(int (*efn)(const struct constant *cp,
				const struct literal *lit,
				void *data),
 		     void *data)
{
	struct exclist *p;

	for (p = head; p && efn(p->cp, p->lit, data) == 0; p = p->next)
		;
}

void
free_exceptions()
{
	while (head) {
		struct exclist *next = head->next;
		free(head);
		head = next;
	}
	tail = NULL;
}

const char *
mf_exception_str(unsigned ex)
{
	return (char*) (dataseg + mf_c_val(dataseg[EXTABIND + ex], size));
}
		    
mf_exception 
mf_status_to_exception(mf_status s)
{
	switch (s) {
	case mf_success:
		return mfe_success;
		
	case mf_not_found:
		return mfe_not_found;
		
	case mf_failure:
		return mfe_failure;
		
	case mf_temp_failure:
	case mf_timeout:
		return mfe_temp_failure;
	}
	return (mf_exception) s;
}
	
