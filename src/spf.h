/* This file is part of Mailfromd.
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#define SPF_MAX_RECURSION 10

typedef enum spf_result {
	spf_none, 
	spf_neutral,
	spf_pass,
	spf_fail,
	spf_soft_fail,
	spf_temp_error,
	spf_perm_error
	
#define	max_spf_result (spf_perm_error+1)
} spf_result;

typedef struct {
	const char *ipstr;       /* Originator IP in textual form */
	const char *domain;      /* Sender domain */
	const char *sender;      /* Sender email */
	const char *helo_domain; /* HELO domain */  
	const char *my_domain;   /* My own domain name */ 
	const char *exp_prefix;  /* Explanation prefix (optional) */
} spf_query_t;

typedef struct {
	char *exp_text;      /* Explanation text */
	char **mechv;        /* SPF mechanisms that decided about the result */
	size_t mechn;        /* Number of elements used in mechv */
	size_t mechmax;      /* Number of elements in mechv */
} spf_answer_t;

spf_result spf_check_host(spf_query_t *query, spf_answer_t *ans);
spf_result spf_test_record(const char *rec,
			   spf_query_t *query, spf_answer_t *ans);

void spf_answer_free(spf_answer_t *ans);
void spf_answer_add_mech(spf_answer_t *ans, char const *mech);
