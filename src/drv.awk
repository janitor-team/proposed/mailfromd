# This file is part of Mailfromd.
# Copyright (C) 2007-2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

/\f/ { node=""; }

node == "" && /\/\*[ \t]*type[ \t][ \t]*.*\*\// {
	node=$3;
	nodecnt++
	nodetab[nodecnt]=$3;
	next
}

/print_type_*/ { prop[node,"print"]=1; next }
/mark_type_*/ { prop[node,"mark"]=1; next }
/optimize_type_*/ { prop[node,"optimize"]=1; next }
/code_type_*/ { prop[node,"code"]=1; next }

function prop_name(i, p)
{
	if (prop[nodetab[i],p])
	    return p "_type_" nodetab[i] 
        else
  	    return "NULL"
}

END {
	print "/* -*- buffer-read-only: t -*- vi: set ro: */"
	print "/* THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT. */"
	print ""	
	if (MODE == "types") {
		print "enum node_type {"
		for (i = 1; i <= nodecnt; i++)
			print "    node_type_" nodetab[i] ","
		print "    max_node_type"
		print "};"		
	} else {
		print "struct node_drv nodetab[] = {"
		for (i = 1; i <= nodecnt; i++) {
			printf("    { %s, %s, %s, %s },\n",
			       prop_name(i, "print"),
			       prop_name(i, "mark"),
			       prop_name(i, "code"),
			       prop_name(i, "optimize"))
		}
		print "};"
	}
}			



