BEGIN {
	print "/* -*- buffer-read-only: t -*- vi: set ro:"
	print "   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT."
	print "*/"
}
{ print "#ifdef __DBGMOD_C_ARRAY"
  print "\"" tolower($1) "\","
  print "#else"
  if (offset)
	  print "# define MF_SOURCE_" $1, "(" offset " + " NR-1 ")"
  else
	  print "# define MF_SOURCE_" $1, NR
  print "#endif" }
