/* -*- buffer-read-only: t -*- vi: set ro: */
/* THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT. */
#line 2 "optab.oph"
enum instr_opcode {
	opcode_nil,
#line 19 "../src/opcodes"
	opcode_locus, 
#line 21 "../src/opcodes"
	opcode_stkalloc, 
#line 22 "../src/opcodes"
	opcode_xchg, 
#line 23 "../src/opcodes"
	opcode_pop, 
#line 24 "../src/opcodes"
	opcode_dup, 
#line 25 "../src/opcodes"
	opcode_push, 
#line 26 "../src/opcodes"
	opcode_symbol, 
#line 27 "../src/opcodes"
	opcode_backref, 
#line 28 "../src/opcodes"
	opcode_ston, 
#line 29 "../src/opcodes"
	opcode_ntos, 
#line 30 "../src/opcodes"
	opcode_adjust, 
#line 31 "../src/opcodes"
	opcode_popreg, 
#line 32 "../src/opcodes"
	opcode_pushreg, 
#line 34 "../src/opcodes"
	opcode_bz, 
#line 35 "../src/opcodes"
	opcode_bnz, 
#line 36 "../src/opcodes"
	opcode_jmp, 
#line 37 "../src/opcodes"
	opcode_cmp, 
#line 38 "../src/opcodes"
	opcode_xlat, 
#line 39 "../src/opcodes"
	opcode_xlats, 
#line 40 "../src/opcodes"
	opcode_jreg, 
#line 42 "../src/opcodes"
	opcode_regex, 
#line 43 "../src/opcodes"
	opcode_regmatch, 
#line 44 "../src/opcodes"
	opcode_fnmatch, 
#line 45 "../src/opcodes"
	opcode_fnmatch_mx, 
#line 46 "../src/opcodes"
	opcode_regmatch_mx, 
#line 47 "../src/opcodes"
	opcode_regcomp, 
#line 49 "../src/opcodes"
	opcode_not, 
#line 50 "../src/opcodes"
	opcode_eqn, 
#line 51 "../src/opcodes"
	opcode_eqs, 
#line 52 "../src/opcodes"
	opcode_nen, 
#line 53 "../src/opcodes"
	opcode_nes, 
#line 54 "../src/opcodes"
	opcode_ltn, 
#line 55 "../src/opcodes"
	opcode_lts, 
#line 56 "../src/opcodes"
	opcode_len, 
#line 57 "../src/opcodes"
	opcode_les, 
#line 58 "../src/opcodes"
	opcode_gtn, 
#line 59 "../src/opcodes"
	opcode_gts, 
#line 60 "../src/opcodes"
	opcode_gen, 
#line 61 "../src/opcodes"
	opcode_ges, 
#line 63 "../src/opcodes"
	opcode_neg, 
#line 64 "../src/opcodes"
	opcode_add, 
#line 65 "../src/opcodes"
	opcode_sub, 
#line 66 "../src/opcodes"
	opcode_mul, 
#line 67 "../src/opcodes"
	opcode_div, 
#line 68 "../src/opcodes"
	opcode_mod, 
#line 70 "../src/opcodes"
	opcode_logand, 
#line 71 "../src/opcodes"
	opcode_logor, 
#line 72 "../src/opcodes"
	opcode_logxor, 
#line 73 "../src/opcodes"
	opcode_lognot, 
#line 75 "../src/opcodes"
	opcode_shl, 
#line 76 "../src/opcodes"
	opcode_shr, 
#line 78 "../src/opcodes"
	opcode_concat, 
#line 80 "../src/opcodes"
	opcode_memstk, 
#line 81 "../src/opcodes"
	opcode_xmemstk, 
#line 82 "../src/opcodes"
	opcode_deref, 
#line 83 "../src/opcodes"
	opcode_asgn, 
#line 84 "../src/opcodes"
	opcode_builtin, 
#line 86 "../src/opcodes"
	opcode_catch, 
#line 87 "../src/opcodes"
	opcode_throw, 
#line 88 "../src/opcodes"
	opcode_saveex, 
#line 89 "../src/opcodes"
	opcode_restex, 
#line 91 "../src/opcodes"
	opcode_echo, 
#line 92 "../src/opcodes"
	opcode_return, 
#line 93 "../src/opcodes"
	opcode_retcatch, 
#line 94 "../src/opcodes"
	opcode_funcall, 
#line 96 "../src/opcodes"
	opcode_next, 
#line 97 "../src/opcodes"
	opcode_result, 
#line 98 "../src/opcodes"
	opcode_header, 
#line 100 "../src/opcodes"
	opcode_sedcomp, 
#line 101 "../src/opcodes"
	opcode_sed, 
#line 5 "optab.oph"
	max_instr_opcode
};
