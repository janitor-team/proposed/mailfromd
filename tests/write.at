# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([write function])
AT_KEYWORDS([io write])

m4_pushdef([TEXT],[Nullam sed felis eu libero tristique eleifend a eget metus. Nullam vitae velit mauris. Maecenas ullamcorper mollis leo eleifend.])

MFT_RUN([
require 'status'
func main(...)
  returns number
do
  number fd open('>test.out')
  write(fd, $1)
  return 0
done
],
["TEXT"])

AT_CHECK([cat test.out],
[0],
TEXT)

MFT_RUN([
require 'status'
func main(...)
  returns number
do
  number fd open('>test.out')
  write(fd, $1, 19)
  return 0
done
],
["TEXT"])

AT_CHECK([cat test.out],
[0],
[Nullam sed felis eu])

m4_popdef([TEXT])

AT_CLEANUP