/* This file is part of Mailfromd.
   Copyright (C) 2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <mailutils/mailutils.h>
#include <libmf.h>

int
main(int argc, char **argv)
{
	mu_stream_t str;
	mu_message_t msg, smsg;

	mu_set_program_name(argv[0]);
	mu_cli_simple(argc, argv,
		      MU_CLI_OPTION_PROG_DOC,
		      "Reads message from FILE and reformats its address "
		      "headers the same way Sendmail does.",
		      MU_CLI_OPTION_PROG_ARGS, "FILE",
		      MU_CLI_OPTION_RETURN_ARGC, &argc,
		      MU_CLI_OPTION_RETURN_ARGV, &argv,
		      MU_CLI_OPTION_END);
	if (argc != 1) {
		mu_error("required argument missing");
		return 2;
	}

	mu_set_user_email_domain("localhost");
	MU_ASSERT(mu_file_stream_create(&str, argv[0], MU_STREAM_READ));
	MU_ASSERT(mu_stream_to_message(str, &msg));
	mu_stream_unref(str);
	MU_ASSERT(sendmail_address_normalize(msg, &smsg));

	MU_ASSERT(mu_message_get_streamref(smsg, &str));
	MU_ASSERT(mu_stream_copy(mu_strout, str, 0, NULL));
	mu_stream_unref(str);
	return 0;
}
