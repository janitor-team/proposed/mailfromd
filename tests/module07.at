# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2010-2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([module: variable allocation])
AT_KEYWORDS([module module07 import import06 variable variables])

# Description: Mailfromd 7.0 would fail to allocate static variables
# from required modules as well as public variables not accessed directly
# from the uplevel module.  For example, given the included prog, v7.0
# would produce:
#   foo
#   foo
#   foo
#   qux
# Affected-versions: < 7.0
# Additional-info: 

AT_DATA([mod1.mf],
[[module 'mod1'.

string s1 "foo"
string s2 "bar"
static string s3 "baz"
static const c1 "qux"

func print_text()
do
  echo s1
  echo s2
  echo s3
  echo c1
done
]])

AT_DATA([mod2.mf],
[[module 'mod2'.
require 'mod1'

func mod2_init()
do
  print_text()
done
]])

MFT_RUN([
#pragma regex +extended
require 'mod2'

func main(...)
  returns number
do
  mod2_init()
  return 0
done
],
[],
[0],
[],
[foo
bar
baz
qux
])

AT_CLEANUP

