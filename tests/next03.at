# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2012-2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Next in a do-while loop])
AT_KEYWORDS([next03 loop next])

# Description: The `next' keyword bypassed conditional in do-while
# loops.  Affected versions: up to 7.99.92 [alpha-7.0.92-mu2-124-ge2ae324]

MFT_RUN([
func main(...)
  returns number
do
  loop for set i 0
  do
    set i i + 1    
    echo i
    next
  done while i < 4
done
],
[],
[0],
[],
[1
2
3
4
])

AT_CLEANUP
