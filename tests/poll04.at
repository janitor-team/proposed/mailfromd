# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2007-2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([on poll without address])
AT_KEYWORDS([poll poll04])

# Synopsis: `on poll' without target address caused coredump in
# version 4.1
# Reported: Jan Rafaj, 2007-08-10
# References: <Pine.LNX.4.58.0708101024210.11880@cedric.unob.cz>

MFT_TEST([
require poll

prog envfrom
do
  on poll host "HOST"
  do
    when 0: pass
  done
done],
[],
[EX_CONFIG],
[],
[mailfromd: prog:6.6-21: recipient address not specified in `on poll' construct
])

AT_CLEANUP

