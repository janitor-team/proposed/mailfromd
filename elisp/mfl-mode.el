;;; mfl-mode.el --- major mode for editing MFL sources

;; Authors: 2007, 2008, 2009 Sergey Poznyakoff
;; Version: 0.1
;; Keywords: Mailfromd, MFL

;; This file is part of Mailfromd
;; Copyright (C) 2007-2022 Sergey Poznyakoff
 
;; Mailfromd is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
 
;; Mailfromd is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Mailfromd.  If not, see <http://www.gnu.org/licenses/>.

;; Installation:
;; You may wish to use precompiled version of the module. To create it
;; run:
;;    emacs -batch -f batch-byte-compile mfl-mode.el
;; Install the file mfl-mode.elc (and, optionally, mfl-mode.el) to
;; any directory in your Emacs load-path.

;; Customization:
;;  To your .emacs or site-start.el add:
;;  (autoload 'mfl-mode "mfl-mode")
;;  (setq auto-mode-alist (append auto-mode-alist
;;                                '(("\\.mf$" . mfl-mode))))

(require 'font-lock)

(defvar mfl-mode-syntax-table nil
  "Syntax table used in mfl-mode buffers.")

(unless mfl-mode-syntax-table
  (setq mfl-mode-syntax-table (make-syntax-table))
  (modify-syntax-entry ?_ "\w" mfl-mode-syntax-table)
  (modify-syntax-entry ?\t "-" mfl-mode-syntax-table)
  (modify-syntax-entry ?\( "()" mfl-mode-syntax-table)
  (modify-syntax-entry ?\) ")(" mfl-mode-syntax-table)
  (modify-syntax-entry ?\' "\"" mfl-mode-syntax-table)
  (modify-syntax-entry ?\" "\"" mfl-mode-syntax-table)
  (modify-syntax-entry ?\\ "\\" mfl-mode-syntax-table)
  (modify-syntax-entry ?\/ ". 14" mfl-mode-syntax-table)
  (modify-syntax-entry ?\* ". 23" mfl-mode-syntax-table))

(defvar mfl-mode-map nil
  "Keymap used in MFL mode.")

(unless mfl-mode-map
  (setq mfl-mode-map (make-sparse-keymap))
  (define-key mfl-mode-map "\t" 'mfl-indent-line)
  (define-key mfl-mode-map "\r" 'mfl-newline-and-indent)
  (define-key mfl-mode-map "\C-c\C-c" 'mfl-check-syntax)
  (define-key mfl-mode-map "\C-\M-a" 'beginning-of-defun)
  (define-key mfl-mode-map "\C-\M-e" 'end-of-defun)

  (define-key mfl-mode-map [menu-bar] (make-sparse-keymap))
  (define-key mfl-mode-map [menu-bar MFL]
    (cons "MFL" mfl-mode-map))
  (define-key mfl-mode-map [mfl-check-syntax]
    '("Check syntax" . mfl-check-syntax))
  (define-key mfl-mode-map [beginning-of-defun]
    '("Beginning of definition" . beginning-of-defun))
  (define-key mfl-mode-map [end-of-defun]
    '("End of definition" . beginning-of-defun)))



(defgroup mfl nil
  "MFL programming utilities"
  :group 'unix
  :group 'languages)

(defgroup mfl nil
  "MFL script mode"
  :group 'mfl
  :prefix "mfl-")

(defgroup mfl-lint nil
  "Variables controlling invocation of mfd in `lint' mode.
"
  :group 'mfl)

(defcustom mfl-mfd-command "mailfromd"
  "*The default mfd command line (without --lint option)"
  :type 'string
  :group 'mfl-lint)

(defcustom mfl-include-path nil
  "*Additional include directories"
  :type '(repeat string)
  :group 'mfl-lint)

(defgroup mfl-indentation nil
  "Variables controlling indentation in MFL scripts.
"
  :group 'mfl)

(defcustom mfl-basic-offset 2
  "*The default indentation increment."
  :type 'integer
  :group 'mfl-indentation)

(defcustom mfl-returns-offset 2
  "*Indentation increment for `alias' and `returns' statements"
  :type 'integer
  :group 'mfl-indentation)

(defcustom mfl-comment-offset 1
  "*Indentation increment for comment lines"
  :type 'integer
  :group 'mfl-indentation)

(defcustom mfl-case-line-offset 0
  "*Indentation increment for `when' and `case' lines."
  :type 'integer
  :group 'mfl-indentation)

(defcustom mfl-loop-statement-offset 5
  "*Indentation increment for `loop' controlling statements."
  :type 'integer
  :group 'mfl-indentation)

(defcustom mfl-loop-continuation-offset 5
  "*Indentation increment for `loop' continuation lines."
  :type 'integer
  :group 'mfl-indentation)

(defun mfl-find-comment-start ()
  "Find the beginning of a multiline comment the point is in."
  (while (not (or (bobp) (looking-at ".*/\\*")))
    (forward-line -1)))

(defun func-indentation ()
  (while (and (not (bobp)) (not (looking-at "^\\s *func\\s ")))
    (forward-line -1))
  (current-indentation))

(defun find-loop (comma)
  (if (bobp)
      nil
    (beginning-of-line)
    (skip-chars-forward " \t")
    (cond
     ((looking-at (regexp-opt '("do" "done") 'words))
      nil)
     ((and (not comma)
	   (looking-at "\\(loop\\s +\\)?\\(for\\|while\\).*[^,]\\s *$"))
      (+ (current-indentation)
	 (if (match-end 1)
	     (- (match-end 1) (match-beginning 1))
	   0)
	 mfl-loop-continuation-offset))
     ((looking-at "loop")
      (+ (current-indentation) mfl-loop-statement-offset))
     (t
      (forward-line -1)
      (find-loop (or comma (looking-at ".*,$")))))))

(defun loop-indentation ()
    (or (save-excursion
	  (find-loop (looking-at ".*,$")))
	(current-indentation)))

(defun mfl-next-line-indentation ()
  "Guess and return the indentation of the next line."
  (save-excursion
    (beginning-of-line)
    (cond
     ((bobp)
      (current-indentation))
     ((not (eolp))
      (skip-chars-forward " \t")
      (cond
       ((looking-at (regexp-opt '("do" "if" "else" "elif") 'words))
	(+ (current-indentation) mfl-basic-offset))
       ((looking-at (regexp-opt '("alias" "returns") 'words))
	(- (func-indentation) mfl-returns-offset)) 
       ((looking-at ".*/\\*")
	(if (not (looking-at ".*\\*/\\s *$"))
	    (+ (current-indentation)
	       (- (match-end 0) (match-beginning 0)) mfl-comment-offset)
	  (forward-line -1)
	  (mfl-next-line-indentation)))
       ((looking-at ".*\\*/\\s *$")
	(mfl-find-comment-start)
	(forward-line -1)
	(current-indentation))
       ((looking-at ".*:[ \t]*$")
	(+ (current-indentation) mfl-basic-offset))
       (t
	(loop-indentation))))
     (t
      (forward-line -1)
      (mfl-next-line-indentation)))))

(defun mfl-find-line-indentation (regexp)
  "Move backwards to the line containing REGEXP, skipping over
block constructs.  Return the indentation of the line, or 0
if no matching line was found."
  (catch 'found
    (while (not (bobp))
      (forward-line -1)
      (beginning-of-line)
      (skip-chars-forward " \t")
      (cond
       ((looking-at regexp)
	(throw 'found (current-indentation)))
       ((looking-at "\\<done\\>")
	(mfl-find-line-indentation "\\(\\<on\\>.*\\)?\\<do\\>"))
       ((looking-at "\\<fi\\>")
	(mfl-find-line-indentation "\\<if\\>"))))
    0))

(defun mfl-find-line-forward (regexp)
  "Move forward to the line containing REGEXP, skipping over
block constructs.  Return t if the line was found, nil otherwise."
  (catch 'found
    (while (not (eobp))
      (forward-line 1)
      (beginning-of-line)
      (skip-chars-forward " \t")
      (cond
       ((looking-at regexp)
	(throw 'found t))
       ((looking-at "\\<do\\>")
	(mfl-find-line-forward "\\<done\\>"))
       ((looking-at "\\<if\\>")
	(mfl-find-line-forward "\\<fi\\>"))))
    nil))

(defun mfl-compute-line-indentation ()
  "Compute the indentation of the current line."
  (save-excursion
    (beginning-of-line)
    (skip-chars-forward " \t")
    (cond
     ((looking-at (regexp-opt '("else" "elif" "fi") 'words))
      (mfl-find-line-indentation (regexp-opt '("if" "elif") 'words)))
     ((looking-at (regexp-opt '("alias" "returns") 'words))
      (+ (func-indentation) mfl-returns-offset))
     ((looking-at "\\<done\\>")
      ; FIXME: Continuation lines are not properly handled
      (mfl-find-line-indentation ".*\\<do\\>"))
     ((looking-at "\\<when\\>")
      (+ (mfl-find-line-indentation "\\<on\\>")
	 mfl-case-line-offset))
     ((looking-at (regexp-opt '("case" "default") 'words))
      (+ (mfl-find-line-indentation "\\<switch\\>")
	 mfl-case-line-offset))
     ((looking-at "\\<do\\>")
      (mfl-find-line-indentation
       (regexp-opt '("func" "prog" "switch" "catch" "try" "loop" "on")
		   'words)))
     (t
      (forward-line -1)
      (mfl-next-line-indentation)))))

(defun mfl-indent-line ()
  "Indent the current line."
  (interactive "*")
  (let ((start-of-line (save-excursion
			  (beginning-of-line)
			  (skip-chars-forward " \t")
			  (point)))
	 (shift-amt (mfl-compute-line-indentation)))
    (if (not (= shift-amt (current-indentation)))
	(let ((off (- (point) start-of-line)))
	  (beginning-of-line)
	  (delete-region (point) start-of-line)
	  (indent-to shift-amt)
	  (if (>= off 0)
	      (goto-char (+ (point) off))
	    (beginning-of-line))))))

(defun mfl-newline-and-indent ()
  "Indent the current line, if necessary, insert a newline, and then indent again.

The current line is indented if it begins with one of the following
keywords: else, elif, fi, done, when, case.
" 
  (interactive "*")
  (if (save-excursion
	(beginning-of-line)
	(skip-chars-forward " \t")
	(looking-at (regexp-opt
		     '("do" "else" "elif" "fi" "done" "when" "case" "default"
		       "alias" "returns")
		     'words)))
      (mfl-indent-line))
  (newline-and-indent))


(defun mfl-check-syntax ()
  "Checks the syntax of the current MFL buffer."
  (interactive "*")
  (compile (concat
	    mfl-mfd-command 
	    " --lint"
	    (if mfl-include-path
		(apply 'concat (mapcar (lambda (x) (concat " -I" x))
				       mfl-include-path))
	      "")
	    " "
	    (buffer-file-name))))



(defun mfl-at-beginning-of-defun-p ()
  "Return true if the point is at the beginning of a defun"
  (or (looking-at "[ \t]*prog [a-z]+")
      (looking-at (concat "[ \t]*"
			  (regexp-opt '("begin" "end") 'words)))
      (looking-at "[ \t]*func\\s +[a-zA-Z_][a-zA-Z0-9_]*\\s *(")))

(defun mfl-search-next-defun ()
  "If the point is at the beginning of a defun, return t.  Otherwise,
move forward to the beginning of a next defun.  Return t on success, nil
otherwise."
  (catch 'loop
    (while (not (eobp))
      (if (mfl-at-beginning-of-defun-p)
	  (throw 'loop t))
      (forward-line 1))
    nil))

(defun mfl-beginning-of-defun ()
  "Interface to `beginning-of-defun'"
  (catch 'loop
    (while (not (bobp))
      (forward-line -1)
      (if (mfl-at-beginning-of-defun-p)
	  (throw 'loop t)))
    nil))

(defun mfl-end-of-defun-function ()
  "Interface to `end-of-defun'"
  (let ((pos (save-excursion
	       (and (mfl-search-next-defun)
		    (mfl-find-line-forward "\\<do\\>")
		    (mfl-find-line-forward "\\<done\\>")
		    (forward-line 1)
		    (point)))))
    (if pos
	(goto-char pos)
      pos)))


(defconst mfl-keywords
  (regexp-opt '("accept" "add" "and" "alias" "begin" "break" "bye" "case"
	        "catch" "const" "continue" "default"
		"delete" "discard" "do" "done"
		"echo" "end" "elif" "else"
		"fi" "fnmatches" "for" "from" "func"
		"if" "import" "loop" "matches" "module" "next"
		"not" "on" "or" "pass" "precious"
		"prog" "reject" "replace" "return"
		"returns" "require" "set" "static" "switch" "tempfail"
		"throw" "try" "vaptr" "when" "while") 'words))

(defconst mfl-on-keywords
  ;; context-dependent keywords
  (regexp-opt '("as" "from" "host" "poll") 'words))

(defconst mfl-constants
  (regexp-opt '("__defpreproc__" "__defstatedir__"
	        "__file__" "__function__" "__line__" "__major__"
		"__minor__" "__module__" "__package__" "__patch__"
		"__preproc__" "__statedir__"
		"__version__") 'words))

(defconst mfl-type-names
  (regexp-opt '("number" "string") 'words))

(defconst mfl-preprocessor-directives
  (regexp-opt '("include" "include_once" "error" "line"
                "pragma" "require" "warning") 'words))

(defconst mfl-m4-keywords
  (regexp-opt '("m4_define" "m4_defn" "m4_undefconst" "m4_builtin"
		"m4_changecom" "m4_changequote" "m4_debugfile" "m4_debugmode"
		"m4_decr" "m4_divert" "m4_divnum" "m4_dumpdef"
		"m4_errprint" "m4_esyscmd" "m4_eval" "m4_format"
		"m4_ifdef" "m4_ifelse" "m4_include" "m4_incr"
		"m4_index" "m4_indir" "m4_len" "m4_exit"
		"m4_wrap" "m4_maketemp" "m4_patsubst" "m4_popdef"
		"m4_pushdef" "m4_regexp" "m4_shift" "m4_sinclude"
		"m4_substr" "m4_symbols" "m4_syscmd" "m4_sysval"
		"m4_traceoff" "m4_traceon" "m4_translit" "m4_undivert"
		"m4_dnl" "m4___line__" "m4___file__") 'words))

(defconst mfl-macros
  (regexp-opt '("defined" "printf" "_" "N_") 'words))

(defconst mfl-status-codes
  (regexp-opt '("success" "not_found" "failure" "temp_failure"
		"ston_conv" "divzero" "regcomp" "invip" 
		"invcidr" "invtime" "dbfailure" "range" 
		"url" "noresolve" "ioerr") 'words)) 



;; Font-lock stuff
(defconst mfl-font-lock-keywords
  (list
   ;; Fontify error and warning directives.
   '("^#[ \t]*error[ \t]+\\(.+\\)" 1 font-lock-warning-face prepend)
   '("^#[ \t]*warning[ \t]+\\(.+\\)" 1 font-lock-warning-face prepend)
   ;;
   ;; Fontify filenames in #include <...> preprocessor directives as strings.
   '("^#[ \t]*\\(include\\|include_once\\)[ \t]*\\(<[^>\"\n]*>?\\)"
     2 font-lock-string-face)
   ;; Fontify module names in #require directives as strings.
   '("^#[ \t]*\\(require\\)[ \t]*\\(.[^\n]*\\)"
     2 font-lock-string-face)
   ;; Fontify otherwise as symbol names, and the preprocessor directive
   ;; names.
   (list
    (concat "^#[ \t]*\\(" mfl-preprocessor-directives
	    "\\)\\>[ \t!]*\\(\\sw+\\)?")
    '(1 font-lock-builtin-face))
   ;; Otherwise, fontify #...\n as comments:
   (list
    "^#\\(.*\\)\n" 1 font-lock-comment-face)

   ;; Fontify all type names.
   `(eval .
       (cons (concat "\\<\\(" ,mfl-type-names "\\)\\>") 'font-lock-type-face))

   ;; Fontify exception and status codes
   `(eval .
       (cons (concat "\\<\\(" ,mfl-status-codes "\\)\\>")
	     'font-lock-constant-face))
     
   ;;
   ;; Fontify all builtin keywords 
   (concat "\\<\\(" mfl-keywords "\\)\\>")
     
   ;; Fontify m4 keywords and macros
   (list
    (concat "\\<\\(" mfl-m4-keywords "\\)\\>")
    1 font-lock-builtin-face)

   ;; Fontify variable names
   (list
    "%\\([a-zA-Z0-9_]+\\)"
    1 font-lock-variable-name-face)
   (list
    "set[ \t]+\\([a-zA-Z0-9_]+\\)"
    1 font-lock-variable-name-face)
   (list
    "defined[ \t]*([ \t]*\\([a-zA-Z0-9_]+\\)[ \t]*)"
    1 font-lock-variable-name-face)
   (list
    (concat mfl-type-names "[ \t]+\\([a-zA-Z0-9_]+\\)")
    2 font-lock-variable-name-face)

   ;; Fontify macro names
   (list
    "${?\\([a-zA-Z0-9_]+\\)}?"
    1 font-lock-constant-face)

   ;; Fontify `on poll' statement
   (list
    (concat "\\<on\\>.*\\(" mfl-on-keywords "\\)")
    1 font-lock-keyword-face)

   (list
    (concat "\\<on\\>.*" mfl-on-keywords ".*\\(" mfl-on-keywords "\\)")
    1 font-lock-keyword-face)

   (list
    (concat "\\<on\\>.*" mfl-on-keywords ".*" mfl-on-keywords ".*\\(" mfl-on-keywords "\\)")
    1 font-lock-keyword-face)

   ;; FIXME: Fontify preprocessor quotes
   ))

;;;###autoload
(defun mfl-mode ()
  "Major mode for viewing MFL filter sources

Key bindings are:
\\{mfl-mode-map}
"
  (interactive)
  (kill-all-local-variables)
  (use-local-map mfl-mode-map)
  (make-local-variable 'beginning-of-defun-function)
  (make-local-variable 'end-of-defun-function)
  (make-local-variable 'compile-command)

  (setq major-mode 'mfl-mode
	mode-name "MFL"
	beginning-of-defun-function 'mfl-beginning-of-defun
	end-of-defun-function 'mfl-end-of-defun-function
	indent-line-function 'mfl-indent-line)

  (set-syntax-table mfl-mode-syntax-table)

  
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults
	'((mfl-font-lock-keywords) nil nil
	  nil
	  nil)))

(provide 'mfl-mode)
;;; mfl-mode ends
