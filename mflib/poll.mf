/* Backward-compatible _poll* functions for Mailfromd     -*- mfl -*-
   Copyright (C) 2010-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

module 'poll'.
require 'callout'

static func __make_extra_arg(string arg, string ehlo, string mailfrom)
  returns string
do
  if ehlo != ""
    set arg arg . " EHLO=\"%ehlo\""
  fi
  if mailfrom != ""
    set arg arg . " MAILFROM=\"%mailfrom\""
  fi
  return arg
done

func _pollhost(string ip, string email, string domain, string mailfrom)
  returns number
do
  return callout_do(__callout_open_default(), email,
                    __make_extra_arg("MODE=hostonly HOST=\"%ip\"",
		                     domain, mailfrom))
done

func _pollmx (string domain, string email, string ehlo, string mailfrom)
  returns number
do
  return callout_do(__callout_open_default(), email,
                    __make_extra_arg("MODE=mxonly HOST=\"%domain\"",
               		              ehlo, mailfrom))
done

func stdpoll(string email, string ehlo, string mailfrom)
  returns number
do
  return callout_do(__callout_open_default(), email,
                    __make_extra_arg("MODE=mxfirst", ehlo, mailfrom))
done

func strictpoll(string host, string email, string ehlo, string mailfrom)
  returns number
do
  return callout_do(__callout_open_default(), email,
                    __make_extra_arg("MODE=hostfirst HOST=\"%host\"",
		                     ehlo, mailfrom))
done


	  
  
  
