/* RHSBL checker.
   Copyright (C) 2006, 2007 Jan Rafaj
   Copyright (C) 2008-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

module 'match_rhsbl'.

require 'status'
require 'dns'
require 'match_cidr'

#pragma regex push +extended
func match_rhsbl(string email, string zone, string iprange)
  returns number
do
  if iprange = 'ANY'
    set iprange '127.0.0.0/8'
  fi
  if not email matches '@.+$'
    throw e_failure "%email: invalid e-mail"
  fi

  return match_cidr (resolve (domainpart(email), zone), iprange)
done
#pragma regex pop
