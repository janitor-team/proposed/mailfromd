/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#define EMAIL_MULTIPLE   0x0001
#define EMAIL_COMMENTS   0x0002
#define EMAIL_PERSONAL   0x0004
#define EMAIL_LOCAL      0x0008
#define EMAIL_DOMAIN     0x0010
#define EMAIL_ROUTE      0x0020
