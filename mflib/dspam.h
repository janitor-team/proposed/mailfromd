/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#define _MFL_DSM_PROCESS     0 /* Process message */
#define _MFL_DSM_CLASSIFY    1 /* Classify message only (do not write changes) */
#define _MFL__DSM_MASK       0x000f
#define _MFL_DSF_SIGNATURE   0x0010 /* Signature Mode (Use a signature) */
#define _MFL_DSF_NOISE       0x0020 /* Use Bayesian Noise Reduction */
#define _MFL_DSF_WHITELIST   0x0040 /* Use Automatic Whitelisting */
#define _MFL_DSZ_WORD        (1<<8) /* Use WORD tokenizer */
#define _MFL_DSZ_CHAIN       (2<<8) /* Use CHAIN tokenizer */
#define _MFL_DSZ_SBPH        (3<<8) /* Use SBPH tokenizer */
#define _MFL_DSZ_OSB         (4<<8) /* Use OSB tokenizer */
#define _MFL__DSZ_MASK       0x0f00
#define _MFL_DST_TEFT        (0<<12) /* Train Everything */
#define _MFL_DST_TOE         (1<<12) /* Train-on-Error */
#define _MFL_DST_TUM         (2<<12) /* Train-until-Mature */
#define _MFL__DST_MASK       0xf000
#define _MFL_DSR_NONE        0 /* No predetermined classification (classify message) */
#define _MFL_DSR_ISSPAM      1 /* Message is spam (learn as spam) */
#define _MFL_DSR_ISINNOCENT  2 /* Message is innocent (learn as innocent) */
#define _MFL__DSR_MASK       0x000f
#define _MFL_DSS_NONE        (0<<4) /* No classification source (use only with	DSR_NONE) */
#define _MFL_DSS_ERROR       (1<<4) /* Misclassification by libdspam */
#define _MFL_DSS_CORPUS      (2<<4) /* Corpused message */
#define _MFL_DSS_INOCULATION (3<<4) /* Message inoculation */
#define _MFL__DSS_MASK       0x00f0
