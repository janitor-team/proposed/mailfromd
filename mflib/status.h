/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#define _MFL_FAMILY_STDIO 0
#define _MFL_FAMILY_UNIX  1
#define _MFL_FAMILY_INET  2
#define _MFL_FAMILY_INET6 3
#define _MFL_R_OK  4               /* Test for read permission. */
#define _MFL_W_OK  2               /* Test for write permission. */
#define _MFL_X_OK  1               /* Test for execute permission. */
#define _MFL_F_OK  0               /* Test for existence. */
#define _MFL_SHUT_RD   0
#define _MFL_SHUT_WR   1
#define _MFL_SHUT_RDWR 2
#define _MFL_BUFFER_NONE 0
#define _MFL_BUFFER_FULL 1
#define _MFL_BUFFER_LINE 2
#define _MFL_BURST_ERR_FAIL   0
#define _MFL_BURST_ERR_IGNORE 1
#define _MFL_BURST_ERR_BODY   2
#define _MFL_BURST_ERR_MASK   0x0f
#define _MFL_BURST_DECODE     0x10
#define _MFL_DKIM_VERIFY_OK       0
#define _MFL_DKIM_VERIFY_PERMFAIL 1
#define _MFL_DKIM_VERIFY_TEMPFAIL 2
#define _MFL_DKIM_EXPL_OK               0
#define _MFL_DKIM_EXPL_NO_SIG           1
#define _MFL_DKIM_EXPL_INTERNAL_ERROR   2
#define _MFL_DKIM_EXPL_SIG_SYNTAX       3
#define _MFL_DKIM_EXPL_SIG_MISS	 4
#define _MFL_DKIM_EXPL_DOMAIN_MISMATCH  5
#define _MFL_DKIM_EXPL_BAD_VERSION      6
#define _MFL_DKIM_EXPL_BAD_ALGORITHM    7
#define _MFL_DKIM_EXPL_BAD_QUERY        8
#define _MFL_DKIM_EXPL_FROM             9 
#define _MFL_DKIM_EXPL_EXPIRED         10
#define _MFL_DKIM_EXPL_DNS_UNAVAIL     11
#define _MFL_DKIM_EXPL_DNS_NOTFOUND    12
#define _MFL_DKIM_EXPL_KEY_SYNTAX      13
#define _MFL_DKIM_EXPL_KEY_REVOKED     14
#define _MFL_DKIM_EXPL_BAD_BODY        15 
#define _MFL_DKIM_EXPL_BAD_BASE64      16
#define _MFL_DKIM_EXPL_BAD_SIG         17
#define _MFL_DKIM_EXPL_BAD_KEY_TYPE    18
#define _MFL_success      e_success
#define _MFL_not_found    e_not_found
#define _MFL_failure      e_failure
#define _MFL_temp_failure e_temp_failure
