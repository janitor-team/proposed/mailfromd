/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1 "dns.mf4"
/* User-level DNS functions                                    -*- mfl -*-
   Copyright (C) 2007-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

module 'dns'.

const
do
  DNS_TYPE_A 1
  DNS_TYPE_NS 2
  DNS_TYPE_PTR 12
  DNS_TYPE_MX 15
  DNS_TYPE_TXT 16
done

func hostname (string ipstr)
  returns string
do
  catch *
  do
    return ipstr
  done
  return primitive_hostname(ipstr)
done

func resolve (string str; string domain)
  returns string
do
  catch *
  do
    return "0"
  done
  if $#>@domain
    return primitive_resolve (str, domain)
  else
    return primitive_resolve (str)
  fi
done

func hasmx (string str)
  returns number
do
  catch *
  do
    return 0
  done
  return primitive_hasmx(str)
done

func ismx (string domain, string ipstr)
  returns number
do
  catch *
  do
    return 0
  done
  return primitive_ismx (domain, ipstr)
done

func hasns (string str)
  returns number
do
  try
  do
    return primitive_hasns (str)
  done
  catch *
  do
    return 0
  done
done

func dns_getname (string ipstr)
  returns string
do
  string result ''
  set n dns_query(DNS_TYPE_PTR, ipstr, 1)
  loop for set i 0,
       while i < dns_reply_count(n),
       set i i + 1
  do
    if result
      set result result . ' '
    fi
    set result result . dns_reply_string(n, i)
  done
  dns_reply_release(n)
  return result
done

func dns_getaddr (string domain)
  returns string
do
  string result ''
  set n dns_query(DNS_TYPE_A, domain, 1)
  loop for set i 0,
       while i < dns_reply_count(n),
       set i i + 1
  do
    if result
      set result result . ' '
    fi
    set result result . dns_reply_string(n, i)
  done
  dns_reply_release(n)
  return result
done

func getns (string domain; number resolve_names, number sort_names)
  returns string
do
  string result ''
  set n dns_query(DNS_TYPE_NS, domain, sort_names, resolve_names)
  loop for set i 0,
       while i < dns_reply_count(n),
       set i i + 1
  do
    if result
      set result result . ' '
    fi
    set result result . dns_reply_string(n, i)
  done
  dns_reply_release(n)
  return result
done

func getmx (string domain; number resolve_names)
  returns string
do
  string result ''
  set n dns_query(DNS_TYPE_MX, domain, 0, resolve_names)
  loop for set i 0,
       while i < dns_reply_count(n),
       set i i + 1
  do
    if result
      set result result . ' '
    fi
    set result result . dns_reply_string(n, i)
  done
  dns_reply_release(n)
  return result
done
  

