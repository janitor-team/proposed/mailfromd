/* Constants for dspam interface.
   Copyright (C) 2011-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

module 'dspam'.

#prefix _MFL_

# Operating Modes:
const DSM_PROCESS     0 /* Process message */
const DSM_CLASSIFY    1 /* Classify message only (do not write changes) */
const _DSM_MASK       0x000f
# Flags:
const DSF_SIGNATURE   0x0010 /* Signature Mode (Use a signature) */
const DSF_NOISE       0x0020 /* Use Bayesian Noise Reduction */
const DSF_WHITELIST   0x0040 /* Use Automatic Whitelisting */

# Tokenizers:
const DSZ_WORD        (1<<8) /* Use WORD tokenizer */
const DSZ_CHAIN       (2<<8) /* Use CHAIN tokenizer */
const DSZ_SBPH        (3<<8) /* Use SBPH tokenizer */
const DSZ_OSB         (4<<8) /* Use OSB tokenizer */
const _DSZ_MASK       0x0f00

# Training Modes:
const DST_TEFT        (0<<12) /* Train Everything */
const DST_TOE         (1<<12) /* Train-on-Error */
const DST_TUM         (2<<12) /* Train-until-Mature */
const _DST_MASK       0xf000

# Classifications:
const DSR_NONE        0 /* No predetermined classification (classify message) */
const DSR_ISSPAM      1 /* Message is spam (learn as spam) */
const DSR_ISINNOCENT  2 /* Message is innocent (learn as innocent) */
const _DSR_MASK       0x000f

# Sources:
const DSS_NONE        (0<<4) /* No classification source (use only with	DSR_NONE) */
const DSS_ERROR       (1<<4) /* Misclassification by libdspam */
const DSS_CORPUS      (2<<4) /* Corpused message */
const DSS_INOCULATION (3<<4) /* Message inoculation */
const _DSS_MASK       0x00f0