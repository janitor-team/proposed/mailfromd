/* MeTA1-style sockmap support.                                              
   Copyright (C) 2009-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

module 'sockmap'.
require 'status'

static func __sockmap_send(number fd, string query)
do
  number len length(query)
  write(fd, "%len:%query,")
done

static func __sockmap_read(number fd)
  returns string
do
  number size getdelim(fd, ":")
  string str read(fd, size)
  if read(fd, 1) != ","
    throw e_failure "missing expected comma"
  fi
  return str
done

func sockmap_lookup(number fd, string map, string arg)
  returns string
do
  __sockmap_send(fd, "%map %arg")
  return __sockmap_read(fd)
done

func sockmap_single_lookup(string url, string map, string arg)
  returns string
do
  number fd open("@ %url")
  string res sockmap_lookup(fd, map, arg)
  close(fd)
  return res
done
