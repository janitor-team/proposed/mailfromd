dnl This file is part of Mailfromd.
dnl Copyright (C) 2008-2022 Sergey Poznyakoff
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program.  If not, see <http://www.gnu.org/licenses/>.

AC_DEFUN([MF_PROCTITLE],[
  AH_TEMPLATE([MF_PROCTITLE_TYPE], [Type of setproctitle implementation]) 
  AH_BOTTOM([
#define MF_PROCTITLE_SETPROCTITLE 0
#define MF_PROCTITLE_REPLACE_ARGV 1
#define MF_PROCTITLE_REWRITE_ARGV 2 
#define MF_PROCTITLE_PSTAT        3 
#define MF_PROCTITLE_PSSTRINGS    4
])
  AC_SUBST(MF_PROCTITLE_LIBS)

  AC_TRY_LINK([extern char *__progname, *__progname_full;],
              [__progname = "foo"; __progname_full = "foo bar";],
              [AC_DEFINE(HAVE___PROGNAME, 1,
                         [Define to 1 if libc has __progname])])

  mf_proctitle_type=no
  AC_CHECK_FUNC(setproctitle,
                [mf_proctitle_type=MF_PROCTITLE_SETPROCTITLE],
                [AC_CHECK_HEADERS(libutil.h)
                 AC_CHECK_LIB(util, setproctitle,
                              [mf_proctitle_type=MF_PROCTITLE_SETPROCTITLE
		               MF_PROCTITLE_LIBS="-lutil"])])

  if test "$mf_proctitle_type" = no; then
    AC_CHECK_HEADER([sys/pstat.h],
           [AC_CHECK_FUNC([pstat],
                          [mf_proctitle_type=MF_PROCTITLE_PSTAT],
		          [mf_proctitle_type=MF_PROCTITLE_REWRITE_ARGV])])
    if test "$mf_proctitle_type" = no; then
      AC_TRY_COMPILE([#include <machine/vmparam.h>
                      #include <sys/exec.h>],
		     [
#include <sys/types.h>
#include <sys/proc.h>
#include <vm/pmap.h>
#include <machine/pmap.h>
#include <machine/vmparam.h>
#include <sys/exec.h>

main()
{
  int i = PS_STRINGS;
}
],
		     [mf_proctitle_type=MF_PROCTITLE_PSSTRINGS])
		     
      if test "$mf_proctitle_type" = no; then
        AC_EGREP_CPP(yes,[
#if defined(__GNU_HURD__)
 yes
#endif
],
                     [mf_proctitle_type=MF_PROCTITLE_REPLACE_ARGV],
		     [mf_proctitle_type=MF_PROCTITLE_REWRITE_ARGV]) 
      fi
    fi
  fi

  AC_DEFINE_UNQUOTED([MF_PROCTITLE_TYPE],$mf_proctitle_type)

])
				   