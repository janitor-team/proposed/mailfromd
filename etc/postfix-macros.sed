# Convert mailfromd --dump-macros output into Postfix configuration statements
# Copyright (C) 2005-2022 Sergey Poznyakoff
# License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

/^connect /s/^connect/milter_connect_macros =/
/^helo /s/^helo/milter_helo_macros =/
/^envfrom /s/^envfrom/milter_mail_macros =/
/^envrcpt /s/^envrcpt/milter_rcpt_macros =/
/^data /s/^data/milter_data_macros =/
/^eoh /s/^eoh/milter_end_of_header_macros =/
/^eom /s/^eom/milter_end_of_data_macros =/
s/,//g
s/ \([a-z][^ ]*\)/ {\1}/g
