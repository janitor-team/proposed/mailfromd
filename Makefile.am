# This file is part of Mailfromd.
# Copyright (C) 2005-2022 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SUBDIRS = \
 .\
 mflib\
 lib\
 gacopyz\
 src\
 mtasim\
 elisp\
 po\
 etc\
 imprimatur\
 doc\
 tests

if PMULT_COND
 SUBDIRS += pmult
endif

noinst_HEADERS = global.h
noinst_DATA = global.texi
BUILT_SOURCES = global.h global.texi
EXTRA_DIST = global.h global.texi global.def global.awk git-describe

SUFFIXES = .def .h .texi

.def.h:
	$(AM_V_GEN)$(AWK) -f $(top_srcdir)/global.awk -v format=C $< > $@

.def.texi:
	$(AM_V_GEN)$(AWK) -f $(top_srcdir)/global.awk -v format=texinfo $< > $@

ACLOCAL_AMFLAGS = -I m4 -I am -I imprimatur
distuninstallcheck_listfiles = find . -type f -not -name 'mailfromd.mf' -print

.PHONY: git-describe
git-describe:
	$(AM_V_GEN)if test -d .git; then \
		dirty=`git diff-index --name-only HEAD 2>/dev/null` || dirty=;\
		test -n "$$dirty" && dirty="-dirty"; \
		descr=`git describe`; \
		echo $${descr}$$dirty > git-describe; \
	fi

dist-hook: ChangeLog git-describe
	@PATCHLEV=`echo "$(PACKAGE_VERSION)" | \
                   sed -r "s/[0-9]+\.[0-9]+\.?//"`; \
	if test $${PATCHLEV:-0} -lt 50; then \
		if grep -q FIXME NEWS; then \
			echo >&2 "NEWS file contains FIXMEs"; \
			exit 1; \
		fi; \
	fi

alpha:
	$(MAKE) dist distdir=$(PACKAGE)-$(VERSION)-`date +"%Y%m%d"`

alphacheck:
	$(MAKE) distcheck distdir=$(PACKAGE)-$(VERSION)-`date +"%Y%m%d"`

# Define the following variables in order to use the ChangeLog rule below:
#  prev_change_log  [optional]  Name of the previous ChangeLog file.
#  gen_start_date   [optional]  Start ChangeLog from this date. 
gen_start_date = 2009-03-13
prev_change_log = ChangeLog.svn

.PHONY: ChangeLog
ChangeLog: 
	$(AM_V_GEN)if test -d .git; then                                     \
          (git log --pretty='format:%ad  %cn  <%ae>%n%n%w(72,8,8)%s%n%n%b'   \
                   --date=short                                              \
                   --since=$(gen_start_date);                                \
           echo "";                                                          \
           sed -e '/Local Variables:/,$$d' $(prev_change_log);               \
	   echo "Local Variables:";                                          \
	   echo "mode: change-log";                                          \
	   echo "version-control: never";                                    \
	   echo "buffer-read-only: t";                                       \
	   echo "End:") > ChangeLog.tmp;                                     \
          cmp ChangeLog ChangeLog.tmp > /dev/null 2>&1 ||                    \
            mv ChangeLog.tmp ChangeLog;                                      \
          rm -f ChangeLog.tmp;                                               \
	fi
